<section class="row s4a_css_results"><br />
    <?php
    $current_user = wp_get_current_user();
    if ($model->ListingSearchResults && $model->ListingSearchResults->ListingSummeries) :
        foreach ($model->ListingSearchResults->ListingSummeries as $ListingSummery) :
        echo '<article class="col-md-4"><div class="css_inner">' . S4A\Controls::search_results($ListingSummery, $current_user->ID) . '</div></article>';
    endforeach;
    else :
        if (!S4A_SIGNUP_ID) {
        echo "<p class='s4a_no-results'>N0o results found, please contact us for assistance.</p>";
    } else {
        echo "<p class='s4a_no-results'><a href='" . get_permalink(S4A_SIGNUP_ID) . "'>List Your Business</a></p>";
    }
    endif;
    ?>
</section>

<?php
$total = $model->ListingSearchResults->total;

if ($model->SearchCriteria->perPage != 0) {
$pages = ceil($total / $model->SearchCriteria->perPage);
} else {
  $pages = ceil($total / $total);
}

// $pages = ($pages == 0) ? 1 : $pages;

$Page = ($model->SearchCriteria->Page) ? $model->SearchCriteria->Page : 1;
?>
<div id='results_navigation'>
    <div class='previous_page'>
        <?php if ($Page > 1) { ?>
            <a href="<?= S4A\Helpers::BuildResultsUrl($model->SearchCriteria, ($Page - 1)) ?>" class="<?= $class ?>">< Previous Page</a>
        <?php
    } else { ?>
            < Previous Page
        <?php
    } ?>
    </div>
    <div class='total_pages'>
        Page <?= $Page ?> of <?= $pages ?>
    </div>
    <div class='next_page'>
        <?php if ($Page < $pages) { ?>
            <a href="<?= S4A\Helpers::BuildResultsUrl($model->SearchCriteria, ($Page + 1)) ?>" class="<?= $class ?>">Next Page ></a>
        <?php
    } else { ?>
            Next Page >
        <?php
    } ?>
    </div>
</div>
