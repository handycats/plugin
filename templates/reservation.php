<?php
$model->Reservation->ReservationCriteria->ListingRoom->rate_plan_promotion = false;
$current_user = wp_get_current_user();
?>
<div class="reservation-page">
  <div id="order_process">
    <?= S4A\Controls::checkout_steps(1); ?>

    <?php if ($model->error) { ?>
      <center>
        <div class="alert alert-danger">
          <?php print_r($model->error); ?>
        </div>
      </center>

    <?php } ?>

    <div class="alert alert-warning">
      <strong>Please note</strong> Your booking or reservation cannot be guaranteed without payment. In order to secure your booking, please ensure that a payment has been made.
    </div>

    <?php if ($model->RateRSA) { ?>
      <hr>
      <center>
        <div class="css_form">
          <label>This is a South African only special, are all your guests South Afican residents?</label>
          <div class="radio-wrap">
            Yes
            <input name="guest_resident" onclick="s4a_show('#s4a_resevation', false)" type="radio" >
            No
            <input name="guest_resident" onclick="s4a_submit('#enquire_listing')" type="radio" >
          </div>
        </div>
      </center>
      <hr>
    <?php } ?>
    <form id="s4a_resevation" method="post" class="css_form <?= ($model->RateRSA) ? 'hidden' : NULL ?>" onSubmit="return S4AReservation('#s4a_resevation')">
      <input type="text" class="s4a_val-special" name="special" placeholder="other" />
      <div class="row">

        <!--SIDE BAR START -->
        <div class="col-md-3">
          <div class="">
            <section class="s4a_css_results amount_details">
              <?php
              echo S4A\Controls::listing_reservation($model->Listing, $current_user->ID, "results", true, true)
              ?>
            </section>
            <?php if($model->Listing->listing_global_information_global_information){ ?>
              <div class="alert amount_details" role="alert" style="width:100%;font-size:14px;display:none;">
                <ul class="fa-ul">
                  <?php
                  foreach ($model->Listing->listing_global_information_global_information as $global_info) {
                    if($global_info->e_type == 'Includes' ){
                      echo '<li>'. \S4A\Helpers::mapIcons($global_info->global_information[0]->v_name) ." ". $global_info->global_information[0]->v_name. '</li>';
                    }
                  }
                  ?>
                </ul>
              </div>
            <?php } ?>

            <?php
            if ($model->Listing->i_tripadvisor){
              wp_enqueue_script('wps4a-tripadvisor', 'https://www.tripadvisor.co.za/WidgetEmbed-selfserveprop?border=true&popIdx=true&iswide=false&locationId=' . $model->Listing->i_tripadvisor . '&display_version=3&uniq=2010&rating=true&lang=en_ZA&nreviews=0&writereviewlink=false', '', null, false);
              ?>
              <div class="row">
                <!-- <div class="col-md-12"> -->
                <div id="TA_selfserveprop2010" class="TA_selfserveprop">
                  <ul id="laWm0i78Xvx" class="TA_links 8342infZV">
                    <li id="J6uIHkDY7" class="zit2VOza">
                    </li>
                  </ul>
                  <!-- </div> -->
                </div>
              </div>
            <?php } ?>
            <br>

            <?php
            $quantity = 0;
            $children = 0;
            $adults = 0;
            $room_count = 1;

            foreach ($model->Reservation->ReservationCriteria->Units as $unit) {

              $quantity += intval($unit->adults);
              $adults += intval($unit->adults);

              if($unit->Children)
              $quantity +=  intval($unit->Children);
              $children += intval($unit->Children);
              ?>
              <div class="coupon_details">
                <table class="amount_details">
                  <tr>
                    <th>Room # <?= $room_count ?></th>
                  </tr>
                  <tr>
                    <td>Room Type:</td>
                    <td class="css_text_right"><?= $model->Reservation->ReservationCriteria->ListingRoom->description ?></td>
                  </tr>
                  <!-- <tr>
                  <td>Special:</td>
                  <td class="css_text_right"><?= $model->Reservation->ReservationCriteria->ListingRoom->special_description ?></td>
                </tr> -->
                <tr>
                  <td>Adults:</td>
                  <td class="css_text_right"><?= $unit->adults  ?></td>
                </tr>
                <tr>
                  <td>Children:</td>
                  <td class="css_text_right"><?php  if ($unit->Children) { echo count($unit->Children); } else {echo 0; }?></td>
                </tr>
              </table>
            </div>
            <?php $room_count++; }  ?>

            <br>
            <div class="panel panel-default hidden-print">
              <div class="panel-heading panel-res-info-head">
                <h3 class="panel-title primary_color_font" data-fontsize="16" data-lineheight="17">Date Summary</h3>
              </div>
              <div class="panel-body">
                <table>
                  <tr>
                    <td>Arrival:</td>
                    <td class="css_text_right">
                      <?php $date1 = date_create($model->Reservation->ReservationCriteria->the_arrival_date);
                      echo date_format($date1,"d M y");
                      ?>
                    </td>
                  </tr>
                  <tr>
                    <td>Departure:</td>
                    <td class="css_text_right">
                      <?php $date2 = date_create($model->Reservation->ReservationCriteria->the_departure_date);
                      echo date_format($date2,"d M y");
                      ?>
                    </tr>
                  </table>
                </div>
              </div>

              <span style="display:none;" id="arrival_date"><?=$model->Reservation->ReservationCriteria->the_arrival_date?></span>
              <span style="display:none;" id="departure_date"><?=$model->Reservation->ReservationCriteria->the_departure_date?></span>
              <?php
              $the_arrival_date = strtotime($model->Reservation->ReservationCriteria->the_arrival_date);
              $the_departure_date = strtotime($model->Reservation->ReservationCriteria->the_departure_date);
              $datediff = $the_departure_date - $the_arrival_date;
              $days = floor($datediff / (60 * 60 * 24));
              ?>
              <div class="panel panel-default hidden-print">
                <div class="panel-heading panel-res-info-head">
                  <h3 class="panel-title primary_color_font" data-fontsize="16" data-lineheight="17">Price Summary</h3>
                </div>
                <div class="panel-body">
                  <table class="total" id="price_summary_table">
                    <tr style="margin-bottom:">
                      <td>Mandatory Services:</td>
                      <td class="css_text_right"><?php echo \S4A\Helpers::DoPrice($model->Reservation->ReservationCriteria->ListingRoom->currency_code, $model->Reservation->ReservationCriteria->ListingRoom->mandatory_services)?></td>
                    </tr>
                    <tr>
                      <td>Optional Services:</td>
                      <td class="css_text_right"><span id="optional_services_total"><?php echo \S4A\Helpers::DoPrice($model->Reservation->ReservationCriteria->ListingRoom->currency_code, 0) ?></span></td>
                    </tr>
                    <tr>
                      <td>Per Night:</td>
                      <td class="css_text_right"> <?php echo \S4A\Helpers::DoPrice($model->Reservation->ReservationCriteria->ListingRoom->currency_code, $model->Reservation->ReservationCriteria->ListingRoom->price) ?></td>
                    </tr>
                  </tr>
                  <tr>
                    <td>Total for <?= $days ?> night(s):</td>
                    <td class="css_text_right"><?php echo \S4A\Helpers::DoPrice($model->Reservation->ReservationCriteria->ListingRoom->currency_code, $model->Reservation->ReservationCriteria->ListingRoom->room_total) ?></td>
                  </tr>
                  <tr class="promo_show" style="display:none;">
                    <td class="primary_color_font">Promo Discount:</td>
                    <td class="css_text_right primary_color_font">
                      <span class="promo_amount" id="promo_amount">
                        <?php echo \S4A\Helpers::DoPrice($model->Reservation->ReservationCriteria->ListingRoom->currency_code, intval($model->EnquiryCriteria->discount_amount)) ?>
                      </span>
                      <span class="promo_percent" id="promo_percent" style="display:none;">
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td><h1 class="res_total">Total:</h1></td>
                    <td class="css_text_right">
                      <h1 id="booking_total" class="res_total"> <?php echo \S4A\Helpers::DoPrice($model->Reservation->ReservationCriteria->ListingRoom->currency_code, $model->Reservation->ReservationCriteria->ListingRoom->total_services) ?></h1>
                      <h1 id="new_booking_total" style="display:none;" class="res_total"> <?php echo \S4A\Helpers::DoPrice($model->Reservation->ReservationCriteria->ListingRoom->currency_code, $model->Reservation->ReservationCriteria->ListingRoom->total_services) ?></h1>
                      <span id="currency_code" style="display:none;"><?=$model->Reservation->ReservationCriteria->ListingRoom->currency_code?></span>
                    </td>
                  </tr>
                  <tr class="exlcuding_message" style="display:none;">
                    <td colspan="2" style="text-align:center;">(Excluding mandatory services)</td>
                  </tr>
                </table>
              </div>
            </div>
          </div>

          <!-- DIV PROMO CODE START -->
          <div class="coupon_details payment_details primary_color" id="coupon_details">
            <div class="row">
              <div class="col-md-12">
                <h3 class="s4a-title" data-fontsize="18" data-lineheight="19" style="color:#fff !important;">Promo Code</h3>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <input placeholder="Enter your promo code" type="text" name="voucher_code" class="s4a_val-voucher_code" value="<?php echo (\S4A\Session::get_promo()) ? \S4A\Session::get_promo() : null  ?>" id="voucher_code" onkeyup="checkCouponValue()"/>
                <input type="hidden" name="coupon_amount" class="s4a_val-coupon_amount" value="<?php echo (\S4A\Session::get_promo()) ? \S4A\Session::get_promo() : ""  ?>" id="coupon_amount"/>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 css_text_right">
                <input onclick="BookingProcesss.check_coupon_code('coupon_details','s4a_resevation',<?= $model->Reservation->ReservationCriteria->ListingRoom->total_services ?>,<?=  $model->AvailabilityCriteria->listing_id ?>,'check_coupon_code')" type="button" value="APPLY" class="submit apply_button" id="apply_button"/>
              </div>
            </div>
          </div>
          <!-- DIV PROMO CODE END -->
            <br>
          <?php echo \S4A\Controls::currency_converter_widget() ?>

        </div>
        <!-- SIDE BAR END -->

        <!-- FORM START -->
        <div class="col-md-9 res_form ">

          <!-- DIV SERVICES START -->
          <?php if($model->Reservation->ReservationCriteria->ListingService){ ?>
            <div id="div_services" class="row payment_details " style="display:none;">
              <div class="col-md-12">
                <h3 class="s4a-title">Additional Booking Services</h3>
                <h4 <h4 class="s4a-title">Mandatory Services</h4>
                <div class="table-responsive">
                  <table class="table ">
                    <thead>
                      <!-- <tr>
                      <td colspan="7"><small>Mandatory services cannot be altered (* Indicates mandatory services)</small></td>
                    </tr> -->
                    <tr class="first-cell">
                      <th>Select</th>
                      <th>Service</th>
                      <th>Date/Use</th>
                      <th>Quantity</th>
                      <th>Adults</th>
                      <th>Children</th>
                      <th>Amount</th>
                    </tr>
                  </thead>
                  <tbody id="t_mandatory">
                    <?php
                    $optional_found = false;

                    foreach($model->Reservation->ReservationCriteria->ListingService as $listingService){
                      $price = \S4A\Helpers::DoPrice($model->Reservation->ReservationCriteria->ListingRoom->currency_code , $listingService->Price);
                      $type = $listingService->Type;
                      $mandatory = $listingService->Mandatory;
                      $checkbox_value = $listingService->Code."|".$listingService->Type."|".$listingService->Price."|".$listingService->Description."|".$listingService->Mandatory;

                      if($mandatory == "false"){
                        $optional_found = true;
                      }

                      if($mandatory == "true"){
                        ?>
                        <tr>
                          <td><input type="checkbox" name="vehicle" disabled="true" checked> <label>&#42;</label> </td>
                          <td><?= $listingService->Description;?></td>
                          <td><?= $listingService->Type;?></td>
                          <td><?= $quantity ?></td>
                          <td><?= $adults ?></td>
                          <td><?= $children ?></td>
                          <td><span><?= $price; ?></span></td>
                        </tr>
                      <?php } } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- DIV SERVICES END -->

          <?php } ?>

          <!-- DIV DETAILS START -->
          <div class="row payment_details" id="div_details">
            <div class="row">
              <div class="col-md-12">
                <h3 class="s4a-title">Your Details</h3>
              </div>
              <div class="col-md-2">

                <label>Title:</label><br/>
                <select name="user_title">
                  <?php
                  if ($model->Titles) {
                    $user_title = ($model->FormData->user_title) ? $model->FormData->user_title : "Mr";
                    foreach ($model->Titles as $key => $value) {
                      $selected = "";
                      if ($user_title == $value)
                      $selected = "selected='selected'";
                      echo '<option value="' . $value . '"  ' . $selected . ' >' . $value . '</option>';
                    }
                  }
                  ?>
                </select>
              </div>
              <div class="col-md-5">
                <label>First Name:</label><br/>
                <input placeholder="Please enter your first name" type="text" name="user_name" class="s4a_val-user_name" value="<?= $model->FormData->user_name ?>" />
              </div>
              <div class="col-md-5">
                <label>Last Name:</label><br/>
                <input placeholder="Please enter your last name" type="text" name="user_last_name" class="s4a_val-user_last_name" value="<?= $model->FormData->user_last_name ?>" />
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <label>Cell Phone:</label><br/>
                <input id="phone"  type="number" name="user_phone" class="s4a_val-user_phone" style="padding-left:84px !important;"/> <label class="input-alert" style="display:none;color:green;font-size:20px;"></label>
              </div>
              <div class="col-md-6">
                <label>Email:</label><br/>
                <input <?= ($current_user->exists()) ? "readonly" : null ?> placeholder="Please enter your email address" type="text" name="user_email" class="s4a_val-user_email" value="<?= $model->FormData->user_email ?>" />
              </div>
            </div>
            <?php if (!$model->RateRSA) { ?>
              <div class="row">
              <?php 
                if($model->Reservation->ReservationCriteria->ListingRoom->rate == "RET1") {?>
                    <div class="col-md-4">
                <?php  } else { ?>
                  <div class="col-md-8">
                <?php  } ?>
                  <label>Nationality:</label><br/>
                  <?php
                  $country = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Antigua and Barbuda", "Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Costa Rica", "Côte d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "North Korea", "South Korea", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestine, State of", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Romania", "Russia", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia and Montenegro", "Seychelles", "Sierra Leone", "Singapore", "Sint Maarten", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "Spain", "Sri Lanka", "Sudan", "Sudan, South", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Virgin Islands, British", "Virgin Islands, U.S.", "Yemen", "Zambia", "Zimbabwe");
                  ?>
                  <select name="user_nationality">
                    <?php
                    if ($country) {
                      $user_nationality = ($model->FormData->user_nationality) ? $model->FormData->user_nationality : "South Africa";
                      foreach ($country as $key => $value) {
                        $selected = "";
                        if ($user_nationality == $value)
                        $selected = "selected='selected'";
                        echo '<option value="' . $value . '"  ' . $selected . ' >' . $value . '</option>';
                      }
                    }
                    ?>

                  </select>
                </div>
                <?php 
                  if($model->Reservation->ReservationCriteria->ListingRoom->rate == "RET1") {?>
                    <div class="col-md-4">
                      <label>ID Number:</label><br/>
                      <input placeholder="Please enter your ID  number" type="text" name="user_id" class="s4a_val-user_id" value="<?= $model->FormData->user_id ?>" />
                      <p>Pensioner rates only apply to people over 60 years old.</p>
                    </div>
                <?php  } ?>
                <div class="col-md-4">
                  <label>Are you an RSA Resident?</label>
                  <div class="radio-wrap">
                    Yes
                    <input name="user_resident" type="radio" value="Yes" checked >
                    No
                    <input name="user_resident" type="radio" value="No" >
                  </div>
                </div>
                <div id="s4a_pensioner" class="col-md-4">
                  <label>Are you a pensioner?</label>
                  <div class="radio-wrap">
                    Yes
                    <input name="user_pension" type="radio" value="Yes" >
                    No
                    <input name="user_pension" type="radio" value="No" checked >
                </div>
                </div>
              </div>
            <?php } ?>
          </div>
          <!-- DIV DETAILS END -->
          <div class="row">
            <div class="col-md-9">
              <br />
              <p><input class="s4a_val-invoice" type="checkbox" onclick="s4a_invoice_checkbox('#s4a_resevation_invoice')"/> Do you require an invoice ?</p>
            </div>
          </div>

          <!-- DIV INVOICE START -->
          <div class="payment_details hidden" id="s4a_resevation_invoice">
            <div class="row">
              <div class="col-md-12">
                <h3 class="s4a-title">Invoice Details</h3>
                <div class="row">
                  <div class="col-md-6">

                    <div class="form-group">
                      <label>Name:</label><br/>
                      <input placeholder="Name" type="text" name="invoice_name" class="s4a_val-invoice_name" value="<?= $model->FormData->invoice_name ?>" />
                    </div>
                    <div class="form-group">
                      <label>Tax Number:</label><br/>
                      <input placeholder="Tax Number" type="text" name="invoice_tax_number" class="s4a_val-invoice_tax_number" value="<?= $model->FormData->invoice_tax_number ?>" />
                    </div>

                  </div>
                  <div class="col-md-6">
                    <label>Address:</label><br/>
                    <textarea placeholder="Address" name="invoice_address" rows="7"><?= $model->FormData->invoice_address ?></textarea>
                  </div>
                </div>
              </div>
            </div>
          </div><br>
          <!-- DIV INVOICE END -->

          <div class="payment_details" id="div_details">
            <?php
            $unitCount = 1;
            foreach ($model->Reservation->ReservationCriteria->Units as $unit) {
              ?>
              <div class="col-md-12">
                <h3 class="s4a-title">Occupant(s) in Unit #<?= $unitCount ?></h3>
              </div>
              <?php
              $guestAdultCount = 0;
              while ($guestAdultCount < $unit->adults) {
                ?>
                <div class="row">
                  <div class="col-md-4">
                    <label>Title:</label><br/>
                    <select name="guests[<?= $unitCount ?>][adults][<?= $guestAdultCount ?>][guest_title]">
                      <?php
                      if ($model->Titles) {
                        $guest_title = "Mr";
                        foreach ($model->Titles as $key => $value) {
                          $selected = "";
                          if ($guest_title == $value)
                          $selected = "selected='selected'";
                          echo '<option value="' . $value . '"  ' . $selected . ' >' . $value . '</option>';
                        }
                      }
                      ?>
                    </select>
                  </div>
                  <div class="col-md-4">
                    <label>First name:</label><br/>
                    <input placeholder="First Name" type="text" name="guests[<?= $unitCount ?>][adults][<?= $guestAdultCount ?>][guest_name]" class="s4a_val-guest_name" value="" />
                  </div>
                  <div class="col-md-4">
                    <label>Last name:</label><br/>
                    <input placeholder="Last Name" type="text" name="guests[<?= $unitCount ?>][adults][<?= $guestAdultCount ?>][guest_last_name]" class="s4a_val-guest_last_name" value="" />
                  </div>
                </div>
                <?php
                $guestAdultCount++;
              }
              ?>

              <?php
              $childCount = 0;

              foreach ($unit->Children as $child) {
                ?>

                <div class="row">
                  <div class="col-md-4">
                    <label>Child:</label><br/>
                    <input placeholder="" type="text" name="guests[<?= $unitCount ?>][child][<?= $childCount ?>][guest_age]" disabled="disabled" class="disabled" value="<?= $child ?> years" />
                  </div>
                  <div class="col-md-4">
                    <label>First name:</label><br/>
                    <input placeholder="First Name" type="text" name="guests[<?= $unitCount ?>][child][<?= $childCount ?>][guest_name]" class="s4a_val-guest_name" value="" />
                  </div>
                  <div class="col-md-4">
                    <label>Last name:</label><br/>
                    <input placeholder="Last Name" type="text" name="guests[<?= $unitCount ?>][child][<?= $childCount ?>][guest_last_name]" class="s4a_val-guest_last_name" value="" />
                  </div>
                </div>
                <?php
                $childCount++;
              }
              $unitCount++;
            }
            ?>
          </div><br>
          <!-- DIV REQUIREMENTS START -->
          <div class="payment_details" id="div_details">

            <div class="col-md-12">
              <h3 class="s4a-title">Special Requirements</h3>
            </div>
            <div class="row">
              <div class="col-md-12">
                <label>Any Special Requirements?</label><br/>
                <textarea placeholder="Any Dietary, Medical, Special needs or Occasions..." name="user_special" rows="10" cols="50"><?= $model->FormData->user_special ?></textarea>
              </div>
              <div class="col-md-12">
                <div class="col-md-4">
                  <p><strong>Your Estimated time of arrival? </strong>(optional)</p>
                  <input type="text" class="time_picker" id="arrival_time"  name="arrival_time" class="time ui-timepicker-input" autocomplete="off" placeholder="Select time...">
                </div>
                <div class="col-md-8">
                  <?php if ($model->Listing->t_check_in_information != '') { ?>
                    <span class="check-in-info"><?= $model->Listing->t_check_in_information ?> </span>
                  <?php } ?>

                </div>
              </div>
            </div>

          </div><br>
          <!-- DIV REQUIREMENTS END -->

          <!-- DIV SERVICE START -->
          <?php if($model->Reservation->ReservationCriteria->ListingService){
            $optional_found = false;
            foreach($model->Reservation->ReservationCriteria->ListingService as $listingService){
              if($listingService->Mandatory == "false"){
                $optional_found = true;
                break;
              }
            }
            if($optional_found){
              ?>
              <div class="row payment_details">
                <br>
                <center>
                  <p><i>Personalize your booking and add additional services or activities to your adventure.</i></p>
                  <i class="fa fa-motorcycle" aria-hidden="true" style="font-size:20px;"></i> <i class="fa fa-bus" aria-hidden="true" style="font-size:20px;"></i><i class="fa fa-music" aria-hidden="true" style="font-size:20px;"></i> <i class="fa fa-gamepad" aria-hidden="true"></i>
                </center>
                <hr>

                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default ">
                    <div class="panel-heading" role="tab" id="headingOne">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapse">
                          Wish to personalise your booking? (click here)
                        </a>
                      </h4>
                    </div>
                    <hr>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                        <table class="table" id="tbl_services">
                          <thead>
                            <tr>
                              <th>Select</th>
                              <th>Service/Activity</th>
                              <th>Date/Use</th>
                              <th>Quantity</th>
                              <th>Adults</th>
                              <th>Children</th>
                              <th>Amount</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                            foreach($model->Reservation->ReservationCriteria->ListingService as $listingService){
                              $price = \S4A\Helpers::DoPrice($model->Reservation->ReservationCriteria->ListingRoom->currency_code , $listingService->Price);
                              $type = $listingService->Type;
                              $mandatory = $listingService->Mandatory;
                              $checkbox_value = $listingService->Code."|".$listingService->Type."|".$listingService->Price."|".$listingService->Description."|".$listingService->Mandatory;

                              if($mandatory == "false"){
                                ?>
                                <tr>
                                  <td data-th="Selet: "><div><input type="checkbox" class="service_select" name="service_select" value="<?= $checkbox_value; ?>"></div></td>
                                  <td data-th="Service/Activity: "><div><?= $listingService->Description;?></div></td>
                                  <?php $date_value = date_create($model->Reservation->ReservationCriteria->the_arrival_date);
                                  $date_value = date_format($date,"m/d/Y");
                                  ?>
                                  <td data-th="Date/Use:"><div><input style="width:100px;"placeholder="Select Date.." type="text" data-field="service_date" id="service_date_<?= $listingService->Code ?>" class="service_date" value="<?=$date_value;?>" ></div></td>
                                  <?php if($type=="Per use"){ ?>
                                    <td data-th="Quantity: "><div>
                                      <select data-type="quantity" class="select_qty" id="quantity_<?= $listingService->Code ?>">
                                        <option value="1" selected="">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                      </select></div>
                                    </td>
                                  <?php }else{ ?>
                                    <td data-th="Quantity: "><div><?= $quantity ?></div></td>
                                  <?php } ?>
                                  <?php if($type=="Per person" || $type=="Per person per night"){ ?>
                                    <td data-th="Adults: "><div>
                                      <select data-type="adults" class="select_qty" id="adults_<?= $listingService->Code ?>">
                                        <option value="1" selected="">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                      </select></div>
                                    </td>
                                    <td data-th="Children: "><div>
                                      <select data-type="children" class="select_qty" id="children_<?= $listingService->Code ?>">
                                        <option value="0" selected="">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                      </select></div>
                                    </td>
                                  <?php }else{ ?>
                                    <td data-th="Adults: "><div><?= $adults ?></div></td>
                                    <td data-th="Children: "><div><?= $children ?></div></td>
                                  <?php } ?>
                                  <td data-th="Amount: "><div>
                                    <span id="<?= $listingService->Code ?>_price"> <?= $price; ?></span>
                                    <span style="display:none;" id="<?= $listingService->Code ?>_new_price"> <?= $price; ?></span></div>
                                  </td>
                                </tr>
                              <?php } } ?>
                            </tbody>
                          </table>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>


              <?php } } ?>

              <br />
              <?php if($model->FormData->AgentCode) {?>
                <div class="payment_details">
                  <div class="row">
                    <div class="col-md-12" style="padding:0px;">
                      <h3 class="s4a-title" data-fontsize="18" data-lineheight="19">Payment Details</h3>
                      <p><b>Payment on Accounts</b></p>
                      <P><input class="s4a_val-personal" type="checkbox" />  I hereby understand that any personal information entered by me will be used to facilitate this reservation and where required will be provided to 3rd parties such as the hotel as required for normal operational processes.</p>
                    </div>
                  </div>
                </div>
                <br />
              <?php } ?>
              <?php if ($model->Terms) { ?>
                <div class="row">
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-12">
                        <P><input class="s4a_val-terms" type="checkbox" /> I agree to terms (<a href="<?= esc_url(get_permalink($model->Terms)); ?>" target="_blank">Terms and Conditions</a>)</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <br />
                <?php } ?>
                <?php if(!$model->FormData->AgentCode) {?>
                  <div class="row">
                    <div class="col-md-12" style="padding:0px;">
                      <div class="col-md-8"  style="padding:0px;">
                        <div class="alert alert-success" role="alert" style="width:100%;border:none !important;font-size:18px;">
                          <i class="fa fa-cc-visa" aria-hidden="true"></i>  <i class="fa fa-cc-mastercard" aria-hidden="true"></i> <i>Safe and Secure Payments</i>
                        </div>
                      </div>
                      <div class="col-md-4"  style="padding:0px;">
                        <button type="submit" align="left" style="float:right;"  class="grn_large_button primary_color" />Make Payment <i class="fa fa-chevron-right" aria-hidden="true"></i></button>
                      </div>
                    </div>
                  </div>
                <?php } else { ?>
                  <div class="row">
                    <div class="col-md-12" style="padding:0px;">
                      <button type="submit" align="left" style="float:right;"  class="grn_large_button primary_color" />Confirm Reservation <i class="fa fa-chevron-right" aria-hidden="true"></i></button>
                    </div>
                  </div>
                <?php } ?>

                <br /><br />
                <input type="hidden" name="isBooking" value="true" />
                <?php $utmData = \S4A\Session::get_utm_data(); ?>
                <input type="hidden" name="source" value="<?= $utmData->utm_source ?>" />
                <input type="hidden" name="AvailabilityCriteria" value='<?= json_encode($model->AvailabilityCriteria, JSON_HEX_APOS) ?>' />

              </div>

            </div>
          </div>
        </div>
        <!-- FORM END -->
      </div>
    </form>
    <form method="post" action="<?= S4A\Helpers::EnquiryUrl($model->AvailabilityCriteria->establishment, $model->AvailabilityCriteria->listing_id) ?>" id="enquire_listing">
      <input type="hidden" id="EnquiryAvailabilityCriteria" name="AvailabilityCriteria" value='<?= json_encode($model->AvailabilityCriteria, JSON_HEX_APOS) ?>'></input>
    </form>
  </div>
</div>

<script type="text/javascript">
  jQuery(document).ready(function(){
    if(jQuery("#voucher_code").val() != ''){
      document.getElementById("apply_button").click();
    }
  });

  function checkCouponValue () {
    if(jQuery("#voucher_code").val() == ''){
      jQuery('#coupon_details').find("#voucher_code").removeClass('s4a_val-error');
      jQuery('#coupon_details').find("#voucher_code").removeClass('s4a_val-sucess');
      jQuery('#coupon_details').find("#voucher_code").removeClass('s4a_val-warning');
      jQuery('#s4a_resevation').find('.payment_details .amount_details tbody .discount').remove();
      jQuery('#coupon_details').find("#coupon_amount").val();
      jQuery('#s4a_resevation').find('.payment_details .total .css_text_right h1').html('R '+ <?= $model->Reservation->ReservationCriteria->ListingRoom->total_services ?>);
    }
  }
</script>
