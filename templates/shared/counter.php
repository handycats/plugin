<?php
//\S4A\Session::remove_counter();
$counter = \S4A\Session::get_counter();
$startTime = $counter->time;
$currentTime = (new DateTime())->format('Y-m-d H:i:s');
$diff = strtotime($currentTime) - strtotime($startTime);
$timeLeft = 1200 - $diff;

if($counter && $timeLeft > 0) {  ?>

  <div id="s4a_counter" class="row" >
    <div class="col-md-6 counter-div" style="text-align:center;color:#fff;height:100%;padding:10px;">
      Your booking will expire in:
    </div>
    <div class="col-md-3 counter-div">
      <div title="start = <?=$startTime?> | current = <?=$currentTime?> | diff = <?=$diff?> " id="display" style="color:#F64242;padding:10px;"></div>
    </div>
    <div class="col-md-3 counter-div-3">
      <a href="<?=$counter->url ?>" target="_blank" class="a-continue-booking">Continue booking <i class="fa fa-arrow-right"></i></a>
    </div>
  </div>

  <script type="text/javascript">
    window.addEventListener('load', function () {
      CounterCountDown(<?=$timeLeft?>);
    });
  </script>
<?php
} else {
  \S4A\Session::remove_counter();
}
