<?php
$current_user = wp_get_current_user();
?>
<form id="s4a_enquiry" action="<?=\S4A\Helpers::EnquiryUrl($model->v_title, $model->listing_id)?>" method="post" class="css_form" onSubmit="return S4AEnquiry('#s4a_enquiry')">
    <input type="text" class="s4a_val-special" name="special" placeholder="other" />
    <div>
        <?php if ($model->error) { ?>
            <center><p class="error_text"><?php print_r($model->error); ?></p></center>
        <?php
    } ?>
        <?php if ($model->AvailabilityCriteria->ListingRoom) { ?>
        <div class="get-available-room cf">
            <table class="s4a_table">
                <tbody>

                    <tr class="first-cell">
                        <th>Establishment</th>
                        <th>Room Type</th>
                        <th>Check In</th>
                        <th>Check Out</th>
                        <th>Price</th>
                    </tr>
                    <tr>
                        <td data-label="Establishment">
                            <?= $model->Reservation->ReservationCriteria->establishment ?>
                        </td>
                        <td data-label="Room Type">
                            <?= $model->Reservation->ReservationCriteria->ListingRoom->description ?>
                        </td>
                        <td data-label="Check In">
                            <?= $model->Reservation->ReservationCriteria->the_arrival_date ?>
                        </td>
                        <td data-label="Check Out">
                            <?= $model->Reservation->ReservationCriteria->the_departure_date ?>
                        </td>
                        <td data-label="Price">
                            <?php echo \S4A\Helpers::DoPrice($model->Reservation->ReservationCriteria->ListingRoom->currency_code , $model->Reservation->ReservationCriteria->ListingRoom->price); ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <?php
    } ?>
    </div>
    <h1 class="s4a-title">Your Details:</h1>
    <div class="row payment_details">
        <div class="col-md-6">
            <label>First Name:</label><br/>
            <input placeholder="Please enter your first name" type="text" name="user_name" class="s4a_val-user_name" value="<?= $model->FormData->user_name ?>" />
        </div>
        <div class="col-md-6">
            <label>Last Name:</label><br/>
            <input placeholder="Please enter your last name" type="text" name="user_last_name" class="s4a_val-user_last_name" value="<?= $model->FormData->user_last_name ?>" />
        </div>
        <div class="col-md-6">
            <label>Cell Phone:</label><br/>
            <input placeholder="e.g. +27 082 000 0000" type="text" name="user_phone" class="s4a_val-user_phone" value="<?= $model->FormData->user_phone ?>" />
        </div>
        <div class="col-md-6">
            <label>Email:</label><br/>
            <input <?= ($current_user->exists()) ? "readonly" : null ?> placeholder="Please enter your email address" type="text" name="user_email" class="s4a_val-user_email" value="<?= $model->FormData->user_email ?>" />
        </div>
    </div>
    <br>
    <div class="row payment_details">
		<?php if(S4A_DISABLE_AVAILABILITY) {?>
        <div class="col-md-8">
            <label>Nationality:</label><br/>
            <?php
            $country = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Antigua and Barbuda", "Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Costa Rica", "Côte d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "North Korea", "South Korea", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestine, State of", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Romania", "Russia", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia and Montenegro", "Seychelles", "Sierra Leone", "Singapore", "Sint Maarten", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "Spain", "Sri Lanka", "Sudan", "Sudan, South", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Virgin Islands, British", "Virgin Islands, U.S.", "Yemen", "Zambia", "Zimbabwe");
            ?>
            <select name="user_nationality">
                <?php
                if ($country) {
                    $user_nationality = ($model->FormData->user_nationality) ? $model->FormData->user_nationality : "South Africa";
                    foreach ($country as $key => $value) {
                        $selected = "";
                        if ($user_nationality == $value)
                            $selected = "selected='selected'";
                        echo '<option value="' . $value . '"  ' . $selected . ' >' . $value . '</option>';
                    }
                }
                ?>

            </select>
        </div>
		<?php } else { ?>
        <div class="col-md-4">
            <label>Nationality:</label><br/>
            <?php
            $country = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Antigua and Barbuda", "Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Costa Rica", "Côte d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "North Korea", "South Korea", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestine, State of", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Romania", "Russia", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia and Montenegro", "Seychelles", "Sierra Leone", "Singapore", "Sint Maarten", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "Spain", "Sri Lanka", "Sudan", "Sudan, South", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Virgin Islands, British", "Virgin Islands, U.S.", "Yemen", "Zambia", "Zimbabwe");
            ?>
            <select name="user_nationality">
                <?php
                if ($country) {
                    $user_nationality = ($model->FormData->user_nationality) ? $model->FormData->user_nationality : "South Africa";
                    foreach ($country as $key => $value) {
                        $selected = "";
                        if ($user_nationality == $value)
                            $selected = "selected='selected'";
                        echo '<option value="' . $value . '"  ' . $selected . ' >' . $value . '</option>';
                    }
                }
                ?>

            </select>
        </div>
        <div class="col-md-4">
            <label>Do you have a promo code?</label><br/>
            <input placeholder="Please enter your promo code here" type="text" name="user_coupon" class="s4a_val-user_coupon" value="<?= ($model->FormData->user_adults) ? $model->FormData->user_coupon : null ?>" />
        </div>
        <div class="col-md-4">
            <label>Are you an RSA Resident?</label>
            <div class="radio-wrap">
                Yes
                <input name="user_resident" type="radio" value="Yes" <?= ($model->FormData->user_resident) ? "checked" : null ?> >
                No
                <input name="user_resident" type="radio" value="No" <?= (!$model->FormData->user_resident) ? "checked" : null ?> >
            </div>
            <span id="s4a_pensioner">
                <label>Are you a pensioner?</label>
                <div class="radio-wrap">
                    Yes
                    <input name="user_pension" type="radio" value="Yes" <?= ($model->FormData->user_pension) ? "checked" : null ?> >
                    No
                    <input name="user_pension" type="radio" value="No" <?= (!$model->FormData->user_pension) ? "checked" : null ?> >
                </div>
            <span>
        </div>
		<?php } ?>
  </div><br>
    <div class="row payment_details">
        <div class="col-md-4">
            <label>How many adults?</label><br/>
            <input placeholder="Please enter the number of adults" type="text" name="user_adults" class="s4a_val-user_adults" value="<?= ($model->FormData->user_adults) ? $model->FormData->user_adults : 1 ?>" />
        </div>
        <div class="col-md-4">
            <label>How many children?</label><br/>
            <input placeholder="Please enter the number of children" type="text" name="user_children" value="<?= $model->FormData->user_children ?>" />
        </div>
        <div class="col-md-4">
            <label>Children's age(s)?</label><br/>
            <input placeholder="Please enter the age(s) of the children" type="text" name="user_children_age" value="<?= $model->FormData->user_children_age ?>" />
        </div>
    </div><br>
	<?php if(!S4A_DISABLE_AVAILABILITY) {?>
    <div class="row payment_details">
        <div class="col-md-12">
            <label>Are your dates flexible?</label>
            <input placeholder="Please specify any alternative dates here." type="text" name="user_flexible" class="s4a_val-user_flexible" value="<?= $model->FormData->user_flexible ?>" />
        </div>
    </div>
	<?php } else { ?>
    <div class="row payment_details">
        <div class="col-md-12">
            <label>What are your travel dates?</label>
            <input placeholder="Please specify your travel dates here." type="text" name="user_flexible" class="s4a_val-user_flexible" value="<?= $model->FormData->user_flexible ?>" />
        </div>
    </div>
	<?php } ?><br>
    <div class="row payment_details">
        <div class="col-md-12">
            <label>Message:</label><br/>
            <textarea placeholder="Any message, special requirements or additional information..." name="user_special" rows="10" cols="50"><?= $model->FormData->user_special ?></textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
          <br>
          <?php if ($model->Terms) { ?>
              <p><input class="s4a_val-terms" type="checkbox" style="float:left;"/> I agree to terms (<a href="<?= esc_url(get_permalink($model->Terms)); ?>" target="_blank">Terms and Conditions</a>)</p>
          <?php
        } ?>
        </div>
        <div class="col-md-6">
          <br>
          <input id="sub-but" type="submit" value="Submit Enquiry" class="grn_button" style="float:right;"/>
        </div>
    </div>
    <br />
    <br/>
    <input type="hidden" name="isBooking" value="true" />
    <?php $utmData = \S4A\Session::get_utm_data(); ?>
    <input type="hidden" name="source" value="<?= $utmData->utm_source ?>" />
    <input type="hidden" name="AvailabilityCriteria" value='<?= json_encode($model->AvailabilityCriteria, JSON_HEX_APOS) ?>' />
</form>
