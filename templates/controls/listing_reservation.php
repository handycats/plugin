<?php

  $return = '<div class="result_tile" id="listing-' . $ListingSummery->id . '">';

  if ($ListingSummery->v_listing_integration_ids) {
    $return .= '<div class="online-booking"><i class="fa fa-check" aria-hidden="true"></i><span>Online Booking</span></div>';
  }

  $return .= '<a href="' . \S4A\Helpers::ListingUrl($ListingSummery->v_title, $ListingSummery->id) . '" class="css_bg_image" target="_blank">';

  if ($noLasy) {
    $return .= '<div class="css_bg_image_pic" style="border-radius:5px;background-image:url(' . S4A_IMG_URL . '?id=' . $ListingSummery->imageid . '&type=latest)"></div>';
  } else {
    $return .= '<div class="css_bg_image_pic lazy" style="border-radius:5px;" data-original="' . S4A_IMG_URL . '?id=' . $ListingSummery->imageid . '&type=latest"></div>';
  }

  $return .= '</a><br>';

  $return .= '<center><h2>' . $ListingSummery->v_title . '</h2></center>';
  $return .= '<hr>';
  $return .= '</div>';


  return $return;
