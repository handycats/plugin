<?php
$rate_data_count = count((array) $room);

if ($booked_out) {
    $html_str .= "<div class='booked-out panel panel-default  box '>";
    $html_str .= "<div class='ribbon-sold ribbon-top-left-sold'><span>Booked Out</span></div>";
} else if ($unavailable) {
    $html_str .= "<div class='unavailable panel panel-default box'>";
    $html_str .= "<div class='ribbon-unavailable ribbon-top-left-unavailable'><span>Unavailable</span></div>";
} else {
    $html_str .= "<div class='panel panel-default box default'>";
}

$html_str .= "<div class='panel-body style='height:auto;padding:20px;'>";
$html_str .= "<div class='row'>";
$test_data = "testing";
$html_str .= '<div class="col-md-3" style="background-color:#eee;padding:0px;height:230px;" >';
$image_url = trim(S4A_IMG_URL) . '?id=' . $room->listing_room_image[0]->id . '&type=avail&file=room';
$html_str .= "<img src= '" . $image_url . "' class='img-responsive center-block' style='height:100%;width:100%;border-radius:5px;'>";
$html_str .= '<div class="overlay"></div>';
$html_str .= '<div class="button-div"><button class="room-info-btn" data-code="' . $room->v_code . '" data-info="' . base64_encode(gzdeflate(serialize($room))) . '"  data-target="room-information-overlay" > MORE INFO </button></div>';
$html_str .= "</div>";
$html_str .= "<div class='col-md-9'>";
$html_str .= "<div class='row'";
$html_str .= "<div class='col-md-12'>";
if ($room->v_room_name) {
    $html_str .= '<h2 style="float:left; width:50%;" class="room-title">' . $room->v_room_name . '</h2>';
} else if ($room->description) {
    $html_str .= '<h2 style="float:left; width:50%;" class="room-title">' . $room->description . '</h2>';
} else {
    $html_str .= '<div style="float:left; width:50%;" class="room-title"><h2 >Room</h2></div>';
}

$html_str .= "<div style='float:right;width:50%;text-align:right;'><td style='text-align:center;'><button style='border: 1px solid #ccc;'  class='room_contct_btn' onclick=\"BookingProcesss.reserve('" . $room->v_code . "', 'enquire_listing','EnquiryAvailabilityCriteria')\">Enquire </button></td></div>";

$html_str .= "<br><div style='float:left;width:100%;' class='room-sleeps-info'>";
if ($room->i_max_occupancy) {
    $html_str .= "<span> <i class='fa fa-group '></i> Sleeps  " . $room->i_max_occupancy . '</span>';
}

if ($room->i_max_adult_occupancy) {
    $html_str .= " |  <span><i class='fa fa-male fa-2x '></i> x " . $room->i_max_adult_occupancy . '</span>';
}

if ($room->i_max_child_occupancy) {
    $html_str .= " |  <span> <i class='fa fa-child '></i> x " . $room->i_max_child_occupancy . '</span>';
}

if ($room->listing_room_bed_types) {
    $html_str .= " |  <span><i class='fa fa-bed '></i> ";
    foreach ($room->listing_room_bed_types as $key => $bed_types) {
        $html_str .= $bed_types->global_information->v_name;
        $html_str .= ($key + 1 < count($room->listing_room_bed_types)) ? ', ' : '';
    }
    $html_str .= '</span></div>';
}
$html_str .= "</div>";
$html_str .= "<div class='col-md-12 clear'>";

if ($booked_out) {
    $html_str .= '<br>
                <div class="alert alert-danger" role="alert" style="border:none !important">
                  <p><h4 class="alert-heading"><i class="fa fa-clock-o" aria-hidden="true"></i> You have missed it!</h4></p>
                  <p>This room is in demand and has been booked out for your selected dates.</p>
                  <p class="mb-0">Please select different dates or make an enquiry for more information.</p>
                </div>';
} else if ($unavailable) {
    $html_str .= '<br>
                <div class="alert alert-warning" role="alert" style="border:none !important">
                  <p>This room is unavailable for the selected search criteria.</p>
                  <p class="mb-0">If you are interested please make an enquiry so we can futher assist you.</p>
                </div>';
} else {
    $html_str .= '<br>
                <div class="alert alert-info" role="alert" style="border:none !important">
                  <p>This room is unavailable for online booking.</p>
                  <p class="mb-0">If you are interested please make an enquiry so we can futher assist you.</p>
                </div>';
}
$html_str .= "</div>";
$html_str .= "</div>";
$html_str .= "</div>";

$html_str .= \S4A\Controls::availability_calendar($roomData, $room->v_code);

$html_str .= "</div>";
$html_str .= "</div>";
$html_str .= "</div>";
