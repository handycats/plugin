<?php

          $html_str .= '';

          $html_str .=    '<div id="se365_converter_widget" class="panel panel-default hidden-print">
          <div class="panel-heading">
          <h3 class="panel-title primary_color_font">Change Currency</h3>
          </div>
          <div class="panel-body">
          <div class="row aha-currency-widget">';

          foreach(\S4A\Cache::InitialisationDataCache()->Currency as $currency) {

            $code = \S4A\Helpers::GetCurrency($currency->code);
            $html_str .=  '<div class="col-xs-3">
            <div class="btn btn-xs btn-default btn-block se365_converter"
            data-value="'.$currency->value.'"
            data-symbol=" '.$code.'"
            data-code="'. $currency->code .'">
            '.$currency->code.'
            </div>
            </div>';
          }
          $html_str .=  '</div>
          </div>
          <div class="panel-footer tiny text-muted">
          <strong class="primary_color_font">Please Note</strong>&nbsp; Rates are for indicative purposes only as bookings are made in their original currency. Updated on the
          <span class="nobr">'. date("d/m/Y").'</span>
          </div>
          </div>';

          return $html_str;
