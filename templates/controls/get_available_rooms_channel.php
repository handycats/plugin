
<?php
$html_str = "<hr>";
$html_str .= "<center><h3><i class='fa fa-list' aria-hidden='true'></i> SEARCH RESULTS</h3></center>";
$html_str .= "<hr>";
$html_str .= "<div class='get-available-room cf'>";
$room_index = 0;
$roomCodesArray = array();
$calender = $roomData->channel->calender;

foreach ($roomData->channel->rooms as $room) {
    array_push($roomCodesArray, $room->room_information->v_code);
    if ($room_index == 0) {
        $html_str .= "<div class='panel panel-default box' id='cheapest'>";
        $html_str .= "<div class='ribbon ribbon-top-left'><span>Best Deal</span></div>";
    } else {
        $html_str .= "<div class='panel panel-default box default'>";
    }
    $html_str .= "<div class='panel-body' style='height:auto;padding:20px;'>";

    $html_str .= "<div class='row'>";

    if ($room->room_information->listing_room_image[0]->id) {
        $html_str .= '<div class="col-md-3" style="background-color:#eee;padding:0px;height:230px;" >';
        $image_url = trim(S4A_IMG_URL) . '?id=' . $room->room_information->listing_room_image[0]->id . '&type=avail&file=room';
        $html_str .= "<img src= '" . $image_url . "' class='img-responsive center-block' style='height:100%;width:100%;border-radius:0px;'>";
        $html_str .= '<div class="overlay"></div>';
        $html_str .= '<div class="button-div"><button class="room-info-btn" data-code="' . $room->room_information->v_code . '" data-info="' . base64_encode(gzdeflate(serialize($room->room_information))) . '"  data-target="room-information-overlay" > MORE INFO </button></div>';
        $html_str .= "</div>";
        $html_str .= "<div class='col-md-9'>";
    } else {
        $html_str .= "<div class='col-md-12'>";
    }

    $html_str .= "<div class='row'";
    $html_str .= "<div class='col-md-12'>";
    if ($room->room_information->v_room_name) {
        $html_str .= '<h2 style="float:left; width:50%;" class="room-title">' . $room->room_information->v_room_name . '</h2>';
    } else if ($room->room_information->description) {
        $html_str .= '<h2 style="float:left; width:50%;" class="room-title">' . $room->room_information->description . '</h2>';
    } else {
        $html_str .= '<h2 style="float:left; width:50%;" class="room-title">Room</h2>';
    }

    $html_str .= "<div class='enquire_btn_section'><td style='text-align:center;'><button class='room_contct_btn' onclick=\"BookingProcesss.reserve('" . $room->room_information->v_code . "', 'enquire_listing','EnquiryAvailabilityCriteria')\">ENQUIRE </button></td></div>";

    $html_str .= "<br><div style='float:left;width:100%;' class='room-sleeps-info'>";

    if ($room->room_information->i_max_occupancy) {
        $html_str .= "<span> <i class='fa fa-group '></i> Sleeps  " . $room->room_information->i_max_occupancy . '</span>';
    }

    if ($room->room_information->i_max_adult_occupancy) {
        $html_str .= " |  <span><i class='fa fa-male fa-2x '></i> x " . $room->room_information->i_max_adult_occupancy . '</span>';
    }

    if ($room->room_information->i_max_child_occupancy) {
        $html_str .= " |  <span> <i class='fa fa-child '></i> x " . $room->room_information->i_max_child_occupancy . '</span>';
    }

    if ($room->room_information->listing_room_bed_types) {
        $html_str .= " |  <span><i class='fa fa-bed '></i> ";
        foreach ($room->room_information->listing_room_bed_types as $key => $bed_types) {
            $html_str .= $bed_types->global_information->v_name;
            $html_str .= ($key + 1 < count($room->room_information->listing_room_bed_types)) ? ', ' : '';
        }
        $html_str .= '</span>';
    }

    $html_str .= "</div>";

    $html_str .= "<div class='col-md-12'> ";
    $html_str .= "<table align='left' border='0' id='rates-table'>";
    $rooms_left = 0;
    $rate_count = 0;
    $from_price = 0;

    foreach ($room->rate_information as $rates) {
        $pricePer = \S4A\Helpers::DoPrice($rates->currency_code, $rates->price);
        $priceTotal = \S4A\Helpers::DoPrice($rates->currency_code, $rates->total);
        $rooms_left += $rates->units;

        $includes_arr = explode(",", $rates->includes);
        $includes_count = count(array_filter($includes_arr, 'strlen'));

        $excludes_arr = explode(",", $rates->excludes);
        $excludes_count = count(array_filter($excludes_arr, 'strlen'));

        $html_str .= "<tr>";
        $html_str .= "<td colspan='2'> <b><i class='fa fa-star' aria-hidden='true'></i> Rate:</b> " . (($rates->special_description) ? $rates->special_description : "N/A") . "</td>";
        $html_str .= "<td ><b> <i class='fa fa-moon-o' aria-hidden='true'></i> Per night: </b> " . $pricePer . "</td>";
        $html_str .= "<td > <b> <i class='fa fa-balance-scale' aria-hidden='true'></i> Total:</b> " . $priceTotal . "</td>";
        $html_str .= "</tr>";

        $html_str .= "<tr style='border-bottom:1px solid #eee;'>";
        $html_str .= "<td colspan='3'>";

        if ($includes_count > 0) {
            $width = ($excludes_count <= 0) ? "100%" : "50%";
            $html_str .= "<div class='div-inc-exc' style='width:" . $width . ";'>
      <button class='hideshow' id='" . $rates->rate_id . "' style='color:#3498db !important;border:1px solid #eee;padding:5px;width:100%;'>
      <i class='fa fa-plus' id='inc_plus_" . $rates->rate_id . "' aria-hidden='true' style='float:left;'></i><i class='fa fa-minus' style='display:none;float:left;' id='inc_minus_" . $rates->rate_id . "' aria-hidden='true' style='float:left;font-weight:bold;'></i> Includes(" . $includes_count . ") </b>
      </button>
      <div id='includes_" . $rates->rate_id . "' style='display:none;'>
      <ul class='fa-ul'>";
            foreach ($includes_arr as $include) {
                $html_str .= "<li>" . \S4A\Helpers::mapIcons($include) . " " . $include . "</li>";
            }
            $html_str .= "</ul>
      </div>
      </div>";
        }

        if ($excludes_count > 0) {
            $width = ($includes_count <= 0) ? "100%" : "50%";
            $html_str .= "<div class='div-inc-exc' style='width:" . $width . ";'>
      <button class='hideshow' id='" . $rates->rate_id . "' style='color:#b94a48 !important;border:1px solid #eee;padding:5px;width:100%;'>

      <i class='fa fa-plus' id='exc_plus_" . $rates->rate_id . "' aria-hidden='true' style='float:left;'></i> <i class='fa fa-minus' style='display:none;float:left;' id='exc_minus_" . $rates->rate_id . "' aria-hidden='true' style='float:left;font-weight:bold;'></i> Excludes(" . $excludes_count . ")

      </button>
      <div id='excludes_" . $rates->rate_id . "' style='display:none;' >
      <ul class='fa-ul'>";
            foreach ($excludes_arr as $exclude) {
                $html_str .= "<li>" . \S4A\Helpers::mapIcons($exclude) . " " . $exclude . "</li>";
            }
            $html_str .= "</ul>
      </div>
      </div>";
        }
        $html_str .= "</td><td style='text-align:center;'>";
        if ($roomData->payment) {
            $html_str .= "<button id='reserve' class='grn_button primary_color' onclick=\"BookingProcesss.reserve('" . $rates->rate_id . "', 'reserve_listing','ReservationAvailabilityCriteria')\">Reserve <i class='fa fa-chevron-right' aria-hidden='true'></i></button>";
        } else {
            $html_str .= "<button id='reserve' class='grn_button primary_color' onclick=\"BookingProcesss.reserve('" . $rates->rate_id . "', 'enquire_listing','EnquiryAvailabilityCriteria')\">Enquire <i class='fa fa-chevron-right' aria-hidden='true'></i></button>";
        }
        $html_str .= "</td></tr>";
    }
    $html_str .= "</table>";
    if($rooms_left < 10)
      $html_str .= "<div class='blink'><span class='availible-units-text'><b>Only " .$rooms_left . " unit(s) left!</b></span></div>";
    $html_str .= "<div class='looking-users primary_color_font'><i class='fa fa-clock-o' aria-hidden='true'></i> Book now, it only takes 2 minutes!</div>";
    $html_str .= "</table></div></div></div></div>";

    //$html_str .= \S4A\Controls::availability_calendar($roomData, $room->room_information->v_cod);

    $html_str .= "</div>";
	  $html_str .= "</div>";
    $room_index++;
}
foreach ($roomData->rooms->rooms as $room) {
    if (!in_array($room->v_code, $roomCodesArray)) {

        $roomAvailable = \S4A\Helpers::RoomAvailable($room, $AvailabilityCriteria);

        if (!$roomAvailable) {
            $unavailable = true;
        } else {
            $booked_out = true;
        }

        include S4A_DIR_PATH . '/templates/controls/room.php';
    }
}
$html_str .= "</div>";
return $html_str;
