<?php
$html_str = "<div id='child_occupents_" . $room . "'>";
for ($child = 1; $child <= $children; $child++) {
    $html_str .= "<br /><div class='child_occupent' data-child='" . $child . "'>"
        . "<p>Child " . $child . ":</p>"
        . "<label> Age (On Arrival):</label>"
        . "<select name='child_age_" . $room . "' data-type='age' required>"
        . "<option value=''>-</option>";

    for ($age = $child_min; $age <= $child_max; $age++) {
        $html_str .= "<option value='" . $age . "'>" . $age . "</option>";
    }

    $html_str .= "</select>"
        . "</div>";
}
$html_str .= "</div>";
return $html_str;
