<?php
$location = \S4A\Controls::getLocationFromURL();

if(!$location)
return null;

$locationType = \S4A\Helpers::Alpha($location);
$locationId = \S4A\Helpers::Numeric($location);

//print_r(\S4A\Cache::AreaCache());

echo "<div id='listingQuickLinks'>";

if ($locationType == "p") {
    $provinceObj = \S4A\Cache::ProvinceCache()[$locationId];
    $locationObj = \S4A\Cache::CountryCache()[$provinceObj->i_country_id];
    $locationArray = \S4A\Cache::ProvinceCache();

if(!$locationArray)
return null;

    echo "<h4>" . $locationObj->v_name . "</h4>";
    echo "<ul class='z_mini_menu'>";
    foreach ($locationArray as $loc) {
if($loc->id != $locationId){
?>
        <li><a href="<?= \S4A\Helpers::ResultsUrl(null, $loc->id, null) ?>"><?= $loc->v_name ?></a></li>
<?php
}
}
echo "</ul>";
} elseif ($locationType == "a") {
$areaObj = \S4A\Cache::AreaCache()[$locationId];
$pId = $areaObj->i_province_id;
$locationObj = \S4A\Cache::ProvinceCache()[$areaObj->i_province_id];
$areasArray = \S4A\Cache::AreaCache();

$locationArray = array_filter(
$areasArray,
function ($e) use (&$pId) {
    return strtolower($e->i_province_id) == $pId;
}
);

if(!$locationArray)
return null;

echo "<a href='" . \S4A\Helpers::ResultsUrl(null, $areaObj->i_province_id, null) . "'><h4>" . $locationObj->v_name . "</h4></a>";
echo "<ul class='z_mini_menu'>";
foreach ($locationArray as $loc) {
if($loc->id != $locationId){
?>
        <li><a href="<?= \S4A\Helpers::ResultsUrl(null, null, $loc->id) ?>"><?= $loc->v_name ?></a></li>
    <?php
}
}
echo "</ul>";
}
?>
</div>
