<?php
$userSearchData = \S4A\Session::get_user_search_data();
if ($instance['wps4a_title']) {
    echo "<h3>" . $instance['wps4a_title'] . "</h3>";
}
?>
<div id="S4A_horizontal_widget" class="s4a-search horizontal_form_container css_horizontalearch row">
    <form action="<?= S4A_SEARCH_URL ?>/" class="horizontal_search_form" name="horizontal_search_form" method="post" >
        <div class="col-md-6">
            <input type="text" name="title" class="keyword" value="<?= str_replace("\\", "", (($userSearchData->title) ? $userSearchData->title : $_GET['title'])); ?>" placeholder="- keyword -">
        </div>
        <div class="css_date">
            <div class="col-md-2">
                <input type="date" autocomplete="off" name="checkInDate" value="<?= ($userSearchData->checkInDate) ? $userSearchData->checkInDate : $_GET['checkInDate']; ?>" placeholder="Check-in" class="datepicker_checkin">
            </div>
            <div class="col-md-2">
                <input type="date" autocomplete="off" name="checkOutDate" value="<?= ($userSearchData->checkOutDate) ? $userSearchData->checkOutDate : $_GET['checkOutDate']; ?>" placeholder="Check-out" class="datepicker_checkout">
            </div>
        </div>
        <div class="col-md-2">
            <input type="submit" name="search" value="Search" class="submit_search grn_button">
        </div>
    </form>
</div>
<script  type="text/javascript">
    window.addEventListener('load', function () {
        DateRangeBuild('#S4A_horizontal_widget .datepicker_checkin', '#S4A_horizontal_widget .datepicker_checkout');
        jQuery('#S4A_horizontal_widget select').SumoSelect();
    });
</script>
