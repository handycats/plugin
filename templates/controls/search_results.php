<?php


$price = \S4A\Helpers::DoPrice($ListingSummery->country_currency , $ListingSummery->i_price_from );


$return = '<div class="result_tile" id="listing-' . $ListingSummery->id . '">';

if ($ListingSummery->v_listing_integration_ids) {
  $return .= '<div class="online-booking"><i class="fa fa-check" aria-hidden="true"></i><span>Online Booking</span></div>';
}

$return .= '<a href="' . \S4A\Helpers::ListingUrl($ListingSummery->v_title, $ListingSummery->id) . '" class="css_bg_image" >';

if ($noLasy) {
  $return .= '<div class="css_bg_image_pic" style="background-image:url(' . S4A_IMG_URL . '?id=' . $ListingSummery->imageid . '&type=latest)"></div>';
} else {
  $return .= '<div class="css_bg_image_pic lazy" data-original="' . S4A_IMG_URL . '?id=' . $ListingSummery->imageid . '&type=latest"></div>';
}
if (!$display && !S4A_DISABLE_AVAILABILITY) {
  if (!preg_match('/night free/', $ListingSummery->t_description) && $ListingSummery->i_percent == 0) {
    $return .= '<div class="result_dis"><div class="result_dis_inner"><span>Great</span>Value</div></div>';
  } else if (!preg_match('/night free/', $ListingSummery->t_description)) {
    $return .= '<div class="result_dis"><div class="result_dis_inner"><span>SAVE</span>' . $ListingSummery->i_percent . '%</div></div>';
  } else {
    $return .= '<div class="result_dis"><div class="result_dis_inner"><span>FREE</span>Night</div></div>';
  }
}

$return .= '</a>';
$return .= '<h3><div class="css_txt_ecli"><a href="' . \S4A\Helpers::ListingUrl($ListingSummery->v_title, $ListingSummery->id) . '" >' . $ListingSummery->v_title . '</a>';
if (S4A_LOGIN && !$noLasy && !$display) {
  $return .= '<i title="' . (($uid && \S4A\Meta::CheckIfFav($uid, $ListingSummery->id)) ? 'Remove from Favorites' : 'Add to Favorites') . '" class="fa ' . (($uid && \S4A\Meta::CheckIfFav($uid, $ListingSummery->id)) ? 'fa-star' : 'fa-star-o') . ' s4a_fav" data-id="' . $ListingSummery->id . '"></i>';
}
$return .= '</div></h3>';

if (!$display) {
  $return .= '<dl class="prop_val">';

  if ($ListingSummery->i_price_from != 0) {
    $return .= '<dt class="prop_fro">From</dt><dd class="prop_fro">' . $price . '</dd>';
  }else {
    $return .= '<dt class="prop_fro">From</dt><dd class="prop_fro">N/A</dd>';
  }

  $return .= '<dt class="prop_typ">Type</dt><dd class="prop_typ"><div class="css_txt_ecli">' . $ListingSummery->listing_type . '</div></dd>';
  $return .= '<dt class="prop_are">Area</dt><dd class="prop_are">';
  $return .= '<div class="css_txt_ecli">';



  if ($ListingSummery->area_name) {
    $return .= $ListingSummery->area_name;
  } else if ($ListingSummery->province_name) {
    $return .= $ListingSummery->province_name;
  } else if ($ListingSummery->country_name) {
    $return .= $ListingSummery->country_name;
  }

  $return .= '</div></dd></dl>';
  $return .= '<a href="' . \S4A\Helpers::ListingUrl($ListingSummery->v_title, $ListingSummery->id) . '" class="result_button" >view accomodation</a>';
  $return .= ' </div>';
}

return $return;
