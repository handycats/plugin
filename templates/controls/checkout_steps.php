<ol class="progress-track">
    <li class="<?= \S4A\Controls::checkout_steps_check($current, 1) ?>">
    <center>
        <div class="icon-wrap">
            <i class="fa fa-chevron-down icon-check-mark" aria-hidden="true"></i>
        </div>
        <span class="progress-text">Reservation</span>
    </center>
</li>

<li class="<?= \S4A\Controls::checkout_steps_check($current, 2) ?>">
<center>
    <div class="icon-wrap">
        <i class="fa fa-chevron-down icon-check-mark" aria-hidden="true"></i>
    </div>
    <span class="progress-text">Payment</span>
</center>
</li>

<li class="<?= \S4A\Controls::checkout_steps_check($current, 3) ?>">
<center>
    <div class="icon-wrap">
        <i class="fa fa-chevron-down icon-check-mark" aria-hidden="true"></i>
    </div>
    <span class="progress-text">Confirmation</span>
</center>
</li>

</ol>
