<?php
$room_data = unserialize(gzinflate(base64_decode($roomData)));
$html_str = '
  <div id="fsModal_'.$room_data->v_code.'" class="modal animated bounceIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="padding:5% !important;">

  <div class="modal-dialog" id="room-modal-dialog" >
      <div class="modal-content" id="room-modal-content">
        <div class="row">
          <div class="col-md-12" id="room-modal-header">
            <h1 id="room-modal-title">'.$room_data->v_room_name.'</h1>
              <button data-dismiss="modal" id="btn-close-modal"> <i class="fa fa-times" aria-hidden="true"></i> </button>
          </div>
        </div>
        <div class="row" >
          <div class="col-md-12" id="room-modal-body" style="width:100%;">
              <div id="roomCarousel" class="carousel slide testimonials-slider" data-ride="carousel">
                <div class="room-images carousel-inner" role="listbox">';
                  foreach ($room_data->listing_room_image as $index => $image)
                  {
                    $image_url  = trim(S4A_IMG_URL).'?id='.$image->id.'&type=latest&file=room';
                    $html_str .= '<div class="testimonial-items item ' .($index == 0 ? "active" : "") . '"><img style="width:100% !important;" class="d-block img-fluid" src= "'.$image_url.'" data-image="'.$image_url.'"></div>';
                  }
                  $html_str .= '
                    <a class="left carousel-control" href="#roomCarousel" role="button" data-slide="prev">
                      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#roomCarousel" role="button" data-slide="next">
                      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                </div>
              </div>
          </div>

          <div class="col-md-12" id="room-modal-body" style="clear:both;">';
            if($room_data->t_description){
              $html_str .= '
              <div class="row" >
                  <div class="col-md-12" style="clear:both;">
                     
                      <h1 class="pop-up-h1 background"><span>Description</span></h1>

                      <p style="text-justify:center;font-size:14px;color:dimgray !important;">'.$room_data->t_description.'</p>
                  </div>
              </div>';
            }
            $html_str .= '<div class="row">';
            if($room_data->i_max_adult_occupancy || $room_data->i_max_child_occupancy || $room_data->listing_room_bed_types){
                $html_str .= '
                <div class="col-md-6" style="clear:both;width:100%;">
                    <h1 class="pop-up-h1 background"><span>Guests</span></h1>';
                      if ($room_data->i_max_occupancy)
                          $html_str .= "<p style='color:dimgray;'><i class='fa fa-group'></i> Sleeps  " . $room_data->i_max_occupancy  . '</p>';
                      if ($room_data->i_max_adult_occupancy)
                          $html_str .= '<p style="color:dimgray;"><i style="font-size:1.5em;" class="fa fa-male fa-2x "></i> X ' . $room_data->i_max_adult_occupancy . '</p>';
                      if ($room_data->i_max_child_occupancy)
                          $html_str .= '<p style="color:dimgray;"><i class="fa fa-child "></i> X ' . $room_data->i_max_child_occupancy . '</p>';
                      if ($room_data->listing_room_bed_types) {
                          $html_str .= "<p style='color:dimgray;'><i class='fa fa-bed'></i> ";
                          foreach ($room_data->listing_room_bed_types as $key => $bed_types) {
                              $html_str .= $bed_types->global_information->v_name;
                              $html_str .= ($key + 1 < count($room_data->listing_room_bed_types)) ? ', ' : '';
                          }
                          $html_str .= '</p>';
                      }
                      $html_str .= '
                      </div>
                </div>';
              }
              if($room_data->listing_room_feature_list){
                $html_str .= '
                <div class="row">
                <div class="col-md-6" style="clear:both;width:100%; padding:5px; margin:0 0 0 0; " id="room-features">
                    <h1 class="pop-up-h1 background"><span>Features</span></h1>
                    <ul class="fa-ul feature-list">';
                      foreach($room_data->listing_room_feature_list as $feature){
                        $html_str .= '<li style="color:dimgray !important;list-style-type: none;">'.\S4A\Helpers::mapIcons($feature->global_information->v_name)."".$feature->global_information->v_name.'</li>';
                      }
                      $html_str .= '
                    </ul>
                    </div>
                </div>';
              }
              $html_str .= '
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>';
  
return $html_str;
