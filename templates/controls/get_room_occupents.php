<?php
$rooms = ($rooms != null) ? $rooms : 1;
$max_adults = ($max_adults != null) ? $max_adults : 2;
$max_children = ($max_children != null) ? $max_children : 0;
$child_min = ($child_min != null) ? $child_min : 2;
$child_max = ($child_max != null) ? $child_max : 12;

$html_str = "<div id='room_occupents'>";
for ($room = 1; $room <= $rooms; $room++) {
  if ($rooms > 1)
    $html_str .= "<p class='HCnumberOfUnits'>Room " . $room . ":</p>";
    $html_str .= "<div class='room_occupent clearfix' id='room_occupent_" . $room . "' data-room='" . $room . "'>";
    if ($max_adults > 0) {
        $html_str .= "<div class='col-md-6'><label>Adults</label>"
           ."<select data-type='adult'>";
        for ($adults = 1; $adults <= $max_adults; $adults++) {

            if ($adults == 2) {
                $html_str .= "<option value='" . $adults . "' selected>" . $adults . "</option>";
            } else {
                $html_str .= "<option value='" . $adults . "'>" . $adults . "</option>";
            }
        }
        $html_str .= "</select></div>";
    }
    if ($max_children > 0) {
        $html_str .= "<div class='col-md-6'><label>Children</label>"
            . "<select data-type='child' id='room_occupent_child_" . $room . "' onchange=\"BookingProcesss.child_occupents('room_occupent_child_" . $room . "', '" . $room . "', 'room_occupent_" . $room . "', 'child_occupents_" . $room . "', " . $child_min . ", " . $child_max . ")\">";
        for ($children = 0; $children <= $max_children; $children++) {

            $html_str .= "<option value='" . $children . "'>" . $children . "</option>";
        }
        $html_str .= "</select></div>";
    }
    $html_str .= "</div><br />";
}
$html_str .= "</div>";
return $html_str;
