<?php
$html_str = "<hr><center><h3><i class='fa fa-list' aria-hidden='true'></i> SEARCH RESULTS</h3></center><hr>";
if($error){
  $html_str .= "<div id='select-alert' class='alert alert-warning' role='alert'>" . $error. "</div>";
}else{
  $hasChildren = false;
  foreach($AvailabilityCriteria->Units as $unit){
    if(!empty($unit->Children)){
      $hasChildren = true;
    }
  }
  if(!$hasChildren){
    $html_str .= "<div id='select-alert' class='alert alert-warning' role='alert'>Unfortunately no units are available for that time period. Please make an enquiry so we can further assist you.</div>";
  } else {
    $html_str .= "<div id='select-alert' class='alert alert-warning' role='alert'>We see that your booking includes children. Please select the enquiry button in order to contact our Central Reservations Department to assist you with completing your booking.</div>";
  }
}
$html_str .= "<div class='get-available-room cf'>";
foreach ($roomData->rooms->rooms as $room) {
  include S4A_DIR_PATH . '/templates/controls/room.php';
}
$html_str .= "</div>";
return $html_str;
