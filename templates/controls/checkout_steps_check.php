<?php
if ($current > $that)
    return "progress-done";

if ($current == $that)
    return "progress-done progress-current";

if ($current < $that)
    return "progress-todo";
