<?php

$userSearchData = \S4A\Session::get_user_search_data();
$html_calender = null;
$return = null;

$userCheckInDate = date_format(date_create($userSearchData->checkInDate), "Y-m-d");
$userCheckOutDate = date_format(date_create($userSearchData->checkOutDate), "Y-m-d");

if ($roomData->channel->calender && $v_code != null) {
    foreach ($roomData->channel->calender as $calRoom) {
        if ($calRoom->code == $v_code) {
            $l = date_create($calRoom->date);
            $arr = date_format(date_create($calRoom->date), "Y-m-d");
            $dep = date('Y-m-d', strtotime($arr . ' + '.$roomData->channel->days .' days'));
            if ($calRoom->units != 'Booked') {
                $selected = (($arr >= $userCheckInDate) && ($arr <= $userCheckOutDate)) ? "inRange" : null;
                $html_calender .= "<li class='callist " . $selected . "'><div><span class='calDate'>" . $calRoom->date . "</span></br><span class='calRoom'>" . $calRoom->units . " unit(s) left!</span></br><button type='button' onclick=\"BookingProcesss.updateDate('" . $arr . "','" . $dep . "')\" class='btn btn-primary btn-sm calButton'>Select</button></div></li>";
            } else {
                $html_calender .= "<li class='callist booked'><div><span class='calDate'>" . $calRoom->date . "</span></br>" . $calRoom->units . "</span></div></li>";
            }
        }
    }
}

if($html_calender){
  $return .= "<div id='example' class='example pagespan'><p><b>Please choose an alternative arrival date:</b></p>";
  $return .= "<button class='backward'><i class='fa fa-angle-left'></i></button>
    <button class='forward'><i class='fa fa-angle-right'></i></button><div class='frame' style='overflow : hidden;'><ul>";

  $return .= $html_calender;

  $return .= "</ul>";
  $return .= "</div>";
}

return $return;

?>
