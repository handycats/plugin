<?php
$html_str = '';
$model = new \stdClass();

if ($atts['id'] != '') {
  $model->Listing = ($listing) ? ($listing) : \S4A\Cache::get_ListingByIdCache($atts['id']);
  $model->Listing->Listing->i_max_units = ($model->Listing->Listing->i_max_units) ? $model->Listing->Listing->i_max_units : 9;
  if(\S4A\Session::get_user_search_data())
  $model->UserSearchDates = \S4A\Session::get_user_search_data();
  if($model->UserSearchDates){
    $model->UserSearchDates->checkInDate = ($model->UserSearchDates != '') && $model->UserSearchDates->checkInDate != null ? $model->UserSearchDates->checkInDate : '';
    $model->UserSearchDates->checkOutDate = ($model->UserSearchDates != '') && $model->UserSearchDates->checkOutDate != null ? $model->UserSearchDates->checkOutDate : '';
  }
  if ($model->Listing) {

    $html_str = '
    <div id="check_availibility_control" class="s4a_availability_control">

      <div class="col-md-12">
        <div id="get_available_rooms" class="">
          <input type="hidden" data-field="id" value="' . $model->Listing->Listing->id . '" />
          <input type="hidden" data-field="establishment" value="' . $model->Listing->Listing->v_title . '" />

          <div class="row">';
            if ($model->Listing->has_booking) {
              $html_str .= '<div class="col-md-12">
              <h3 class="Enqform s4a-title-plugin primary_color_font" id="js_SearchForSpecials"><i class="fa fa-search" aria-hidden="true"></i> Search for Rates</h3>
              <p id="search-availability" class="primary_color"><b><i class="fa fa-check" aria-hidden="true"></i> Online Booking</b></p>
          </div>

        <div class="col-md-4">';
            if ($model->Listing->Listing->i_max_units > 1){
              $html_str .= '<div class="HCnumberOfUnits">
              <label>Number of Units:</label>
              <select onchange=\'BookingProcesss.occupents("the_rooms", "room_occupents_wrap", "room_occupents", "' . $model->Listing->Listing->i_max_adults . '","' . $model->Listing->Listing->i_max_children . '","' . $model->Listing->Listing->i_child_min . '","' . $model->Listing->Listing->i_child_max . '")\' id="the_rooms" name="the_rooms">';

              for ($i = 1; $i <= $model->Listing->Listing->i_max_units; $i++) {
                $html_str .= '<option value="' . $i . '">' . $i . '</option>';
              }

              $html_str .= '</select>
        </div><br>';
         }


      else {
        $html_str .= '<input type="hidden" data-field="id" value="1" id="the_rooms" name="the_rooms" />';

      }
      $html_str .='<div class="css_rooms" id="room_occupents_wrap">
      ' . \S4A\Controls::get_room_occupents(null, $model->Listing->Listing->i_max_adults, $model->Listing->Listing->i_max_children, $model->Listing->Listing->i_child_min, $model->Listing->Listing->i_child_max) . '
      </div>
      </div>
      <div class="col-md-6 css_text_right" id="search_dates">
          <input placeholder="Check-in" type="date" autocomplete="off" data-field="the_arrival_date" id="datepicker_checkin" class="datepicker datepicker_checkin" value="' . $model->UserSearchDates->checkInDate . '" />
          <span class="input-group-addon">to</span>
          <input placeholder="Check-out" type="date" autocomplete="off" data-field="the_departure_date" id="datepicker_checkout" class="datepicker datepicker_checkout" value="' . $model->UserSearchDates->checkOutDate . '" />
        </div>
        <div class="col-md-2 css_text_right">
        <button onclick=\'BookingProcesss.get_available_rooms("get_available_rooms", "available_rooms", "available_rooms")\' class="submit grn_button primary_color" primary_color_border/><i class="fa fa-search" aria-hidden="true"></i> SEARCH</button>
      </div>';
    } else {
      $html_str .=
      '<div class="col-md-12">
        <h3 class="Enqform s4a-title-plugin" id="js_SearchForSpecials"><i class="fa fa-search" aria-hidden="true"></i> Search for Rooms</h3>
        <p id="search-availability-offline"><b><i class="fa fa-times" aria-hidden="true"></i> No Online Booking.</b></p>
      </div>
      <div class="col-md-9 css_text_right" id="search_dates">
        <input placeholder="Check-in" type="date" autocomplete="off" data-field="the_arrival_date" id="datepicker_checkin" class="datepicker datepicker_checkin" value="' . $model->UserSearchDates->checkInDate . '" />
        <span class="input-group-addon">to</span>
        <input placeholder="Check-out" type="date" autocomplete="off" data-field="the_departure_date" id="datepicker_checkout" class="datepicker datepicker_checkout" value="' . $model->UserSearchDates->checkOutDate . '" />
        </div>
        <div class="col-md-3 css_text_center">
        <button onclick=\'BookingProcesss.get_available_rooms("get_available_rooms", "available_rooms", "available_rooms")\' class="submit grn_button primary_color" primary_color_border/><i class="fa fa-search" aria-hidden="true"></i> SEARCH</button>
      </div>';
    }
    $html_str .= '</div></div>';

    $html_str .=  '<div id="available_rooms"></div>
    <form method="post" action="' . \S4A\Helpers::ReservationUrl($model->Listing->Listing->v_title, $model->Listing->Listing->id) . '" id="reserve_listing">
    <input type="hidden" id="ReservationAvailabilityCriteria" name="AvailabilityCriteria" value></input>
    </form>
    <form method="post" action="' . \S4A\Helpers::EnquiryUrl($model->Listing->Listing->v_title, $model->Listing->Listing->id) . '" id="enquire_listing">
    <input type="hidden" id="EnquiryAvailabilityCriteria" name="AvailabilityCriteria" value></input>
    </form><div>';
    $html_str .= "<hr>" . \S4A\Controls::currency_converter_widget();
    $html_str.=  '</div></div><div class="clearfix"></div>';

    $html_str .= '<div class="modal fade" role="dialog" tabindex="-1" data-keyboard="true" id="room-information-overlay"></div></div>';
  }
}
return $html_str;
