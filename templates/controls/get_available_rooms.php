<?php
$html_str = "<hr><center><h3><i class='fa fa-list' aria-hidden='true'></i> SEARCH RESULTS</h3></center><hr>";
if($error){ $html_str .= '<div id="select-alert" class="alert alert-warning" role="alert">'.$error.'</div>'; }

foreach ($roomData->local->rooms as $room)
{
  $html_str .= "<div class='panel panel-default box default '>";
  $html_str .= "<div class='panel-body style='height:auto;padding:20px;'>";

  $html_str .= "<div class='row'>";
if($room->room_information->listing_room_image[0]->id){
  $html_str .= '<div class="col-md-3" style="background-color:#eee;padding:0px;height:230px;" >';
  $image_url  = trim(S4A_IMG_URL).'?id='.$room->room_information->listing_room_image[0]->id.'&type=avail&file=room';
  $html_str .= "<img src= '".$image_url."' class='img-responsive center-block' style='height:100%;width:100%;border-radius:5px;'>";
  $html_str .= '<div class="overlay"></div>';
  $html_str .= '<div class="button-div"><button class="room-info-btn" data-code="'.$room->room_information->v_code .'" data-info="'.base64_encode(gzdeflate(serialize($room->room_information))).'"  data-target="room-information-overlay" > MORE INFO </button></div>';
  $html_str .= "</div>";
  $html_str .= "<div class='col-md-9'>";
} else {
  $html_str .= "<div class='col-md-12'>";
}

  $html_str .= "<div class='row'";
  $html_str .= "<div class='col-md-12'>";
  if ($room->room_information->v_room_name){
    $html_str .= '<h2 style="float:left; width:50%;" class="room-title">' . $room->room_information->v_room_name .'</h2>';
  }else if($room->room_information->description){
    $html_str .= '<h2 style="float:left; width:50%;" class="room-title">' . $room->room_information->description .'</h2>';
  }else{
    $html_str .= '<h2 style="float:left; width:50%;" class="room-title">Room</h2>';
  }

  //$html_str .= "<div style='float:right;width:50%;text-align:right;'><td style='text-align:center;'></td></div>";

  $html_str .= "<br><div style='float:left;width:100%;' class='room-sleeps-info'>";

  if ($room->room_information->i_max_occupancy)
  $html_str .= "<span> <i class='fa fa-group'></i> Sleeps  " . $room->room_information->i_max_occupancy  . '</span>';
  if ($room->room_information->i_max_adult_occupancy)
  $html_str .= " |  <span><i class='fa fa-male fa-2x'></i> x " . $room->room_information->i_max_adult_occupancy . '</span>';
  if ($room->room_information->i_max_child_occupancy)
  $html_str .= " |  <span> <i class='fa fa-child'></i> x " . $room->room_information->i_max_child_occupancy . '</span>';
  if ($room->room_information->listing_room_bed_types) {
    $html_str .= " |  <span><i class='fa fa-bed '></i> ";
    foreach ($room->room_information->listing_room_bed_types as $key => $bed_types) {
      $html_str .= $bed_types->global_information->v_name;
      $html_str .= ($key + 1 < count($room->room_information->listing_room_bed_types)) ? ', ' : '';
    }
    $html_str .= '</span>';
  }

  $html_str .= "</div>";

  $html_str .= "<div class='col-md-12'>";
  $html_str .= "<table align='left' border='0' id='rates-table'>";
  $rooms_left = 0;
  $rate_count = 0;
  $from_price = 0;

  foreach ($room->rate_information as $rates)
  {
    $pricePer = \S4A\Helpers::DoPrice($rates->currency_code , $rates->rack_rate );
    $priceTotal = \S4A\Helpers::DoPrice($rates->currency_code , $rates->price );
    $rooms_left += $rates->units;

    if($rate_count == 0){
      $from_price = $pricePer;
    }

    $discount;

    if (!preg_match('/night free/', $rates->special_description)) {
      $discount = \S4A\Helpers::CalculateDiscount($rates->price, $rates->rack_rate);
    } else {
      $discount .= "Free Night";
    }

    $html_str .= "<tr>";
    $html_str .= "<td> <b><i class='fa fa-star' aria-hidden='true'></i> Rate:</b> ".(($rates->special_description) ? $rates->special_description : "N/A")."</td>";
    $html_str .= "<td> <b><i class='fa 	fa fa-check' aria-hidden='true'></i> Normal:</b> ".$pricePer."</td>";
    $html_str .= "<td> <b><i class='fa fa fa-check' aria-hidden='true'></i> Discount:</b> ".$discount ."</td>";
    $html_str .= "<td> <b><i class='fa fa fa-check' aria-hidden='true'></i> Special:</b> ".$priceTotal ."</td>";
    $html_str .= "<td ><b> <i class='fa fa-moon-o' aria-hidden='true'></i> Per night: </b> ".(($rates->price_type) ? $rates->price_type : "N/A")."</td>";
    $html_str .= "<td ><button style='border: 1px solid #ccc;'  class='rates_contct_btn' onclick=\"BookingProcesss.reserve(".$rates->rate_id.",'enquire_listing','EnquiryAvailabilityCriteria')\">ENQUIRE</button></td>";
    $html_str .= "</tr>";

  }
  $html_str .= "</table>";

  $html_str .= "</div>";
  $html_str .= "</div>";
  $html_str .= "</div>";
  $html_str .= "</div>";
  $html_str .= "</div>";
  $html_str .= "</div>";
  $room_index++;
}
return $html_str;
