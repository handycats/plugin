<?php
$location = \S4A\Controls::getLocationFromURL();
$userSearchData = \S4A\Session::get_user_search_data();
$count = 0;
$facts_data = $facilities_data = $activities_data = '';
if (!empty($userSearchData->facts)) {
    $facts_data = explode(',', $userSearchData->facts);
    $count += count($facts_data);
}
if (!empty($userSearchData->facilities)) {
    $facilities_data = explode(',', $userSearchData->facilities);
    $count += count($facilities_data);
}
if (!empty($userSearchData->activities)) {
    $activities_data = explode(',', $userSearchData->activities);
    $count += count($activities_data);
}
?>
<div id="S4A_search_widget" class="s4a-search property_form_container css_sidesearch">
<?php
if ($instance['wps4a_title']) {
    echo "<h3>" . $instance['wps4a_title'] . "</h3>";
}
?>
    <form action="<?= S4A_SEARCH_URL ?>/" class="property_search_form" name="property_search_form" method="post" >

        <input type="text" name="title" class="keyword" value="<?= str_replace("\\", "", ((($userSearchData->title) ? $userSearchData->title : $_GET['title']))); ?>" placeholder="- keyword -">

        <select name="location" id="countryId" >
            <option value="">- ALL LOCATIONS -</option>
<?php foreach (\S4A\Cache::InitialisationDataCache()->Locations as $country) { ?>
                <option <?= ($country == "c" . $country->id) ? "selected" : null ?> value="c<?php echo $country->id; ?>"><?= $country->v_name ?></option>
                <?php if ($country->province_area) { ?>
                    <?php foreach ($country->province_area as $province) { ?>
                        <option <?= ($location == "p" . $province->id) ? "selected" : null ?> value="p<?php echo $province->id; ?>"> - <?= $province->v_name ?></option>
                        <?php if ($province->area) { ?>
                            <?php foreach ($province->area as $area) {
                                ?>
                                <option <?= ($location == "a" . $area->id) ? "selected" : null ?> value="a<?php echo $area->id; ?>"> - - <?= $area->v_name ?></option>
                            <?php

                        } ?>
                        <?php

                    } ?>
                    <?php

                } ?>
                <?php

            } ?>
            <?php

        } ?>
        </select>

        <div class="listingtype_container">
            <select multiple="multiple" name="listingType[]" id="listingType">
<?php
foreach (\S4A\Cache::InitialisationDataCache()->ListingTypes as $ListingType) {
    $selected = ($_GET['listingType'] && in_array($ListingType->id, $_GET['listingType'])) ? "selected" : null;
    ?>
                    <option <?php echo $selected; ?> value="<?php echo $ListingType->id; ?>"><?php echo $ListingType->v_name; ?>
                    <?php

                }
                ?>
            </select>
        </div>
        <select name="specialId" id="specialId">
            <option value=""> - ALL SPECIALS - </option>
<?php
foreach (\S4A\Cache::InitialisationDataCache()->ListingSpecials as $SpecialList) {
    $selected = ($_GET['specialId'] == $SpecialList->id) ? "selected" : null;
    echo '<option ' . $selected . ' value="' . $SpecialList->id . '" ' . $selected . ' >' . $SpecialList->t_description . '</option>';
}
?>
        </select>
        <input type="text" id="range" value="" name="range" />

        <input type="hidden" name="priceFrom"  id="priceFrom" value="<?= ($userSearchData->priceFrom) ? $userSearchData->priceFrom : $_GET['priceFrom']; ?>" placeholder="Price From..">
        <input type="hidden" name="priceTo" id="priceTo" value="<?= ($userSearchData->priceTo) ? $userSearchData->priceTo : $_GET['priceTo']; ?>" placeholder="Price To..">
        
        <div class="css_date">
            <input type="date" autocomplete="off" name="checkInDate" value="<?= ($userSearchData->checkInDate) ? $userSearchData->checkInDate : $_GET['checkInDate']; ?>" placeholder="Check-in" class="datepicker_checkin">
            <input type="date" autocomplete="off" name="checkOutDate" value="<?= ($userSearchData->checkOutDate) ? $userSearchData->checkOutDate : $_GET['checkOutDate']; ?>" placeholder="Check-out" class="datepicker_checkout">
        </div>
        <input type="submit" name="search" value="Search" class="submit_search grn_button">

        <div class="advance_serach"><a href="javascript:ShowModal('#advanceSearchModal');"> Advanced Search <?= ($count > 0) ? '(' . ($count) . ')' : '' ?></a></div>
        <div id="advanced_search_tool" style="display:none;">
<?php
if ($facts_data) {
    foreach ($facts_data as $facts) {
        ?>
                    <input class="facts" type="checkbox" name="facts[]" value="<?= $facts ?>" checked="true"/>
                    <?php

                }
            }
            if ($facilities_data) {
                foreach ($facilities_data as $facilities) {
                    ?>
                    <input class="facilities" type="checkbox" name="facilities[]" value="<?= $facilities ?>" checked="true"/>
                    <?php

                }
            }
            if ($activities_data) {
                foreach ($activities_data as $activities) {
                    ?>
                    <input class="activities" type="checkbox" name="activities[]" value="<?= $activities ?>" checked="true"/>
                <?php

            }
        }
        ?>
        </div>
    </form>
</div>
<script type="text/javascript">
    window.addEventListener('load', function () {
        jQuery('#S4A_search_widget select#listingTypeS4A_search_widget select#listingType').SumoSelect({placeholder: ' - All Types - ', selectAll: true});
        DateRangeBuild('#S4A_search_widget .datepicker_checkin', '#S4A_search_widget .datepicker_checkout');
        jQuery('#S4A_search_widget select').SumoSelect();
        var range = jQuery("#S4A_search_widget #range");
        range.ionRangeSlider({
        hide_min_max: true,
                keyboard: true,
                min: 500,
                max: 10000,
                type: 'double',
                step: 100,
                prefix: "R",
                grid: false
<?php
if ($_GET['priceFrom']) {
    echo ",from:" . $_GET['priceFrom'];
}
?>
<?php
if ($_GET['priceTo']) {
    echo ",to: " . $_GET['priceTo'];
}
?>
    });
    range.on("change", function () {
    jQuery("#S4A_search_widget #priceFrom").val(jQuery(this).data("from"));
            jQuery("#S4A_search_widget #priceTo").val(jQuery(this).data("to"));
    });
    }
    );
</script>
