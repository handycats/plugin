<?php if (is_user_logged_in()): ?>
<div class="container myaccount-page content_area">
<h1 class="entry-title s4a-title primary_color_font">My Account</h1>
    <div class="row menu">
        <?php foreach ($model->Menu as $nav) {?>
            <div class="col-xs-12 col-sm-2">
                <a href="<?=$nav['url']?>" class="<?=($nav['name'] == $model->Page) ? 'active' : null?>">
                    <i class="fa fa-<?=$nav['icon']?> primary_color_font" aria-hidden="true"></i><?=$nav['name']?>
                </a>
            </div>
        <?php }
?>
    </div>
</div>
    <?php
if ($model->Page == "My Account"):
    require_once S4A_DIR_PATH . 'templates/account/profile.php';
elseif ($model->Page == "My Companions"):
    require_once S4A_DIR_PATH . 'templates/account/companions.php';
elseif ($model->Page == "My Favorites"):
    require_once S4A_DIR_PATH . 'templates/account/favorites.php';
elseif ($model->Page == "My Alerts"):
    require_once S4A_DIR_PATH . 'templates/account/alerts.php';
elseif ($model->Page == "My Bookings"):
    require_once S4A_DIR_PATH . 'templates/account/bookings.php';
elseif ($model->Page == "My Enquiries"):
    require_once S4A_DIR_PATH . 'templates/account/enquiries.php';
elseif ($model->Page == "My Vouchers"):
    require_once S4A_DIR_PATH . 'templates/account/vouchers.php';
endif;
?>
<?php else:
    require_once S4A_DIR_PATH . 'templates/account/login.php';
endif;
