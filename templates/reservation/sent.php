<?php
if ($model->Reservation) { ?>
    <br/>
    <br/>
    <table class="listing-information s4a_table">
        <tr>
            <th> Booking Summary </th>
        </tr>
        <tr>
            <td data-label="Lodge Name">
                <b> Lodge Name: </b> <?= $model->Reservation->lodge_name ?>
            </td>
        </tr>
        <tr>
            <td data-label="Lodge Address">
                <b> Lodge Address: </b> <?= $model->Reservation->lodge_address ?>
            </td>
        </tr>
        <tr>
            <td data-label="Booking Reference Number">
                <b> Booking Reference Number: </b><?= $model->Reservation->booking_reference_number ?>
            </td>
        </tr>
        <tr>
            <td data-label="Travel Dates">
                <b> Travel Dates: </b>
                <br/>
                Arrival Date: <?= $model->Reservation->the_arrival_date ?>
                <br/>
                Departure Date: <?= $model->Reservation->the_departure_date ?>
            </td>
        </tr>
        <tr>
            <td data-label="Contact Person Details">
                <b> Contact Person's Details: </b><br/>
                Title: <?= $model->Reservation->contact_person_details->user_title ?><br/>
                First Name: <?= $model->Reservation->contact_person_details->user_name ?><br/>
                Last Name: <?= $model->Reservation->contact_person_details->user_last_name ?><br/>
                User Email: <?= $model->Reservation->contact_person_details->user_email ?><br/>
                Phone: <?= $model->Reservation->contact_person_details->user_phone ?><br/>
                Nationality: <?= $model->Reservation->contact_person_details->user_nationality ?><br/>
            </td>
        </tr>
        <tr>
            <td data-label="Guest Details">
                <b> Guest's Details: </b>
                <?php
                foreach ($model->Reservation->guest_details as $guest_detail) {
                    if ($guest_detail->adults) {
                        ?>
                        <br/> <b> Adults: </b>
                        <?php foreach ($guest_detail->adults as $adult) { ?>
                            <?= $adult->guest_title ?>
                            <?= $adult->guest_name ?>
                            <?= $adult->guest_last_name ?>
                            <br/>
                            <?php
                        }
                    } if ($guest_detail->child) {
                        ?>
                        <br/> <b> Child: </b>
                        <?php foreach ($guest_detail->child as $child) { ?>
                            <?= $child->guest_title ?>
                            <?= $child->guest_name ?>
                            <?= $child->guest_last_name ?>
                            <br/>
                            <?php
                        }
                    }
                }
              ?>
            </td>
        </tr>
        <tr>
            <td data-label="Special Message">
                <b> Special Message:</b><br/>
                <p><?= $model->Reservation->user_special ?></p>
            </td>
        </tr>
        <!--
        <tr>
            <td data-label="Important Check-In Information">
                <b> Important Check-In Information: </b>
                //$model->Listing->t_check_in_information
            </td>
        </tr>-->

    </table>

<?php } ?>
