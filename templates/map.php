<div id="map_search">
    <div class="map-view">
    <input id="pac-input" class="controls" type="text" placeholder="Search Map">
    <div id="map_canvas"></div></div> <!--end map-view-->
    <script type="text/javascript" async defer>
        window.addEventListener('load', function () {
            MapSearch();
        });
    </script>
</div>