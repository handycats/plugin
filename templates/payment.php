<?= S4A\Controls::checkout_steps(2); ?>
<?php if ($model->error) { ?>
    <center><p class="error_text"><?php print_r($model->error); ?></p></center>
<?php } ?>
<div id="order_process" style="display:none">
    <div class="row">
        <div class="col-md-9">
            <div class="get-available-room cf">
                <table class="s4a_table">
                    <tbody>

                        <tr class="first-cell">
                            <th>Establishment</th>
                            <th>Room Type</th>
                            <th>Special Type</th>
                            <th>Adults</th>
                            <th>Children</th>
                            <th>Check In</th>
                            <th>Check Out</th>
                        </tr>

                        <?php
                        foreach ($model->BookingCriteria->Reservation->ReservationCriteria->Units as $unit) {
                            ?>
                            <tr>
                                <td data-label="Establishment">
                                    <?= $model->BookingCriteria->Reservation->ReservationCriteria->establishment ?>
                                </td>
                                <td data-label="Room Type">
                                    <?= $model->BookingCriteria->Reservation->ReservationCriteria->ListingRoom->description ?>
                                </td>
                                <td data-label="Special Type">
                                    <?= $model->BookingCriteria->Reservation->ReservationCriteria->ListingRoom->special_description ?>
                                </td>
                                <td data-label="Adults">
                                    <?= $unit->adults ?>
                                </td>
                                <td data-label="Children">
                                    <?php
                                    if ($unit->Children) {
                                        echo count($unit->Children);
                                    } else {
                                        echo 0;
                                    }
                                    ?>
                                </td>
                                <td data-label="Check In">
                                    <?= $model->BookingCriteria->Reservation->ReservationCriteria->the_arrival_date ?>
                                </td>
                                <td data-label="Check Out">
                                    <?= $model->BookingCriteria->Reservation->ReservationCriteria->the_departure_date ?>
                                </td>
                            </tr>

                        <?php }
                        ?>
                    </tbody>
                </table>
            </div>
            <br />
            <?= $model->PaymentButton ?>
        </div>
        <div class="col-md-3">
            <section class="s4a_css_results">
                <?php
                $current_user = wp_get_current_user();
                echo S4A\Controls::search_results($model->Listing, $current_user->ID, "results", false, true)
                ?>
            </section>
            <hr />
            <?php

              $pricePerNight = \S4A\Helpers::DoPrice($model->BookingCriteria->Reservation->ReservationCriteria->ListingRoom->currency_code , $model->BookingCriteria->Reservation->ReservationCriteria->ListingRoom->price );
              $price = \S4A\Helpers::DoPrice($model->BookingCriteria->Reservation->ReservationCriteria->ListingRoom->currency_code,$model->BookingCriteria->Reservation->ReservationCriteria->ListingRoom->room_total );
              $priceTotal = \S4A\Helpers::DoPrice($model->BookingCriteria->Reservation->ReservationCriteria->ListingRoom->currency_code ,$model->BookingCriteria->Reservation->ReservationCriteria->ListingRoom->total );

            foreach ($model->BookingCriteria->Reservation->ReservationCriteria->ListingService as $listingService) {
                if ($listingService->Mandatory) {
                    echo "<p>" . $listingService->Description . ": " . $listingService->Price . " (Mandatory)</p>";
                }
            }
            $the_arrival_date = strtotime($model->BookingCriteria->Reservation->ReservationCriteria->the_arrival_date);
            $the_departure_date = strtotime($model->BookingCriteria->Reservation->ReservationCriteria->the_departure_date);
            $datediff = $the_departure_date - $the_arrival_date;
            $days = floor($datediff / (60 * 60 * 24));
            ?>
            <p>Per Night: <?php echo $pricePerNight ?></p>
            <p>Total for <?= $days ?> night(s):  <?php echo $price ?></p>
            <hr />
            <p><b>Total:  <?php echo $priceTotal ?></b></p>
        </div>
    </div>
</div>
<center>
<div class="transition_notice">
    <p>  Please wait while you are redirected to our secure payment gateway...  </p>
    <div class="loader"> <img src="<?= S4A_DIR_URL ?>assets/images/loading-spinner-blue.gif" alt="Loading.."> </div>
    <p> <a href="javascript:document.paygate_process_form.submit();"> Click here if you are not redirected within a few seconds</a></p>
</div>
</center>
<script>
    window.addEventListener("load", function () {
        setTimeout(function () {
            document.paygate_process_form.submit();
        }, 4000);
    });
</script>
<br>
