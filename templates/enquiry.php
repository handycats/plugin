<div class="alert alert-warning">
  <strong>Please note</strong> Upon enquiring, you will receive an estimated quote, this does not constitute as a booking or reservation. If you would like to book online please select a room and click on the ‘Reserve’ button.
</div>
<?php
  include S4A_DIR_PATH . '/templates/shared/enquiry.php';
?>
