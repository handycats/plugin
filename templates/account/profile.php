<div class="container myaccount-page content_area">
<h3 class="s4a-title">Your Details</h3>
<form method="post" action="">
    <div class="my-profile">
        <label for="user_title">Title:</label>
        <?php
        $titles = array("Adv" => "Adv", "Ahl" => "Ahl", "Alha", "Alha", "Amb" => "Amb", "Barr" => "Barr", "Bishop" => "Bishop", "Capt" => "Capt", "Capt/Mrs" => "Capt/Mrs", "Chief" => "Chief", "Chief/Mrs" => "Chief/Mrs", "Col" => "Col", "Count" => "Count", "Dr" => "Dr", "Dr&Mrs" => "Dr&Mrs", "Dr/Mr" => "Dr/Mr", "Engr" => "Engr", "Exc" => "Exc", "Fam" => "Fam", "Family" => "Family", "Gen" => "Gen", "Gen&Mrs" => "Gen&Mrs", "Gov" => "Gov", "HRH" => "HRH", "HRM" => "HRM", "Hon" => "Hon", "Hon&Mrs", "Hon&Mrs", "Judge" => "Judge", "KING" => "KING", "Lady" => "Lady", "Lord" => "Lord", "Maj" => "Maj", "Mal" => "Mal", "Mast" => "Mast", "Mes", "Mes", "Min" => "Min", "Min&Mrs" => "Min&Mrs", "Miss" => "Miss", "Mr" => "Mr", "Mr/Mrs" => "Mr/Mrs", "Mr/Ms" => "Mr/Ms", "Mrs" => "Mrs", "Ms" => "Ms", "Ms/Ms" => "Ms/Ms", "Past" => "Past", "Prem" => "Prem", "Pres" => "Pres", "Prince" => "Prince", "Princess" => "Princess", "Prof" => "Prof", "Prof&Mr" => "Prof&Mr", "Prof&Mrs" => "Prof&Mrs", "Proph" => "Proph", "REV" => "REV", "Rev" => "Rev", "SAN" => "SAN", "Sen" => "Sen", "Ser" => "Ser", "Sheik" => "Sheik", "Sir" => "Sir", "Sirs" => "Sirs");
        ?>
        <select name="user_title" id="user_title">
            <?php
            if ($titles) {
                $user_title = ($model->Profile->user_title) ? $model->Profile->user_title : "Mr";
                foreach ($titles as $key => $value) {
                    $selected = "";
                    if ($user_title == $value)
                        $selected = "selected='selected'";
                    echo '<option value="' . $value . '"  ' . $selected . ' >' . $value . '</option>';
                }
            }
            ?>
        </select>
        <label for="user_name">First Name:</label>
        <input placeholder="First Name" type="text"  name="user_name" id="user_name" value="<?= $model->Profile->user_name ?>">
        <label for="user_last_name">Last Name:</label>
        <input placeholder="Last Name" type="text"  name="user_last_name" id="user_last_name" value="<?= $model->Profile->user_last_name ?>">
        <label for="user_phone">Phone:</label>
        <input placeholder="Phone" type="text"  name="user_phone" id="user_phone" value="<?= $model->Profile->user_phone ?>">
        <label for="user_nationality">Nationality:</label>
        <?php
        $country = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Antigua and Barbuda", "Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Costa Rica", "Côte d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "North Korea", "South Korea", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestine, State of", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Romania", "Russia", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia and Montenegro", "Seychelles", "Sierra Leone", "Singapore", "Sint Maarten", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "Spain", "Sri Lanka", "Sudan", "Sudan, South", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Virgin Islands, British", "Virgin Islands, U.S.", "Yemen", "Zambia", "Zimbabwe");
        ?>
        <select name="user_nationality" id="user_nationality">
            <?php
            if ($country) {
                $user_nationality = ($model->Profile->user_nationality) ? $model->Profile->user_nationality : "South Africa";
                foreach ($country as $key => $value) {
                    $selected = "";
                    if ($user_nationality == $value)
                        $selected = "selected='selected'";
                    echo '<option value="' . $value . '"  ' . $selected . ' >' . $value . '</option>';
                }
            }
            ?>
        </select>
        <label for="user_email">Email Address:</label>
        <input class="disabled" placeholder="Email Address" type="email" name="user_email" id="user_email"  value="<?= $model->Profile->user_email ?>" readonly>
        <h3 class="s4a-title">Your Travel Requirements</h3>
        <label for="user_disabilities">Special Requirements:</label>
        <textarea placeholder="Special Requirements" name="user_special" id="user_special"><?= $model->Profile->user_special ?></textarea>
        <br />
        <input name="profile" value="Save" class="submit_search grn_button primary_color" type="submit">
    </div>
</form>
</div>
