<div class="container myaccount-page content_area">
<h3 class="s4a-title">Your Booking History</h3>
<div class="table-outer">
    <?php if ($model->BookingHistory) { ?>
        <table>
            <thead>
                <tr>
                    <th>Lodge Name</th>
                    <th>Lodge Address</th>
                    <th>Arrival Date</th>
                    <th>Departure Date</th>
                    <th>Reservation Id</th>
                    <th>Price</th>
                    <th>Discount</th>
                </tr>
            </thead>
            <tbody>
              <?php
                $currency = \S4A\Helpers::DoPrice($listingRoomAvailability->currency_code , $listingRoomAvailability->discount_amount )
                ?>
                    <tr>
                        <td><?= $listingRoomAvailability->lodge_name ?></td>
                        <td><?= $listingRoomAvailability->lodge_address ?></td>
                        <td><?= $listingRoomAvailability->the_arrival_date ?></td>
                        <td><?= $listingRoomAvailability->the_departure_date ?></td>
                        <td><?= $listingRoomAvailability->reservation_id ?></td>
                        <td> <?php echo \S4A\Helpers::DoPrice($listingRoomAvailability->currency_code ,$listingRoomAvailability->price ) ?></td>
                        <td> <?= ($listingRoomAvailability->discount_amount) ? $currency  : '' ?></td>
                    </tr>
            </tbody>
        </table>
    <?php } else { ?>
    <p>You don't currently have any online booking history.</p>
    <?php } ?>
</div>
</div>
