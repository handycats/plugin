<div class="container myaccount-page content_area">
<h3 class="s4a-title">Your Enquiry History</h3>
<div class="table-outer">
    <?php if ($model->EnquiryHistory) { ?>
        <table>
            <thead>
                <tr>
                    <th>Lodge Name</th>
                    <th>Lodge Address</th>
                    <th>Arrival Date</th>
                    <th>Departure Date</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model->EnquiryHistory as $listingEnquiry) { ?>
                    <tr>
                        <td><?= $listingEnquiry->lodge_name ?></td>
                        <td><?= $listingEnquiry->lodge_address ?></td>
                        <td><?= $listingEnquiry->the_arrival_date ?></td>
                        <td><?= $listingEnquiry->the_departure_date ?></td>
                        <td>R <?= $listingEnquiry->price ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php } else { ?>
    <p>You don't currently have any enquiry history.</p>
    <?php } ?>
</div>
</div>
