<div class="container myaccount-page content_area">
<h3 class="s4a-title">Your Companions</h3>
<form method="post" action="">
    <div class="my-profile">
        <label for="user_adults">How many adults?</label>
        <input placeholder="Number of Adults" type="text"  name="user_adults" id="user_adults" value="<?= $model->Profile->user_adults ?>">
        <label for="user_children">How many children?</label>
        <input placeholder="Number of Children" type="text"  name="user_children" id="user_children" value="<?= $model->Profile->user_children ?>">
        <label for="user_children_age">Children's age(s)?</label>
        <input placeholder="Ages of Children" type="text"  name="user_children_age" id="user_children_age" value="<?= $model->Profile->user_children_age ?>">
        <br />
        <input name="profile" value="Save" class="submit_search grn_button primary_color" type="submit">
    </div>
</form>
</div>
