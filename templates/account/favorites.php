<div class="container myaccount-page content_area">
<h3 class="s4a-title">Your Favorites</h3>
<section class="row s4a_css_results">
    <?php
    $current_user = wp_get_current_user();
    if ($model->ListingSearchResults && $model->ListingSearchResults->ListingSummeries):
        foreach ($model->ListingSearchResults->ListingSummeries as $ListingSummery) :
            echo '<article class="col-md-4"><div class="css_inner">' . S4A\Controls::search_results($ListingSummery, $current_user->ID) . '</div></article>';
        endforeach;
    else:
        echo "<p class='s4a_no-results'>There are no favorites currently associated to this account.</p>";
    endif;
    ?>
</section>
</div>
