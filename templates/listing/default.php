<div class="single-property-list row">
    <div class="container">
        <div class="col-md-9">
          <!-- Gallery START -->
            <?php if (!empty($model->Listing->Listing->listing_image)) { ?>
                <div id="gallery" style="display:none">
                            <?php if (!empty(($model->Listing->Listing->listing_video))) {

                                foreach ($model->Listing->Listing->listing_video as $listing_video) { ?>

                                <?php
                                $curr_url = $listing_video->v_url;

                                $yt_rx = '/^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/';
                                $has_match_youtube = preg_match($yt_rx, $curr_url, $yt_matches);

                                $vm_rx = '/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([‌​0-9]{6,11})[?]?.*/';
                                $has_match_vimeo = preg_match($vm_rx, $curr_url, $vm_matches);

                                if($has_match_youtube) {
                                $video_id = $yt_matches[5];
                                $type = 'youtube';
                                }
                                elseif($has_match_vimeo) {
                                $video_id = $vm_matches[5];
                                $type = 'vimeo';
                                }
                                else {
                                $video_id = 0;
                                $type = 'none';
                                }
                            $id = $video_id;
                        ?>

                          <?php if ($type == 'youtube') { ?>
                              <img data-type="youtube" data-videoid="<?php echo $id ?>" data-image="//img.youtube.com/vi/<?php echo $id ?>/0.jpg">
                          <?php } ?>

                          <?php if ($type == 'vimeo') {

                              $link = "http://vimeo.com/". $id;
                              $link = str_replace('http://vimeo.com/', 'http://vimeo.com/api/v2/video/', $link) . '.php';
                              $html_returned = unserialize(file_get_contents($link));
                              $thumb_url = $html_returned[0]['thumbnail_large'];
                              ?>
                            <img 	data-type="vimeo"	src="<?php echo $thumb_url ?>"	data-image="<?php echo $thumb_url ?>"		 data-videoid="<?php echo $id ?>">

                          <?php } ?>
                    <?php } }?>

                    <?php foreach ($model->Listing->Listing->listing_image as $image) { ?>
                        <img src="<?= S4A_IMG_URL . '?id=' . $image->id . '&type=listing_thumb' ?>" data-image="<?= S4A_IMG_URL . '?id=' . $image->id . '&type=origional' ?>">
                    <?php
                } ?>
                </div>
            <?php
        } ?>
        <!-- Gallery END -->
        </div>
        <div class="col-md-3" id="listing_side">
        <div class="list-intro">
        <div class="row prop-prices">
        <div class="col-md-6">
        <?php if ($model->Listing->Listing->i_price_from) { ?>
            <div class="prop-fro">
                <small>From</small>
                <?php echo \S4A\Helpers::DoPrice($model->Listing->Listing->listing_country->v_code , $model->Listing->Listing->i_price_from) ?>
            </div>
        <?php } ?>
        </div>
        
        <div class="col-md-6">
        <?php if (!S4A_DISABLE_AVAILABILITY) { ?>
            <a href="javascript:ScrollToTarget('#js_SpecialsOnOffer');" class="row el_button grn_button">View Specials</a>
            <?php
        } else { ?>
            <a href="<?=\S4A\Helpers::EnquiryUrl($model->Listing->Listing->v_title, $model->Listing->Listing->id)?>" class="row el_button grn_button">Make Enquiry</a>
            <?php
        } ?>
        </div>
        </div>
        
            <div class="row css_listing_det">
                <dl class="prop_val">
                    
                    <?php if ($Country = \S4A\Cache::CountryCache()[$model->Listing->Listing->i_country_id]) { ?><dt class="prop_cou">Country</dt><dd class="prop_cou"><?= $Country->v_name ?></dd><?php
                                                                                                                                                                                                } ?>
                    <?php if ($Province = \S4A\Cache::ProvinceCache()[$model->Listing->Listing->i_province_id]) { ?><dt class="prop_pro">Province</dt><dd class="prop_pro"><?= $Province->v_name ?></dd><?php
                                                                                                                                                                                                    } ?>
                    <?php if ($Area = \S4A\Cache::AreaCache()[$model->Listing->Listing->i_area_id]) { ?><dt class="prop_are">Area</dt><dd class="prop_are"><?= $Area->v_name ?></dd><?php
                                                                                                                                                                                } ?>

                </dl>
            </div>
        </div>
            <div class="css_listing_fac">
                <h3>Quick Facts</h3>

                <ul>
                    <?php
                    foreach (\S4A\Cache::InitialisationDataCache()->Facts as $fact) {
                        $value = ($model->FactsIdArry != null && in_array($fact->id, $model->FactsIdArry)) ? "Yes" : "No";
                        echo "<li>" . $fact->v_name . ": <b>" . $value . "</b></li>";
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>

    <div class="container">

        <div class="col-md-12">
            <?php if ($model->Listing->Listing->t_full_description) { ?>
                <h3 class="s4a-title">Description</h3>
                <div class="addthis_inline_share_toolbox"></div>
                <div class="full-description">
                    <?= nl2br($model->Listing->Listing->t_full_description, false) ?>
                </div>
            <?php
        } ?>
        </div>

    </div>


    <?php if ($model->Listing->Listing->listing_room) { ?>
    <div class="container rateTabel">

        <div class="col-md-12">
            <h3 class="s4a-title">Rates</h3>
                <?php foreach($model->Listing->Listing->listing_room as $listing_room) { ?>
                    <?php if($listing_room->listing_room_rate) { ?>
                        <b class="room_rate">Rates for <?=$listing_room->v_room_name?></b></br></br>
                        <?php 
                        if ($listing_room->listing_room_rate) { 

                            uasort($listing_room->listing_room_rate, function ($a, $b) {
                                return $a->d_from_date > $b->d_from_date;
                            });

                            ?>
                                    <table>
                                        <tr class="primary_color">
                                            <th>
                                                From
                                            </th>
                                            <th>
                                                To
                                            </th>
                                            <th>
                                                Rate
                                            </th>
                                            <th>
                                                Per night
                                            </th>
                                            <th>
                                                Normal
                                            </th>
                                            <th>
                                                Discount
                                            </th>
                                            <th>
                                                <b>Special</b>
                                            </th>
                                        </tr>
                                        <?php foreach($listing_room->listing_room_rate as $listing_room_rate) { ?>
                                            <tr>
                                                <td>
                                                    <?=$listing_room_rate->d_from_date?>
                                                </td>
                                                <td>
                                                    <?=$listing_room_rate->d_to_date?>
                                                </td>
                                                <td>
                                                    <?=$listing_room_rate->listing_special_type->t_description?>
                                                </td>
                                                <td>
                                                    <?=$listing_room_rate->e_price_type?>
                                                </td>
                                                <td>
                                                    <?=\S4A\Helpers::DoPrice($model->Listing->Listing->listing_country->v_code , $listing_room_rate->i_rack_rate )?>
                                                </td>
                                                <td>
                                                    <?=$listing_room_rate->i_percent?> %
                                                </td>
                                                <td>
                                                    <b><?=\S4A\Helpers::DoPrice($model->Listing->Listing->listing_country->v_code , $listing_room_rate->i_price )?></b>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </table></br></br>

                            <?php } ?>
                        <?php } ?>
                <?php } ?>
        </div>

    </div>
    <?php } ?>

    <?php if (!S4A_DISABLE_AVAILABILITY) { ?>
    <div id="check_availibility_control">
        <div class="container">
            <?php if ($model->Listing->Listing->listing_room_rate_special) { ?>
                <div class="col-md-12">
                    <h3 class="s4a-title" id="js_SpecialsOnOffer">Specials on Offer</h3>
                    <div class="row css_specials">
                        <?php foreach ($model->Listing->Listing->listing_room_rate_special as $special) { ?>
                            <a href="javascript:ScrollToTarget('#js_SearchForSpecials');">
                                <article class="col-md-4">
                                    <div class="special-pack-cell">
                                        <div class="css_bg_image_pic">
                                            <h4><?= $special->listing_special_type->t_description ?></h4>
                                            <ul>
                                                <?php
                                                if ($special->listing_special_type->t_terms_condition) {
                                                    $special_tc_array = explode("\n", $special->listing_special_type->t_terms_condition);
                                                    foreach ($special_tc_array as $special_tc) {
                                                        if (trim($special_tc))
                                                            echo "<li>" . $special_tc . "</li>";
                                                    }
                                                }
                                                ?>
                                                <?php if ($special->room_rate_information) { ?>
                                                    <?php foreach ($special->room_rate_information as $information) { ?>
                                                        <li><?= $information->room_information->v_name ?></li>
                                                    <?php
                                                } ?>
                                                <?php
                                            } ?>
                                                <?php
                                                $global_tc_array = explode("\n", get_option('wps4a_global_settings_tc'));
                                                foreach ($global_tc_array as $global_tc) {
                                                    if (trim($global_tc))
                                                        echo "<li>" . $global_tc . "</li>";
                                                }
                                                ?>
                                            </ul>
                                            <div class="valid-cell">
                                                <span>Valid From: <?php echo $special->d_from_date; ?> to <?php echo $special->d_to_date; ?></span>
                                            </div>
                                        </div>
                                        <div class="valid_dis">
                                            <?php if (!preg_match('/night free/', $special->listing_special_type->t_description) && $special->i_percent == 0) { ?>
                                                <div class="valid_dis_inner"><span>Great</span>Value</div>
                                            <?php
                                        } else if (!preg_match('/night free/', $special->listing_special_type->t_description)) { ?>
                                                <div class="valid_dis_inner"><span>SAVE</span><?= $special->i_percent ?>%</div>
                                            <?php
                                        } else { ?>
                                                <div class="valid_dis_inner"><span>FREE</span>Night</div>
                                            <?php
                                        } ?>
                                        </div>
                                    </div>
                                </article>
                            </a>
                        <?php
                    } ?>
                    </div>
                    <hr />
                </div>
            <?php
        }

        $atts['id'] = $model->Listing->Listing->id;
        echo \S4A\Controls::listing_availability_search($atts);
        ?>
    </div>
    </div>
    <?php
} ?>
    <div class="container">
        <div class="css_features cf">
            <div class="col-md-6">
                <h3 class="s4a-title">Features</h3>
                <div id="accordion" role="tablist" aria-multiselectable="true">
                    <?php if ($model->Includes) { ?>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingOne">
                                <h4 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#features_inc" aria-expanded="true" aria-controls="features_inc" class="collapsed">
                                        <i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i>Includes
                                    </a>
                                </h4>
                            </div>
                            <div id="features_inc" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="card-block css_features_inc">
                                    <ul>
                                        <?php
                                        foreach ($model->Includes as $listingGlobalInformation) {
                                            echo "<li>" . $listingGlobalInformation->v_name . "</li>";
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php
                } ?>
                    <?php if ($model->FactsFacilities) { ?>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingOne">
                                <h4 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#listing_faci" aria-expanded="true" aria-controls="listing_faci" class="collapsed">
                                        <i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i>Facilities
                                    </a>
                                </h4>
                            </div>
                            <div id="listing_faci" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="card-block css_listing_faci">
                                    <ul>
                                        <?php
                                        foreach ($model->FactsFacilities[0] as $listingGlobalInformation) {
                                            echo "<li>" . $listingGlobalInformation->v_name . "</li>";
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php
                } ?>
                    <?php if ($model->Activities) { ?>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingOne">
                                <h4 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#listing_act" aria-expanded="true" aria-controls="listing_act" class="collapsed">
                                        <i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i>Activities
                                    </a>
                                </h4>
                            </div>
                            <div id="listing_act" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="card-block css_listing_act">
                                    <ul>
                                        <?php
                                        foreach ($model->Activities as $listingGlobalInformation) {
                                            echo "<li>" . $listingGlobalInformation->v_name . "</li>";
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php
                } ?>
                </div>
            </div>
            <div class="col-md-6">
                <h3 class="s4a-title">Terms & Conditions</h3>
                <div id="accordion" role="tablist" aria-multiselectable="true">
                    <?php if ($model->Excludes) { ?>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingOne">
                                <h4 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#features_exc" aria-expanded="true" aria-controls="features_exc" class="collapsed">
                                        <i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i>Excludes
                                    </a>
                                </h4>
                            </div>
                            <div id="features_exc" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="card-block css_features_exc">
                                    <ul>
                                        <?php
                                        foreach ($model->Excludes as $listingGlobalInformation) {
                                            echo "<li>" . $listingGlobalInformation->v_name . "</li>";
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php
                } ?>
                    <?php if ($model->Policies) { ?>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingOne">
                                <h4 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#policies_exc" aria-expanded="true" aria-controls="policies_exc" class="collapsed">
                                        <i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i>Child Policy
                                    </a>
                                </h4>
                            </div>
                            <div id="policies_exc" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="card-block css_policies_exc">
                                    <ul>
                                        <?php
                                        foreach ($model->Policies as $listingGlobalInformation) {
                                            echo "<li>" . $listingGlobalInformation->v_name . "</li>";
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php
                } ?>
                    <?php if ($model->Listing->Listing->terms_conditions || get_option('wps4a_global_settings_tc')) { ?>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingOne">
                                <h4 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#features_ter" aria-expanded="true" aria-controls="features_ter" class="collapsed">
                                        <i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i>Terms and Conditions
                                    </a>
                                </h4>
                            </div>
                            <div id="features_ter" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="card-block css_features_ter">
                                    <?= $model->Listing->Listing->t_terms_conditions ?>
                                </div>
                            </div>
                        </div>
                    <?php
                } ?>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <?php if ($model->Listing->Listing->i_tripadvisor) { ?>
                    <div class="col-md-3">
                        <div id="TA_selfserveprop2010" class="TA_selfserveprop">
                            <ul id="laWm0i78Xvx" class="TA_links 8342infZV">
                                <li id="J6uIHkDY7" class="zit2VOza">
                                </li>
                            </ul>
                        </div>
                    </div>
                <?php
            } ?>
                <?php
                $latLong = $model->Listing->Listing->v_geo_coordinates;

                if ($latLong) {
                    $coordinates = explode(',', $latLong);
                    $longitude = floatval($coordinates[0]);
                    $latitude = floatval($coordinates[1]);
                    ?>
                    <div class="col-md-9">
                        <div id="map_canvas"></div>
                    </div>
                <?php
            } ?>
            </div>
            </br>
        </div>
        <div class="col-md-12">
             <?php
                 if(isset($model->Listing->Listing->listing_relative_summary) && !empty($model->Listing->Listing->listing_relative_summary)){
                    $current_user = wp_get_current_user();
                    ?><h3 class="s4a-title">You might also like</h3><?php
                    $return = '<section class="row s4a_css_results">';
                    foreach ($model->Listing->Listing->listing_relative_summary as $ListingSummery){
                     $return .= '<article class="col-md-4"><div class="css_inner">' . \S4A\Controls::search_results($ListingSummery, $current_user->ID) . '</div></article>';
                    }
                   echo $return;
                 }
                ?>
        </div>
    </div>
</div>
<script type="text/javascript">

    window.addEventListener('load', function () {

		<?php if ($latLong) { ?>
		jQuery('#map_canvas').appear(function() {
		  initialize_GMap("<?= $longitude ?>", "<?= $latitude ?>");
		});
		<?php } ?>

        jQuery("#gallery").unitegallery({
            gallery_width: null,
            gallery_height: null,
            slider_zoom_mousewheel: false,
            gallery_autoplay: true,
            gallery_play_interval: 10000
        });

		if (typeof fbq === 'function') {

			fbq('track', 'ViewContent', {
				content_name: '<?= \S4A\Helpers::AlphaNumeric($model->Listing->Listing->v_title) ?>',
				content_category: 'Accomodation',
				content_ids: '<?= $model->Listing->Listing->id ?>',
				content_type: 'product',
				value: '<?= $model->Listing->Listing->i_price_from ?>',
				currency: 'ZAR'
			});

		}


    });
</script>
