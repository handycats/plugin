<?php

namespace S4A {

    if (is_admin()) {
        $Options = new Options();
    }

    class Options {

        public function __construct() {
            add_action('admin_menu', array($this, 'admin_menu'));
        }

        function admin_tabs($current = 'cus_auth') {
            $tabs = array(
                "cus_auth" => "Customer Authentication",
                "global_settings" => "Global Settings",
                "engine" => "Engine",
                "locations" => "Locations",
                "oporators_locations" => "Oporators Locations",
                "property_single" => "Property Single",
                "make_reservation" => "Reservations",
                "make_enquiry" => "Enquiries",
                "make_payment" => "Payments",
                "my_account" => "My Account",
                "map_search" => "Map Search",
                "contact_details" => "Contact Details"
            );
            echo "<h2 class='nav-tab-wrapper' >";
            foreach ($tabs as $tab => $name) {
                $class = ( $tab == $current ) ? ' nav-tab-active' : '';
                echo "<a class='nav-tab$class' href='?page=" . S4A_NAME_SHORT . "-options&tab=$tab'>$name</a>";
            }
            echo "</h2>";
        }

        //Admin Menu
        function admin_menu() {
            if (current_user_can("manage_options")) {
                $settings_page = add_options_page(S4A_NAME . " Setting", S4A_NAME . " Setting", 'manage_options', S4A_NAME_SHORT . "-options", array($this, "get_settings_page_html"));
                add_action("load-{$settings_page}", array($this, 'load_options_page'));
            }
        }

        function load_options_page() {
            if ($_POST[S4A_NAME_SHORT . "-options-submit"] == "Y") {
                check_admin_referer(S4A_NAME_SHORT . "-options-page");
                $this->save_settings();
                $url_parameters = isset($_GET['tab']) ? 'updated=true&tab=' . $_GET['tab'] : 'updated=true';
                wp_redirect(admin_url("options-general.php?page=" . S4A_NAME_SHORT . "-options&" . $url_parameters));
                exit;
            }
        }

        function save_settings() {
            global $pagenow;
            if ($pagenow == "options-general.php" && $_GET['page'] == S4A_NAME_SHORT . "-options") {
                if (isset($_GET['tab'])) {
                    $tab = $_GET['tab'];
                } else {
                    $tab = 'cus_auth';
                }
                $update_permalinks = false;
                switch ($tab) {
                    case "cus_auth":
                        if ($_POST['wps4a_username'])
                            Helpers::add_update_option('wps4a_username', sanitize_text_field($_POST['wps4a_username']));
                        if ($_POST['wps4a_pass'])
                            Helpers::add_update_option('wps4a_pass', sanitize_text_field($_POST['wps4a_pass']));
                        if ($_POST['wps4a_booking_service'])
                            Helpers::add_update_option('wps4a_booking_service', sanitize_text_field($_POST['wps4a_booking_service']));
                        if ($_POST['wps4a_image_server'])
                            Helpers::add_update_option('wps4a_image_server', sanitize_text_field($_POST['wps4a_image_server']));
                        if ($_POST['wps4a_url_base'])
                            Helpers::add_update_option('wps4a_url_base', sanitize_text_field($_POST['wps4a_url_base']));
                        if ($_POST['wps4a_url_base_operator'])
                            Helpers::add_update_option('wps4a_url_base_operator', sanitize_text_field($_POST['wps4a_url_base_operator']));
                        break;
                    case "global_settings":
                        if ($_POST['wps4a_global_settings_disable_cache'])
                            Helpers::add_update_option('wps4a_global_settings_disable_cache', sanitize_text_field($_POST['wps4a_global_settings_disable_cache']));
                        if ($_POST['wps4a_global_settings_disable_search_cache'])
                            Helpers::add_update_option('wps4a_global_settings_disable_search_cache', sanitize_text_field($_POST['wps4a_global_settings_disable_search_cache']));
                        if ($_POST['wps4a_global_settings_disable_listing_cache'])
                            Helpers::add_update_option('wps4a_global_settings_disable_listing_cache', sanitize_text_field($_POST['wps4a_global_settings_disable_listing_cache']));
                        if ($_POST['wps4a_global_settings_tc'])
                            Helpers::add_update_option('wps4a_global_settings_tc', sanitize_textarea_field($_POST['wps4a_global_settings_tc']));
                            if ($_POST['wps4a_global_settings_currency'])
                                Helpers::add_update_option('wps4a_global_settings_currency', sanitize_textarea_field($_POST['wps4a_global_settings_currency']));
                                if ($_POST['wps4a_global_settings_color_one'])
                                    Helpers::add_update_option('wps4a_global_settings_color_one', sanitize_textarea_field($_POST['wps4a_global_settings_color_one']));
                                    if ($_POST['wps4a_global_settings_color_two'])
                                        Helpers::add_update_option('wps4a_global_settings_color_two', sanitize_textarea_field($_POST['wps4a_global_settings_color_two']));
                                if ($_POST['wps4a_global_settings_environment'])
                                Helpers::add_update_option('wps4a_global_settings_environment', sanitize_textarea_field($_POST['wps4a_global_settings_environment']));
                                if ($_POST['wps4a_global_settings_template'])
                                    Helpers::add_update_option('wps4a_global_settings_template', sanitize_textarea_field($_POST['wps4a_global_settings_template']));
                        if ($_POST['wps4a_global_settings_font_awesome'])
                            Helpers::add_update_option('wps4a_global_settings_font_awesome', sanitize_text_field($_POST['wps4a_global_settings_font_awesome']));
                        if ($_POST['wps4a_global_settings_bootstrap'])
                            Helpers::add_update_option('wps4a_global_settings_bootstrap', sanitize_text_field($_POST['wps4a_global_settings_bootstrap']));
                        if ($_POST['wps4a_global_settings_availability'])
                            Helpers::add_update_option('wps4a_global_settings_availability', sanitize_text_field($_POST['wps4a_global_settings_availability']));
                            if ($_POST['wps4a_property_disable_wpuser'])
                                Helpers::add_update_option('wps4a_property_disable_wpuser', sanitize_text_field($_POST['wps4a_property_disable_wpuser']));
                        break;
                    case "locations":

                        $locations = Cache::InitialisationDataCache()->Locations;

                        foreach ($locations as $country) {

                            if ($_POST[S4A_AREA_OPTION . "c" . $country->id] != "")
                                Helpers::add_update_option(S4A_AREA_OPTION . "c" . $country->id, sanitize_text_field($_POST[S4A_AREA_OPTION . "c" . $country->id]));

                            foreach ($country->province_area as $province) {

                                if ($_POST[S4A_AREA_OPTION . "p" . $province->id] != "")
                                    Helpers::add_update_option(S4A_AREA_OPTION . "p" . $province->id, sanitize_text_field($_POST[S4A_AREA_OPTION . "p" . $province->id]));

                                foreach ($province->area as $area) {

                                    if ($_POST[S4A_AREA_OPTION . "a" . $area->id] != "")
                                        Helpers::add_update_option(S4A_AREA_OPTION . "a" . $area->id, sanitize_text_field($_POST[S4A_AREA_OPTION . "a" . $area->id]));
                                }
                            }
                        }
                        $listings = Cache::InitialisationDataCache()->ListingName;
                        // print_r($_POST);
                        foreach ($listings as $listing_name) {
                          // print_r($listing_name);
                            if ($_POST[S4A_AREA_OPTION . "l" . $listing_name->id] != ""){

                                Helpers::add_update_option(S4A_AREA_OPTION . "l" . $listing_name->id, sanitize_text_field($_POST[S4A_AREA_OPTION . "l" . $listing_name->id]));
}
                              }

                        if ($_POST['wps4a_location_template'] != "") {
                            Helpers::add_update_option('wps4a_location_template', sanitize_text_field($_POST['wps4a_location_template']));
                        }

                        if ($_POST['wps4a_items_per_page'] != "") {
                            Helpers::add_update_option('wps4a_items_per_page', sanitize_text_field($_POST['wps4a_items_per_page']));
                        }

                        $update_permalinks = true;
                        break;
                        case "oporators_locations":

                        $oporators_locations = OperatorsCache::InitialisationDataCache()->Locations;

                        foreach ($oporators_locations as $country) {

                            if ($_POST[S4A_AREA_OPTION_OPERATOR . "c" . $country->id] != "")
                                Helpers::add_update_option(S4A_AREA_OPTION_OPERATOR . "c" . $country->id, sanitize_text_field($_POST[S4A_AREA_OPTION_OPERATOR . "c" . $country->id]));
                        }

                        if ($_POST['wps4a_location_template'] != "") {
                            Helpers::add_update_option('wps4a_location_template', sanitize_text_field($_POST['wps4a_location_template']));
                        }

                        if ($_POST['wps4a_items_per_page'] != "") {
                            Helpers::add_update_option('wps4a_items_per_page', sanitize_text_field($_POST['wps4a_items_per_page']));
                        }

                        $update_permalinks = true;
                        break;
                    case "property_single":
                        if ($_POST['wps4a_property_single_page'] != "") {
                            Helpers::add_update_option('wps4a_property_single_page', sanitize_text_field($_POST['wps4a_property_single_page']));
                        }
                        if ($_POST['wps4a_property_single_page_template'] != "") {
                            Helpers::add_update_option('wps4a_property_single_page_template', sanitize_text_field($_POST['wps4a_property_single_page_template']));
                        }
                        $update_permalinks = true;
                        break;
                    case "make_reservation":
                        if ($_POST['wps4a_property_reservation_page'] != "") {
                            Helpers::add_update_option('wps4a_property_reservation_page', sanitize_text_field($_POST['wps4a_property_reservation_page']));
                        }
                        if ($_POST['wps4a_reservation_template'] != "") {
                            Helpers::add_update_option('wps4a_reservation_template', sanitize_text_field($_POST['wps4a_reservation_template']));
                        }
                        if ($_POST['wps4a_property_reservation_sent_page'] != "") {
                            Helpers::add_update_option('wps4a_property_reservation_sent_page', sanitize_text_field($_POST['wps4a_property_reservation_sent_page']));
                        }
                        if ($_POST['wps4a_reservation_sent_template'] != "") {
                            Helpers::add_update_option('wps4a_reservation_sent_template', sanitize_text_field($_POST['wps4a_reservation_sent_template']));
                        }
                        if ($_POST['wps4a_reservation_terms_page'] != "") {
                            Helpers::add_update_option('wps4a_reservation_terms_page', sanitize_text_field($_POST['wps4a_reservation_terms_page']));
                        }
                        $update_permalinks = true;
                        break;
                    case "make_payment":
                        if ($_POST['wps4a_property_payment_page'] != "") {
                            Helpers::add_update_option('wps4a_property_payment_page', sanitize_text_field($_POST['wps4a_property_payment_page']));
                        }
                        if ($_POST['wps4a_payment_template'] != "") {
                            Helpers::add_update_option('wps4a_payment_template', sanitize_text_field($_POST['wps4a_payment_template']));
                        }
                        $update_permalinks = true;
                        break;
                    case "make_enquiry":
                        if ($_POST['wps4a_property_enquiry_page'] != "") {
                            Helpers::add_update_option('wps4a_property_enquiry_page', sanitize_text_field($_POST['wps4a_property_enquiry_page']));
                        }
                        if ($_POST['wps4a_enquiry_template'] != "") {
                            Helpers::add_update_option('wps4a_enquiry_template', sanitize_text_field($_POST['wps4a_enquiry_template']));
                        }
                        if ($_POST['wps4a_property_enquiry_sent_page'] != "") {
                            Helpers::add_update_option('wps4a_property_enquiry_sent_page', sanitize_text_field($_POST['wps4a_property_enquiry_sent_page']));
                        }
                        if ($_POST['wps4a_enquiry_sent_template'] != "") {
                            Helpers::add_update_option('wps4a_enquiry_sent_template', sanitize_text_field($_POST['wps4a_enquiry_sent_template']));
                        }
                        $update_permalinks = true;
                        break;
                    case "my_account":
                        if ($_POST['wps4a_property_my_account_enable'])
                            Helpers::add_update_option('wps4a_property_my_account_enable', sanitize_text_field($_POST['wps4a_property_my_account_enable']));
                        if ($_POST['wps4a_property_my_account_page'] != "") {
                            Helpers::add_update_option('wps4a_property_my_account_page', sanitize_text_field($_POST['wps4a_property_my_account_page']));
                        }
                        if ($_POST['wps4a_property_loyalty_enable'])
                            Helpers::add_update_option('wps4a_property_loyalty_enable', sanitize_text_field($_POST['wps4a_property_loyalty_enable']));
                        if ($_POST['wps4a_my_account_template'] != "") {
                            Helpers::add_update_option('wps4a_my_account_template', sanitize_text_field($_POST['wps4a_my_account_template']));
                        }
                        $update_permalinks = true;
                    case "map_search":
                        if ($_POST['wps4a_map_search_page'] != "") {
                            Helpers::add_update_option('wps4a_map_search_page', sanitize_text_field($_POST['wps4a_map_search_page']));
                        }
                        if ($_POST['wps4a_map_search_template'] != "") {
                            Helpers::add_update_option('wps4a_map_search_template', sanitize_text_field($_POST['wps4a_map_search_template']));
                        }
                        if ($_POST['wps4a_map_search_latitude'])
                            Helpers::add_update_option('wps4a_map_search_latitude', sanitize_textarea_field($_POST['wps4a_map_search_latitude']));
                        if ($_POST['wps4a_map_search_longitude'])
                            Helpers::add_update_option('wps4a_map_search_longitude', sanitize_textarea_field($_POST['wps4a_map_search_longitude']));
                        break;
                    case "contact_details":
                        if ($_POST['wps4a_contact_details_page'] != "") {
                            Helpers::add_update_option('wps4a_contact_details_page', sanitize_text_field($_POST['wps4a_contact_details_page']));
                        }
                        if ($_POST['wps4a_signup_page'] != "") {
                            Helpers::add_update_option('wps4a_signup_page', sanitize_text_field($_POST['wps4a_signup_page']));
                        }
                        if ($_POST['wps4a_contact_details_phone'])
                            Helpers::add_update_option('wps4a_contact_details_phone', sanitize_textarea_field($_POST['wps4a_contact_details_phone']));
                        break;
                }
                if ($update_permalinks) {
                    global $wp_rewrite;
                    $wp_rewrite->flush_rules(true);
                }
            }
        }

        function get_settings_page_html() {
            global $pagenow;
            ?>

            <div class="wrap">
                <h2><?php echo S4A_NAME; ?> Options</h2>

                <?php
                /* if (esc_attr($_GET['updated']) == "true") {
                  echo "<div class='updated' ><p>" . S4A_NAME_SHORT . " options updated.</p></div>";
                  } */
                if (isset($_GET['tab'])) {
                    $this->admin_tabs($_GET['tab']);
                } else {
                    $this->admin_tabs('cus_auth');
                }
                ?>

                <div id="poststuff" >
                    <form method="post" action="<?php admin_url('options-general.php?page=' . S4A_NAME_SHORT . '-options'); ?>" >
                        <?php
                        wp_nonce_field(S4A_NAME_SHORT . "-options-page");
                        if ($pagenow == 'options-general.php' && $_GET['page'] == S4A_NAME_SHORT . '-options') {
                            if (isset($_GET['tab'])) {
                                $tab = $_GET['tab'];
                            } else {
                                $tab = "cus_auth";
                            }
                            ?>
                            <table class="form-table" >
                                <?php
                                switch ($tab) {
                                    case "cus_auth":
                                        ?>
                                        <tr valign="top">
                                            <th><label>Username:</label></th>
                                            <td>
                                                <input type="text" name="wps4a_username" value="<?php echo get_option('wps4a_username'); ?>" required />
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Password:</label></th>
                                            <td>
                                                <input type="password" name="wps4a_pass" value="<?php echo get_option('wps4a_pass'); ?>" required />
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Booking Service:</label></th>
                                            <td>
                                                <input type="text" name="wps4a_booking_service" value="<?php echo get_option('wps4a_booking_service'); ?>" />
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Image Server:</label></th>
                                            <td>
                                                <input type="text" name="wps4a_image_server" value="<?php echo get_option('wps4a_image_server'); ?>" />
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>URL Base:</label></th>
                                            <td>
                                                <input type="text" name="wps4a_url_base" value="<?php echo get_option('wps4a_url_base'); ?>" />
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>URL Base Operators:</label></th>
                                            <td>
                                                <input type="text" name="wps4a_url_base_operator" value="<?php echo get_option('wps4a_url_base_operator'); ?>" />
                                            </td>
                                        </tr>
                                        <?php
                                        break;
                                    case "global_settings":
                                        ?>
                                        <tr valign="top">
                                            <th><label>Terms and Conditions:</label></th>
                                            <td>
                                                <textarea name="wps4a_global_settings_tc"><?php echo get_option('wps4a_global_settings_tc'); ?></textarea>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Currency:</label></th>
                                            <td>
                                                <input type="text" value="<?php echo get_option('wps4a_global_settings_currency'); ?>" name="wps4a_global_settings_currency"/>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Primary Color:</label></th>
                                            <td>
                                                <input type="text" value="<?php echo get_option('wps4a_global_settings_color_one'); ?>" name="wps4a_global_settings_color_one"/>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Secondary Color:</label></th>
                                            <td>
                                                <input type="text" value="<?php echo get_option('wps4a_global_settings_color_two'); ?>" name="wps4a_global_settings_color_two"/>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Environment:</label></th>
                                            <td>
                                                <input type="text" value="<?php echo get_option('wps4a_global_settings_environment'); ?>" name="wps4a_global_settings_environment"/>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Template:</label></th>

                                            <td>
                                                <input type="text" value="<?php echo get_option('wps4a_global_settings_template'); ?>" name="wps4a_global_settings_template"/><span><p>"default" or "wide"</p></span>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Include Font Awesome:</label></th>
                                            <td>
                                                Yes: <input <?= (get_option('wps4a_global_settings_font_awesome') == "true") ? "checked" : null ?> type="radio" name="wps4a_global_settings_font_awesome" value="true" />
                                                No: <input <?= (get_option('wps4a_global_settings_font_awesome') == "false" || get_option('wps4a_global_settings_font_awesome') == null) ? "checked" : null ?> type="radio" name="wps4a_global_settings_font_awesome" value="false" />
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Include Bootstrap:</label></th>
                                            <td>
                                                Yes: <input <?= (get_option('wps4a_global_settings_bootstrap') == "true") ? "checked" : null ?> type="radio" name="wps4a_global_settings_bootstrap" value="true" />
                                                No: <input <?= (get_option('wps4a_global_settings_bootstrap') == "false" || get_option('wps4a_global_settings_bootstrap') == null) ? "checked" : null ?> type="radio" name="wps4a_global_settings_bootstrap" value="false" />
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Disable Availability Search:</label></th>
                                            <td>
                                                Yes: <input <?= (get_option('wps4a_global_settings_availability') == "true") ? "checked" : null ?> type="radio" name="wps4a_global_settings_availability" value="true" />
                                                No: <input <?= (get_option('wps4a_global_settings_availability') == "false" || get_option('wps4a_global_settings_availability') == null) ? "checked" : null ?> type="radio" name="wps4a_global_settings_availability" value="false" />
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Disable Cache:</label></th>
                                            <td>
                                                Yes: <input <?= (get_option('wps4a_global_settings_disable_cache') == "true") ? "checked" : null ?> type="radio" name="wps4a_global_settings_disable_cache" value="true" />
                                                No: <input <?= (get_option('wps4a_global_settings_disable_cache') == "false" || get_option('wps4a_global_settings_disable_cache') == null) ? "checked" : null ?> type="radio" name="wps4a_global_settings_disable_cache" value="false" />
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Disable Search Cache:</label></th>
                                            <td>
                                                Yes: <input <?= (get_option('wps4a_global_settings_disable_search_cache') == "true") ? "checked" : null ?> type="radio" name="wps4a_global_settings_disable_search_cache" value="true" />
                                                No: <input <?= (get_option('wps4a_global_settings_disable_search_cache') == "false" || get_option('wps4a_global_settings_disable_search_cache') == null) ? "checked" : null ?> type="radio" name="wps4a_global_settings_disable_search_cache" value="false" />
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Disable Listing Cache:</label></th>
                                            <td>
                                                Yes: <input <?= (get_option('wps4a_global_settings_disable_listing_cache') == "true") ? "checked" : null ?> type="radio" name="wps4a_global_settings_disable_listing_cache" value="true" />
                                                No: <input <?= (get_option('wps4a_global_settings_disable_listing_cache') == "false" || get_option('wps4a_global_settings_disable_listing_cache') == null) ? "checked" : null ?> type="radio" name="wps4a_global_settings_disable_listing_cache" value="false" />
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Disable WP User Creation:</label></th>
                                            <td>
                                                Yes: <input <?= (get_option('wps4a_property_disable_wpuser') == "true") ? "checked" : null ?> type="radio" name="wps4a_property_disable_wpuser" value="true" />
                                                No: <input <?= (get_option('wps4a_property_disable_wpuser') == "false" || get_option('wps4a_property_disable_wpuser') == null) ? "checked" : null ?> type="radio" name="wps4a_property_disable_wpuser" value="false" />
                                            </td>
                                        </tr>
                                        <?php
                                        break;
                                        case "engine":
                                            ?>
                                            <tr valign="top">
                                                <td>
                                                    <iframe src="<?php echo str_replace('/ws', '', get_option('wps4a_booking_service')); ?>?v_email=<?php echo get_option('wps4a_username'); ?>&password=<?php echo urlencode(get_option('wps4a_pass')); ?>" width="100%" height="1000px"></iframe>
                                                </td>
                                            </tr>
                                            <?php
                                            break;
                                    case "locations":
                                        ?>
                                        <tr valign="top">
                                            <th><label>Locations:</label></th>
                                            <td>
                                                <?php
                                                $locations = Cache::InitialisationDataCache()->Locations;

                                                foreach ($locations as $country) {

                                                    echo $country->v_name;

                                                    $countries_args = array(
                                                        'show_option_no_change' => S4A_NONE_KEY,
                                                        'echo' => 1,
                                                        'name' => S4A_AREA_OPTION . "c" . $country->id,
                                                        'id' => S4A_AREA_OPTION . "c" . $country->id,
                                                        'selected' => get_option(S4A_AREA_OPTION . "c" . $country->id, '')
                                                    );
                                                    wp_dropdown_pages($countries_args);
                                                    echo "<br />";

                                                    foreach ($country->province_area as $province) {

                                                        echo $country->v_name . " > " . $province->v_name;

                                                        $province_args = array(
                                                            'show_option_no_change' => S4A_NONE_KEY,
                                                            'echo' => 1,
                                                            'name' => S4A_AREA_OPTION . "p" . $province->id,
                                                            'id' => S4A_AREA_OPTION . "p" . $province->id,
                                                            'selected' => get_option(S4A_AREA_OPTION . "p" . $province->id, '')
                                                        );
                                                        wp_dropdown_pages($province_args);
                                                        echo "<br />";

                                                        foreach ($province->area as $area) {

                                                            echo $country->v_name . " > " . $province->v_name . " > " . $area->v_name;

                                                            $area_args = array(
                                                                'show_option_no_change' => S4A_NONE_KEY,
                                                                'echo' => 1,
                                                                'name' => S4A_AREA_OPTION . "a" . $area->id,
                                                                'id' => S4A_AREA_OPTION . "a" . $area->id,
                                                                'selected' => get_option(S4A_AREA_OPTION . "a" . $area->id, '')
                                                            );
                                                            wp_dropdown_pages($area_args);
                                                            echo "<br />";
                                                        }
                                                    }
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Listing Mapping:</label></th>
                                            <td>
                                                <?php

                                                $SearchCriteria = new Structs\SearchCriteria(null);
                                                $SearchCriteria->perPage = "1000";
                                                $listing = Cache::get_ListingBySearchCache($SearchCriteria);

                                                foreach ($listing->ListingSummeries as $name) {

                                                    echo $name->v_title;

                                                    $countries_args = array(
                                                        'show_option_no_change' => S4A_NONE_KEY,
                                                        'echo' => 1,
                                                        'name' => S4A_AREA_OPTION . "l" . $name->id,
                                                        'id' => S4A_AREA_OPTION . "l" . $name->id,
                                                        'selected' => get_option(S4A_AREA_OPTION . "l" . $name->id, '')
                                                    );
                                                    wp_dropdown_pages($countries_args);
                                                    echo "<br />";
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Locations page template:</label></th>
                                            <td>
                                                <select name="wps4a_location_template">
                                                    <option> - none - </option>
                                                    <?php
                                                    $templates = get_page_templates();

                                                    foreach ($templates as $template_name => $template_filename) {
                                                        $selected = ($template_filename == get_option('wps4a_location_template')) ? 'selected="selected"' : null;
                                                        echo "<option value='" . $template_filename . "' " . $selected . ">" . $template_name . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Items per page:</label></th>
                                            <td>
                                                <?php
                                                $wps4a_items_per_page = get_option('wps4a_items_per_page') ? get_option('wps4a_items_per_page') : 5;
                                                ?>
                                                <input type="number" name="wps4a_items_per_page" value="<?php echo $wps4a_items_per_page; ?>" min="1" max="99" required style="width: 50px;" />
                                            </td>
                                        </tr>
                                        <?php
                                        break;
                                        case "oporators_locations":
                                        ?>
                                        <tr valign="top">
                                            <th><label>Oporators Locations:</label></th>
                                            <td>
                                                <?php
                                                $oporators_locations = OperatorsCache::InitialisationDataCache()->Locations;

                                                foreach ($oporators_locations as $country) {

                                                    echo $country->v_name;

                                                    $countries_args = array(
                                                        'show_option_no_change' => S4A_NONE_KEY,
                                                        'echo' => 1,
                                                        'name' => S4A_AREA_OPTION_OPERATOR . "c" . $country->id,
                                                        'id' => S4A_AREA_OPTION_OPERATOR . "c" . $country->id,
                                                        'selected' => get_option(S4A_AREA_OPTION_OPERATOR . "c" . $country->id, '')
                                                    );
                                                    wp_dropdown_pages($countries_args);
                                                    echo "<br />";
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Oporators Locations page template:</label></th>
                                            <td>
                                                <select name="wps4a_location_template">
                                                    <option> - none - </option>
                                                    <?php
                                                    $templates = get_page_templates();

                                                    foreach ($templates as $template_name => $template_filename) {
                                                        $selected = ($template_filename == get_option('wps4a_location_template')) ? 'selected="selected"' : null;
                                                        echo "<option value='" . $template_filename . "' " . $selected . ">" . $template_name . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Items per page:</label></th>
                                            <td>
                                                <?php
                                                $wps4a_items_per_page = get_option('wps4a_items_per_page') ? get_option('wps4a_items_per_page') : 5;
                                                ?>
                                                <input type="number" name="wps4a_items_per_page" value="<?php echo $wps4a_items_per_page; ?>" min="1" max="99" required style="width: 50px;" />
                                            </td>
                                        </tr>
                                        <?php
                                        break;
                                    case "property_single":
                                        ?>
                                        <tr valign="top">
                                            <th><label>Single property page:</label></th>
                                            <td>
                                                <?php
                                                $the_single_property_pages_ddl_args = array(
                                                    'show_option_no_change' => S4A_NONE_KEY,
                                                    'echo' => 1,
                                                    'name' => "wps4a_property_single_page",
                                                    'id' => "ddlwps4a_property_single_page",
                                                    'selected' => get_option('wps4a_property_single_page', '')
                                                );
                                                wp_dropdown_pages($the_single_property_pages_ddl_args);
                                                ?>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Single property page template:</label></th>
                                            <td>
                                                <select name="wps4a_property_single_page_template">
                                                    <option> - none - </option>
                                                    <?php
                                                    $templates = get_page_templates();

                                                    foreach ($templates as $template_name => $template_filename) {
                                                        $selected = ($template_filename == get_option('wps4a_property_single_page_template')) ? 'selected="selected"' : null;
                                                        echo "<option value='" . $template_filename . "' " . $selected . ">" . $template_name . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <?php
                                        break;
                                    case "make_reservation":
                                        ?>
                                        <tr valign="top">
                                            <th><label>Reservation page:</label></th>
                                            <td>
                                                <?php
                                                $the_make_reservation_pages_ddl_args = array(
                                                    'show_option_no_change' => S4A_NONE_KEY,
                                                    'echo' => 1,
                                                    'name' => "wps4a_property_reservation_page",
                                                    'id' => "ddlwps4a_property_reservation_page",
                                                    'selected' => get_option('wps4a_property_reservation_page', '')
                                                );
                                                wp_dropdown_pages($the_make_reservation_pages_ddl_args);
                                                ?>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Reservation page template:</label></th>
                                            <td>
                                                <select name="wps4a_reservation_template">
                                                    <option> - none - </option>
                                                    <?php
                                                    $templates = get_page_templates();

                                                    foreach ($templates as $template_name => $template_filename) {
                                                        $selected = ($template_filename == get_option('wps4a_reservation_template')) ? 'selected="selected"' : null;
                                                        echo "<option value='" . $template_filename . "' " . $selected . ">" . $template_name . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Reservation sent page:</label></th>
                                            <td>
                                                <?php
                                                $the_make_reservation_sent_pages_ddl_args = array(
                                                    'show_option_no_change' => S4A_NONE_KEY,
                                                    'echo' => 1,
                                                    'name' => "wps4a_property_reservation_sent_page",
                                                    'id' => "ddlwps4a_property_reservation_sent_page",
                                                    'selected' => get_option('wps4a_property_reservation_sent_page', '')
                                                );
                                                wp_dropdown_pages($the_make_reservation_sent_pages_ddl_args);
                                                ?>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Reservation sent page template:</label></th>
                                            <td>
                                                <select name="wps4a_reservation_sent_template">
                                                    <option> - none - </option>
                                                    <?php
                                                    $templates = get_page_templates();

                                                    foreach ($templates as $template_name => $template_filename) {
                                                        $selected = ($template_filename == get_option('wps4a_reservation_sent_template')) ? 'selected="selected"' : null;
                                                        echo "<option value='" . $template_filename . "' " . $selected . ">" . $template_name . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Terms and Conditions Page:</label></th>
                                            <td>
                                                <?php
                                                $the_wps4a_reservation_terms_pages_ddl_args = array(
                                                    'show_option_no_change' => S4A_NONE_KEY,
                                                    'echo' => 1,
                                                    'name' => "wps4a_reservation_terms_page",
                                                    'id' => "ddlwps4a_reservation_terms_page",
                                                    'selected' => get_option('wps4a_reservation_terms_page', '')
                                                );
                                                wp_dropdown_pages($the_wps4a_reservation_terms_pages_ddl_args);
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                        break;
                                    case "make_payment":
                                        ?>
                                        <tr valign="top">
                                            <th><label>Payment page:</label></th>
                                            <td>
                                                <?php
                                                $the_make_payment_pages_ddl_args = array(
                                                    'show_option_no_change' => S4A_NONE_KEY,
                                                    'echo' => 1,
                                                    'name' => "wps4a_property_payment_page",
                                                    'id' => "ddlwps4a_property_payment_page",
                                                    'selected' => get_option('wps4a_property_payment_page', '')
                                                );
                                                wp_dropdown_pages($the_make_payment_pages_ddl_args);
                                                ?>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Payment page template:</label></th>
                                            <td>
                                                <select name="wps4a_payment_template">
                                                    <option> - none - </option>
                                                    <?php
                                                    $templates = get_page_templates();

                                                    foreach ($templates as $template_name => $template_filename) {
                                                        $selected = ($template_filename == get_option('wps4a_payment_template')) ? 'selected="selected"' : null;
                                                        echo "<option value='" . $template_filename . "' " . $selected . ">" . $template_name . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <?php
                                        break;
                                    case "make_enquiry":
                                        ?>
                                        <tr valign="top">
                                            <th><label>Enquiry page:</label></th>
                                            <td>
                                                <?php
                                                $the_make_enquiry_pages_ddl_args = array(
                                                    'show_option_no_change' => S4A_NONE_KEY,
                                                    'echo' => 1,
                                                    'name' => "wps4a_property_enquiry_page",
                                                    'id' => "ddlwps4a_property_enquiry_page",
                                                    'selected' => get_option('wps4a_property_enquiry_page', '')
                                                );
                                                wp_dropdown_pages($the_make_enquiry_pages_ddl_args);
                                                ?>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Enquiry page template:</label></th>
                                            <td>
                                                <select name="wps4a_enquiry_template">
                                                    <option> - none - </option>
                                                    <?php
                                                    $templates = get_page_templates();

                                                    foreach ($templates as $template_name => $template_filename) {
                                                        $selected = ($template_filename == get_option('wps4a_enquiry_template')) ? 'selected="selected"' : null;
                                                        echo "<option value='" . $template_filename . "' " . $selected . ">" . $template_name . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Enquiry sent page:</label></th>
                                            <td>
                                                <?php
                                                $the_make_enquiry_sent_pages_ddl_args = array(
                                                    'show_option_no_change' => S4A_NONE_KEY,
                                                    'echo' => 1,
                                                    'name' => "wps4a_property_enquiry_sent_page",
                                                    'id' => "ddlwps4a_property_enquiry_sent_page",
                                                    'selected' => get_option('wps4a_property_enquiry_sent_page', '')
                                                );
                                                wp_dropdown_pages($the_make_enquiry_sent_pages_ddl_args);
                                                ?>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Enquiry sent page template:</label></th>
                                            <td>
                                                <select name="wps4a_enquiry_sent_template">
                                                    <option> - none - </option>
                                                    <?php
                                                    $templates = get_page_templates();

                                                    foreach ($templates as $template_name => $template_filename) {
                                                        $selected = ($template_filename == get_option('wps4a_enquiry_sent_template')) ? 'selected="selected"' : null;
                                                        echo "<option value='" . $template_filename . "' " . $selected . ">" . $template_name . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <?php
                                        break;
                                    case "my_account":
                                        ?>
                                        <tr valign="top">
                                            <th><label>Enable my account page:</label></th>
                                            <td>
                                                Yes: <input <?= (get_option('wps4a_property_my_account_enable') == "true") ? "checked" : null ?> type="radio" name="wps4a_property_my_account_enable" value="true" />
                                                No: <input <?= (get_option('wps4a_property_my_account_enable') == "false" || get_option('wps4a_property_my_account_enable') == null) ? "checked" : null ?> type="radio" name="wps4a_property_my_account_enable" value="false" />
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>My account page:</label></th>
                                            <td>
                                                <?php
                                                $the_my_account_pages_ddl_args = array(
                                                    'show_option_no_change' => S4A_NONE_KEY,
                                                    'echo' => 1,
                                                    'name' => "wps4a_property_my_account_page",
                                                    'id' => "ddlwps4a_property_my_account_page",
                                                    'selected' => get_option('wps4a_property_my_account_page', '')
                                                );
                                                wp_dropdown_pages($the_my_account_pages_ddl_args);
                                                ?>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>My account page template:</label></th>
                                            <td>
                                                <select name="wps4a_my_account_template">
                                                    <option> - none - </option>
                                                    <?php
                                                    $templates = get_page_templates();

                                                    foreach ($templates as $template_name => $template_filename) {
                                                        $selected = ($template_filename == get_option('wps4a_my_account_template')) ? 'selected="selected"' : null;
                                                        echo "<option value='" . $template_filename . "' " . $selected . ">" . $template_name . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Enable Loyalty System:</label></th>
                                            <td>
                                                Yes: <input <?= (get_option('wps4a_property_loyalty_enable') == "true") ? "checked" : null ?> type="radio" name="wps4a_property_loyalty_enable" value="true" />
                                                No: <input <?= (get_option('wps4a_property_loyalty_enable') == "false" || get_option('wps4a_property_loyalty_enable') == null) ? "checked" : null ?> type="radio" name="wps4a_property_loyalty_enable" value="false" />
                                            </td>
                                        </tr>
                                        <?php
                                        break;
                                    case "map_search":
                                        ?>
                                        <tr valign="top">
                                            <th><label>Map search page:</label></th>
                                            <td>
                                                <?php
                                                $the_map_search_pages_ddl_args = array(
                                                    'show_option_no_change' => S4A_NONE_KEY,
                                                    'echo' => 1,
                                                    'name' => "wps4a_map_search_page",
                                                    'id' => "ddlwps4a_map_search_page",
                                                    'selected' => get_option('wps4a_map_search_page', '')
                                                );
                                                wp_dropdown_pages($the_map_search_pages_ddl_args);
                                                ?>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Map search page template:</label></th>
                                            <td>
                                                <select name="wps4a_map_search_template">
                                                    <option> - none - </option>
                                                    <?php
                                                    $templates = get_page_templates();

                                                    foreach ($templates as $template_name => $template_filename) {
                                                        $selected = ($template_filename == get_option('wps4a_map_search_template')) ? 'selected="selected"' : null;
                                                        echo "<option value='" . $template_filename . "' " . $selected . ">" . $template_name . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Map Search:</label></th>
                                            <td>
                                                Latitude: <input type="text" name="wps4a_map_search_latitude" value="<?php echo get_option('wps4a_map_search_latitude'); ?>" />
                                                Longitude: <input type="text" name="wps4a_map_search_longitude" value="<?php echo get_option('wps4a_map_search_longitude'); ?>" />
                                            </td>
                                        </tr>
                                        <?php
                                        break;
                                    case "contact_details":
                                        ?>
                                        <tr valign="top">
                                            <th><label>Contact details page:</label></th>
                                            <td>
                                                <?php
                                                $the_contact_details_pages_ddl_args = array(
                                                    'show_option_no_change' => S4A_NONE_KEY,
                                                    'echo' => 1,
                                                    'name' => "wps4a_contact_details_page",
                                                    'id' => "ddlwps4a_contact_details_page",
                                                    'selected' => get_option('wps4a_contact_details_page', '')
                                                );
                                                wp_dropdown_pages($the_contact_details_pages_ddl_args);
                                                ?>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Signup page:</label></th>
                                            <td>
                                                <?php
                                                $the_signup_pages_ddl_args = array(
                                                    'show_option_no_change' => S4A_NONE_KEY,
                                                    'echo' => 1,
                                                    'name' => "wps4a_signup_page",
                                                    'id' => "ddlwps4a_signup_page",
                                                    'selected' => get_option('wps4a_signup_page', '')
                                                );
                                                wp_dropdown_pages($the_signup_pages_ddl_args);
                                                ?>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th><label>Phone:</label></th>
                                            <td>
                                                <input type="text" name="wps4a_contact_details_phone" value="<?php echo get_option('wps4a_contact_details_phone'); ?>" />
                                            </td>
                                        </tr>
                                        <?php
                                        break;
                                }
                                ?>
                            </table>
                            <?php
                        }
                        ?>
                        <p class="submit" style="clear: both;">
                            <input id="btn_submit_plugin_options" type="submit" name="Submit" class="button-primary" value="Update Options" />
                            <input type="hidden" name="<?php echo S4A_NAME_SHORT; ?>-options-submit" value="Y" />
                        </p>
                        <div class="div-clear" ></div>
                        <div id="div-msg-plugin-options" ></div>
                    </form>
                </div>
            </div>
            <?php
            echo $this->get_default_options_content(S4A_NAME_SHORT . "-options");
        }

        function get_default_options_content($display) {
            $html_str = "<input type='hidden' class='hval-" . S4A_NAME_SHORT . "-options-js-lib' value='" . $display . "' />";
            return $html_str;
        }

    }

}
