<?php

namespace S4A {

    class Meta {

        const FIRSTNAMEKEY = "s4a_user_first_name";
        const LASTNAMEKEY = "s4a_user_last_name";
        const PHONEKEY = "s4a_user_phone";
        const NATIONALITYKEY = "s4a_user_nationality";
        const TITLEKEY = "s4a_user_title";
        const RESIDENTKEY = "s4a_user_resident";
        const PENSIONKEY = "s4a_user_pension";
        const FAVKEY = "s4a_user_favs";
        const FLEXIBLEKEY = "s4a_user_flexible";
        const ADULTSKEY = "s4a_user_adults";
        const CHILDRENKEY = "s4a_user_children";
        const CHILDRENAGEKEY = "s4a_user_children_age";
        const SPECIALKEY = "s4a_user_special";
        const INVOICENAME = "s4a_invoice_name";
        const INVOICETAXNUMBER = "s4a_invoice_tax_number";
        const INVOICEADDRESS = "s4a_invoice_address";
        const LOYALTYPOINTS = "s4a_loyalty_points";
        const LOYALTYLEVEL = "s4a_loyalty_level";
        const LOYALTYCARD = "s4a_loyalty_number";
        const AGENTCODE = "AgentCode";
        const AGENTTYPE = "AgentType";

        public static function UpdateProfile($uid, $post) {
            if (($user_name = Helpers::GetClean($post, 'user_name')))
                update_user_meta($uid, Meta::FIRSTNAMEKEY, $user_name);
            if (($user_last_name = Helpers::GetClean($post, 'user_last_name')))
                update_user_meta($uid, Meta::LASTNAMEKEY, $user_last_name);
            if (($user_phone = Helpers::GetClean($post, 'user_phone')))
                update_user_meta($uid, Meta::PHONEKEY, $user_phone);
            if (($user_title = Helpers::GetClean($post, 'user_title')))
                update_user_meta($uid, Meta::TITLEKEY, $user_title);
            if (($user_nationality = Helpers::GetClean($post, 'user_nationality')))
                update_user_meta($uid, Meta::NATIONALITYKEY, $user_nationality);
            if (($user_resident = Helpers::GetClean($post, 'user_resident')))
                update_user_meta($uid, Meta::RESIDENTKEY, $user_resident);
            if (($user_pension = Helpers::GetClean($post, 'user_pension')))
                update_user_meta($uid, Meta::PENSIONKEY, $user_pension);
            if (($user_flexible = Helpers::GetClean($post, 'user_flexible')))
                update_user_meta($uid, Meta::FLEXIBLEKEY, $user_flexible);
            if (($user_adults = Helpers::GetClean($post, 'user_adults')))
                update_user_meta($uid, Meta::ADULTSKEY, $user_adults);
            if (($user_children = Helpers::GetClean($post, 'user_children')))
                update_user_meta($uid, Meta::CHILDRENKEY, $user_children);
            if (($user_children_age = Helpers::GetClean($post, 'user_children_age')))
                update_user_meta($uid, Meta::CHILDRENAGEKEY, $user_children_age);
            if (($user_special = Helpers::GetClean($post, 'user_special')))
                update_user_meta($uid, Meta::SPECIALKEY, $user_special);
            if (($invoice_name = Helpers::GetClean($post, 'invoice_name')))
                update_user_meta($uid, Meta::INVOICENAME, $invoice_name);
            if (($invoice_tax_number = Helpers::GetClean($post, 'invoice_tax_number')))
                update_user_meta($uid, Meta::INVOICETAXNUMBER, $invoice_tax_number);
            if (($invoice_address = Helpers::GetClean($post, 'invoice_address')))
                update_user_meta($uid, Meta::INVOICEADDRESS, $invoice_address);
            if (($user_points = Helpers::GetClean($post, 'user_points')))
                update_user_meta($uid, Meta::LOYALTYPOINTS, $user_points);
            if (($user_level = Helpers::GetClean($post, 'user_level')))
                update_user_meta($uid, Meta::LOYALTYLEVEL, $user_level);
            if (($loyalty_number = Helpers::GetClean($post, 'loyalty_number')))
                update_user_meta($uid, Meta::LOYALTYCARD, $loyalty_number);
            if (($AgentCode = Helpers::GetClean($post, 'AgentCode')))
                update_user_meta($uid, Meta::AGENTCODE, $AgentCode);
            if (($AgentType = Helpers::GetClean($post, 'AgentType')))
                update_user_meta($uid, Meta::AGENTTYPE, $AgentType);
        }

        public static function GetProfile($uid) {

            $profile = new \stdClass();

            $user_info = get_userdata($uid);

            $profile->user_email = $user_info->user_email;
            $profile->ID  = $user_info->ID;
            $profile->user_name = get_user_meta($uid, Meta::FIRSTNAMEKEY, true);
            $profile->user_last_name = get_user_meta($uid, Meta::LASTNAMEKEY, true);
            $profile->user_phone = get_user_meta($uid, Meta::PHONEKEY, true);
            $profile->user_title = get_user_meta($uid, Meta::TITLEKEY, true);
            $profile->user_nationality = get_user_meta($uid, Meta::NATIONALITYKEY, true);
            $profile->user_resident = get_user_meta($uid, Meta::RESIDENTKEY, true);
            $profile->user_pension = get_user_meta($uid, Meta::PENSIONKEY, true);
            $profile->user_flexible = get_user_meta($uid, Meta::FLEXIBLEKEY, true);
            $profile->user_adults = get_user_meta($uid, Meta::ADULTSKEY, true);
            $profile->user_children = get_user_meta($uid, Meta::CHILDRENKEY, true);
            $profile->user_children_age = get_user_meta($uid, Meta::CHILDRENAGEKEY, true);
            $profile->user_special = get_user_meta($uid, Meta::SPECIALKEY, true);
            $profile->invoice_name = get_user_meta($uid, Meta::INVOICENAME, true);
            $profile->invoice_tax_number = get_user_meta($uid, Meta::INVOICETAXNUMBER, true);
            $profile->invoice_address = get_user_meta($uid, Meta::INVOICEADDRESS, true);
            $profile->user_points = get_user_meta($uid, Meta::LOYALTYPOINTS, true);
            $profile->user_level = get_user_meta($uid, Meta::LOYALTYLEVEL, true);
            $profile->loyalty_number = get_user_meta($uid, Meta::LOYALTYCARD, true);
            $profile->AgentCode = get_user_meta($uid, Meta::AGENTCODE, true);
            $profile->AgentType = get_user_meta($uid, Meta::AGENTTYPE, true);

            return $profile;
        }

        public static function CheckIfFav($uid, $id) {
            $fav = Meta::GetFav($uid);
            return ($fav && in_array($id, $fav));
        }

        public static function GetFav($uid) {
            return get_user_meta($uid, Meta::FAVKEY, true);
        }

        public static function AddFav($uid, $id) {

            $fav = Meta::GetFav($uid);

            if ($fav) {
                if (in_array($id, $fav)) {
                    unset($fav[$id]);
                    update_user_meta($uid, Meta::FAVKEY, $fav);
                    return "remove";
                } else {
                    $fav[$id] = $id;
                    update_user_meta($uid, Meta::FAVKEY, $fav);
                    return "add";
                }
            } else {
                $fav = array();
                $fav[$id] = $id;
                update_user_meta($uid, Meta::FAVKEY, $fav);
                return "add";
            }
        }

    }

}
