<?php

namespace S4A {

    class Ajax {

        public function ajax_submit() {

            $return['error'] = true;

            if (!$cmd = Helpers::GetClean($_POST, 'cmd')) {
                $cmd = Helpers::GetClean($_GET, 'cmd');
            }
            switch ($cmd){
              case 'qty-selection':
                 $qty_child_adult= Helpers::GetClean($_POST, 'qty_child_adult');
                 $price = Helpers::GetClean($_POST, 'price');
                 $children = Helpers::GetClean($_POST, 'children');
                 $adults = Helpers::GetClean($_POST, 'adults');
                 $qty = Helpers::GetClean($_POST, 'qty');
                 $currency_code = Helpers::GetClean($_PST,'currency_code');
                 $new_price = 0;

                 if($qty_child_adult == "quantity")
                 {
                  $new_price = intval($price) * $qty;
                 }
                 else if($qty_child_adult=="adults")
                 {
                   if($children > 0){
                     $qty = $qty + intval($children);
                     $new_price = intval($price) * $qty;
                   }else{
                    $new_price = intval($price) * $qty;
                   }
                 }
                 else if($qty_child_adult=="children"){
                   if($adults > 0){
                     $qty = $qty + intval($adults);
                     $new_price = intval($price) * $qty;
                   }else{
                     $new_price = intval($price) * $qty;
                   }
                 }

                 $new_price = \S4A\Helpers::DoPrice($currency_code,$new_price);

                 $return['html_str'] = $new_price;
                 $return['error'] = false;
               break;
              case 'remove-booking-services':
                    $code = Helpers::GetClean($_POST, 'code');
                    $currency_code = Helpers::GetClean($_PST,'currency_code');
                    $st_session_data_arr = Session::get_user_booking_services_data();
                    $session_booking_total = Session::get_user_booking_total_data();
                    $service_total_price = 0;
                    $final_booking_total = 0;
                    $price_remove = 0;

                    if($st_session_data_arr){
                      foreach($st_session_data_arr as $key => $value)
                      {
                        if($code == $value["code"]){
                         $price_remove = $value["price"];
                         Session::remove_user_service_data($key);
                         break;
                        }
                      }
                      $final_st_data = Session::get_user_booking_services_data();

                      foreach($final_st_data as $key => $value)
                      {
                         $service_total_price += floatval($value["price"]);
                      }

                    $final_booking_total = floatval($session_booking_total) - floatval($price_remove);
                    Session::user_booking_total_data($final_booking_total);

                    $service_total_price = \S4A\Helpers::DoPrice($currency_code,$service_total_price);
                    $final_booking_total = \S4A\Helpers::DoPrice($currency_code,$final_booking_total);

                    $return['html_str'] = $service_total_price."|".$final_booking_total;
                    $return['error'] = false;
                   }
                   else
                   {
                     $return['html_str'] = 0;
                     $return['error'] = false;
                   }
                   break;
                case 'add-booking-services':
                   $st_new_data_arr = $_POST['services_data'];
                   $booking_total = Helpers::GetClean($_POST, 'booking_total');
                   $currency_code = Helpers::GetClean($_PST,'currency_code');
                   $st_session_data_arr = Session::get_user_booking_services_data();
                   $service_total_price = 0;
                   $final_booking_total = 0;

                   if($st_session_data_arr)
                   {
                      foreach($st_new_data_arr as $key1 => $value1)
                      {
                        $foo = true;
                        foreach($st_session_data_arr as $key => $value)
                        {
                           if($st_new_data_arr[$key1]["code"] == $value["code"])
                           {
                              if( ($st_new_data_arr[$key1]["price"] != $value["price"]) || ($st_new_data_arr[$key1]["date"] != $value["date"]) ){
                                Session::remove_user_service_data($value["code"]);
                              }else{
                                $foo = false;
                              }
                           }
                        }
                        if($foo)
                        {
                          Session::user_booking_services_data($st_new_data_arr[$key1]["code"],$st_new_data_arr, $key1);
                        }
                      }
                   }
                   else
                   {
                     foreach($st_new_data_arr as $key => $value)
                     {
                        Session::user_booking_services_data($st_new_data_arr[$key]["code"],$st_new_data_arr,$key);
                     }
                   }
                   $final_st_data = Session::get_user_booking_services_data();

                   foreach($final_st_data as $key => $value)
                   {
                      $service_total_price += floatval($value["price"]);
                   }

                   $booking_total = str_replace(',','',$booking_total);
                   $final_booking_total = floatval($booking_total) + $service_total_price;

                   Session::user_booking_total_data($final_booking_total);

                   $service_total_price = \S4A\Helpers::DoPrice($currency_code,$service_total_price);
                   $final_booking_total = \S4A\Helpers::DoPrice($currency_code,$final_booking_total);

                   $return['html_str'] = $service_total_price."|".$final_booking_total;
                   $return['error'] = false;
                   break;
                case 'add-fav':
                    $current_user = wp_get_current_user();
                    $id = Helpers::GetClean($_POST, 'id');

                    $result = Meta::AddFav($current_user->ID, $id);

                    $return['html_str'] = $result;
                    $return['error'] = false;
                    break;
                case 'user-curr':
                  $value = Helpers::GetClean($_POST, 'val');
                  $symbol = Helpers::GetClean($_POST, 'sym');
                  $code = Helpers::GetClean($_POST, 'cde');

                  $data = array(
                    'value' => $value,
                    'symbol' => $symbol,
                    'code' => $code
                  );

                  $result = Session::currency_data($data);

                  $return['html_str'] = $result;
                  $return['error'] = false;
                  break;
                case 'login':
                    $email = Helpers::GetClean($_POST, 'email');
                    $password = Helpers::GetClean($_POST, 'password');

                    $creds = array(
                        'user_login' => $email,
                        'user_password' => $password,
                        'remember' => true
                    );

                    $user = wp_signon($creds, false);

                    if (is_wp_error($user)) {
                        $return['html_str'] = $user->get_error_message();
                        $return['error'] = true;
                    } else {
                        $return['html_str'] = "";
                        $return['error'] = false;
                    }
                    break;
                case 'register':
                    $email = Helpers::GetClean($_POST, 'email');
                    $password = Helpers::GetClean($_POST, 'password');

                    if (username_exists($email) || email_exists($email)) {
                        $return['html_str'] = "This user already exists.";
                        $return['error'] = true;
                    } else {

                        $user_id = wp_create_user($email, $password, $email);
                        if (!$user_id) {
                            $return['html_str'] = "There was an error creating your account, please contact us.";
                            $return['error'] = true;
                        } else {
                            $creds = array(
                                'user_login' => $email,
                                'user_password' => $password,
                                'remember' => true
                            );

                            $user = wp_signon($creds, false);

                            if (is_wp_error($user)) {
                                $return['html_str'] = "Your account was created but there was an issue automatically loging you into it. " . $user->get_error_message();
                                $return['error'] = true;
                            } else {
                                $return['html_str'] = "";
                                $return['error'] = false;
                            }
                          }

                    }
                    break;
                case 'ajax-validation-email':
                    $email = Helpers::GetClean($_POST, 'email');

                    $vmail = new VerifyEmail\verifyEmail();
                    $vmail->setStreamTimeoutWait(20);
                    $vmail->Debug= TRUE;
                    $vmail->Debugoutput= 'log';

                    $vmail->setEmailFrom('testing.demo@gmail.com');

                    if ($vmail->check($email)) {
                        $return['error'] = false;
                    } elseif (verifyEmail::validate($email)) {
                        $return['error'] = true;
                    } else {
                        $return['error'] = true;
                    }
                    break;
                case 'getListingBySearch':
                    $SearchCriteria = new Structs\SearchCriteria($_GET);

                    $ListingSummeries = Cache::get_ListingBySearchCache($SearchCriteria);

                    $current_user = wp_get_current_user();

                    $mapListings = array();

                    if ($ListingSummeries->ListingSummeries):
                        foreach ($ListingSummeries as $ListingSummery)
                            \S4A\Controls::search_results($ListingSummery, $current_user->ID, "latest");
                    endif;

                    $return['html_str'] = $mapListings;
                    $return['error'] = false;
                    break;
                case 'build_child_occupents_list':
                    $the_rooms = Helpers::GetClean($_POST, 'the_room');
                    $the_children = Helpers::GetClean($_POST, 'the_children');
                    $child_min = Helpers::GetClean($_POST, 'child_min');
                    $child_max = Helpers::GetClean($_POST, 'child_max');
                    $return['html_str'] = Controls::get_child_occupents($the_rooms, $the_children, $child_min, $child_max);
                    $return['error'] = false;
                    break;
                case 'build_occupents_list':
                    $the_rooms = Helpers::GetClean($_POST, 'the_rooms');
                    $max_adults = Helpers::GetClean($_POST, 'max_adults');
                    $max_children = Helpers::GetClean($_POST, 'max_children');
                    $child_min = Helpers::GetClean($_POST, 'child_min');
                    $child_max = Helpers::GetClean($_POST, 'child_max');
                    $return['html_str'] = Controls::get_room_occupents($the_rooms, $max_adults, $max_children, $child_min, $child_max);
                    $return['error'] = false;
                    break;
                case 'get-provinces':
                    $the_country = Helpers::GetClean($_POST, 'the_country');
                    $the_province_id_name_str = Helpers::GetClean($_POST, 'the_province_id_name_str');
                    $return['html_str'] = Service::get_provinces($the_country, $the_province_id_name_str);
                    $return['error'] = false;
                    break;
                case 'get-areas':
                    $the_province = Helpers::GetClean($_POST, 'the_province');
                    $the_area_id_name_str = Helpers::GetClean($_POST, 'the_area_id_name_str');
                    $return['html_str'] = Service::get_areas($the_province, $the_area_id_name_str);
                    $return['error'] = false;
                    break;
                case 'room_information_popup':
                    $roomData = $_POST['room_data'];
                    $room_code = Helpers::GetClean($_POST, 'room_code');
                    $return['html_str'] = Controls::get_room_information_popup($roomData);
                    $return['error'] = false;
                    break;
                case 'check_coupon_code':
                    $price = Helpers::GetClean($_POST, 'price');
                    $code = Helpers::GetClean($_POST, 'code');
                    $email = Helpers::GetClean($_POST, 'email');
                    $listingId = Helpers::GetClean($_POST, 'listingId');
                    $return['html_str'] = Service::check_coupon_code($price,$email,$code,$listingId);
                    $return['error'] = false;
                    break;
                case 'get_ListingBySearchonMap':
                    $SearchCriteria = new Structs\SearchCriteria($_GET);
                    $SearchCriteria->perPage = 0;

                    $ListingSummeries = Cache::get_ListingBySearchCache($SearchCriteria);

                    $mapListings = array();

                    if ($ListingSummeries->ListingSummeries):

                        foreach ($ListingSummeries->ListingSummeries as $ListingSummery) :

                            $listing = array();

                            $coordinates = explode(',', $ListingSummery->v_geo_coordinates);
                            $longitude = floatval($coordinates[0]);
                            $latitude = floatval($coordinates[1]);

                            $marker = array();

                            $marker['title'] = $ListingSummery->v_title;
                            $marker['longitude'] = $longitude;
                            $marker['latitude'] = $latitude;

                            $listing['markers'] = $marker;

                            $listing['infoWindowContent'] = '<div class="s4a_css_results map">' . \S4A\Controls::search_results($ListingSummery, null, "results", true) . '</div>';

                            array_push($mapListings, $listing);

                        endforeach;
                    endif;

                    $return['html_str'] = $mapListings;
                    $return['error'] = false;
                    break;
                case 'get_available_rooms':

                    $dates = array();
                    $dates['checkInDate'] = \S4A\Helpers::GetClean($_POST, 'the_arrival_date');
                    $dates['checkOutDate'] = \S4A\Helpers::GetClean($_POST, 'the_departure_date');

                    $SearchCriteria = new \S4A\Structs\SearchCriteria($dates);
                    \S4A\Session::UserBookingData($SearchCriteria, true);

                    $AvailabilityCriteria = new Structs\AvailabilityCriteria();

                    $AvailabilityCriteria->the_arrival_date = \S4A\Helpers::GetClean($_POST, 'the_arrival_date');
                    $AvailabilityCriteria->the_departure_date = \S4A\Helpers::GetClean($_POST, 'the_departure_date');
                    $AvailabilityCriteria->listing_id = \S4A\Helpers::GetClean($_POST, 'listing_id');

                    $Units = array();

                    if ($_POST['Units']) {

                        foreach ($_POST['Units'] as $unit) {

                            $Unit = new Structs\Unit(null);

                            $Unit->adults = Helpers::GetClean($unit, 'adults');

                            $children = array();

                            if ($unit['Children']) {

                                foreach ($unit['Children'] as $child) {
                                    $children[] = \S4A\Helpers::Clean($child);
                                }
                            }

                            $Unit->Children = $children;

                            $Units[] = $Unit;
                        }
                    }

                    $AvailabilityCriteria->Units = $Units;

                    $availibleRooms = Service::get_available_rooms($AvailabilityCriteria);

                    if (!empty($availibleRooms->channel->rooms))
                        $result['ListingRooms']['channel'] = Controls::get_available_rooms_channel($availibleRooms, $AvailabilityCriteria);
                    else if (!empty($availibleRooms->local->rooms))
                        $result['ListingRooms']['local'] = Controls::get_available_rooms($availibleRooms, ((is_array($availibleRooms->channel->Error)) ? $availibleRooms->channel->Error[0] : $availibleRooms->channel->Error));
                    else if (!empty($availibleRooms->rooms->rooms))
                        $result['ListingRooms']['local'] = Controls::get_rooms($availibleRooms, ((is_array($availibleRooms->channel->Error)) ? $availibleRooms->channel->Error[0] : $availibleRooms->channel->Error), $AvailabilityCriteria);
                    else
                        $result['ListingRooms']['local'] = '<br>
                                                <div class="alert alert-danger" role="alert">
                                                  <p><i class="fa fa-info" aria-hidden="true"></i> There are no units available for the dates selected, please contact us to speak to a consultant <a href="'.esc_url(get_permalink(S4A_CONTACT_PAGE_ID)).'">click here</a></p>
                                                </div>';
                    $return['html_str'] = $result;

                    $return['error'] = false;

                    break;

            }
            $response = json_encode($return);
            header("Content-Type: application/json");
            echo $response;
            exit;
        }

    }

}
