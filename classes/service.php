<?php

namespace S4A {

    class Service {

        function __construct() {

        }

        public static function get_BookingHistoryByUserId() {

            $result = Service::Docurl(S4A_SERVICE_URL . 'get_BookingHistoryByUserId');

            $json_decoded = json_decode($result);

            return $json_decoded;
        }

        public static function get_EnquiryHistoryByUserId() {

            $result = Service::Docurl(S4A_SERVICE_URL . 'get_EnquiryHistoryByUserId');

            $json_decoded = json_decode($result);

            return $json_decoded;
        }

        public static function get_InitialisationData() {

            $result = Service::Docurl(S4A_SERVICE_URL . 'get_InitialisationData', $postFields);

            $json_decoded = json_decode($result);

            return $json_decoded;
        }

        public static function do_authentication($input){

          $result = Service::Docurl(S4A_SERVICE_URL . 'do_authentication', $input);

          $json_decoded = json_decode($result);

          return $json_decoded;

        }

        public static function create_wp_user($user){

        $postFields = array(
            "wp_user_data" => $user
        );

          $result = Service::Docurl(S4A_SERVICE_URL . 'create_wp_user', $postFields);

          $json_decoded = json_decode($result);

          return $json_decoded;

        }

        public static function do_Reservation($BookingCriteria) {

            $postFields = array(
                "BookingCriteria" => $BookingCriteria
            );

            $result = Service::Docurl(S4A_SERVICE_URL . 'do_Reservation', $postFields);

            $json_decoded = json_decode($result);

            return $json_decoded;
        }

        public static function do_Payment($transactionData, $reservation_id) {

            $postFields = array(
                "TransactionData" => $transactionData,
                "reservation_id" => $reservation_id,
            );

            $result = Service::Docurl(S4A_SERVICE_URL . 'do_Payment', $postFields);

            $json_decoded = json_decode($result);

            return $json_decoded;
        }

        public static function ignore_Reservation($reservation_id, $listing_id) {

            $postFields = array(
                "reservation_id" => $reservation_id,
                "listing_id" => $listing_id
            );

            $result = Service::Docurl(S4A_SERVICE_URL . 'ignore_Reservation', $postFields);

            $json_decoded = json_decode($result);

            return $json_decoded;
        }

        public static function do_Enquiry($BookingCriteria) {

            $postFields = array(
                "BookingCriteria" => $BookingCriteria
            );

            $result = Service::Docurl(S4A_SERVICE_URL . 'do_Enquiry', $postFields);

            $json_decoded = json_decode($result);

            $return['sucess'] = $json_decoded->sucess;
            $return['error'] = $json_decoded->error;

            return $return;
        }

        public static function get_Reservation($reservation_id) {

            $postFields = array(
                "reservation_id" => $reservation_id
            );

            $result = Service::Docurl(S4A_SERVICE_URL . 'get_Reservation', $postFields);

            $json_decoded = json_decode($result);

            return $json_decoded;
        }

        public static function get_reservationDetailsByID($reservation_id) {
            $postFields = array(
                "reservation_id" => $reservation_id
            );

            $result = Service::Docurl(S4A_SERVICE_URL . 'get_reservationDetailsByID', $postFields);

            $json_decoded = json_decode($result);

            return $json_decoded;
        }

        public static function get_ListingById($id) {

            $postFields = array(
                "listingId" => $id
            );

            $result = Service::Docurl(S4A_SERVICE_URL . 'get_ListingById', $postFields);

            $json_decoded = json_decode($result);

            return $json_decoded;
        }

        public static function get_available_rooms($availabilityCriteria) {

            $postFields = array(
                "AvailabilityCriteria" => $availabilityCriteria
            );

            $result = Service::Docurl(S4A_SERVICE_URL . 'get_available_rooms', $postFields);

            $json_decoded = json_decode($result);

            return $json_decoded;
        }

        public static function get_ListingBySearch($searchCriteria) {

            $postFields = array(
                "SearchCriteria" => $searchCriteria
            );

            $result = Service::Docurl(S4A_SERVICE_URL . 'get_ListingBySearch', $postFields);
            $json_decoded = json_decode($result);

            return $json_decoded;
        }

        public static function check_coupon_code($price,$email,$code,$listingId) {

            $postFields = array(
                "Price" => $price,
                "Email" => $email,
                "Code" => $code,
                "ListingId" => $listingId
            );
            $result = Service::Docurl(S4A_SERVICE_URL . 'check_coupon_code', $postFields);
            $json_decoded = json_decode($result);

            return $json_decoded;
        }

        public static function get_user_role() {

            $result = Service::Docurl(S4A_SERVICE_URL . 'get_user_role');
            $json_decoded = json_decode($result);
            return $json_decoded;
        }

        public static function DoCurl($url, $postFields = null) {

            $defaultFields = array(
                "session_id" => session_id(),
                "user_ip" => Service::getRealIpAddr(),
                "wp_user" => $_SESSION["get_current_user"],
                "userName" => get_option("wps4a_username"),
                "password" => get_option("wps4a_pass"),
                "url" => ((isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'])
            );

            if ($postFields && $defaultFields) {
                $theFields = array_merge($defaultFields, $postFields);
            } else if ($postFields) {
                $theFields = $postFields;
            } else {
                $theFields = $defaultFields;
            }

            $fields_string = "";
            foreach ($theFields as $key => $value) {
                if (is_object($value)) {
                    $fields_string .= $key . '=' . str_replace("&", "and", json_encode($value)) . '&';
                } else {
                    $fields_string .= $key . '=' . str_replace("&", "and", $value) . '&';
                }
            }
            rtrim($fields_string, '&');
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, count($theFields));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);

			if($error = curl_error($ch))
				Service::ServiceDown($error);

			return $result;
            curl_close($ch);
        }

		public static function ServiceDown($error) {
            if(S4A_ENVIRONMENT != "production") return;

			global $bugsnagWordpress;
			if($bugsnagWordpress)
				$bugsnagWordpress->notifyException(new \RuntimeException("Service Down" . $error));
						header('HTTP/1.1 503 Service Temporarily Unavailable');
			header('Status: 503 Service Temporarily Unavailable');
			header('Retry-After: 300');
			include S4A_DIR_PATH . '/templates/errors/503.php';
			die();


		}

        public static function getRealIpAddr() {
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            return $ip;
        }

    }

}
