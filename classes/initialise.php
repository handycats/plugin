<?php

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

$initialise = new Initialise();

class Initialise
{

    public function __construct()
    {

        $ajax = new S4A\Ajax();
        $controls = new S4A\Controls();

        // $controlsOp = new S4A\OperatorControls();
        add_action('init', array($this, 's4a_redirects'));
        add_action('init', array($this, 'utm_parameters'));
        add_action('init', array($this, 's4a_service_user'));
        // add_action('init', array($this, 'operator_results'));
        add_action('wp_enqueue_scripts', array($this, 'load_js'));
        add_action('wp_enqueue_scripts', array($this, 'load_css'));
        //add_shortcode('wp4sa_property', array($this, 'handler')); PB: Hook to display shortcodes
        add_action('wp_ajax_nopriv_' . S4A_NAME_SHORT . '-ajax-submit', array($ajax, 'ajax_submit'));
        add_action('wp_ajax_' . S4A_NAME_SHORT . '-ajax-submit', array($ajax, 'ajax_submit'));
        add_action('plugins_loaded', array($this, 'session_override'));
        add_action('wp_footer', array($this, 's4a_footer'), 1);
        add_action('wp_head', array($this, 's4a_colors'));
        add_action('wp_head', array($this, 's4a_timer'));
        add_action('wp_head', array($this, 's4a_loader'));
        add_filter('authenticate', array($this, 'authentication'), 30, 30);
        add_shortcode('listing_availability_search', array($controls, 'listing_availability_search'));
        add_shortcode('operator_results', array($this, 'operator_results'));
        add_shortcode('listing_results', array($this, 'listing_results'));
        add_shortcode('listing_map', array($this, 'listing_map'));
    }

    public function authentication($user, $username, $password)
    {

        if (!S4A_LOYALTY_ENABLE || ($username == "" || $password == "") || filter_var($username, FILTER_VALIDATE_EMAIL)) {
            return $user;
        }

        $input = array('l_user' => $username, 'l_pass' => $password, 'l_wp_user' => $user);

        $return = \S4A\Service::do_authentication($input);

        if ($user->ID == 0) {

            $userdata = array(
                'user_pass' => $password,
                'user_login' => $username,
                'user_email' => $return->email,
                'display_name' => $return->user_name,
            );

            $userUpdate = (array) $return;

            $new_user_id = wp_insert_user($userdata);

            $user = new WP_User($new_user_id);

            \S4A\Meta::UpdateProfile($user->ID, $userUpdate);

            $get = S4A\Meta::GetProfile($user->ID);

            \S4A\Service::create_wp_user($get);

            return $user;

        } else {

            $userUpdate = (array) $return;

            \S4A\Meta::UpdateProfile($user->ID, $userUpdate);

            $get = S4A\Meta::GetProfile($user->ID);

            \S4A\Service::create_wp_user($get);

            return $user;
        }
        return $user;
    }

    public function s4a_colors()
    {?>
        <style>

          .primary_color {
            background-color:<?=S4A_PRIMARY_COLOR?>!important;
          }
          .primary_color_font {
            color:<?=S4A_PRIMARY_COLOR?>!important;
          }
          .secondary_color_font {
            color:<?=S4A_SECONDARY_COLOR?>!important;
          }
          .secondary_color {
            background-color:<?=S4A_SECONDARY_COLOR?>!important;
            border-color:<?=S4A_SECONDARY_COLOR?>!important;
          }
          .primary_color_border{
            border:1px solid <?=S4A_PRIMARY_COLOR?>!important;
          }

        </style>


    <?php
}

    public function s4a_service_user()
    {
        $_SESSION["get_current_user"] = wp_get_current_user();
    }

    public function listing_map($attr)
    {
        $model = new stdClass();

        $model->latitude = (isset($attr['latitude'])) ? $attr['latitude'] : get_option('wps4a_map_search_latitude');
        $model->longitude = (isset($attr['longitude'])) ? $attr['longitude'] : get_option('wps4a_map_search_longitude');
        $model->zoom = (isset($attr['zoom'])) ? $attr['zoom'] : 6;
        $width = (isset($attr['width'])) ? $attr['width'] : "100%";
        $height = (isset($attr['height'])) ? $attr['height'] : "300px";

        add_action('wp_footer', function () use ($model) {?>
            <script type='text/javascript'>
            var s4aMAP = {"latitude":<?=$model->latitude?>,"longitude":<?=$model->longitude?>,"zoom":<?=$model->zoom?>};
            </script>
            <?php
}, 99);

        return "<div id='map_search'><div class='map-view'><div id='map_canvas' style='width:" . $width . ";height:" . $height . ";'></div></div> <!--end map-view--><script type='text/javascript'>window.addEventListener('load', function () {MapSearch();});</script></div>";
    }

    public function listing_results($attr)
    {

        $model = new stdClass();

        $criteria['areaId'] = $attr['areaId'];
        $listingTypeArray = explode(",", $attr['type']);

        $criteria['listingTypes'] = $listingTypeArray;

        $model->SearchCriteria = new \S4A\Structs\SearchCriteria($criteria);

        $model->ListingSearchResults = \S4A\Cache::get_ListingBySearchCache($model->SearchCriteria);
        $current_user = wp_get_current_user();

        $return = '<section class="row s4a_css_results">';
        if ($model->ListingSearchResults):
            foreach ($model->ListingSearchResults->ListingSummeries as $ListingSummery) {
                $return .= '<article class="col-md-4"><div class="css_inner">' . \S4A\Controls::search_results($ListingSummery, $current_user->ID) . '</div></article>';
            } else :
            if (!S4A_SIGNUP_ID) {
                $return .= "<p class='s4a_no-results'>No results found, please contact us for assistance.</p>";
            } else {
                $return .= "<p class='s4a_no-results'><a href='" . get_permalink(S4A_SIGNUP_ID) . "'>List Your Business</a></p>";
            }
        endif;

        $return .= '</section>';

        return $return;
    }

    public function operator_results($attr)
    {

        $model = new stdClass();

        $criteria = array();

        if(isset($attr['type']))
          $criteria['operatorTypes'] = array($attr['type']);

        $model->SearchCriteria = new \S4A\OperatorsStructs\SearchCriteria($criteria);

        if(isset($attr['perpage']))
          $model->SearchCriteria->perPage = $attr['perpage'];

        $model->ListingSearchResults = \S4A\OperatorsCache::get_ListingBySearchCache($model->SearchCriteria);
        $current_user = wp_get_current_user();

        $return = '<section class="row s4a_css_results">';
        if ($model->ListingSearchResults && $model->ListingSearchResults->OperatorSummeries):
            foreach ($model->ListingSearchResults->OperatorSummeries as $ListingSummery) {
                $return .= '<article class="col-md-4"><div class="css_inner">' . \S4A\OperatorsControls::search_results($ListingSummery, $current_user->ID) . '</div></article>';
            } else :
            if (!S4A_SIGNUP_ID) {
                $return .= "<p class='s4a_no-results'>No results found, please contact us for assistance.</p>";
            } else {
                $return .= "<p class='s4a_no-results'><a href='" . get_permalink(S4A_SIGNUP_ID) . "'>List Your Business</a></p>";
            }
        endif;

        $return .= '</section>';

        return $return;
    }

    public function s4a_redirects()
    {
        global $wp_query;
        $uri = $_SERVER['REQUEST_URI'];
        $post_id = url_to_postid($uri);
        $listing_id = \S4A\Helpers::GetURLId($uri);
        if ($post_id) {
            if (array_key_exists($post_id, \S4A\Cache::LocationRedirectCache())) {
                if (\S4A\Cache::LocationRedirectCache()[$post_id] != get_site_url() . $uri) {
                    wp_redirect(\S4A\Cache::LocationRedirectCache()[$post_id], 301);
                    exit;
                }
            }
            if (array_key_exists($post_id, \S4A\OperatorsCache::LocationRedirectCache())) {
                if (\S4A\OperatorsCache::LocationRedirectCache()[$post_id] != get_site_url() . $uri) {
                    wp_redirect(\S4A\OperatorsCache::LocationRedirectCache()[$post_id], 301);
                    exit;
                }
            }
        } else {
            if (array_key_exists($listing_id, \S4A\Cache::ListingRedirectCache())) {

                if (strpos(get_site_url() . $uri, 'listing') !== false) {
                    if (\S4A\Cache::ListingRedirectCache()[$listing_id] != get_site_url() . $uri) {
                        wp_redirect(\S4A\Cache::ListingRedirectCache()[$listing_id], 301);
                        exit;
                    }
                }
            }
        }
    }

    public function load_js()
    {
        if (!isset($_GET['s4a_testJS'])) {
            wp_enqueue_script(S4A_NAME_SHORT . '_js_scripts', plugins_url('/assets/js/scripts.js', dirname(__FILE__)), array('jquery'), S4A_CACHE_KEY, true);
        } else {
            wp_enqueue_script(S4A_NAME_SHORT . '_js_scripts', plugins_url('/assets/javascript/datepicker.js', dirname(__FILE__)), array('jquery'), S4A_CACHE_KEY, true);
            wp_enqueue_script(S4A_NAME_SHORT . '_js_scripts2', plugins_url('/assets/javascript/modal.js', dirname(__FILE__)), array('jquery'), S4A_CACHE_KEY, true);
            wp_enqueue_script(S4A_NAME_SHORT . '_js_scripts3', plugins_url('/assets/javascript/reservation.js', dirname(__FILE__)), array('jquery'), S4A_CACHE_KEY, true);
            wp_enqueue_script(S4A_NAME_SHORT . '_js_scripts4', plugins_url('/assets/javascript/enquiry.js', dirname(__FILE__)), array('jquery'), S4A_CACHE_KEY, true);
            wp_enqueue_script(S4A_NAME_SHORT . '_js_scripts5', plugins_url('/assets/javascript/payment.js', dirname(__FILE__)), array('jquery'), S4A_CACHE_KEY, true);
            wp_enqueue_script(S4A_NAME_SHORT . '_js_scripts6', plugins_url('/assets/javascript/gallery.js', dirname(__FILE__)), array('jquery'), S4A_CACHE_KEY, true);
            wp_enqueue_script(S4A_NAME_SHORT . '_js_scripts7', plugins_url('/assets/javascript/gallery_default.js', dirname(__FILE__)), array('jquery'), S4A_CACHE_KEY, true);
            wp_enqueue_script(S4A_NAME_SHORT . '_js_scripts8', plugins_url('/assets/javascript/gallery_carousel.js', dirname(__FILE__)), array('jquery'), S4A_CACHE_KEY, true);
            wp_enqueue_script(S4A_NAME_SHORT . '_js_scripts9', plugins_url('/assets/javascript/booking.js', dirname(__FILE__)), array('jquery'), S4A_CACHE_KEY, true);
            wp_enqueue_script(S4A_NAME_SHORT . '_js_scriptsa', plugins_url('/assets/javascript/slider.js', dirname(__FILE__)), array('jquery'), S4A_CACHE_KEY, true);
            wp_enqueue_script(S4A_NAME_SHORT . '_js_scriptsb', plugins_url('/assets/javascript/select.js', dirname(__FILE__)), array('jquery'), S4A_CACHE_KEY, true);
            wp_enqueue_script(S4A_NAME_SHORT . '_js_scriptsc', plugins_url('/assets/javascript/lazy.js', dirname(__FILE__)), array('jquery'), S4A_CACHE_KEY, true);
            wp_enqueue_script(S4A_NAME_SHORT . '_js_scriptsd', plugins_url('/assets/javascript/map.js', dirname(__FILE__)), array('jquery'), S4A_CACHE_KEY, true);
            wp_enqueue_script(S4A_NAME_SHORT . '_js_scriptse', plugins_url('/assets/javascript/global.js', dirname(__FILE__)), array('jquery'), S4A_CACHE_KEY, true);
            wp_enqueue_script(S4A_NAME_SHORT . '_js_scriptsf', plugins_url('/assets/javascript/overlay_loader.js', dirname(__FILE__)), array('jquery'), S4A_CACHE_KEY, true);
            wp_enqueue_script(S4A_NAME_SHORT . '_js_scriptsg', plugins_url('/assets/javascript/sly.min.js', dirname(__FILE__)), array('jquery'), S4A_CACHE_KEY, true);
            wp_enqueue_script(S4A_NAME_SHORT . '_js_scriptsh', plugins_url('/assets/javascript/intlTelInput.js', dirname(__FILE__)), array('jquery'), S4A_CACHE_KEY, true);
            wp_enqueue_script(S4A_NAME_SHORT . '_js_scriptsi', plugins_url('/assets/javascript/jquery.timepicker.min.js', dirname(__FILE__)), array('jquery'), S4A_CACHE_KEY, true);
            wp_enqueue_script(S4A_NAME_SHORT . '_js_scriptsj', plugins_url('/assets/javascript/utils.js', dirname(__FILE__)), array('jquery'), S4A_CACHE_KEY, true);
        }

        if (S4A_INCLUDE_BOOTSTRAP) {
            wp_enqueue_script(S4A_NAME_SHORT . '_bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), S4A_CACHE_KEY, true);
        }

        wp_localize_script(S4A_NAME_SHORT . '_js_scripts', S4A_NAME_SHORT . 'JSO', array(
            'the_unique_plugin_name' => S4A_NAME_SHORT,
            'ajax_url' => admin_url('admin-ajax.php'),
            'listing_url' => S4A_LISTING_URL,
            'img_url' => S4A_IMG_URL,
            'logged' => (is_user_logged_in()) ? true : false,
            'home_page_url' => esc_url(home_url('/')),
        ));
    }

    public function session_override()
    {

        if (!isset($_SESSION[S4A\Session::STORESESSIONKEY]) && !isset($_GET['s4a_plugin'])) {
            session_start();
        }

    }

    public function s4a_footer()
    {
        require_once S4A_DIR_PATH . 'templates/shared/modal.php';
    }

    public function load_css()
    {
        wp_enqueue_style(S4A_NAME_SHORT . '_css', plugins_url('/assets/css/styles.css', dirname(__FILE__)), null, S4A_CACHE_KEY);

        if (S4A_INCLUDE_BOOTSTRAP) {
            wp_enqueue_style(S4A_NAME_SHORT . '_bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', null, S4A_CACHE_KEY);
            wp_enqueue_style(S4A_NAME_SHORT . '_bootstrap_theme', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css', null, S4A_CACHE_KEY);
        }
    }

    public function utm_parameters()
    {
        $uri = $_GET;

        if (array_key_exists('arrival', $_GET) && array_key_exists('depature', $_GET)) {

            $SearchCriteria = new stdClass();

            $SearchCriteria->checkInDate = $_GET['arrival'];
            $SearchCriteria->checkOutDate = $_GET['depature'];

            \S4A\Session::UserBookingData($SearchCriteria, true);
        }

        if (array_key_exists('promo', $_GET)) {
            \S4A\Session::promo_data($_GET['promo']);
        }

        if (array_key_exists('utm_source', $uri)) {
            $UtmParameters = new S4A\Structs\UTMParameters($_GET);
            \S4A\Session::utm_data($UtmParameters);
        }

        if (array_key_exists('gclid', $uri)) {
            $GoogleAdwordData = array('utm_source' => 'adwords', 'utm_medium' => 'cpc', 'utm_campaign' => 'default');
            $UtmParameters = new S4A\Structs\UTMParameters($GoogleAdwordData);
            \S4A\Session::utm_data($UtmParameters);
        }
    }

    public function s4a_timer()
    {
        require_once S4A_DIR_PATH . 'templates/shared/counter.php';
    }
    public function s4a_loader()
    {
        require_once S4A_DIR_PATH . 'templates/shared/loader.php';
    }
}
