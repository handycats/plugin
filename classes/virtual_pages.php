<?php

class S4A_VirtualPages {

    public $title = '';
    public $body = '';
    private $vpages = array();  // the main array of virtual pages
    private $mypath = '';
    public $blankcomments = "blank-comments.php";
    
    function body_classes($classes) {
        $classes[] = 's4a';
        return $classes;
    }

    function __construct($plugin_path = null, $blankcomments = null) {
        if (empty($plugin_path))
            $plugin_path = dirname(__FILE__);
        $this->mypath = $plugin_path;
        if (!empty($blankcomments))
            $this->blankcomments = $blankcomments;
        add_action('parse_request', array(&$this, 'vtp_parse_request'));
    }

    function add($virtual_regexp, $contentfunction) {
        $this->vpages[$virtual_regexp] = $contentfunction;
    }

    function vtp_parse_request(&$wp) {
        //global $wp;
        /*
          if (empty($wp->query_vars['pagename']))
          return; // page isn't permalink
         */
//$p = $wp->query_vars['pagename'];
        $p = $_SERVER['REQUEST_URI'];
        $matched = 0;

        foreach ($this->vpages as $regexp => $func) {
            if (preg_match($regexp, $p)) {
                $matched = 1;
                break;
            }
        }

        if (!$matched)
            return;

        add_filter('body_class', array(&$this, 'body_classes'));

        remove_filter('the_content', 'wpautop');
        remove_filter('the_excerpt', 'wpautop');
        remove_all_actions('template_redirect');
        add_action('template_redirect', array(&$this, 'template_redir'));
        add_filter('the_posts', array(&$this, 'vtp_createdummypost'));
        add_filter('comments_template', array(&$this, 'disable_comments'), 11);
        $this->template = $this->subtemplate = null;
        $this->title = null;
        unset($this->body);
        call_user_func_array($func, array(&$this, $p));
        if (!isset($this->body)) //assert
            wp_die("Virtual Themed Pages: must save ->body [VTP07]");
        return($wp);
    }

    function vtp_createdummypost($posts) {
        global $wp, $wp_query;
        $p = new stdClass;
        $p->ID = -1;
        $p->post_author = 1;
        $p->post_date = current_time('mysql');
        $p->post_date_gmt = current_time('mysql', $gmt = 1);
        $p->post_content = $this->body;
        $p->post_title = $this->title;
        $p->post_excerpt = '';
        $p->post_status = 'publish';
        $p->ping_status = 'closed';
        $p->post_password = '';
        //$p->post_name = 'movie_details'; // slug
        $p->post_name = $wp->request; // slug
        $p->to_ping = '';
        $p->pinged = '';
        $p->modified = $p->post_date;
        $p->modified_gmt = $p->post_date_gmt;
        $p->post_content_filtered = '';
        $p->post_parent = 0;
        $p->guid = get_home_url('/' . $p->post_name); // use url instead?
        $p->menu_order = 0;
        $p->post_type = 'page';
        $p->post_mime_type = '';
        $p->comment_status = 'closed';
        $p->comment_count = 0;
        $p->filter = 'raw';
        $p->ancestors = array(); // 3.6
        $wp_query->is_page = TRUE;
        $wp_query->is_singular = TRUE;
        $wp_query->is_home = FALSE;
        $wp_query->is_archive = FALSE;
        $wp_query->is_category = FALSE;
        unset($wp_query->query['error']);
        $wp->query = array();
        $wp_query->query_vars['error'] = '';
        $wp_query->is_404 = FALSE;
        $wp_query->current_post = $p->ID;
        $wp_query->found_posts = 1;
        $wp_query->post_count = 1;
        $wp_query->comment_count = 0;
        $wp_query->current_comment = null;
        $wp_query->is_singular = 1;
        $wp_query->post = $p;
        $wp_query->posts = array($p);
        $wp_query->queried_object = $p;
        $wp_query->queried_object_id = $p->ID;
        $wp_query->current_post = $p->ID;
        $wp_query->post_count = 1;
        return array($p);
    }

    function template_redir() {
        if (!empty($this->template) && !empty($this->subtemplate)) {
            get_template_part($this->template, $this->subtemplate);
        } elseif (!empty($this->template)) {
            get_template_part($this->template);
        } elseif (!empty($this->subtemplate)) {
            get_template_part($this->subtemplate);
        } else {
            get_template_part('page');
        }
        exit;
    }

    function disable_comments($file) {
        if (file_exists($this->blankcomments))
            return($this->mypath . '/' . $blankcomments);
        return($file);
    }

}
