<?php

namespace S4A {

  class Controls
  {

    private static function checkout_steps_check($current, $that)
    {
      return include S4A_DIR_PATH . '/templates/controls/checkout_steps_check.php';
    }

    public static function checkout_steps($current)
    {
      include S4A_DIR_PATH . '/templates/controls/checkout_steps.php';
    }

    public static function availability_calendar($roomData, $v_code)
    {
      return include S4A_DIR_PATH . '/templates/controls/availability_calendar.php';
    }

    public static function get_available_rooms_channel($roomData, $AvailabilityCriteria)
    {
      return include S4A_DIR_PATH . '/templates/controls/get_available_rooms_channel.php';
    }

    public static function get_available_rooms($roomData, $error)
    {
      return include S4A_DIR_PATH . '/templates/controls/get_available_rooms.php';
    }

    public static function get_rooms($roomData, $error, $AvailabilityCriteria)
    {
      return include S4A_DIR_PATH . '/templates/controls/get_rooms.php';
    }

    public static function get_room_information_popup($roomData)
    {
      return include S4A_DIR_PATH . '/templates/controls/get_room_information_popup.php';
    }

    public static function get_room_occupents($rooms = 1, $max_adults = 2, $max_children = 2, $child_min = 13, $child_max = 18)
    {
      return include S4A_DIR_PATH . '/templates/controls/get_room_occupents.php';
    }

    public static function get_child_occupents($room, $children, $child_min, $child_max)
    {
      return include S4A_DIR_PATH . '/templates/controls/get_child_occupents.php';
    }

    public static function search_results($ListingSummery, $uid, $imageType = "results", $noLasy = false, $display = false)
    {
      return include S4A_DIR_PATH . '/templates/controls/search_results.php';
    }

    public static function listing_reservation($ListingSummery, $uid, $imageType = "results", $noLasy = false, $display = false)
    {
      return include S4A_DIR_PATH . '/templates/controls/listing_reservation.php';
    }

    public static function latest_listings_widget($ListingSummeries)
    {
      include S4A_DIR_PATH . '/templates/controls/latest_listings_widget.php';
    }

    public static function getLocationFromURL()
    {
      return include S4A_DIR_PATH . '/templates/controls/getLocationFromURL.php';
    }

    public static function horizontal_search_widget($instance)
    {
      include S4A_DIR_PATH . '/templates/controls/horizontal_search_widget.php';
    }

    public static function listing_availability_search($atts, $listing = null)
    {
      return include S4A_DIR_PATH . '/templates/controls/listing_availability_search.php';
    }

    public static function the_search_widget($instance)
    {
      include S4A_DIR_PATH . '/templates/controls/the_search_widget.php';
    }

    public static function currency_converter_widget()
    {
      return include S4A_DIR_PATH . '/templates/controls/currency_converter_widget.php';
    }

    public static function the_listing_quick_links_widget($instance)
    {
      return include S4A_DIR_PATH . '/templates/controls/the_listing_quick_links_widget.php';
    }

  }

}
?>
