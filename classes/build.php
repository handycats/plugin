<?php

$font_awesome = get_option('wps4a_global_settings_font_awesome');

if (isset($_GET['s4a_plugin']) || $S4A_ASWOME != $font_awesome):
    require_once S4A_DIR_PATH . '/includes/less/lessc.inc.php';
    require_once S4A_DIR_PATH . '/includes/minify/src/Minify.php';
    require_once S4A_DIR_PATH . '/includes/minify/src/JS.php';
    try {
        if (isset($_GET['js'])):
            $minifier = new MatthiasMullie\Minify\JS(
                S4A_DIR_PATH . '/assets/javascript/datepicker.js',
                S4A_DIR_PATH . '/assets/javascript/overlay_loader.js',
                S4A_DIR_PATH . '/assets/javascript/modal.js',
                S4A_DIR_PATH . '/assets/javascript/reservation.js',
                S4A_DIR_PATH . '/assets/javascript/enquiry.js',
                S4A_DIR_PATH . '/assets/javascript/payment.js',
                S4A_DIR_PATH . '/assets/javascript/gallery.js',
                S4A_DIR_PATH . '/assets/javascript/gallery_default.js',
                S4A_DIR_PATH . '/assets/javascript/gallery_carousel.js',
                S4A_DIR_PATH . '/assets/javascript/booking.js',
                S4A_DIR_PATH . '/assets/javascript/slider.js',
                S4A_DIR_PATH . '/assets/javascript/select.js',
                S4A_DIR_PATH . '/assets/javascript/lazy.js',
                S4A_DIR_PATH . '/assets/javascript/map.js',
                S4A_DIR_PATH . '/assets/javascript/appear.js',
                S4A_DIR_PATH . '/assets/javascript/global.js',
                S4A_DIR_PATH . '/assets/javascript/intlTelInput.js',
                //, S4A_DIR_PATH . '/assets/javascript/intlTelInput.min.js'
                S4A_DIR_PATH . '/assets/javascript/jquery.timepicker.min.js',
                S4A_DIR_PATH . '/assets/javascript/utils.js',
                S4A_DIR_PATH . '/assets/javascript/sly.min.js'
            );

            $minifiedPath = S4A_DIR_PATH . '/assets/js/scripts.js';

            $minifier->minify($minifiedPath);
        endif;

        $less = new lessc;

        $less->setFormatter("compressed");

        $lessPath = ($font_awesome == "true") ? '/assets/less/include_awsome.less' : '/assets/less/include.less';
        $less->compileFile(S4A_DIR_PATH . $lessPath, S4A_DIR_PATH . '/assets/css/styles.css');

        $S4A_CACHE_KEY = rand();

        $s4aBuild = S4A_DIR_PATH . '/s4a.php';

        $contentsCurrent = file_get_contents($s4aBuild);

        $contentsKey = preg_replace('/S4A_CACHE_KEY = "(.*)".*/iU'
            , 'S4A_CACHE_KEY = "' . $S4A_CACHE_KEY . '"'
            , $contentsCurrent);

        $contents = preg_replace('/S4A_ASWOME = "(.*)".*/iU'
            , 'S4A_ASWOME = "' . $font_awesome . '"'
            , $contentsKey);

        if ($contents && strpos($contents, '$S4A_CACHE_KEY = "' . $S4A_CACHE_KEY . '";') !== false) {
            file_put_contents($s4aBuild, $contents);
            $result = '<div id="s4a_less_build">Done S4A_CACHE_KEY = ' . $S4A_CACHE_KEY . '</div>';
        } else {
            $result = '<div id="s4a_less_build" class="failed">Failed to update version number.</div>';
        }
    } catch (exception $e) {
        $result = '<div id="s4a_less_build" class="failed">fatal error: ' . $e->getMessage() . '</div>';
    }

    add_action('wp_footer', function () use ($result) {
        echo $result;
    }, 2);
endif;
