<?php

require_once( S4A_DIR_PATH . '/widgets/property_search.php' );
require_once( S4A_DIR_PATH . '/widgets/horizontal_search.php' );
require_once( S4A_DIR_PATH . '/widgets/latest_listings.php' );
require_once( S4A_DIR_PATH . '/widgets/listing_quick_links.php' );
require_once(S4A_DIR_PATH. '/widgets/currency_converter.php');
