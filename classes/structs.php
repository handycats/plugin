<?php

namespace S4A\Structs {

  class Counter {

      public $url;
      public $time;

      function __construct($data) {
          $this->url = $data['url'];
          $this->time = $data['time'];
      }

  }

    class UTMParameters {

        public $utm_source;
        public $utm_medium;
        public $utm_campaign;

      function __construct($data) {
          $this->utm_source = $data['utm_source'];
          $this->utm_medium = $data['utm_medium'];
          $this->utm_campaign = $data['utm_campaign'];
      }

    }

    class SearchWidgetCriteria
    {

        public $countryId;
        public $provinceId;

        function __construct($countryId, $provinceId)
        {

            $this->countryId = $countryId;
            $this->provinceId = $provinceId;
        }

    }

    class Reservation
    {

        public $price;
        public $room_total;
        public $total;
        public $discount;
        public $ReservationCriteria;
        public $ListingSpecial;

        function __construct()
        {

        }

        static function create($data)
        {
            $Reservation = new Reservation();
            $Reservation->price = $data->price;
            $Reservation->room_total = $data->room_total;
            $Reservation->ReservationCriteria = new ReservationCriteria($data->ReservationCriteria);
            return $Reservation;
        }

    }

    class PaymentCriteria
    {

        public $card_first_name;
        public $card_last_name;
        public $card_email;
        public $card_number;
        public $card_expiry_month;
        public $card_expiry_year;
        public $card_cvv;

        function __construct()
        {

        }

    }

    class BookingCriteria
    {

        public $wp_user;
        public $sucess;
        public $message;
        public $Reservation;
        public $EnquiryCriteria;
        public $PaymentCriteria;

        function __construct()
        {

        }

        static function create($data)
        {

            $BookingCriteria = new BookingCriteria();

            $BookingCriteria->sucess = $data->sucess;
            $BookingCriteria->message = $data->message;
            $BookingCriteria->Reservation = Reservation::create($data->Reservation);
            $BookingCriteria->EnquiryCriteria = EnquiryCriteria::create($data->EnquiryCriteria);
            $BookingCriteria->PaymentCriteria = Reservation::create($data->PaymentCriteria);

            return $BookingCriteria;
        }

    }

    class AvailabilityCriteria
    {

        public $listing_id;
        public $the_arrival_date;
        public $the_departure_date;
        public $establishment;
        public $rate_id;
        public $Units;

        function __construct()
        {

        }

        static function create($data)
        {

            $AvailabilityCriteria = new AvailabilityCriteria();
            $AvailabilityCriteria->listing_id = $data->listing_id;
            $AvailabilityCriteria->establishment = $data->establishment;
            $AvailabilityCriteria->the_arrival_date = $data->the_arrival_date;
            $AvailabilityCriteria->the_departure_date = $data->the_departure_date;
            $AvailabilityCriteria->rate_id = $data->rate_id;

            $Units = array();
            if ($data) {
                foreach ($data->Units as $unit) {
                    $Units[] = new Unit($unit);
                }
            }

            $AvailabilityCriteria->Units = $Units;
            return $AvailabilityCriteria;
        }

    }

    class ReservationCriteria
    {

        public $listing_id;
        public $the_arrival_date;
        public $the_departure_date;
        public $establishment;
        public $Units;
        public $ListingRoom;
        public $ListingService;

        function __construct($data)
        {
            $this->listing_id = $data->listing_id;
            $this->establishment = $data->establishment;
            $this->the_arrival_date = $data->the_arrival_date;
            $this->the_departure_date = $data->the_departure_date;
            $this->ListingRoom = new ListingRoom($data->ListingRoom);
            $this->ListingService = $data->ListingService;

            $Units = array();
            if ($data) {
                foreach ($data->Units as $unit) {
                    $Units[] = new Unit($unit);
                }
            }

            $this->Units = $Units;
        }

    }

    class Unit
    {

        public $Children;
        public $adults;

        function __construct($data)
        {

            if ($data) {

                $this->adults = $data->adults;

                $children = array();

                foreach ($data->Children as $child) {
                    $children[] = $child;
                }

                $this->Children = $children;
            }
        }

    }

    class SearchCriteria
    {

        public $listingIds;
        public $title;
        public $areaId;
        public $areaName;
        public $countryId;
        public $countryName;
        public $provinceId;
        public $provinceName;
        public $locationTypes;
        public $listingTypes;
        public $priceFrom;
        public $priceTo;
        public $specialId;
        public $checkInDate;
        public $checkOutDate;
        public $Page;
        public $perPage;
        public $coordinates;
        public $facts;
        public $facilities;
        public $activities;
        public $live;

        function __construct($data)
        {
            $this->listingIds = \S4A\Helpers::GetClean($data, 'listingIds');
            $this->live = \S4A\Helpers::GetClean($data, 'live');
            $this->title = \S4A\Helpers::GetClean($data, 'title');
            $this->areaId = \S4A\Helpers::GetClean($data, 'areaId');
            $this->countryId = \S4A\Helpers::GetClean($data, 'countryId');
            $this->provinceId = \S4A\Helpers::GetClean($data, 'provinceId');

            if ($data['listingType']) {
                if (is_array($data['listingType'])) {
                    $this->listingTypes = implode(",", $data['listingType']);
                } else {
                    $this->listingTypes = $data['listingType'];
                }
            }
            $this->priceFrom = \S4A\Helpers::GetClean($data, 'priceFrom');
            $this->priceTo = \S4A\Helpers::GetClean($data, 'priceTo');
            $this->specialId = \S4A\Helpers::GetClean($data, 'specialId');
            if ($data['checkInDate']) {
                $this->checkInDate = date("Y-m-d", strtotime(htmlspecialchars($data['checkInDate'])));
            }
            if ($data['checkOutDate']) {
                $this->checkOutDate = date("Y-m-d", strtotime(htmlspecialchars($data['checkOutDate'])));
            }
            if ($data['facts']) {
                if (is_array($data['facts'])) {
                    $this->facts = implode(",", $data['facts']);
                } else {
                    $this->facts = $data['facts'];
                }
            }
            if ($data['facilities']) {
                if (is_array($data['facilities'])) {
                    $this->facilities = implode(",", $data['facilities']);
                } else {
                    $this->facilities = $data['facilities'];
                }
            }
            if ($data['activities']) {
                if (is_array($data['activities'])) {
                    $this->activities = implode(",", $data['activities']);
                } else {
                    $this->activities = $data['activities'];
                }
            }
            $this->Page = \S4A\Helpers::GetClean($data, 'Page');
            $this->perPage = get_option('wps4a_items_per_page');

            $this->populateLocationData($data);
        }

        private function populateLocationData($data)
        {

            $location = $data['location'];

            if ($location) {

                $areaId = intval(preg_replace('/[^0-9]+/', '', $location), 0);

                $this->locationType = substr($location, 0, 1);

                switch ($this->locationType) {
                    case "c":
                        $Country = \S4A\Cache::CountryCache()[$areaId];

                        $this->countryId = $Country->id;
                        $this->countryName = $Country->v_name;

                        break;
                    case "p":

                        $Province = \S4A\Cache::ProvinceCache()[$areaId];
                        $Country = \S4A\Cache::CountryCache()[$Province->i_country_id];

                        $this->countryId = $Country->id;
                        $this->countryName = $Country->v_name;

                        $this->provinceId = $Province->id;
                        $this->provinceName = $Province->v_name;

                        break;
                    case "a":
                        $Area = \S4A\Cache::AreaCache()[$areaId];
                        $Province = \S4A\Cache::ProvinceCache()[$Area->i_province_id];
                        $Country = \S4A\Cache::CountryCache()[$Province->i_country_id];

                        $this->countryId = $Country->id;
                        $this->countryName = $Country->v_name;

                        $this->provinceId = $Province->id;
                        $this->provinceName = $Province->v_name;

                        $this->areaId = $Area->id;
                        $this->areaName = $Area->v_name;

                        break;
                }
            }
        }

    }

    class ListingSpecial
    {

        public $id;
        public $special_name;
        public $special_type_id;
        public $discount_type;
        public $from_date;
        public $to_date;
        public $amount;
        public $specialType;

        function __construct($data)
        {
            $this->id = $data->id;
            $this->special_type_id = $data->special_type_id;
            $this->discount_type = $data->discount_type;
            $this->from_date = $data->from_date;
            $this->to_date = $data->to_date;
            $this->amount = $data->amount;
            if ($data->specialType) {
                $this->specialType = new specialType($data->specialType);
            }
        }

    }

    class SpecialType
    {

        public $id;
        public $description;
        public $terms_condition;

        function __construct($data)
        {
            $this->id = $data->id;
            $this->description = $data->description;
            $this->terms_condition = $data->terms_condition;
        }

    }

    class ListingRoom
    {

        public $rate_id;
        public $description;
        public $max_adult_occupancy;
        public $max_child_occupancy;
        public $max_occupancy;
        public $price;
        public $total;
        public $mandatory_services;
        public $total_services;
        public $room_total;
        public $currency_code;
        public $code;
        public $rate;
        public $special_description;
        public $units;
        public $price_type;
        public $rate_plan_type;

        function __construct($data)
        {

            $this->rate_id = $data->rate_id;
            $this->description = $data->description;
            $this->max_adult_occupancy = $data->max_adult_occupancy;
            $this->max_child_occupancy = $data->max_child_occupancy;
            $this->max_occupancy = $data->max_occupancy;
            $this->price = $data->price;
            $this->total = $data->total;
            $this->mandatory_services = $data->mandatory_services;
            $this->total_services = $data->total_services;
            $this->room_total = $data->room_total;
            $this->currency_code = $data->currency_code;
            $this->code = $data->code;
            $this->rate = $data->rate;
            $this->special_description = $data->special_description;
            $this->units = $data->units;
            $this->price_type = $data->price_type;
            $this->rate_plan_promotion = $data->rate_plan_promotion;
        }

    }

}
