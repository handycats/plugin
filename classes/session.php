<?php

namespace S4A {

    class Session {

        const STORESESSIONKEY = "search_data";
        const USERMETAKEY = "search_criteria";
        const STOREUTMKEY = "utm_data";
        const STORECOUNTERKEY = "counter_data";
        const STOREUSERCURRENCY = 'currency_code';
        const STOREUSERSERVICES = 'user_services_data';
        const STOREPROMOKEY = 'promo_code';
        const STOREUSERBOOKINGTOTAL = 'user_booking_total_data';

        public static function get_session_info($session_field_name) {
            if (isset($_SESSION[$session_field_name])) {
                return $_SESSION[$session_field_name];
            } else {
                return false;
            }
        }

        public static function set_session_info($session_field_name, $session_field_value) {
            $_SESSION[$session_field_name] = $session_field_value;
        }

        public static function UserBookingData($SearchCriteria, $updateDatesOnly = false) {

            if (is_user_logged_in()) {

                if ($updateDatesOnly) {

                    $SearchData = get_user_meta(get_current_user_id(), Session::USERMETAKEY, true);

                    if ($SearchData) {

                        $SearchData->checkInDate = $SearchCriteria->checkInDate;
                        $SearchData->checkOutDate = $SearchCriteria->checkOutDate;

                        $SearchCriteria = $SearchData;
                    }
                }

                update_user_meta(get_current_user_id(), Session::USERMETAKEY, unserialize(serialize($SearchCriteria)));

            } else {

                if ($updateDatesOnly) {

                    $SearchData = $_SESSION[Session::STORESESSIONKEY];

                    if ($SearchData) {

                        $SearchData->checkInDate = $SearchCriteria->checkInDate;
                        $SearchData->checkOutDate = $SearchCriteria->checkOutDate;

                        $SearchCriteria = $SearchData;

                    }
                }

                Session::set_session_info(Session::STORESESSIONKEY, $SearchCriteria);
            }
        }

        public static function get_user_search_data() {

            $SearchData = new \stdClass();

            $SearchData = get_user_meta(get_current_user_id(), Session::USERMETAKEY, true);

            if (!$SearchData)
                if (isset($_SESSION[Session::STORESESSIONKEY]))
                    $SearchData = $_SESSION[Session::STORESESSIONKEY];

            $newDate = date("Y-m-d");
            $inDate = $SearchData->checkInDate;

            if($SearchData && $inDate < $newDate){
              $SearchData->checkInDate = " ";
              $SearchData->checkOutDate = " ";
            }

            return $SearchData;
        }

        public static function promo_data($prom) {

                Session::set_session_info(Session::STOREPROMOKEY, $prom);
       }

       public static function get_promo() {

               if (isset($_SESSION[Session::STOREPROMOKEY]))
                   return $_SESSION[Session::STOREPROMOKEY];

       }

        public static function utm_data($utmData){

                Session::set_session_info(Session::STOREUTMKEY, $utmData);

        }

        public static function get_utm_data() {

                if (isset($_SESSION[Session::STOREUTMKEY]))
                    return $_SESSION[Session::STOREUTMKEY];

        }

        public static function counter($data){
            if (is_user_logged_in()) {

                $counterData = get_user_meta(get_current_user_id(), Session::STORECOUNTERKEY, true);

                if ($counterData) {

                    $counterData->url = $data->url;
                    $counterData->time = $data->time;

                    $data = $counterData;

                }

                update_user_meta(get_current_user_id(), Session::STORECOUNTERKEY, unserialize(serialize($data)));

            } else {

                Session::set_session_info(Session::STORECOUNTERKEY, $data);
            }
        }

        public static function get_counter() {

            $counterData = get_user_meta(get_current_user_id(), Session::STORECOUNTERKEY, true);

            if (!$counterData)
                if (isset($_SESSION[Session::STORECOUNTERKEY]))
                    $counterData = $_SESSION[Session::STORECOUNTERKEY];

            return $counterData;
        }

        public static function remove_counter() {

            if (isset($_SESSION[Session::STORECOUNTERKEY]))
              unset($_SESSION[Session::STORECOUNTERKEY]);

            delete_user_meta( get_current_user_id(), Session::STORECOUNTERKEY );
        }

        public static function currency_data($data) {
          if (is_user_logged_in()) {

              $currencyCode = get_user_meta(get_current_user_id(), Session::STOREUSERCURRENCY, true);

              if ($currencyCode) {

                  $currencyCode['value'] = $data['value'];
                  $currencyCode['symbol'] =  $data['symbol'];
                  $currencyCode['code'] =  $data['code'];
                  $data = $currencyCode;
              }

              update_user_meta(get_current_user_id(), Session::STOREUSERCURRENCY, unserialize(serialize($data)));

          } else {

              Session::set_session_info(Session::STOREUSERCURRENCY, $data);

          }
      }

      public static function get_currency() {
          $currencyCode = get_user_meta(get_current_user_id(), Session::STOREUSERCURRENCY, true);

          if (!$currencyCode)
              if (isset($_SESSION[Session::STOREUSERCURRENCY]))
                  $currencyCode = $_SESSION[Session::STOREUSERCURRENCY];

          return $currencyCode;
     }

    public static function user_booking_services_data($id ,$data, $key) {
      //if (is_user_logged_in()){
      $service_data = array();//get_user_meta(get_current_user_id(), Session::STOREUSERSERVICES, true);
     //if ($service_data) {
      $service_data['code'] = $data[$key]['code'];
      $service_data['type'] = $data[$key]['type'];
      $service_data['price'] = $data[$key]['price'];
      $service_data['description'] = $data[$key]['description'];
      $service_data['mandatory'] = $data[$key]['mandatory'];
      $service_data['date'] = $data[$key]["date"];
      $service_data['quantity'] = $data[$key]['quantity'];
      $service_data['adults'] = $data[$key]['adults'];
      $service_data['children'] = $data[$key]['children'];
      //$data = $service_data;
      //}
      Session::set_session_array_info($id,Session::STOREUSERSERVICES, $service_data);
      //update_user_meta(get_current_user_id(), Session::STOREUSERSERVICES, unserialize(serialize($data)));
      //} else {
      // Session::set_session_array_info($key,Session::STOREUSERSERVICES, $data);
      //}
    }
    public static function set_session_array_info($id,$session_field_name, $session_field_value){
       if(!isset($_SESSION[$session_field_name])){
         if (session_status() == PHP_SESSION_NONE) {
           session_start();
         }
         $_SESSION[$session_field_name] = array();
       }
       $_SESSION[$session_field_name][$id] = $session_field_value;
    }

     public static function get_user_booking_services_data() {
       $service_data = array();//get_user_meta(get_current_user_id(), Session::STOREUSERSERVICES, true);
       //if (!$service_data)
       if (isset($_SESSION[Session::STOREUSERSERVICES])){
          $service_data = $_SESSION[Session::STOREUSERSERVICES];
        }
       return $service_data;
     }
     public static function remove_user_service_data($id){
       if(isset($_SESSION[Session::STOREUSERSERVICES][$id])){
        unset($_SESSION[Session::STOREUSERSERVICES][$id]);
        //delete_user_meta(get_current_user_id(), Session::STOREUSERSERVICES);
      }
     }
     public static function clear_user_service_data(){
       if(isset($_SESSION[Session::STOREUSERSERVICES])){
        unset($_SESSION[Session::STOREUSERSERVICES]);
        $_SESSION[Session::STOREUSERSERVICES] = array();
        //delete_user_meta(get_current_user_id(), Session::STOREUSERSERVICES);
      }
    }
    public static function set_session_total_info($session_field_name, $session_field_value){
      if(session_status() == PHP_SESSION_NONE) {
        session_start();
      }
      $_SESSION[$session_field_name] = $session_field_value;
    }
    public static function user_booking_total_data($total) {
      Session::set_session_total_info(Session::STOREUSERBOOKINGTOTAL, $total);
    }
    public static function get_user_booking_total_data() {
      $service_data = null;
      if(isset($_SESSION[Session::STOREUSERBOOKINGTOTAL])){
         $service_data = $_SESSION[Session::STOREUSERBOOKINGTOTAL];
       }
      return $service_data;
    }
    public static function clear_user_booking_total_data(){
      if(isset($_SESSION[Session::STOREUSERBOOKINGTOTAL])){
       unset($_SESSION[Session::STOREUSERBOOKINGTOTAL]);
     }
   }

  }

}
