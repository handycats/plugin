<?php

$vp = new S4A_VirtualPages();

$search_url = "/" . S4A_URL_BASE . "/search/";
$listing_url = "/" . S4A_URL_BASE . "/listing/";
$reservation_url = "/" . S4A_URL_BASE . "/reservation/";
$payment_url = "/" . S4A_URL_BASE . "/payment/";
$enquiry_url = "/" . S4A_URL_BASE . "/enquiry/";
$account_companions_url = "/" . S4A_URL_BASE . "/account/companions/";
$account_favorites_url = "/" . S4A_URL_BASE . "/account/favorites/";
$account_alerts_url = "/" . S4A_URL_BASE . "/account/alerts/";
$account_bookings_url = "/" . S4A_URL_BASE . "/account/bookings/";
$account_enquiries_url = "/" . S4A_URL_BASE . "/account/enquiries/";
$account_vouchers_url = "/" . S4A_URL_BASE . "/account/vouchers/";
$account_url = "/" . S4A_URL_BASE . "/account/";
$map_url = "/" . S4A_URL_BASE . "/map/";
$csv_url = "/" . S4A_URL_BASE . "/csv/";
$results_url = "/" . S4A_URL_BASE . "/";

define('S4A_RESULTS_URL', site_url() . $results_url);
define('S4A_SEARCH_URL', site_url() . $search_url);
define('S4A_LISTING_URL', site_url() . $listing_url);
define('S4A_RESERVATION_URL', site_url() . $reservation_url);
define('S4A_PAYMENT_URL', site_url() . $payment_url);
define('S4A_ENQUIRY_URL', site_url() . $enquiry_url);
define('S4A_ACCOUNT_COMPANIONS_URL', site_url() . $account_companions_url);
define('S4A_ACCOUNT_FAVORITES_URL', site_url() . $account_favorites_url);
define('S4A_ACCOUNT_ALERTS_URL', site_url() . $account_alerts_url);
define('S4A_ACCOUNT_BOOKINGS_URL', site_url() . $account_bookings_url);
define('S4A_ACCOUNT_ENQUIRIES_URL',site_url() . $account_enquiries_url);
define('S4A_ACCOUNT_VOUCHERS_URL', site_url() . $account_vouchers_url);
define('S4A_ACCOUNT_URL', site_url() . $account_url);

$vp->add('#' . $search_url . '#i', 'S4A_Search');
$vp->add('#' . $listing_url . '([a-z0-9\-]*)/([0-9]*)#i', 'S4A_Listing');
$vp->add('#' . $reservation_url . '([a-z0-9\-]*)/([0-9]*)#i', 'S4A_Reservation');
$vp->add('#' . $enquiry_url . '([a-z0-9\-]*)/([0-9]*)#i', 'S4A_Enquiry');
$vp->add('#' . $payment_url . '([a-z0-9\-]*)/([0-9]*)#i', 'S4A_Payment');
$vp->add('#' . $account_companions_url . '#i', 'S4A_Account_Companions');
$vp->add('#' . $account_favorites_url . '#i', 'S4A_Account_Favorites');
$vp->add('#' . $account_alerts_url . '#i', 'S4A_Account_Alerts');
$vp->add('#' . $account_bookings_url . '#i', 'S4A_Account_Bookings');
$vp->add('#' . $account_enquiries_url . '#i', 'S4A_Account_Enquiries');
$vp->add('#' . $account_vouchers_url . '#i', 'S4A_Account_Vouchers');
$vp->add('#' . $account_url . '#i', 'S4A_Account');
$vp->add('#' . $map_url . '#i', 'S4A_Map');
$vp->add('#' . $csv_url . '#i', 'S4A_CSV');

$vp->add('#' . $results_url . '([a-z\-]*)/([a-z\-]*)/([a-z\-]*)/([a-z\-]*)/([0-9]*)#i', 'S4A_Type');
$vp->add('#' . $results_url . '([a-z\-]*)/([a-z\-]*)/([a-z\-]*)/([0-9]*)#i', 'S4A_Area');
$vp->add('#' . $results_url . '([a-z\-]*)/([a-z\-]*)/([0-9]*)#i', 'S4A_Province');
$vp->add('#' . $results_url . '([a-z\-]*)/([0-9]*)#i', 'S4A_Country');
$vp->add('#/' . S4A_URL_BASE . '/?#i', 'S4A_Results');
$vp->add('#/' . S4A_URL_BASE . '/$#i', 'S4A_Results');
$vp->add('#/' . S4A_URL_BASE . '$#i', 'S4A_Results');
