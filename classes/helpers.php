<?php

namespace S4A {

  class Helpers
  {

    public static function Throw404() {
      \S4A\Helpers::LogError("Not Found");
      global $wp_query;
      $wp_query->set_404();
      add_action( 'wp_title', function () {
          return 'Page not found - 404';
      }, 9999 );
      status_header( 404 );
      nocache_headers();
      require get_404_template();
      //wp_redirect( home_url(), 301 );
      exit;
    }

    public static function LogError($message) {
      if(S4A_ENVIRONMENT == "production"){
          global $bugsnagWordpress;
          if($bugsnagWordpress)
              $bugsnagWordpress->notifyException(new \RuntimeException(json_encode($message)));
      }
    }

    public static function GetCurrency($code) {

      $currencies = array(
        "ZAR" => "R",
        "ZMW" => "ZK",
        "USD" => "$",
        "GBP" => "£",
        "EUR" => "€",
        "ZWD" => "Z$",
        "NAD" => "N$",
        "BWP" => "‎P",
        "AUD" => "A$",
        "CNY" => "¥"
      );

      $return = $currencies[$code];

      return $return;

    }

    public static function DoPrice($currency, $amount) {

      $Currency = \S4A\Helpers::GetCurrency($currency);
      $value = \S4A\Cache::CurrencyCache();
      $total =  ($value[$currency] != 0) ? $amount / $value[$currency] : $amount;

      if($result = Session::get_currency()) {

        $Currency = $result['symbol'];
        $amount = $total * $result['value'];

      }

      return "<span class='price_wrapper'><span class='currency_code' data-value=\"".$Currency."\"  >". $Currency . "</span><span class='value' data-value=\"".$total."\" >". number_format($amount) . "</span></span>";

    }

    public static function mapIcons($feature)
    {
      if((strpos($feature, 'Car') !== false) || (strpos($feature, 'Drive') !== false) || (strpos($feature, 'Drives') !== false))
      {
        return '<i class="fa-li fa fa-car"></i>';
      }
      else if((strpos($feature, 'Meals') !== false) || (strpos($feature, 'Food') !== false) || (strpos($feature, 'Breakfast') !== false) || (strpos($feature, 'Dinner') !== false))
      {
        return '<i class="fa-li fa fa-cutlery"></i>';
      }
      else if((strpos($feature, 'Accommodation') !== false)){
        return '<i class="fa-li fa fa-home"></i>';
      }
      else if((strpos($feature, 'TV') !== false) || (strpos($feature, 'DSTV') !== false))
      {
        return '<i class="fa-li fa fa-tv"></i>';
      }
      else if((strpos($feature, 'Telephone') !== false) || (strpos($feature, 'Calls') !== false))
      {
        return '<i class="fa-li fa fa-phone"></i>';
      }
      else if((strpos($feature, 'Shower') !== false))
      {
        return '<i class="fa-li fa fa-shower"></i>';
      }
      else if((strpos($feature, 'Walks') !== false))
      {
        return '<i class="fa-li fa fa-blind"></i>';
      }
      else if((strpos($feature, 'Fireplace') !== false))
      {
        return '<i class="fa-li fa fa-fire"></i>';
      }
      else if((strpos($feature, 'Bath') !== false))
      {
        return '<i class="fa-li fa fa-bath"></i>';
      }
      else if((strpos($feature, 'Drinks') !== false) || (strpos($feature, 'Beverages') !== false))
      {
        return '<i class="fa-li fa fa-glass"></i>';
      }
      else if((strpos($feature, 'Tea') !== false) || (strpos($feature, 'Coffee') !== false))
      {
        return '<i class="fa-li fa fa-coffee"></i>';
      }
      else{
        return '<i class="fa-li fa fa-check-square"></i>';
      }
    }

    public static function AlphaNumeric($text)
    {
      return preg_replace("/[^0-9a-zA-Z ]/", "", $text);
    }

    public static function Alpha($text)
    {
      return preg_replace('/[0-9]+/', '', $text);
    }

    public static function Numeric($text)
    {
      return preg_replace('/[a-zA-Z]+/', '', $text);
    }

    public static function Html2text($html, $numchars)
    {
      // Remove the HTML tags
      $html = strip_tags($html);
      // Convert HTML entities to single characters
      $html = html_entity_decode($html, ENT_QUOTES, 'UTF-8');
      // Make the string the desired number of characters
      // Note that substr is not good as it counts by bytes and not characters
      $html = mb_substr($html, 0, $numchars, 'UTF-8');
      // Add an elipsis
      $html .= "…";
      return $html;
    }

    public static function RoomAvailable($room, $AvailabilityCriteria){

      $availible = true;

      if($AvailabilityCriteria->Units){
        foreach ($AvailabilityCriteria->Units as $unit) {
          if($unit->adults > $room->i_max_adult_occupancy)
          $availible = false;
          if(count($unit->Children) > $room->i_max_child_occupancy)
          $availible = false;
        }
      }

      if($room)
      return $availible;
    }

    public static function CalculateDiscount($new, $old)
    {
      if ($new && $old)
      return round((1 - $new / $old) * 100) . '%';
      return 'Great Value';
    }

    public static function AddMetaDescription($description)
    {
      $description = Helpers::Html2text($description, 160);
      add_action('wp_head', function () use ($description) {
        echo '<meta name="description" content="' . $description . '" />' . "\n";
      }, 2);
    }

    public static function SEF($str)
    {
      return strtolower(trim(preg_replace('/[^a-zA-Z0-9\/]+/', '-', $str), '-'));
    }

    public static function trim_text($input, $length, $ellipses = true, $strip_html = true)
    {
      if ($strip_html) {
        $input = strip_tags($input);
      }

      if (strlen($input) <= $length) {
        return $input;
      }

      $last_space = strrpos(substr($input, 0, $length), ' ');
      $trimmed_text = substr($input, 0, $last_space);

      if ($ellipses) {
        $trimmed_text .= '...';
      }

      return $trimmed_text;
    }

    public static function EnquiryUrl($title, $listingId)
    {
      return S4A_ENQUIRY_URL
      . Helpers::SEF($title) . "/" . $listingId . "/";
    }

    public static function ReservationUrl($title, $listingId)
    {
      return S4A_RESERVATION_URL
      . Helpers::SEF($title) . "/" . $listingId . "/";
    }

    public static function PaymentUrl($title, $listingId, $reservation_id)
    {
      return S4A_PAYMENT_URL
      . Helpers::SEF($title) . "/" . $listingId . "/?reservation_id=" . $reservation_id;
    }

    public static function ListingUrl($title, $listingId)
    {
      return S4A_LISTING_URL
      . Helpers::SEF($title) . "/" . $listingId . "/";
    }

    public static function GetWpAreaPage($areaId, $type)
    {
      $wpId = get_option(S4A_AREA_OPTION . $type . $areaId, false);

      if(!$wpId)
      return null;

      return get_post($wpId);
    }

    public static function GetURLId($url)
    {

      $urlPath = strtok($url, '?');
      $urlArray = explode("/", $urlPath);
      $arrayFilter = array_filter($urlArray);

      return (int)end($arrayFilter);
    }

    public static function GetURLType($url)
    {

      $urlPath = strtok($url, '?');
      $urlArray = explode("/", $urlPath);
      $arrayFilter = array_filter($urlArray);
      $ListingType = str_replace("-", " ", $arrayFilter[2]);
      $ListingTypes = \S4A\Cache::InitialisationDataCache()->ListingTypes;

      $neededObject = array_filter(
        $ListingTypes,
        function ($e) use (&$ListingType) {
          return strtolower($e->v_name) == $ListingType;
        }
      );

      return array_values($neededObject)[0]->id;
    }

    public static function BuildBreadcrumbs($countryId = null, $provineId = null, $areaId = null, $listing = null)
    {

      $breadcrumbs = array();

      if ($countryId) {
        $countryData['location'] = "c" . $countryId;
        $countrySearchCriteria = new \S4A\Structs\SearchCriteria($countryData);
        if (!$listing && !$provineId) {
          $countryBreadcrumb = $countrySearchCriteria->countryName;
        } else {
          $countryBreadcrumb['name'] = $countrySearchCriteria->countryName;
          $countryBreadcrumb['url'] = \S4A\Helpers::BuildResultsUrl($countrySearchCriteria);
        }
        $breadcrumbs[] = $countryBreadcrumb;
      }

      if ($provineId) {
        $provineData['location'] = "p" . $provineId;
        $provineSearchCriteria = new \S4A\Structs\SearchCriteria($provineData);
        if (!$listing && !$areaId) {
          $provineBreadcrumb = $provineSearchCriteria->provinceName;
        } else {
          $provineBreadcrumb['name'] = $provineSearchCriteria->provinceName;
          $provineBreadcrumb['url'] = \S4A\Helpers::BuildResultsUrl($provineSearchCriteria);
        }
        $breadcrumbs[] = $provineBreadcrumb;
      }

      if ($areaId) {
        $areaData['location'] = "a" . $areaId;
        $areaSearchCriteria = new \S4A\Structs\SearchCriteria($areaData);
        if (!$listing) {
          $areaBreadcrumb = $areaSearchCriteria->areaName;
        } else {
          $areaBreadcrumb['name'] = $areaSearchCriteria->areaName;
          $areaBreadcrumb['url'] = \S4A\Helpers::BuildResultsUrl($areaSearchCriteria);
        }
        $breadcrumbs[] = $areaBreadcrumb;
      }

      if ($listing)
      $breadcrumbs[] = $listing;

      return $breadcrumbs;
    }

    public static function ResultsUrl($countryId = null, $provineId = null, $areaId = null)
    {

      if ($areaId) {
        $data['location'] = "a" . $areaId;
      } else if ($provineId) {
        $data['location'] = "p" . $provineId;
      } else if ($countryId) {
        $data['location'] = "c" . $countryId;
      }
      $SearchCriteria = new \S4A\Structs\SearchCriteria($data);

      return \S4A\Helpers::BuildResultsUrl($SearchCriteria);
    }

    public static function BuildResultsUrl($SearchCriteria, $Page = null)
    {
      if ($SearchCriteria->areaId) {
        $location .= $SearchCriteria->areaName . "/";
        $location .= $SearchCriteria->provinceName . "/";
        $location .= $SearchCriteria->countryName . "/";
        $location .= $SearchCriteria->areaId . "/";
      } else if ($SearchCriteria->provinceId) {
        $location .= $SearchCriteria->provinceName . "/";
        $location .= $SearchCriteria->countryName . "/";
        $location .= $SearchCriteria->provinceId . "/";
      } else if ($SearchCriteria->countryId) {
        $location .= $SearchCriteria->countryName . "/";
        $location .= $SearchCriteria->countryId . "/";
      }

      $urlParams = array();

      if ($Page)
      $urlParams["Page"] = $Page;

      if ($SearchCriteria->title)
      $urlParams["title"] = $SearchCriteria->title;

      if ($SearchCriteria->listingTypes) {
        $listingTypesArray = explode(",", $SearchCriteria->listingTypes);

        $listingTypes = array();

        foreach ($listingTypesArray as $listingType) {
          array_push($listingTypes, $listingType);
        }

        $urlParams["listingType"] = $listingTypes;
      }

      if ($SearchCriteria->priceFrom)
      $urlParams["priceFrom"] = $SearchCriteria->priceFrom;

      if ($SearchCriteria->priceTo)
      $urlParams["priceTo"] = $SearchCriteria->priceTo;

      if ($SearchCriteria->specialId)
      $urlParams["specialId"] = $SearchCriteria->specialId;

      if ($SearchCriteria->checkInDate)
      $urlParams["checkInDate"] = $SearchCriteria->checkInDate;

      if ($SearchCriteria->checkOutDate)
      $urlParams["checkOutDate"] = $SearchCriteria->checkOutDate;

      if ($SearchCriteria->facts)
      $urlParams["facts"] = $SearchCriteria->facts;

      if ($SearchCriteria->facilities)
      $urlParams["facilities"] = $SearchCriteria->facilities;

      if ($SearchCriteria->activities)
      $urlParams["activities"] = $SearchCriteria->activities;

      if ($SearchCriteria->live)
      $urlParams["live"] = $SearchCriteria->live;

      $http_build_query = http_build_query($urlParams, null, '&');
      $queryString = str_replace("%5D", "]", str_replace("%5B", "[", $http_build_query));


      return S4A_RESULTS_URL . Helpers::SEF($location) . (($urlParams) ? "?" : null) . $queryString;
    }

    public static function CreateGetUser($email, $user_name = null, $last_name = null)
    {

      $user = get_user_by('email', $email);

      if (!$user) {

        if (!S4A_DISABLE_WPUSER || S4A_DISABLE_WPUSER == "false" ) {

          $user_pass = wp_generate_password();

          $newUser = array(
            'user_login' => $email,
            'user_pass' => $user_pass,
            'user_email' => $email,
            'first_name' => $user_name,
            'last_name' => $last_name
          );

          $user_id = wp_insert_user($newUser);

          wp_new_user_notification($user_id, $user_pass);

          if (S4A_LOGIN) {

            $creds = array(
              'user_login' => $email,
              'user_password' => $user_pass,
              'remember' => true
            );

            $user = wp_signon($creds, false);
          } else {
            $user = get_user_by('email', $email);
          }
        } else {
          $user = array(
            'user_login' => $email,
            'user_email' => $email
          );
        }
      }

      return $user->data;
    }

    public static function CleanQuotes($value)
    {
      return htmlspecialchars($value, ENT_QUOTES);
    }

    public static function GetClean($array, $value)
    {
      return htmlspecialchars(trim($array[$value]));
    }

    public static function Clean($value)
    {
      return htmlspecialchars(trim($value));
    }

    public static function add_update_option($option_name, $new_value)
    {
      $allok = false;
      if (get_option($option_name) == $new_value) {
        return "same";
      } else {
        $updateok = update_option($option_name, $new_value);
      }
      if ($updateok) {
        return "updateok";
      } else {
        $deprecated = ' ';
        $autoload = 'no';
        $addok = add_option($option_name, $new_value, $deprecated, $autoload);
      }
      if ($addok) {
        return "addok";
      } else {
        return "notok";
      }
    }

    public static function AccountNavigation()
    {

      $navItemsArray = [];

      $ProfileArray = [];
      $ProfileArray["name"] = "My Profile";
      $ProfileArray["icon"] = "user";
      $ProfileArray["url"] = S4A_ACCOUNT_URL;
      $navItemsArray["Profile"] = $ProfileArray;

      $CompanionsArray = [];
      $CompanionsArray["name"] = "My Companions";
      $CompanionsArray["icon"] = "users";
      $CompanionsArray["url"] = S4A_ACCOUNT_COMPANIONS_URL;
      $navItemsArray["Companions"] = $CompanionsArray;

      $FavoritesArray = [];
      $FavoritesArray["name"] = "My Favorites";
      $FavoritesArray["icon"] = "star";
      $FavoritesArray["url"] = S4A_ACCOUNT_FAVORITES_URL;
      $navItemsArray["Favorites"] = $FavoritesArray;

      $HistoryArray = [];
      $HistoryArray["name"] = "My Bookings";
      $HistoryArray["icon"] = "calendar";
      $HistoryArray["url"] = S4A_ACCOUNT_BOOKINGS_URL;
      $navItemsArray["History"] = $HistoryArray;

      $EnquiriesArray = [];
      $EnquiriesArray["name"] = "My Enquiries";
      $EnquiriesArray["icon"] = "calendar";
      $EnquiriesArray["url"] = S4A_ACCOUNT_ENQUIRIES_URL;
      $navItemsArray["Enquiries"] = $EnquiriesArray;

      return $navItemsArray;
    }

    public static function RenderTemplate($v, $title, $template, $model, $vbody = null, $pageTemplate = null, $wrap = null) {

      ob_start();

      require_once( S4A_DIR_PATH . 'templates/shared/header.php' );

      $Template = get_stylesheet_directory() . S4A_DIR_PATH . '/templates/' . $template . '.php';
      require_once( file_exists($Template) ? $Template : S4A_DIR_PATH . 'templates/' . $template . '.php' );

      require_once( S4A_DIR_PATH . 'templates/shared/footer.php' );

      $body .= ob_get_contents();

      ob_end_clean();

      $v->title = $title;
      $v->body = nl2br($vbody) . $body;

      $v->template = ($pageTemplate && $pageTemplate != S4A_NONE_KEY) ? str_replace(".php", "", $pageTemplate) : 'page';
    }
  }
}
