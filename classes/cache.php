<?php

namespace S4A {

    require_once(S4A_DIR_PATH . '/includes/phpFastCache/phpFastCache.php');

    class Cache {

        const CACHEDURATION = 7200;
        const INITIALISATIONDATACACHEKEY = "INITIALISATIONDATACACHE";
        const LOCATIONREDIRECTCACHEKEY = "LOCATIONREDIRECTCACHE";
        const LISTINGREDIRECTCACHEKEY = "LISTINGREDIRECTCACHEKEY";
        const COUNTRYCACHEKEY = "COUNTRYCACHE";
        const PROVINCECACHEKEY = "PROVINCECACHE";
        const AREACACHEKEY = "AREACACHE";
        const CURRENCYCACHEKEY = "CURRENCYCACHE";

        private static $InitialisationDataCache;
        private static $CountryCache;
        private static $ProvinceCache;
        private static $AreaCache;
        private static $LocationRedirectCache;
        private static $ListingRedirectCache;
        private static $CurrencyCache;

        static function InitialisationDataCache() {

            if (!Cache::$InitialisationDataCache) {

                Cache::$InitialisationDataCache = Cache::Get(Cache::INITIALISATIONDATACACHEKEY);

                if (!Cache::$InitialisationDataCache || isset($_GET['s4a_plugin']) || S4A_DISABLECACHE) {

                    $InitialisationDataCache = Service::get_InitialisationData();

                    Cache::$InitialisationDataCache = $InitialisationDataCache;

                    Cache::Put(Cache::INITIALISATIONDATACACHEKEY, $InitialisationDataCache, Cache::CACHEDURATION);
                }
            }

            return Cache::$InitialisationDataCache;
        }
        static function CurrencyCache() {

            if (!Cache::$CurrencyCache) {

                Cache::$CurrencyCache = Cache::Get(Cache::CURRENCYCACHEKEY);

                if (!Cache::$CurrencyCache || isset($_GET['s4a_plugin']) || S4A_DISABLECACHE) {

                      $CurrencyCache = array();

                      foreach(\S4A\Cache::InitialisationDataCache()->Currency as $currency) {
                          $CurrencyCache[$currency->code] = $currency->value;
                      }

                    Cache::$CurrencyCache = $CurrencyCache;

                    Cache::Put(Cache::CURRENCYCACHEKEY, $CurrencyCache, Cache::CACHEDURATION);
                }
            }

            return Cache::$CurrencyCache;
        }

        static function ListingRedirectCache() {

          if(!Cache::$ListingRedirectCache){

            Cache::$ListingRedirectCache = Cache::Get(Cache::LISTINGREDIRECTCACHEKEY);

                if (!Cache::$ListingRedirectCache || isset($_GET['s4a_plugin']) || S4A_DISABLECACHE) {

                  $listingRedirect = array();

                  $initDataCache = Cache::InitialisationDataCache()->ListingName;

                  foreach ($initDataCache as $listing){

                    $data['listing'] = "l" . $listing->id;
                    $postID = get_option(S4A_AREA_OPTION . $data['listing'], '');

                    if ($postID > 0){
                      $listingRedirect[$listing->id] = get_permalink($postID);

                      }
                    }

                    Cache::$ListingRedirectCache = $listingRedirect;

                    Cache::Put(Cache::LISTINGREDIRECTCACHEKEY, $listingRedirect, Cache::CACHEDURATION);

                  }
                }

          return Cache::$ListingRedirectCache ;
        }
        static function LocationRedirectCache() {

            if (!Cache::$LocationRedirectCache) {

                Cache::$LocationRedirectCache = Cache::Get(Cache::LOCATIONREDIRECTCACHEKEY);

                if (!Cache::$LocationRedirectCache || isset($_GET['s4a_plugin']) || S4A_DISABLECACHE) {

                    $redirectsArry = array();

                    $InitialisationDataCache = Cache::InitialisationDataCache()->Locations;


                    foreach ($InitialisationDataCache as $countries) {

                        $data['location'] = "c" . $countries->id;
                        $postID = get_option(S4A_AREA_OPTION . $data['location'], '');

                        if ($postID > 0) {
                            $redirectsArry[$postID] = \S4A\Helpers::BuildResultsUrl(new \S4A\Structs\SearchCriteria($data));
                        }

                        foreach ($countries->province_area as $province) {

                            $data['location'] = "p" . $province->id;
                            $postID = get_option(S4A_AREA_OPTION . $data['location'], '');

                            if ($postID > 0) {
                                $redirectsArry[$postID] = \S4A\Helpers::BuildResultsUrl(new \S4A\Structs\SearchCriteria($data));
                            }

                            foreach ($province->area as $area) {

                                $data['location'] = "a" . $area->id;
                                $postID = get_option(S4A_AREA_OPTION . $data['location'], '');

                                if ($postID > 0) {
                                    $redirectsArry[$postID] = \S4A\Helpers::BuildResultsUrl(new \S4A\Structs\SearchCriteria($data));
                                }
                            }
                        }
                    }

                    Cache::$LocationRedirectCache = $redirectsArry;

                    Cache::Put(Cache::LOCATIONREDIRECTCACHEKEY, $redirectsArry, Cache::CACHEDURATION);
                }
            }

            return Cache::$LocationRedirectCache;
        }

        static function CountryCache() {

            if (!Cache::$CountryCache) {

                Cache::$CountryCache = Cache::Get(Cache::COUNTRYCACHEKEY);

                if (!Cache::$CountryCache || isset($_GET['s4a_plugin']) || S4A_DISABLECACHE) {

                    $InitialisationDataCache = Cache::InitialisationDataCache();

                    $Countries = array();

                    foreach ($InitialisationDataCache->Locations as $Location) {
                        $Countries[$Location->id] = $Location;
                    }

                    Cache::$CountryCache = $Countries;

                    Cache::Put(Cache::COUNTRYCACHEKEY, $Countries, Cache::CACHEDURATION);
                }
            }

            return Cache::$CountryCache;
        }

        static function ProvinceCache() {

            if (!Cache::$ProvinceCache) {

                Cache::$ProvinceCache = Cache::Get(Cache::PROVINCECACHEKEY);

                if (!Cache::$ProvinceCache || isset($_GET['s4a_plugin']) || S4A_DISABLECACHE) {

                    $InitialisationDataCache = Cache::InitialisationDataCache();

                    $Provinces = array();

                    foreach ($InitialisationDataCache->Locations as $Location) {
                        if ($Location->province_area) {
                            foreach ($Location->province_area as $Province) {
                                $Provinces[$Province->id] = $Province;
                            }
                        }
                    }

                    Cache::$ProvinceCache = $Provinces;

                    Cache::Put(Cache::PROVINCECACHEKEY, $Provinces, Cache::CACHEDURATION);
                }
            }

            return Cache::$ProvinceCache;
        }

        static function AreaCache() {

            if (!Cache::$AreaCache) {

                Cache::$AreaCache = Cache::Get(Cache::AREACACHEKEY);

                if (!Cache::$AreaCache || isset($_GET['s4a_plugin']) || S4A_DISABLECACHE) {

                    $InitialisationDataCache = Cache::InitialisationDataCache();

                    $Areas = array();

                    foreach ($InitialisationDataCache->Locations as $Location) {
                        if ($Location->province_area) {
                            foreach ($Location->province_area as $Province) {
                                if ($Province->area) {
                                    foreach ($Province->area as $Area) {
                                        $Areas[$Area->id] = $Area;
                                    }
                                }
                            }
                        }
                    }

                    Cache::$AreaCache = $Areas;

                    Cache::Put(Cache::AREACACHEKEY, $Areas, Cache::CACHEDURATION);
                }
            }

            return Cache::$AreaCache;
        }

        static function get_ListingByIdCache($id) {

            if (S4A_DISABLECACHE || S4A_DISABLESEARCHCACHE)
                return Service::get_ListingById($id);

            $ListingCacheKey = Cache::ListingCacheKey($id);

            $listing = \S4A\Cache::Get($ListingCacheKey);

            if (!$listing || isset($_GET['s4a_plugin'])) {

                $listing = Service::get_ListingById($id);

                \S4A\Cache::Put($ListingCacheKey, $listing, Cache::CACHEDURATION);
            }

            return $listing;
        }

        static function get_ListingBySearchCache($searchCriteria) {

            if (S4A_DISABLECACHE || S4A_DISABLESEARCHCACHE)
                return Service::get_ListingBySearch($searchCriteria);

            $SearchCriteriaCacheKey = \S4A\Cache::SearchCriteriaCacheKey($searchCriteria);

            $results = \S4A\Cache::Get($SearchCriteriaCacheKey);

            if (!$results || isset($_GET['s4a_plugin'])) {

                $results = Service::get_ListingBySearch($searchCriteria);

                if ($results->total > 0)
                \S4A\Cache::Put($SearchCriteriaCacheKey, $results, Cache::CACHEDURATION);
            }

            return $results;
        }

        static function Put($key, $data, $duration) {
            $cache = \phpFastCache();
            $cache->set($key, $data, $duration);
        }

        static function Get($key) {
            $cache = \phpFastCache();
            return $cache->get($key);
        }

        static function ListingCacheKey($id) {
            $key = "ListingCacheKey_";
            $key .= $id;

            return $key;
        }

        static function SearchCriteriaCacheKey($SearchCriteria) {

            $key = "SearchCriteriaCacheKey_";
            $key .= "title" . $SearchCriteria->title;
            $key .= "areaId" . $SearchCriteria->areaId;
            $key .= "countryId" . $SearchCriteria->countryId;
            $key .= "provinceId" . $SearchCriteria->provinceId;
            $key .= "listingTypes" . $SearchCriteria->listingTypes;
            $key .= "priceFrom" . $SearchCriteria->priceFrom;
            $key .= "priceTo" . $SearchCriteria->priceTo;
            $key .= "specialId" . $SearchCriteria->specialId;
            $key .= "checkInDate" . $SearchCriteria->checkInDate;
            $key .= "checkOutDate" . $SearchCriteria->checkOutDate;
            $key .= "Page" . $SearchCriteria->Page;
            $key .= "perPage" . $SearchCriteria->perPage;

            if ($SearchCriteria->listingIds) {
                foreach ($SearchCriteria->listingIds as $listingIds) {
                    $key .= "listingIds" . $listingIds;
                }
            }

            return $key;
        }

    }

}
