<?php

/**
 * Plugin Name: Sleep Easy 365
 * Plugin URI: https://www.sleepeasy365.com/
 * Description: Advanced Channel Aggregator.
 * Version: 1.1
 * Author: HandyCats
 * License: GPL v3
 * Requires at least: 4.1
 * Text Domain: S4A
 * Domain Path: /languages
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

$S4A_ASWOME = "true";
$S4A_CACHE_KEY = "1774";

define('S4A_DIR_PATH', plugin_dir_path(__FILE__));
define('S4A_DIR_URL', plugin_dir_url(__FILE__));
define('S4A_DIR_PATH_OPERATORS', S4A_DIR_PATH . '/operators/');

try {

require_once( S4A_DIR_PATH . '/classes/login.php' );
require_once( S4A_DIR_PATH . '/classes/constants.php' );
require_once( S4A_DIR_PATH . '/classes/session.php' );
require_once( S4A_DIR_PATH . '/classes/meta.php' );
require_once( S4A_DIR_PATH . '/classes/cache.php' );
require_once( S4A_DIR_PATH . '/classes/build.php' );
require_once( S4A_DIR_PATH . '/classes/define.php' );
require_once( S4A_DIR_PATH . '/classes/helpers.php' );
require_once( S4A_DIR_PATH . '/classes/service.php' );
require_once( S4A_DIR_PATH . '/classes/virtual_pages.php' );
require_once( S4A_DIR_PATH . '/classes/routing.php' );
require_once( S4A_DIR_PATH . '/classes/controls.php' );
require_once( S4A_DIR_PATH . '/classes/structs.php' );
require_once( S4A_DIR_PATH . '/classes/options.php' );
require_once( S4A_DIR_PATH . '/classes/ajax.php' );
require_once( S4A_DIR_PATH . '/classes/initialise.php' );
require_once( S4A_DIR_PATH . '/classes/widgets.php' );
require_once( S4A_DIR_PATH . '/classes/verifyEmail.php' );

require_once( S4A_DIR_PATH . '/controllers/account.php' );
require_once( S4A_DIR_PATH . '/controllers/account_alerts.php' );
require_once( S4A_DIR_PATH . '/controllers/account_companions.php' );
require_once( S4A_DIR_PATH . '/controllers/account_favorites.php' );
require_once( S4A_DIR_PATH . '/controllers/account_bookings.php' );
require_once( S4A_DIR_PATH . '/controllers/account_enquiries.php' );
require_once( S4A_DIR_PATH . '/controllers/account_vouchers.php' );
require_once( S4A_DIR_PATH . '/controllers/listing.php' );
require_once( S4A_DIR_PATH . '/controllers/map.php' );
require_once( S4A_DIR_PATH . '/controllers/csv.php' );
require_once( S4A_DIR_PATH . '/controllers/reservation.php' );
require_once( S4A_DIR_PATH . '/controllers/payment.php' );
require_once( S4A_DIR_PATH . '/controllers/enquiry.php' );
require_once( S4A_DIR_PATH . '/controllers/results.php' );
require_once( S4A_DIR_PATH . '/controllers/search.php' );

/* Operator */

require_once( S4A_DIR_PATH_OPERATORS . '/classes/cache.php' );
require_once( S4A_DIR_PATH_OPERATORS . '/classes/controls.php' );
require_once( S4A_DIR_PATH_OPERATORS . '/classes/helpers.php' );
require_once( S4A_DIR_PATH_OPERATORS . '/classes/routing.php' );
require_once( S4A_DIR_PATH_OPERATORS . '/classes/service.php' );
require_once( S4A_DIR_PATH_OPERATORS . '/classes/structs.php' );

require_once( S4A_DIR_PATH_OPERATORS . '/controllers/listing.php' );
require_once( S4A_DIR_PATH_OPERATORS . '/controllers/map.php' );
require_once( S4A_DIR_PATH_OPERATORS . '/controllers/enquiry.php' );
require_once( S4A_DIR_PATH_OPERATORS . '/controllers/results.php' );
require_once( S4A_DIR_PATH_OPERATORS . '/controllers/search.php' );

} catch (Exception $ex) {
    if(S4A_ENVIRONMENT == "production"){
        global $bugsnagWordpress;
        if($bugsnagWordpress)
            $bugsnagWordpress->notifyException(new \RuntimeException($ex));
    }
}
