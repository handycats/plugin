<?php

function S4A_Type($v, $url)
{
  $id = S4A\Helpers::GetURLId($url);
  $type = S4A\Helpers::GetURLType($url);
  S4A_Results($v, $url, "a" . $id, $type);
}

function S4A_TypeProvince($v, $url, $type)
{
  $id = S4A\Helpers::GetURLId($url);
  S4A_Results($v, $url, "p" . $id, $type);
}

function S4A_Area($v, $url)
{
  if ($type = S4A\Helpers::GetURLType($url)) {
    S4A_TypeProvince($v, $url, $type);
  } else {
    $id = S4A\Helpers::GetURLId($url);
    S4A_Results($v, $url, "a" . $id);
  }
}

function S4A_Province($v, $url)
{
  $id = S4A\Helpers::GetURLId($url);
  S4A_Results($v, $url, "p" . $id);
}

function S4A_Country($v, $url)
{
  $id = S4A\Helpers::GetURLId($url);
  S4A_Results($v, $url, "c" . $id);
}

function S4A_Results($v, $url, $location = null, $type = null)
{

  $model = new stdClass();

  $get = $_GET;

  $get['location'] = $location;

  $model->SearchCriteria = new S4A\Structs\SearchCriteria($get);


  if ($type)
  $model->SearchCriteria->listingTypes = $type;

  $model->UserSearchDates = S4A\Session::get_user_search_data();

  $model->ListingSearchResults = S4A\Cache::get_ListingBySearchCache($model->SearchCriteria);

  if((!$model->ListingSearchResults->Location->v_name || (strpos($url, \S4A\Helpers::SEF($model->ListingSearchResults->Location->v_name)) === false)) && !$_GET && $location)
	  \S4A\Helpers::Throw404();

  $model->Breadcrumbs = S4A\Helpers::BuildBreadcrumbs($model->ListingSearchResults->Locations->Countries->id, $model->ListingSearchResults->Locations->Province->id, $model->ListingSearchResults->Locations->Area->id);

  switch ($model->SearchCriteria->locationType) {
    case "c":
    if (!$type && $countyPage = S4A\Helpers::GetWpAreaPage($model->SearchCriteria->countryId, $model->SearchCriteria->locationType)) {
      if ($metaDescription = get_post_meta($countyPage->ID, '_yoast_wpseo_metadesc', true))
      \S4A\Helpers::AddMetaDescription($metaDescription);
      \S4A\Helpers::RenderTemplate($v, $countyPage->post_title, "results", $model, ($model->SearchCriteria->Page < 2) ? $countyPage->post_content . $body : $body, get_option('wps4a_location_template'));
    } else {
      \S4A\Helpers::RenderTemplate($v, "Results in " . $model->ListingSearchResults->Location->v_name, "results", $model, null, get_option('wps4a_location_template'));
    }
    break;
    case "p":
    if (!$type && $provincePage = S4A\Helpers::GetWpAreaPage($model->SearchCriteria->provinceId, $model->SearchCriteria->locationType)) {
      if ($metaDescription = get_post_meta($provincePage->ID, '_yoast_wpseo_metadesc', true))
      \S4A\Helpers::AddMetaDescription($metaDescription);
      \S4A\Helpers::RenderTemplate($v, $provincePage->post_title, "results", $model, ($model->SearchCriteria->Page < 2) ? $provincePage->post_content . $body : $body, get_option('wps4a_location_template'));
    } else {
      \S4A\Helpers::RenderTemplate($v, "Results in " . $model->ListingSearchResults->Location->v_name, "results", $model, null, get_option('wps4a_location_template'));
    }
    break;
    case "a":
    if (!$type && $areaPage = S4A\Helpers::GetWpAreaPage($model->SearchCriteria->areaId, $model->SearchCriteria->locationType)) {
      if ($metaDescription = get_post_meta($areaPage->ID, '_yoast_wpseo_metadesc', true))
      \S4A\Helpers::AddMetaDescription($metaDescription);
      \S4A\Helpers::RenderTemplate($v, $areaPage->post_title, "results", $model, ($model->SearchCriteria->Page < 2) ? $areaPage->post_content . $body : $body, get_option('wps4a_location_template'));
    } else {
      \S4A\Helpers::RenderTemplate($v, "Results in " . $model->ListingSearchResults->Location->v_name, "results", $model, null, get_option('wps4a_location_template'));
    }
    break;
    default:
    \S4A\Helpers::RenderTemplate($v, "Search Results", "results", $model, null, get_option('wps4a_location_template'));
    break;
  }
}
