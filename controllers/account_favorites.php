<?php
function S4A_Account_Favorites($v, $url) {

    $current_user = wp_get_current_user();

    $fav = S4A\Meta::GetFav($current_user->ID);

    if ($fav) {
        $SearchCriteria = new S4A\Structs\SearchCriteria(array());
        $SearchCriteria->perPage = 0;
        $SearchCriteria->order = "discount";
        $SearchCriteria->listingIds = $fav;
        $ListingSummeries = S4A\Cache::get_ListingBySearchCache($SearchCriteria);
    }

    $model = new stdClass();

    $model->Menu = S4A\Helpers::AccountNavigation();
    $model->Page = "My Favorites";
    $model->ListingSearchResults = $ListingSummeries;

	\S4A\Helpers::RenderTemplate($v, $model->Page, "account", $model, ($this_post = get_option("wps4a_property_my_account_page")) ? $this_post->post_content : null, get_option('wps4a_my_account_template'));
}