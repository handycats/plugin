<?php

function S4A_Reservation($v, $url) {

    $model = new stdClass();
    $model->BookingCriteria = new S4A\Structs\BookingCriteria();
    $model->Reservation = new S4A\Structs\Reservation();

    $URLiD = S4A\Helpers::GetURLId($url);

    $searchCriteria = new S4A\Structs\SearchCriteria(array());
    $searchCriteria->listingIds = [$URLiD];

    $listing = S4A\Cache::get_ListingByIdCache($URLiD);

    if (!property_exists($listing, 'Listing'))
		\S4A\Helpers::Throw404();

    $model->Listing = $listing->Listing;

    $model->AvailabilityCriteria = S4A\Structs\AvailabilityCriteria::create(json_decode(stripslashes($_POST['AvailabilityCriteria'])));

    if (!$model->AvailabilityCriteria->listing_id) {
        wp_redirect(\S4A\Helpers::ListingUrl($model->Listing->v_title, $URLiD));
        exit;
    }

    $availibleRooms = S4A\Service::get_available_rooms($model->AvailabilityCriteria);

    $reservationCriteriaData = $model->AvailabilityCriteria;
    $reservationCriteriaData->ListingService = $availibleRooms->channel->services;
    $reservationCriteriaData->ListingRoom = $availibleRooms->ListingRoom;

    if(!$availibleRooms->ListingRoom->description) {
        wp_redirect(\S4A\Helpers::ListingUrl($model->Listing->v_title, $URLiD));
        exit;
    }

    if ($reservationCriteriaData->ListingRoom->terms) {
        $model->RateTerms = explode(',', $reservationCriteriaData->ListingRoom->terms);
        $array = array_map('trim', $model->RateTerms);
        $model->RateRSA = in_array("South African Only", $array);
    }

    $model->Reservation->ReservationCriteria = new S4A\Structs\ReservationCriteria($reservationCriteriaData);

    $model->Terms = get_option('wps4a_reservation_terms_page');

    $model->Titles = array("Adv" => "Adv", "Ahl" => "Ahl", "Alha", "Alha", "Amb" => "Amb", "Barr" => "Barr", "Bishop" => "Bishop", "Capt" => "Capt", "Capt/Mrs" => "Capt/Mrs", "Chief" => "Chief", "Chief/Mrs" => "Chief/Mrs", "Col" => "Col", "Count" => "Count", "Dr" => "Dr", "Dr&Mrs" => "Dr&Mrs", "Dr/Mr" => "Dr/Mr", "Engr" => "Engr", "Exc" => "Exc", "Fam" => "Fam", "Family" => "Family", "Gen" => "Gen", "Gen&Mrs" => "Gen&Mrs", "Gov" => "Gov", "HRH" => "HRH", "HRM" => "HRM", "Hon" => "Hon", "Hon&Mrs", "Hon&Mrs", "Judge" => "Judge", "KING" => "KING", "Lady" => "Lady", "Lord" => "Lord", "Maj" => "Maj", "Mal" => "Mal", "Mast" => "Mast", "Mes", "Mes", "Min" => "Min", "Min&Mrs" => "Min&Mrs", "Miss" => "Miss", "Mr" => "Mr", "Mr/Mrs" => "Mr/Mrs", "Mr/Ms" => "Mr/Ms", "Mrs" => "Mrs", "Ms" => "Ms", "Ms/Ms" => "Ms/Ms", "Past" => "Past", "Prem" => "Prem", "Pres" => "Pres", "Prince" => "Prince", "Princess" => "Princess", "Prof" => "Prof", "Prof&Mr" => "Prof&Mr", "Prof&Mrs" => "Prof&Mrs", "Proph" => "Proph", "REV" => "REV", "Rev" => "Rev", "SAN" => "SAN", "Sen" => "Sen", "Ser" => "Ser", "Sheik" => "Sheik", "Sir" => "Sir", "Sirs" => "Sirs");

    if ($_POST['isBooking']) {
        $result = new stdClass();
        $email = S4A\Helpers::GetClean($_POST, "user_email");

        if ($email) {

            $wp_user = S4A\Helpers::CreateGetUser($email, S4A\Helpers::GetClean($_POST, 'user_name'), S4A\Helpers::GetClean($_POST, 'user_last_name'));
            if ($wp_user->ID) {
                S4A\Meta::UpdateProfile($wp_user->ID, $_POST);

                $model->BookingCriteria->Reservation = $model->Reservation;
                $EnquiryCriteria = S4A\Meta::GetProfile($wp_user->ID);

                $EnquiryCriteria->guests = $_POST['guests'];

                $EnquiryCriteria->wp_user = $wp_user;
                $EnquiryCriteria->listing_id = $model->Reservation->ReservationCriteria->listing_id;
                $EnquiryCriteria->price = $model->Reservation->ReservationCriteria->ListingRoom->price;

                $total = \S4A\Session::get_user_booking_total_data();

                if($total){
                  $EnquiryCriteria->total = $total;
                  $model->BookingCriteria->Reservation->ReservationCriteria->ListingRoom->total = $total;
                }else{
                  $EnquiryCriteria->total = $model->Reservation->ReservationCriteria->ListingRoom->total;
                }

                $EnquiryCriteria->establishment = $model->Reservation->ReservationCriteria->establishment;
                $EnquiryCriteria->room = $model->Reservation->ReservationCriteria->ListingRoom->description;
                $EnquiryCriteria->user_checkInDate = $model->Reservation->ReservationCriteria->the_arrival_date;
                $EnquiryCriteria->user_checkOutDate = $model->Reservation->ReservationCriteria->the_departure_date;
                $EnquiryCriteria->source = $_POST['source'];


                if(isset($_POST['arrival_time']) && $_POST['arrival_time'] != ''){
                  $arrival_time = $_POST['arrival_time'];

                   if(isset($EnquiryCriteria->user_special)){
                      $EnquiryCriteria->user_special = $EnquiryCriteria->user_special." ,Arrival time: ".$arrival_time;
                   }else{
                     $current_user = wp_get_current_user();
                     $profile = S4A\Meta::GetProfile($current_user->ID);

                     if(isset($profile->user_special)){
                       $profile->user_special = $profile->user_special ." ,Arrival time: ".$arrival_time;
                     }

                   }
                }

                $formData = new stdClass();
                $formData->user_special = ($model->EnquiryCriteria->user_special) ? $model->EnquiryCriteria->user_special : $profile->user_special;

                if(isset($_POST['coupon_amount']) && $_POST['coupon_amount'] != ''){
                  if($total){
                    $model->BookingCriteria->Reservation->ReservationCriteria->ListingRoom->total = $total - $_POST['coupon_amount'];
                      //$model->BookingCriteria->Reservation->ReservationCriteria->ListingRoom->room_total = $total - $_POST['coupon_amount'];
                  }else{
                    $model->BookingCriteria->Reservation->ReservationCriteria->ListingRoom->total = $model->BookingCriteria->Reservation->ReservationCriteria->ListingRoom->total - $_POST['coupon_amount'];
                    //$model->BookingCriteria->Reservation->ReservationCriteria->ListingRoom->room_total = $model->BookingCriteria->Reservation->ReservationCriteria->ListingRoom->room_total - $_POST['coupon_amount'];
                  }
                }

                if(isset($_POST['voucher_code']) && $_POST['voucher_code'] != ''){
                    $model->BookingCriteria->Reservation->ReservationCriteria->ListingRoom->coupon = $_POST['voucher_code'];
                }
                $EnquiryCriteria->voucher_code = $_POST['voucher_code'];
                $EnquiryCriteria->discount_amount = $_POST['coupon_amount'];

                $model->BookingCriteria->EnquiryCriteria = $EnquiryCriteria;
                $model->BookingCriteria->wp_user = $wp_user;

                $booking_services = \S4A\Session::get_user_booking_services_data();
                $model->BookingCriteria->ListingService =  (!empty($booking_services)) ? $booking_services : null;

                $result = S4A\Service::do_Reservation($model->BookingCriteria);

            } else {
                $result->sucess = false;
                $result->error = "An error occured while proccessing your enquiry, please try again, if this continues please contact us.";
            }
        } else {
            $result->sucess = false;
            $result->error = "Email is a required field, please fill in your email to continue.";
        }

        if ($result->sucess){
            \S4A\Session::clear_user_booking_total_data();//clear booking total
            \S4A\Session::clear_user_service_data();//clear booking service data
            \S4A\Session::remove_counter();
            $counterVars = array('url' => S4A\Helpers::PaymentUrl($model->Reservation->ReservationCriteria->establishment, $model->Reservation->ReservationCriteria->listing_id, $result->reservation_id), 'time' => (new DateTime())->format('Y-m-d H:i:s'));
            $count = new S4A\Structs\Counter($counterVars);
            \S4A\Session::counter($count);
            wp_redirect(S4A\Helpers::PaymentUrl($model->Reservation->ReservationCriteria->establishment, $model->Reservation->ReservationCriteria->listing_id, $result->reservation_id));
            die();
        }
    }

    if (!$_POST || !$result->sucess) {

        $formData = new stdClass();

        $current_user = wp_get_current_user();

        $profile = S4A\Meta::GetProfile($current_user->ID);

        $formData->user_email = ($model->EnquiryCriteria->user_email) ? $model->EnquiryCriteria->user_email : $profile->user_email;
        $formData->user_name = ($model->EnquiryCriteria->user_name) ? $model->EnquiryCriteria->user_name : $profile->user_name;
        $formData->user_last_name = ($model->EnquiryCriteria->user_last_name) ? $model->EnquiryCriteria->user_last_name : $profile->user_last_name;
        $formData->user_phone = ($model->EnquiryCriteria->user_phone) ? $model->EnquiryCriteria->user_phone : $profile->user_phone;
        $formData->user_title = ($model->EnquiryCriteria->user_title) ? $model->EnquiryCriteria->user_title : $profile->user_title;
        $formData->user_nationality = ($model->EnquiryCriteria->user_nationality) ? $model->EnquiryCriteria->user_nationality : $profile->user_nationality;
        $formData->user_resident = ($model->EnquiryCriteria->user_resident) ? $model->EnquiryCriteria->user_resident : $profile->user_resident;
        $formData->user_pension = ($model->EnquiryCriteria->user_pension) ? $model->EnquiryCriteria->user_pension : $profile->user_pension;
        $formData->user_food = ($model->EnquiryCriteria->user_food) ? $model->EnquiryCriteria->user_food : $profile->user_food;
        $formData->user_disabilities = ($model->EnquiryCriteria->user_disabilities) ? $model->EnquiryCriteria->user_disabilities : $profile->user_disabilities;
        $formData->user_flexible = ($model->EnquiryCriteria->user_flexible) ? $model->EnquiryCriteria->user_flexible : $profile->user_flexible;
        $formData->user_adults = ($model->EnquiryCriteria->user_adults) ? $model->EnquiryCriteria->user_adults : $profile->user_adults;
        $formData->user_children = ($model->EnquiryCriteria->user_children) ? $model->EnquiryCriteria->user_children : $profile->user_children;
        $formData->user_children_age = ($model->EnquiryCriteria->user_children_age) ? $model->EnquiryCriteria->user_children_age : $profile->user_children_age;
        $formData->user_special = ($model->EnquiryCriteria->user_special) ? $model->EnquiryCriteria->user_special : $profile->user_special;
        $formData->invoice_name = ($model->EnquiryCriteria->invoice_name) ? $model->EnquiryCriteria->invoice_name : $profile->invoice_name;
        $formData->invoice_tax_number = ($model->EnquiryCriteria->invoice_tax_number) ? $model->EnquiryCriteria->invoice_tax_number : $profile->invoice_tax_number;
        $formData->invoice_address = ($model->EnquiryCriteria->invoice_address) ? $model->EnquiryCriteria->invoice_address : $profile->invoice_address;
        $formData->AgentCode = $profile->AgentCode;

        $model->FormData = $formData;

        if($result->error){
          \S4A\Helpers::LogError($result->error);
          $model->error = "An error occured while proccessing your reservation, please try again, if this continues please contact us.";// $result->error; //pb: todo change res error message
        }
        \S4A\Helpers::RenderTemplate($v, "Reservation", "reservation", $model, ($this_post = get_option("wps4a_property_reservation_page")) ? $this_post->post_content : null, get_option('wps4a_reservation_template'));
    }
}
