<?php

function S4A_Enquiry($v, $url) {

    $model = new stdClass();
    $model->BookingCriteria = new S4A\Structs\BookingCriteria();
    $model->Reservation = new S4A\Structs\Reservation();

    $model->listing_id = S4A\Helpers::GetURLId($url);

    $searchCriteria = new S4A\Structs\SearchCriteria(array());
    $searchCriteria->listingIds = [$model->listing_id];
    $listingSummeries = S4A\Cache::get_ListingBySearchCache($searchCriteria);
    $model->ListingSummeries = $listingSummeries;

    if ($model->ListingSummeries->total == 0)
		\S4A\Helpers::Throw404();

    $model->AvailabilityCriteria = S4A\Structs\AvailabilityCriteria::create(json_decode(stripslashes($_POST['AvailabilityCriteria'])));
    $model->v_title = $model->AvailabilityCriteria->establishment;

    if (!$model->AvailabilityCriteria->establishment) {
        $model->AvailabilityCriteria->establishment = $model->ListingSummeries->ListingSummeries[0]->v_title;
        $searchCriteria->v_title = $model->AvailabilityCriteria->establishment;
    }

    if (!$model->AvailabilityCriteria->listing_id) {
        $model->AvailabilityCriteria->listing_id = $model->ListingSummeries->ListingSummeries[0]->id;
    }

    $availibleRooms = S4A\Service::get_available_rooms($model->AvailabilityCriteria);

    $reservationCriteriaData = $model->AvailabilityCriteria;
    $reservationCriteriaData->ListingService = $availibleRooms->channel->services;
    $reservationCriteriaData->ListingRoom = $availibleRooms->ListingRoom;

    $model->Terms = get_option('wps4a_reservation_terms_page');

    $model->Reservation->ReservationCriteria = new S4A\Structs\ReservationCriteria($reservationCriteriaData);

    if ($_POST['isBooking']) {

        $email = S4A\Helpers::GetClean($_POST, "user_email");

        if ($email) {

            $wp_user = S4A\Helpers::CreateGetUser($email, S4A\Helpers::GetClean($_POST, 'user_name'), S4A\Helpers::GetClean($_POST, 'user_last_name'));

            if ($wp_user->ID) {
                S4A\Meta::UpdateProfile($wp_user->ID, $_POST);

                $model->BookingCriteria->Reservation = $model->Reservation;

                $EnquiryCriteria = S4A\Meta::GetProfile($wp_user->ID);
                $EnquiryCriteria->wp_user = $wp_user;
                $EnquiryCriteria->listing_id = $model->Reservation->ReservationCriteria->listing_id;
                $EnquiryCriteria->price = $model->Reservation->ReservationCriteria->ListingRoom->price;
                $EnquiryCriteria->establishment = $model->Reservation->ReservationCriteria->establishment;
                $EnquiryCriteria->room = $model->Reservation->ReservationCriteria->ListingRoom->description;
                $EnquiryCriteria->user_checkInDate = $model->Reservation->ReservationCriteria->the_arrival_date;
                $EnquiryCriteria->user_checkOutDate = $model->Reservation->ReservationCriteria->the_departure_date;
                $EnquiryCriteria->source = $_POST['source'];
                $model->BookingCriteria->EnquiryCriteria = $EnquiryCriteria;

                $model->BookingCriteria->wp_user = $wp_user;

                $result = S4A\Service::do_Enquiry($model->BookingCriteria);
            } else {
                $result['sucess'] = false;
                $result['error'] = "We were unable to process your enquiry, please contact us.";
            }
        } else {
            $result['sucess'] = false;
            $result['error'] = "Email is a required field.";
        }
    }


    if ($result['sucess']) {
        add_action('wp_footer', function() use ($model) {
            echo '<script type="text/javascript">'
            . 'window.addEventListener("load", function () {'
            . 'TrackGoogleAnalyticsEvent("Enquiries", "Enquiry Sent", "' . $model->Reservation->ReservationCriteria->establishment . '", null);'
            . 'if (typeof fbq === "function") {fbq(\'track\', \'Lead\');}'
            . '});'
            . '</script>';
        }, 99);

        \S4A\Helpers::RenderTemplate($v, "Enquiry Sent", "enquiry/sent", $model, ($this_post = get_option("wps4a_property_enquiry_sent_page")) ? get_post($this_post)->post_content : null, get_option('wps4a_enquiry_sent_template'));
    } else {

        //populate form

        $formData = new stdClass();

        $current_user = wp_get_current_user();

        $profile = S4A\Meta::GetProfile($current_user->ID);

        $formData->user_email = ($model->EnquiryCriteria->user_email) ? $model->EnquiryCriteria->user_email : $profile->user_email;
        $formData->user_name = ($model->EnquiryCriteria->user_name) ? $model->EnquiryCriteria->user_name : $profile->user_name;
        $formData->user_last_name = ($model->EnquiryCriteria->user_last_name) ? $model->EnquiryCriteria->user_last_name : $profile->user_last_name;
        $formData->user_phone = ($model->EnquiryCriteria->user_phone) ? $model->EnquiryCriteria->user_phone : $profile->user_phone;
        $formData->user_nationality = ($model->EnquiryCriteria->user_nationality) ? $model->EnquiryCriteria->user_nationality : $profile->user_nationality;
        $formData->user_resident = ($model->EnquiryCriteria->user_resident) ? $model->EnquiryCriteria->user_resident : $profile->user_resident;
        $formData->user_pension = ($model->EnquiryCriteria->user_pension) ? $model->EnquiryCriteria->user_pension : $profile->user_pension;
        $formData->user_food = ($model->EnquiryCriteria->user_food) ? $model->EnquiryCriteria->user_food : $profile->user_food;
        $formData->user_disabilities = ($model->EnquiryCriteria->user_disabilities) ? $model->EnquiryCriteria->user_disabilities : $profile->user_disabilities;
        $formData->user_flexible = ($model->EnquiryCriteria->user_flexible) ? $model->EnquiryCriteria->user_flexible : $profile->user_flexible;
        $formData->user_adults = ($model->EnquiryCriteria->user_adults) ? $model->EnquiryCriteria->user_adults : $profile->user_adults;
        $formData->user_children = ($model->EnquiryCriteria->user_children) ? $model->EnquiryCriteria->user_children : $profile->user_children;
        $formData->user_children_age = ($model->EnquiryCriteria->user_children_age) ? $model->EnquiryCriteria->user_children_age : $profile->user_children_age;
        $formData->user_special = ($model->EnquiryCriteria->user_special) ? $model->EnquiryCriteria->user_special : $profile->user_special;

        $model->FormData = $formData;

        $model->error = $result['error']; //pb: todo change res error message
		\S4A\Helpers::RenderTemplate($v, "Enquiry", "enquiry", $model, ($this_post = get_option("wps4a_property_single_page")) ? $this_post->post_content : null, get_option('wps4a_enquiry_template'));
    }
}
