<?php
function S4A_Account_Vouchers($v, $url) {
    $model = new stdClass();

    $model->Menu = S4A\Helpers::AccountNavigation();
    $model->Page = "My Vouchers";

    \S4A\Helpers::RenderTemplate($v, $model->Page, "account", $model, ($this_post = get_option("wps4a_property_my_account_page")) ? $this_post->post_content : null, get_option('wps4a_my_account_template'));
}