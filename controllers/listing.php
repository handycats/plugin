<?php

function S4A_Listing($v, $url)
{

    $model = new stdClass();

    $model->listing_id = S4A\Helpers::GetURLId($url);

    $listing = S4A\Cache::get_ListingByIdCache($model->listing_id);
    $model->Listing = $listing;

    if (!property_exists($model->Listing, 'Listing') || (strpos($url, \S4A\Helpers::SEF($model->Listing->Listing->v_title)) === false)) 
        \S4A\Helpers::Throw404();

    $model->Provider = $listing->provider;

    $model->v_title = $model->Listing->Listing->v_title;

    \S4A\Helpers::AddMetaDescription($model->Listing->Listing->t_full_description);

    $model->UserSearchDates = S4A\Session::get_user_search_data();

    if ($model->Listing->Listing->v_geo_coordinates)
        wp_enqueue_script('wps4a-gmap', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBxFxGBHK0LuiBCNhNCVhJU3d1pU58gir0', '', null, true);

    if ($model->Listing->Listing->i_tripadvisor)
        wp_enqueue_script('wps4a-tripadvisor', 'https://www.tripadvisor.co.za/WidgetEmbed-selfserveprop?border=true&popIdx=true&iswide=false&locationId=' . $model->Listing->Listing->i_tripadvisor . '&display_version=2&uniq=2010&rating=true&lang=en_ZA&nreviews=5&writereviewlink=true', '', null, true);

    wp_enqueue_script('wps4a-addthis', '//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-595ab63edcbb8fd0', '', null, true);

    $ListingGlobalInformation = $model->Listing->Listing->listing_global_information_global_information;

    if ($ListingGlobalInformation)
        uasort($ListingGlobalInformation, function ($a, $b) {
        return strcmp($a->v_name, $b->v_name);
    });

    if ($ListingGlobalInformation) {

        foreach ($ListingGlobalInformation as $listingGlobalInformation) {
            if ($listingGlobalInformation->e_type == 'Activities')
                $model->Activities[] = $listingGlobalInformation->global_information[0];
            if ($listingGlobalInformation->e_type == 'Facilities')
                $model->Facilities[] = $listingGlobalInformation->global_information[0];
            if ($listingGlobalInformation->e_type == 'Facts')
                $model->Facts[] = $listingGlobalInformation->global_information[0];
            if ($listingGlobalInformation->e_type == 'Includes')
                $model->Includes[] = $listingGlobalInformation->global_information[0];
            if ($listingGlobalInformation->e_type == 'Excludes')
                $model->Excludes[] = $listingGlobalInformation->global_information[0];
            if ($listingGlobalInformation->e_type == 'Policies')
                $model->Policies[] = $listingGlobalInformation->global_information[0];
        }

        if ($model->Facts && $model->Facilities) {

            $FactsFacilities = array_merge($model->Facts, $model->Facilities);

            $usortFactsFacilities = uasort($FactsFacilities, function ($a, $b) {
                return strcmp($a->v_name, $b->v_name);
            });

            if ($usortFactsFacilities)
                $model->FactsFacilities[] = $FactsFacilities;
        } else {
          $FactsFacilities = $model->Facilities;
          $model->FactsFacilities[] = $FactsFacilities;
        }
    }

    $model->FactsIdArry = array();

    if($model->Facts)
        foreach ($model->Facts as $fact)
            $model->FactsIdArry[$fact->id] = $fact->id;

    $model->Breadcrumbs = S4A\Helpers::BuildBreadcrumbs($model->Listing->Listing->i_country_id, $model->Listing->Listing->i_province_id, $model->Listing->Listing->i_area_id, $model->Listing->Listing->v_title);

    if (S4A_LOGIN) {

        $current_user = wp_get_current_user();

        $fav = new stdClass();

        $fav->ShowFav = true;
        $fav->IsFav = (($current_user->ID && S4A\Meta::CheckIfFav($current_user->ID, $model->Listing->Listing->id)) ? true : false);
        $fav->ListingId = $model->Listing->Listing->id;

        $model->Fav = $fav;
    }

	\S4A\Helpers::RenderTemplate($v, $model->Listing->Listing->v_title, "listing/" . S4A_LISTING_TEMPLATE, $model, ($this_post = get_option("wps4a_property_single_page")) ? $this_post->post_content : null, get_option('wps4a_property_single_page_template'), true);
}
