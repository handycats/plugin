<?php
function S4A_Search($v, $url) {
    $SearchCriteria = new S4A\Structs\SearchCriteria($_POST);
    S4A\Session::UserBookingData($SearchCriteria);
    header("Location: " . S4A\Helpers::BuildResultsUrl($SearchCriteria));
    die();
}