<?php
function S4A_Account_Companions($v, $url) {
    $current_user = wp_get_current_user();

    if ($_POST)
        S4A\Meta::UpdateProfile($current_user->ID, $_POST);

    $model = new stdClass();

    $model->Profile = S4A\Meta::GetProfile($current_user->ID);

    $model->Menu = S4A\Helpers::AccountNavigation();
    $model->Page = "My Companions";

    \S4A\Helpers::RenderTemplate($v, $model->Page, "account", $model, ($this_post = get_option("wps4a_property_my_account_page")) ? $this_post->post_content : null, get_option('wps4a_my_account_template'));
}