<?php

function CleaText($text) {
    $textDescription = \S4A\Helpers::trim_text($text, 160, false);
    return trim(preg_replace('/\s+/', ' ', str_replace(',', '', $textDescription)));
}

function S4A_CSV($v, $url) {

    	if(isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION'])) { list($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) = explode(':', base64_decode(substr($_SERVER['REDIRECT_HTTP_AUTHORIZATION'], 6))); }

    $AUTH_USER = 'admin321';
    $AUTH_PASS = 'admin123';
    header('Cache-Control: no-cache, must-revalidate, max-age=0');
    $has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));
    $is_not_authenticated = (
            !$has_supplied_credentials ||
            $_SERVER['PHP_AUTH_USER'] != $AUTH_USER ||
            $_SERVER['PHP_AUTH_PW'] != $AUTH_PASS
            );
    if ($is_not_authenticated) {
        header('HTTP/1.1 401 Authorization Required');
        header('WWW-Authenticate: Basic realm="Access denied"');
        exit;
    }

    header("Content-Type: text/csv");

    $model = new stdClass();

    $SearchCriteria = new S4A\Structs\SearchCriteria($_GET);
    $SearchCriteria->perPage = 0;

    $model->ListingSummeries = S4A\Cache::get_ListingBySearchCache($SearchCriteria);

    //echo "hotel_id,name,description,brand,address.addr1,address.city,address.city_id,address.region,address.country,address.postal_code,latitude,longitude,neighborhood[0],neighborhood[1],margin_level,base_price,image[0].url,url";
    echo "hotel_id,name,description,address.addr1,address.city,address.region,address.country,latitude,longitude,base_price,sale_price,image[0].url,url";
    echo "\r\n";
    foreach ($model->ListingSummeries->ListingSummeries as $ListingSummery) {

        $first = true;

        $ListingSummeryArray = (array) $ListingSummery;

        echo $ListingSummeryArray['id'] . ","; //hotel_id
        echo CleaText($ListingSummeryArray['v_title']) . ","; //name
        echo CleaText($ListingSummeryArray['t_full_description']) . ","; //description

        echo $ListingSummeryArray['area_name'] . ","; //address.addr1
        echo $ListingSummeryArray['area_name'] . ","; //address.city
        echo $ListingSummeryArray['province_name'] . ","; //address.region
        echo $ListingSummeryArray['country_name'] . ","; //address.country
        echo $ListingSummeryArray['v_geo_coordinates'] . ","; //latitude,longitude
        echo "R" . $ListingSummeryArray['i_rack_rate'] . ","; //base_price
        echo "R" . $ListingSummeryArray['i_price'] . ",";//sale_price
        echo S4A_IMG_URL . '?id=' . $ListingSummeryArray['imageid'] . '&type=latest' . ","; //image[0].url
        echo \S4A\Helpers::ListingUrl($ListingSummeryArray['v_title'], $ListingSummeryArray['id']) . "?utm_source=facebook&utm_medium=ppc&utm_source=feed"; //url
        echo "\r\n";
    }

    die();
}
