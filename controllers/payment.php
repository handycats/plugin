<?php

function S4A_Payment($v, $url) {
    $model = new stdClass();
    $URLiD = S4A\Helpers::GetURLId($url);
    $listing = S4A\Cache::get_ListingByIdCache($URLiD);

    if (!property_exists($listing, 'Listing'))
		\S4A\Helpers::Throw404();

    $model->Listing = $listing->Listing;
    $reservation_id = stripslashes($_GET['reservation_id']);
    $VCS_payment_result = stripslashes($_GET['result']);

    if (!$reservation_id || $_POST["TRANSACTION_STATUS"] == 4 || $_GET['result'] == 'cancelled') {
        wp_redirect(\S4A\Helpers::ListingUrl($model->Listing->v_title, $URLiD));
        exit;
    }

    $get_Reservation = S4A\Service::get_reservationDetailsById($reservation_id);
    $model->Reservation = $get_Reservation;

    $priceTotal = S4A\Helpers::DoPrice( $model->Reservation->BookingCriteria->BookingCriteria->Reservation->ReservationCriteria->ListingRoom->currency_code ,$model->Reservation->BookingCriteria->Reservation->ReservationCriteria->ListingRoom->total );

    $model->priceTotal = $model->Reservation->BookingCriteria->Reservation->ReservationCriteria->ListingRoom->total;

    $current_user = wp_get_current_user();
    $profile = S4A\Meta::GetProfile($current_user->ID);

    if ($profile->AgentCode) {

      $result = S4A\Service::do_Payment(null, $reservation_id);

      if ($result->sucess) {

          \S4A\Session::remove_counter();

          add_action('wp_footer', function() use ($model) {

              $utmData = \S4A\Session::get_utm_data();

              echo '<script type="text/javascript">window.addEventListener("load", function () {TrackGoogleAnalyticsEvent("Reservations", "Reservation Sent", "' . $model->Listing->v_title . '", null)});</script>';
              echo '<script type="text/javascript">window.addEventListener("load", function () {TrackGoogleAnalyticsEvent("Reservations", "Reservation Source", "' . $utmData->utm_source . '", null)});</script>';
              echo '<script type="text/javascript">window.addEventListener("load", function () {TrackGoogleAnalyticsEvent("Reservations", "Reservation Value", "Value", "' . str_replace(",", "", $model->priceTotal) . '")});</script>';

          }, 99);

          \S4A\Helpers::RenderTemplate($v, "Reservation Sent", "reservation/sent", $model, ($this_post = get_option("wps4a_property_reservation_sent_page")) ? $this_post->post_content : null, get_option('wps4a_reservation_sent_template'));
          return;
      }
      
    }

    if($_GET['result'] == 'success'){

      $transactionDataArray = ['TRANSACTION_STATUS' => 0];
      $transactionData = (object) $transactionDataArray;
      $result = S4A\Service::do_Payment($transactionData, $reservation_id);

      if ($result->sucess) {
        \S4A\Session::remove_counter();

        add_action('wp_footer', function() use ($model) {

            $utmData = \S4A\Session::get_utm_data();

            echo '<script type="text/javascript">window.addEventListener("load", function () {TrackGoogleAnalyticsEvent("Reservations", "Reservation Sent", "' . $model->Listing->v_title . '", null)});</script>';
            echo '<script type="text/javascript">window.addEventListener("load", function () {TrackGoogleAnalyticsEvent("Reservations", "Reservation Source", "' . $utmData->utm_source . '", null)});</script>';
            echo '<script type="text/javascript">window.addEventListener("load", function () {TrackGoogleAnalyticsEvent("Reservations", "Reservation Value", "Value", "' . str_replace(",", "", $model->priceTotal) . '")});</script>';

        }, 99);

        \S4A\Helpers::RenderTemplate($v, "Reservation Sent", "reservation/sent", $model, ($this_post = get_option("wps4a_property_reservation_sent_page")) ? $this_post->post_content : null, get_option('wps4a_reservation_sent_template'));
        return;
      }
      
    }

    if ($_POST['TRANSACTION_STATUS']) {

        $transactionDataArray = array(
            'PAY_REQUEST_ID' => $_POST['PAY_REQUEST_ID'],
            'TRANSACTION_STATUS' => $_POST['TRANSACTION_STATUS'],
            'CHECKSUM' => $_POST['CHECKSUM']
        );

        $transactionData = (object) $transactionDataArray;

        $result = S4A\Service::do_Payment($transactionData, $reservation_id);

        if ($result->sucess) {

            \S4A\Session::remove_counter();

            add_action('wp_footer', function() use ($model) {

                $utmData = \S4A\Session::get_utm_data();

                echo '<script type="text/javascript">window.addEventListener("load", function () {TrackGoogleAnalyticsEvent("Reservations", "Reservation Sent", "' . $model->Listing->v_title . '", null)});</script>';
                echo '<script type="text/javascript">window.addEventListener("load", function () {TrackGoogleAnalyticsEvent("Reservations", "Reservation Source", "' . $utmData->utm_source . '", null)});</script>';
                echo '<script type="text/javascript">window.addEventListener("load", function () {TrackGoogleAnalyticsEvent("Reservations", "Reservation Value", "Value", "' . str_replace(",", "", $model->priceTotal) . '")});</script>';

            }, 99);

            \S4A\Helpers::RenderTemplate($v, "Reservation Sent", "reservation/sent", $model, ($this_post = get_option("wps4a_property_reservation_sent_page")) ? $this_post->post_content : null, get_option('wps4a_reservation_sent_template'));
            return;
        }
        
    }

    if ($_POST['MERCHANTREFERENCE']) {

      $transactionDataArray = array(
          'LITE_MERCHANT_APPLICATIONID' => $_POST['LITE_MERCHANT_APPLICATIONID'],
          'LITE_PAYMENT_CARD_STATUS' => $_POST['LITE_PAYMENT_CARD_STATUS'],
          'LITE_BANKREFERENCE' => $_POST['LITE_BANKREFERENCE']
      );

      $transactionData = (object) $transactionDataArray;

      $result = S4A\Service::do_Payment($transactionData, $reservation_id);

      if ($result->sucess) {

      \S4A\Session::remove_counter();

      add_action('wp_footer', function() use ($model) {

          $utmData = \S4A\Session::get_utm_data();

          echo '<script type="text/javascript">window.addEventListener("load", function () {TrackGoogleAnalyticsEvent("Reservations", "Reservation Sent", "' . $model->Listing->v_title . '", null)});</script>';
          echo '<script type="text/javascript">window.addEventListener("load", function () {TrackGoogleAnalyticsEvent("Reservations", "Reservation Source", "' . $utmData->utm_source . '", null)});</script>';
          echo '<script type="text/javascript">window.addEventListener("load", function () {TrackGoogleAnalyticsEvent("Reservations", "Reservation Value", "Value", "' . str_replace(",", "", $model->priceTotal) . '")});</script>';

      }, 99);

      \S4A\Helpers::RenderTemplate($v, "Reservation Sent", "reservation/sent", $model, ($this_post = get_option("wps4a_property_reservation_sent_page")) ? $this_post->post_content : null, get_option('wps4a_reservation_sent_template'));
      return;
    }
      
    }

    $model->BookingCriteria = $model->Reservation->BookingCriteria;

    if($get_Reservation->error){
        $model->error = "An error occured while proccessing your payment, please contact us.";
    } elseif($_POST || $result->sucess || $_GET['result'] == 'success') {
      $model->error = "An error occured while proccessing your payment, we will try again";
    }
    
    $model->PaymentButton = $get_Reservation->PaymentButton;
    \S4A\Helpers::RenderTemplate($v, "Payment", "payment", $model, ($this_post = get_option("wps4a_property_payment_page")) ? $this_post->post_content : null, get_option('wps4a_payment_template'));
    
}
