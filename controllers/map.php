<?php
function S4A_Map($v, $url)
{

    $model = new stdClass();

    add_action('wp_footer', function () use ($model) { ?>
<script type='text/javascript'>
var s4aMAP = {"latitude":<?= get_option('wps4a_map_search_latitude') ?>,"longitude":<?= get_option('wps4a_map_search_longitude') ?>,"zoom":6};
</script>
<?php 
}, 99);

\S4A\Helpers::RenderTemplate($v, "Map Search", "map", $model, ($this_post = get_option("wps4a_map_search_page")) ? $this_post->post_content : null, get_option('wps4a_map_search_template'));
}