
function S4AReservation(wrapper) {
    //check if id number field found 
    let id = jQuery(wrapper).find(".s4a_val-user_id").val();

    if (id != null && id != ""){
        S4AValidation("pent", jQuery(wrapper).find(".s4a_val-user_id"));
    }

    S4AValidation("none", jQuery(wrapper).find(".s4a_val-special"));
    S4AValidation("null", jQuery(wrapper).find(".s4a_val-user_name"));
    S4AValidation("null", jQuery(wrapper).find(".s4a_val-user_last_name"));
    S4AValidation("phone", jQuery(wrapper).find(".s4a_val-user_phone"));
    S4AValidation("email", jQuery(wrapper).find(".s4a_val-user_email"));
    S4AValidation("null", jQuery(wrapper).find(".s4a_val-user_adults"));

    S4AValidation("checked", jQuery(wrapper).find(".s4a_val-terms"));
    S4AValidation("checked", jQuery(wrapper).find(".s4a_val-personal"));

    jQuery(wrapper).find(".s4a_val-guest_name").each(function () {
        S4AValidation("null", jQuery(this));
    });

    jQuery(wrapper).find(".s4a_val-guest_last_name").each(function () {
        S4AValidation("null", jQuery(this));
    });

    if (!S4AValidationSucess(wrapper)) {
        return false;
    } else {
        jQuery("#s4a_loader").show();
        TrackGoogleAnalyticsEvent("Reservations", "Reservation Submitted", "", null);
    }
}
