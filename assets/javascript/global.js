function CounterCountDown(duration) {
  if (!isNaN(duration)) {
    let timer = duration;


    let minutes;
    let seconds;

    let interVal = setInterval(function () {
      minutes = parseInt(timer / 60, 10);
      seconds = parseInt(timer % 60, 10);

      minutes = minutes < 10 ? '0' + minutes : minutes;
      seconds = seconds < 10 ? '0' + seconds : seconds;

      jQuery('#display').html('<b>' + minutes + 'm : ' + seconds + 's' + '</b>');
      if (--timer < 0) {
        timer = duration;
        jQuery('#s4a_counter').remove();
        jQuery('#display').empty();
        clearInterval(interVal);
      }
    }, 1000);
  }
}

function checkAvailibilty() {
  if (jQuery('.datepicker_checkin').val() && jQuery('.datepicker_checkout').val()) {
    BookingProcesss.get_available_rooms('get_available_rooms', 'available_rooms', 'available_rooms', false);
  }
}

window.addEventListener('load', function () {
  jQuery(".ourWeb").click(function () {
    jQuery('.ourWeb').hide();
    jQuery('.web_hidden').show();
  });
  jQuery('#get_available_rooms').on('change', function () {
    setTimeout(
      function () {
        checkAvailibilty();
      }, 1000);
  });

  jQuery('.time_picker').timepicker({
    // timeFormat: 'h:mm p',
    // interval: 60,
    // minTime: '10',
    // maxTime: '6:00pm',
    // defaultTime: '11',
    // startTime: '10:00',
    // dynamic: false,
    // dropdown: true,
    // scrollbar: true
  });

  jQuery('#phone').intlTelInput({
    geoIpLookup: function (callback) {
      jQuery.get('https://ipinfo.io', function () {}, 'jsonp').always(function (resp) {
        let countryCode = (resp && resp.country) ? resp.country : '';
        console.log(countryCode);
        callback(countryCode);
      });
    },
    initialCountry: 'auto',
    separateDialCode: true,
    utilsScript: './javascript/utils.js',
  });

  DateRangeBuild('#get_available_rooms .datepicker_checkin', '#get_available_rooms .datepicker_checkout');

  checkAvailibilty();

  let myLazyLoadBG = new LazyLoad({
    elements_selector: '.lazy',
  });

  jQuery('.s4a_fav').click(function () {
    let _this = jQuery(this);
    let ListingId = _this.data('id');

    TrackGoogleAnalyticsEvent('User Behaviour', 'Favourite Added', ListingId, null);

    if (!s4aJSO.logged) {
      ShowModal('#s4a_login_model');
    } else {
      if (_this.hasClass('fa-star')) {
        _this.removeClass('fa-star');
        _this.addClass('fa-star-o');
      } else {
        _this.removeClass('fa-star-o');
        _this.addClass('fa-star');
      }
      jQuery.ajax({
        url: s4aJSO.ajax_url,
        method: 'POST',
        dataType: 'json',
        data: {
          action: s4aJSO.the_unique_plugin_name + '-ajax-submit',
          cmd: 'add-fav',
          id: ListingId,
        },

      });
    }
  });

  service_date_range();

  jQuery(document).on('change', '#chk_extra_services', function () {
    if (jQuery(this).prop('checked')) {
      jQuery('#div_extra_services').slideDown();
    } else {
      jQuery('#div_extra_services').slideUp();
    }
  });

  jQuery('.select_qty').on('change', function () {
    let _this = jQuery(this);

    let id = _this.attr('id');
    let qty = parseInt(_this.val());
    let currency_code = jQuery('#currency_code').text();
    let id_arr = id.split('_');
    let qty_child_adult = id_arr[0];
    let code = id_arr[1];
    let price = jQuery('#' + code + '_price').find('.value').attr('data-value');
    let children = jQuery('#children_' + code).val();
    let adults = jQuery('#adults_' + code).val();

    jQuery.ajax({
      url: s4aJSO.ajax_url,
      method: 'POST',
      dataType: 'json',
      data: {
        action: s4aJSO.the_unique_plugin_name + '-ajax-submit',
        cmd: 'qty-selection',
        qty: qty,
        qty_child_adult: qty_child_adult,
        price: price,
        children: children,
        adults: adults,
        currency_code: currency_code,
      },
      success: function (data) {
        if (data.error == false) {
          jQuery('#' + code + '_new_price').html(data.html_str);
          jQuery('#' + code + '_price').hide();
          jQuery('#' + code + '_new_price').show();
          jQuery('.service_select').trigger('change');
        } else if (data.error == true) {
          alert(data.error);
        }
      },
      error: function (data) {
        alert('Something went wrong, please try again.');
      },
    });
  });

  jQuery(document).on('change', '.service_select', function () {
    let currency_code = jQuery('#currency_code').text();
    let booking_total = jQuery('#booking_total').find('.value').attr('data-value');

    if (jQuery(this).prop('checked')) {
      let service_data = {};

      var select_st_data = jQuery(this).val();
      var select_data_array = select_st_data.split('|');
      let service_array = {};

      service_array['code'] = select_data_array[0];
      service_array['type'] = select_data_array[1];

      let price = jQuery('#' + select_data_array[0] + '_new_price').find('.value').attr('data-value');

      service_array['price'] = price;
      service_array['description'] = select_data_array[3];
      service_array['mandatory'] = select_data_array[4];

      let date = jQuery('#service_date_' + select_data_array[0]).val();
      let quantity = jQuery('#quantity_' + select_data_array[0]).val();
      let adults = jQuery('#adults_' + select_data_array[0]).val();
      let children = jQuery('#children_' + select_data_array[0]).val();

      if (quantity == null) {
        quantity = 1;
      }
      if (adults == null) {
        adults = 0;
      }
      if (children == null) {
        children = 0;
      }

      service_array['date'] = date;
      service_array['quantity'] = quantity;
      service_array['adults'] = adults;
      service_array['children'] = children;
      service_data[0] = service_array;

      jQuery.ajax({
        url: s4aJSO.ajax_url,
        method: 'POST',
        dataType: 'json',
        data: {
          action: s4aJSO.the_unique_plugin_name + '-ajax-submit',
          cmd: 'add-booking-services',
          services_data: service_data,
          booking_total: booking_total,
          currency_code: currency_code,
        },
        success: function (data) {
          if (data.error == false) {
            let st_data = data.html_str;
            let data_arr = st_data.split('|');
            let service_total = data_arr[0];
            let booking_total = data_arr[1];
            jQuery('#optional_services_total').html(service_total);
            jQuery('#new_booking_total').html(booking_total);
            jQuery('#new_booking_total').show();
            jQuery('#booking_total').hide();
          } else if (data.error == true) {
            alert(data.error);
          }
        },
        error: function (data) {
          alert('Something went wrong, please try again.');
        },
      });
    } else {
      var select_st_data = jQuery(this).val();
      var select_data_array = select_st_data.split('|');
      let code = select_data_array[0];

      jQuery.ajax({
        url: s4aJSO.ajax_url,
        method: 'POST',
        dataType: 'json',
        data: {
          action: s4aJSO.the_unique_plugin_name + '-ajax-submit',
          cmd: 'remove-booking-services',
          code: code,
          currency_code: currency_code,
        },
        success: function (data) {
          if (data.error == false) {
            let st_data = data.html_str;
            let data_arr = st_data.split('|');
            let service_total = data_arr[0];
            let booking_total = data_arr[1];

            jQuery('#new_booking_total').html(booking_total);
            jQuery('#optional_services_total').html(service_total);
          } else if (data.error == true) {
            alert(data.error);
          }
        },
        error: function (data) {
          alert('Something went wrong, please try again!.');
        },
      });
    }
  });
});

function TrackGoogleAnalyticsEvent(category, action, label, value = null) {
  if (typeof ga === 'function') {
    ga('send', 'event', {
      eventCategory: category,
      eventAction: action,
      eventLabel: label,
      eventValue: value
    });
  }
}

function s4a_show(target, scroll) {
  if (scroll == null) {
    scroll = true;
  } else {
    scroll = scroll;
  }

  jQuery(function ($) {
    jQuery(target).removeClass('hidden');
    if (scroll) {
      ScrollToTarget(target);
    }
  });
}

function s4a_submit(target) {
  jQuery(function ($) {
    jQuery(target).submit();
  });
}

function s4a_hide(target) {
  jQuery(function ($) {
    jQuery(target).addClass('hidden');
  });
}

function ShowModal(model) {
  TrackGoogleAnalyticsEvent('User Behaviour', 'Show Modal', model, null);
  jQuery(function ($) {
    jQuery(model).modal('toggle');
  });
}

function ScrollToTarget(wrapper) {
  TrackGoogleAnalyticsEvent('User Behaviour', 'Scroll To', wrapper, null);
  ScrollTo(jQuery(wrapper), -80, 500);
}

function S4AValidationSucess(wrapper) {
  let target = jQuery(wrapper).find('.s4a_val-error');
  if (target.length) {
    jQuery('html, body').animate({
      scrollTop: target.offset().top - 200,
    }, 200);
    return false;
  }
  return true;
}

function S4ARemoveValidation(target) {
  target.removeClass('s4a_val-error');
  target.removeClass('s4a_val-sucess');
}

function S4AValidation(type, target) {
  target.removeClass('s4a_val-error');
  target.removeClass('s4a_val-sucess');

  let validate = target.val();

  switch (type) {
    case 'email':
      var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (validate && validate.match(emailPattern)) {
        target.addClass('s4a_val-sucess');
        return validate;
      }
      break;
    case 'phone':
      if (validate_phone()) {
        return null;
      }
      break;
    case 'null':
      if (validate) {
        target.addClass('s4a_val-sucess');
        return validate;
      } else {
        target.addClass('s4a_val-error');
        return null;
      }
      break;
    case 'pent':
        var idNumber = validate;
        // assume everything is correct and if it later turns out not to be, just set this to false
        var correct = true;
        //Ref: http://www.sadev.co.za/content/what-south-african-id-number-made
        // SA ID Number have to be 13 digits, so check the length
        if (idNumber.length != 13) {
            correct = false;
        }
        // get first 6 digits as a valid date
        var tempDate = new Date(idNumber.substring(0, 2), idNumber.substring(2, 4) - 1, idNumber.substring(4, 6));

        var id_date = tempDate.getDate();
        var id_month = tempDate.getMonth();
        var id_year = tempDate.getFullYear();
    
        if (!((tempDate.getYear() == idNumber.substring(0, 2)) && (id_month == idNumber.substring(2, 4) - 1) && (id_date == idNumber.substring(4, 6)))) {
            correct = false;
        }
    
        var ageDifMs = Date.now() - tempDate.getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        var ageIs = Math.abs(ageDate.getUTCFullYear() - 1970);

        if (correct && ageIs > 59) {
          target.addClass('s4a_val-sucess');
          return validate;
        } else {
          target.addClass('s4a_val-error');
          return null;
        }
      break;
    case 'none':
      if (!validate) {
        target.addClass('s4a_val-sucess');
        return null;
      }
      break;
    case 'checked':
      if (target.is(':checked')) {
        target.addClass('s4a_val-sucess');
        return null;
      }
      break;
    default:
      alert('Validation Error');
  }
  target.addClass('s4a_val-error');

  // jQuery('html, body').animate({
  //   scrollTop: target.offset().top - 200;
  // }, 200)

  return null;
}

let typingTimer;
let doneTypingInterval = 3000;
let phone_input = jQuery('#phone');
let input_alert = jQuery('.input-alert');

phone_input.on('keyup', function () {
  phone_input.removeClass('s4a_val-sucess');
  phone_input.removeClass('s4a_val-error');
  input_alert.text('');
  input_alert.hide();
  clearTimeout(typingTimer);
  typingTimer = setTimeout(validate_phone, doneTypingInterval);
});

phone_input.on('keydown', function () {
  clearTimeout(typingTimer);
});

function validate_phone() {
  let phone = phone_input.val().trim();
  let results = false;
  if (phone) {
    if (phone_input.intlTelInput('isValidNumber')) {
      phone_input.addClass('s4a_val-sucess');
      input_alert.html('&#10004;');
      input_alert.css('color', 'green');
      input_alert.show();
      results = true;
    } else {
      phone_input.addClass('s4a_val-error');
      input_alert.css('color', 'red');
      input_alert.html('&#10006;');
      input_alert.show();
      results = false;
    }
  } else {
    phone_input.addClass('s4a_val-error');
    input_alert.css('color', 'red');
    input_alert.html('&#63;');
    input_alert.show();
    results = false;
  }
  return results;
}

function S4AValidationAjax(target) {
  let dfrd1 = $.Deferred();

  if (!target.hasClass('s4a_val-error')) {
    if (!target.hasClass('s4a_val-warning-again') && !target.hasClass('s4a_val-warning')) {
      let validate = target.val();
      jQuery.ajax({
        url: s4aJSO.ajax_url,
        method: 'POST',
        dataType: 'json',
        data: {
          action: s4aJSO.the_unique_plugin_name + '-ajax-submit',
          cmd: 'ajax-validation-email',
          email: validate,
        },

      }).done(function (data) {
        if (!data.error) {
          target.addClass('s4a_val-sucess');
        } else {
          target.addClass('s4a_val-warning');
        }
      }).fail(function () {
        target.addClass('s4a_val-warning');
      }).always(function (data) {
        dfrd1.resolve();
      });
    } else {
      target.removeClass('s4a_val-warning');
      target.addClass('s4a_val-warning-again');
      dfrd1.resolve();
    }
  }
  return $.when(dfrd1).done(function () {}).promise();
}

function initialize_GMap(lat, long) {
  let lat_Long_Coords = new google.maps.LatLng(lat, long);
  let mapOptions = {
    scrollwheel: false,
    zoom: 10,
    center: lat_Long_Coords,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
  };
  let myMap = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
  let marker = new google.maps.Marker({
    position: lat_Long_Coords,
    map: myMap,
  });
  blnGMapLoaded = true;
}

function ScrollTo(target, diff, speed) {
  jQuery('html, body').animate({
    scrollTop: target.offset().top + diff,
  }, speed);
}

function DateRangeBuild(checkinItem, checkoutItem) {
  if (jQuery(window).width() < 768) {
    return false;
  }

  jQuery(checkinItem).attr('type', 'text');
  jQuery(checkoutItem).attr('type', 'text');

  let nowTemp = new Date();
  let now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

  var checkin = jQuery(checkinItem).datepicker({
    onRender: function (date) {
      return date.valueOf() < now.valueOf() ? 'disabled' : '';
    },
    format: 'yyyy-mm-dd',
  }).on('changeDate', function (ev) {
    if (ev.date.valueOf() > checkout.date.valueOf()) {
      let newDate = new Date(ev.date);
      newDate.setDate(newDate.getDate() + 1);
      checkout.setValue(newDate);
    } else {
      checkout.setValue(new Date(checkout.date));
    }
    checkin.hide();
    jQuery(checkoutItem)[0].focus();
  }).data('datepicker');
  var checkout = jQuery(checkoutItem).datepicker({
    onRender: function (date) {
      return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
    },
    format: 'yyyy-mm-dd',
  }).on('changeDate', function (ev) {
    checkout.hide();
    jQuery(checkoutItem).change();
  }).data('datepicker');
}

function AdvanceSearchApply() {
  jQuery(function ($) {
    jQuery('#advanceSearchModal').modal('hide');
    let html = '<div id="advanced_search_tool" style="display:none;">';
    jQuery('.property_advance_search_form input').each(function (i, v) {
      if (jQuery(v).is(':checked')) {
        html += jQuery('<div />').append(jQuery(v).clone().attr('checked', 'checked')).html();
      }
    });
    html += '</div>';
    setTimeout(function () {
      jQuery('.property_search_form #advanced_search_tool').remove();
      jQuery('.property_search_form').append(html);
      jQuery('.property_search_form').submit();
    }, 10);
  });
}

function AdvanceSearchReset() {
  jQuery(function ($) {
    jQuery('.property_advance_search_form')[0].reset();
    jQuery('.property_advance_search_form input[type="checkbox"]').each(function () {
      jQuery(this).removeAttr('checked');
    });
  });
}

function s4a_invoice_checkbox(target) {
  jQuery(function ($) {
    if (jQuery('.s4a_val-invoice').is(':checked')) {
      s4a_show(target);
    } else {
      s4a_hide(target);
    }
  });
}
jQuery('.se365_converter').click(function () {
  jQuery('.se365_converter').removeClass('active');
  jQuery(this).addClass('active');
  s4a_currency_converter(jQuery(this));
  return false;
});

function s4a_currency_converter(_this) {
  let value = _this.data('value');
  let symbol = _this.data('symbol');
  let code = _this.data('code');

  jQuery('.price_wrapper').each(function () {
    let amount = jQuery(this).find('.value').data('value');
    let conversion = amount * value;
    jQuery(this).find('.currency_code').html(symbol);
    jQuery(this).find('.value').html(conversion.toLocaleString(undefined, {
      maximumFractionDigits: 0,
    }));
  });

  jQuery.ajax({
    url: s4aJSO.ajax_url,
    method: 'POST',
    dataType: 'json',
    data: {
      action: s4aJSO.the_unique_plugin_name + '-ajax-submit',
      cmd: 'user-curr',
      val: value,
      sym: symbol,
      cde: code,
    },
  });
}

function service_date_range() {
  let arrival_date = new Date(jQuery('#arrival_date').text());
  let departure_date = new Date(jQuery('#departure_date').text());
  let arrival_formated = new Date(arrival_date.getFullYear(), arrival_date.getMonth(), arrival_date.getDate(), 0, 0, 0, 0);
  let departure_formated = new Date(departure_date.getFullYear(), departure_date.getMonth(), departure_date.getDate(), 0, 0, 0, 0);

  jQuery('.service_date').each(function () {
    let service_date = jQuery(this).datepicker({
      onRender: function (date) {
        return ((date.valueOf() < arrival_formated.valueOf()) || (date.valueOf() > departure_formated.valueOf() + 1)) ? 'disabled' : '';
      },
    }).on('changeDate', function (ev) {
      service_date.hide();
      service_date.change();
    }).data('datepicker');
  });
}

jQuery(document).on('click', '.room-info-btn', function () {
  let _this = jQuery(this);
  let room_data = _this.data('info');
  let room_code = _this.data('code');
  // var target = _this.data('target');

  jQuery.ajax({
    url: s4aJSO.ajax_url,
    method: 'POST',
    dataType: 'json',
    data: {
      action: s4aJSO.the_unique_plugin_name + '-ajax-submit',
      cmd: 'room_information_popup',
      room_data: room_data,
    },

  }).done(function (data) {
    if (!data.error) {
      if (!jQuery('#fsModal_' + room_code).length) {
        jQuery(data.html_str).insertAfter('#get_available_rooms');
      }
      jQuery('#fsModal_' + room_code).modal({
        fadeDuration: 1000,
        fadeDelay: 0.50,
      });

      setTimeout(function () {
        jQuery('#roomCarousel').carousel({
          interval: false,
        });
      }, 100);
    }
  }).fail(function () {}).always(function (data) {});
});

jQuery(document).on('click', '.hideshow', function () {
  let id = jQuery(this).attr('id');
  let includes = jQuery('#includes_' + id);
  let excludes = jQuery('#excludes_' + id);
  let incl_plus = jQuery('#inc_plus_' + id);
  let incl_minus = jQuery('#inc_minus_' + id);
  let exc_plus = jQuery('#exc_plus_' + id);
  let exc_minus = jQuery('#exc_minus_' + id);

  excludes.toggle();
  includes.toggle();

  if (includes.is(':visible')) {
    incl_minus.show();
    incl_plus.hide();
    exc_minus.show();
    exc_plus.hide();
  } else if (includes.is(':hidden')) {
    incl_minus.hide();
    incl_plus.show();
    exc_minus.hide();
    exc_plus.show();
  }
});

function calenderStart() {
  /* global Sly */
  jQuery(function ($) {
    'use strict';


    $( ".example" ).each(function() {

      var $frame  = jQuery(this).find('.frame');
      var $slidee = $frame.children('ul').eq(0);
      var $wrap   = $frame.parent();

      (function () {

        $frame.sly({
          horizontal: 1,
          itemNav: 'forceCentered',
          activateMiddle: 1,
          smart: 1,
          activateOn: 'click',
          mouseDragging: 1,
          touchDragging: 1,
          releaseSwing: 1,
          startAt: 14,
          scrollBar: $wrap.find('.scrollbar'),
          scrollBy: 1,
          pagesBar: $wrap.find('.pages'),
          activatePageOn: 'click',
          speed: 200,
          moveBy: 600,
          elasticBounds: 1,
          dragHandle: 1,
          dynamicHandle: 1,
          clickBar: 1,

          // Buttons
          prev: $wrap.find('.backward'),
          next: $wrap.find('.forward')
        });

      }());

    });
  });
};
