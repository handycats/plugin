let BookingProcesss = {

  wrapper: '#check_availibility_control',

  id: function(element) {
    return jQuery(this.wrapper).find('#' + element);
  },
  data: function(data) {
    return jQuery(this.wrapper).find('[' + data + ']');
  },
  occupents: function(caller, target, control, max_adults, max_children, child_min, child_max) {
    let _this = this;

    let the_rooms = _this.id(caller).val();

    jQuery.ajax({
      url: s4aJSO.ajax_url,
      method: 'POST',
      dataType: 'json',
      data: {
        action: s4aJSO.the_unique_plugin_name + '-ajax-submit',
        cmd: 'build_occupents_list',
        the_rooms: the_rooms,
        max_adults: max_adults,
        max_children: max_children,
        child_min: child_min,
        child_max: child_max,
      },

    }).done(function(data) {
      if (!data.error) {
        _this.id(control).remove();
        _this.id(target).append(data.html_str);
        ScrollTo(jQuery(_this.id(target)), -235, 100);
      }
    }).fail(function() {}).always(function(data) {});
  },
  child_occupents: function(caller, the_room, target, control, child_min, child_max) {
    let _this = this;

    let the_children = _this.id(caller).val();

    jQuery.ajax({
      url: s4aJSO.ajax_url,
      method: 'POST',
      dataType: 'json',
      data: {
        action: s4aJSO.the_unique_plugin_name + '-ajax-submit',
        cmd: 'build_child_occupents_list',
        the_room: the_room,
        the_children: the_children,
        child_min: child_min,
        child_max: child_max,
      },

    }).done(function(data) {
      if (!data.error) {
        _this.id(control).remove();
        _this.id(target).append(data.html_str);
      }
    }).fail(function() {}).always(function(data) {});
  },
  form_fields: function() {
    let _this = this;

    let listing_id = _this.data('data-field="id"').val();
    let establishment = _this.data('data-field="establishment"').val();
    let the_arrival_date = _this.data('data-field="the_arrival_date"').val();
    let the_departure_date = _this.data('data-field="the_departure_date"').val();

    let roomCount = _this.id('the_rooms').val();

    let Units = {};

    for (unitNumber = 1; unitNumber <= roomCount; unitNumber++) {
      let values = {};

      values['adults'] = _this.data('data-room="' + unitNumber + '"').find('[data-type="adult"]').val();

      let Children = {};
      let age = {};
      let childCount = _this.data('data-room="' + unitNumber + '"').find('[data-type="child"]').val();

      for (childNumber = 1; childNumber <= childCount; childNumber++) {
        age[childNumber] = _this.data('data-room="' + unitNumber + '"').find('[data-child="' + childNumber + '"]').find('[data-type="age"]').val();
      }

      Children['age'] = age;
      values['Children'] = age;
      Units[unitNumber] = values;
    }

    let result = {};

    result['listing_id'] = listing_id;
    result['establishment'] = establishment;
    result['the_arrival_date'] = the_arrival_date;
    result['the_departure_date'] = the_departure_date;
    result['Units'] = Units;

    return result;
  },
  reserve: function(rate_id, form, field) {
    let _this = this;
    let form_fields = _this.form_fields();
    let reserveForm = _this.id(form);

    TrackGoogleAnalyticsEvent('User Behaviour', 'Book', form, null);

    form_fields['rate_id'] = rate_id;

    _this.id(field).val(JSON.stringify(form_fields));
    jQuery('#s4a_loader').show();
    reserveForm.submit();
  },
  validateChildrenAgeSelection: function(units) {
    let size = Object.keys(units).length;
    let i;
    let foo = true;

    for (i = 0; i < size; i++) {
      let index = i + 1;
      var children_obj = units[index]['Children'];

      let children_arr = Object.keys(children_obj).map(function(key) {
        return [Number(key), children_obj[key]];
      });
      let arr_size = children_arr.length;
      if (arr_size > 0) {
        var x;
        for (x = 0; x < arr_size; x++) {
          if (children_arr[x][1] === '' || children_arr[x][1] === undefined || children_arr[x][1] === null) {
            foo = false;
            break;
          }
        }
      }
    }
    return foo;
  },
  updateDate: function(startDate, endDate) {
    let _this = this;
    jQuery('.datepicker_checkin').val(startDate);
    jQuery('.datepicker_checkout').val(endDate);
    checkAvailibilty();
    jQuery('html, body').animate({
        scrollTop: jQuery(_this.id("js_SearchForSpecials")).offset().top
    }, 50);
  },
  get_available_rooms: function(caller, rooms, target, showValidate) {
    TrackGoogleAnalyticsEvent('User Behaviour', 'Availibility Search', 'Search', null);

    if (showValidate == null) {
      showValidate = true;
    } else {
      showValidate = showValidate;
    }

    let _this = this;
    let form_fields = _this.form_fields();

    S4AValidation('null', jQuery(_this.id(caller)).find('.datepicker_checkin'));
    S4AValidation('null', jQuery(_this.id(caller)).find('#datepicker_checkout'));
    jQuery(_this.id(caller)).find('input.s4a_val-error:first').focus();

    if (_this.validateChildrenAgeSelection(form_fields['Units'])) {
      if (jQuery('#select-alert').length > 0) {
        jQuery('#select-alert').remove();
      }

      if (S4AValidationSucess(_this.id(caller))) {
        // _this.id(rooms).html('<hr/><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div>');

        jQuery('#s4a_loader').show();

        jQuery.ajax({
          url: s4aJSO.ajax_url,
          method: 'POST',
          dataType: 'json',
          data: {
            action: s4aJSO.the_unique_plugin_name + '-ajax-submit',
            cmd: 'get_available_rooms',
            listing_id: form_fields.listing_id,
            the_arrival_date: form_fields.the_arrival_date,
            the_departure_date: form_fields.the_departure_date,
            Units: form_fields.Units,
          },
        }).done(function(data) {
          jQuery('#s4a_loader').hide();
          if (!data.error) {
            let local = data.html_str['ListingRooms']['local'];
            let channel = data.html_str['ListingRooms']['channel'];

            if (channel) {
              _this.id(rooms).html(channel);
            } else {
              _this.id(rooms).html(local);
            }
            calenderStart();
          }
        }).fail(function() {
          jQuery('#s4a_loader').hide();
        }).always(function(data) {
          jQuery('#s4a_loader').hide();
        });
      } else if (!showValidate) {
        S4ARemoveValidation(jQuery(_this.id(caller)).find('.datepicker_checkin'));
        S4ARemoveValidation(jQuery(_this.id(caller)).find('#datepicker_checkout'));
      }
    } else {
      if (jQuery('#select-alert').length == 0) {
        jQuery('<br><div id=\'select-alert\' class=\'alert alert-info\' role=\'alert\'> Please select children(s) age to continue!</div>').insertAfter('#get_available_rooms');
      }
    }
  },
  check_coupon_code: function(caller, form, price, listingId, target) {
    /* jQuery('#'+caller + ' #apply_button').unbind('click');
                    jQuery('#'+caller + ' #apply_button').off('click'); */

    let code = jQuery('#' + caller).find('#voucher_code').val();
    if(code == '') return;
    jQuery('#' + caller + ' #apply_button').attr('disabled', 'disabled');

    let email = jQuery('#' + form).find('.s4a_val-user_email').val();
    let flag = true;
    S4AValidation('null', jQuery(jQuery('#' + caller)).find('#voucher_code'));

    if (S4AValidationSucess(jQuery('#' + caller)) && flag) {
      flag = false;

      if (jQuery('#s4a_resevation').find('.payment_details .amount_details tbody .discount').length == 0) {
        jQuery.ajax({
          url: s4aJSO.ajax_url,
          method: 'POST',
          dataType: 'json',
          data: {
            action: s4aJSO.the_unique_plugin_name + '-ajax-submit',
            cmd: 'check_coupon_code',
            price: price,
            code: code,
            email: email,
            listingId: listingId,
          },

        }).done(function(data) {
          if (!data.error) {
            if (!data.html_str['error'] ) {
              if (data.html_str['status'] == "Active") {

                jQuery('#' + form).find('.promo_show').show();
                jQuery('#' + form).find('.exlcuding_message').show();

                $coupon_amount = data.html_str['coupon_amount'];
                $discount_type = data.html_str['discount_type'];

                if ($discount_type == "Fixed Amount Discount") 
                {
                  jQuery('#' + form).find('.promo_percent').hide();
                  jQuery('#' + form).find('.promo_amount').show();
                  jQuery('#' + form).find('.promo_amount .price_wrapper  .value').html($coupon_amount.toLocaleString());
                  jQuery('#' + form).find('.promo_amount .price_wrapper  .value').attr('data-value', $coupon_amount);
                } 
                else if ($discount_type == "Percentage Discount") 
                {
                  jQuery('#' + form).find('.promo_amount').hide();
                  jQuery('#' + form).find('.promo_percent').show();
                  jQuery('#' + form).find('#promo_percent').html($coupon_amount.toLocaleString()+'%');
                }

                jQuery('#' + caller).find('#coupon_amount').val(data.html_str['discount_amount']);
                jQuery('#' + form).find('.payment_details .amount_details tbody').append('<tr class="discount"><td>Discount: </td><td class="css_text_right"> -' + data.html_str['discount_amount'] + '</td></tr>');
                
                $total_amount = price - data.html_str['discount_amount'];
                jQuery('#' + form).find('.res_total  .price_wrapper  .value').html($total_amount.toLocaleString());
                jQuery('#' + form).find('.res_total  .price_wrapper  .value').attr('data-value', $total_amount);

                alert("Promo code applied successfully!");
              } 
              else 
              {
                alert("Promo code is inactive!");
                jQuery(jQuery('#' + caller)).find('#voucher_code').addClass('s4a_val-error');
              }
            }
            else 
            {
              alert("Invalid promo code!");
              jQuery(jQuery('#' + caller)).find('#voucher_code').addClass('s4a_val-error');
            }
          }
        }).fail(function() {}).always(function(data) {
          jQuery('#' + caller + ' #apply_button').removeAttr('disabled');
          flag = true;
        });
      } else {
        // One coupon already applied
        flag = true;
        jQuery(jQuery('#' + caller)).find('#voucher_code').addClass('s4a_val-warning');
        jQuery('#' + caller + ' #apply_button').removeAttr('disabled');
      }
    } else {
      jQuery('#' + caller + ' #apply_button').removeAttr('disabled');
    }
  },
};
