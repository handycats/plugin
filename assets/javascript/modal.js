function S4ALogin(wrapper) {

    S4AValidation("none", jQuery(wrapper).find(".s4a_val-special"));
    var email = S4AValidation("null", jQuery(wrapper).find(".s4a_val-email"));
    var password = S4AValidation("null", jQuery(wrapper).find(".s4a_val-password"));
    jQuery(wrapper).find("input.s4a_val-error:first").focus();
    if (S4AValidationSucess(wrapper)) {
        jQuery.ajax({
            url: s4aJSO.ajax_url,
            method: "POST",
            dataType: 'json',
            data: {
                action: s4aJSO.the_unique_plugin_name + '-ajax-submit',
                cmd: 'login',
                email: email,
                password: password
            }

        }).done(function (data) {
            if (!data.error) {
                location.reload();
            } else {
                jQuery(wrapper).find(".s4a_error-message").html(data.html_str);
            }
        }).fail(function () {
        }).always(function (data) {
        });
    }
}

function S4ARegister(wrapper) {
    S4AValidation("none", jQuery(wrapper).find(".s4a_val-special"));
    var email = S4AValidation("email", jQuery(wrapper).find(".s4a_val-email"));
    var password = S4AValidation("null", jQuery(wrapper).find(".s4a_val-password"));
    jQuery(wrapper).find("input.s4a_val-error:first").focus();
    if(email != '' && email != null){
        jQuery(function(){
			/*
            S4AValidationAjax(jQuery(wrapper).find(".s4a_val-email")).done(function(){
               if(S4AValidationSucess(wrapper)){
                    jQuery.ajax({
                        url: s4aJSO.ajax_url,
                        method: "POST",
                        dataType: 'json',
                        data: {
                            action: s4aJSO.the_unique_plugin_name + '-ajax-submit',
                            cmd: 'register',
                            email: email,
                            password: password
                        }
            
                    }).done(function (data) {
                        if (!data.error) {
                            location.reload();
                        } else {
                            jQuery(wrapper).find(".s4a_error-message").html(data.html_str);
                        }
                    }).fail(function () {
                    }).always(function (data) {
                    }); 
                }
            });
			*/
               if(S4AValidationSucess(wrapper)){
                    jQuery.ajax({
                        url: s4aJSO.ajax_url,
                        method: "POST",
                        dataType: 'json',
                        data: {
                            action: s4aJSO.the_unique_plugin_name + '-ajax-submit',
                            cmd: 'register',
                            email: email,
                            password: password
                        }
            
                    }).done(function (data) {
                        if (!data.error) {
                            location.reload();
                        } else {
                            jQuery(wrapper).find(".s4a_error-message").html(data.html_str);
                        }
                    }).fail(function () {
                    }).always(function (data) {
                    }); 
                }
        });
    }
}