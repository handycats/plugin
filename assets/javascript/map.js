function MapSearch() {
    jQuery(function ($) {
        // Asynchronously Load the map API 
        var script = document.createElement('script');
        script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBxFxGBHK0LuiBCNhNCVhJU3d1pU58gir0&libraries=places&callback=MapSearchDataInitialize";
        document.body.appendChild(script);
    });
}

function MapSearchDataInitialize() {
    jQuery.ajax({
        url: s4aJSO.ajax_url,
        method: "GET",
        data: {
            'action': "s4a-ajax-submit",
            'cmd': "get_ListingBySearchonMap"
            // 'title': "house"
        },
        success: function (result) {
            var obj = result;
            var error = obj.error;
            if (error != true && obj.html_str != "No Listing") {

                var markers = [];
                var infoWindowContent = [];

                jQuery.each(obj.html_str, function (idx, ob) {
                    markers.push([ob.markers.title,ob.markers.longitude,ob.markers.latitude]);
                    infoWindowContent.push([ob.infoWindowContent]);
                });
                MapSearchInitialize(markers, infoWindowContent);
                MapSearching(markers,infoWindowContent);
               }
        },
        error: function (errorThrown) {
            //alert("No listing Found");
        }    
    });
}

function MapSearchInitialize(markers, infoWindowContent) {
    var bounds = new google.maps.LatLngBounds();

    var lat_Long_Coords = new google.maps.LatLng(s4aMAP.latitude, s4aMAP.longitude);

    var mapOptions = {
        mapTypeId: 'roadmap',
        center: lat_Long_Coords,
        zoom: s4aMAP.zoom,
        scrollwheel: false
    };

    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);

    var infoWindow = new google.maps.InfoWindow(), marker, i;

    for (i = 0; i < markers.length; i++) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));
    }

    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function (event) {
        google.maps.event.removeListener(boundsListener);
    });

}

function MapSearching(markers, infoWindowContent) {
    var map;
    var bounds = new google.maps.LatLngBounds();

    // var lat_Long_Coords = new google.maps.LatLng(s4aMAP.latitude, s4aMAP.longitude);

    var map = new google.maps.Map(document.getElementById("map_canvas"), {
        mapTypeId : 'roadmap',
        center : {lat: s4aMAP.latitude, lng: s4aMAP.longitude},
        zoom : s4aMAP.zoom,
        scrollwheel : false
    });
    map.setTilt(45);

    var infoWindow = new google.maps.InfoWindow(), marker, i;

    for (i = 0; i < markers.length; i++) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));
    }

    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function (event) {
        google.maps.event.removeListener(boundsListener);
    });
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
      });

    var markers = [];
    searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
          return;
        }

        markers.forEach(function(marker) {
          marker.setMap(null);
        });
        markers = [];

        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
          if (!place.geometry) {
            console.log("Returned place contains no geometry");
            return;
          }
          var icon = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
          };

          markers.push(new google.maps.Marker({
            map: map,
            icon: icon,
            title: place.name,
            position: place.geometry.location
          }));

          if (place.geometry.viewport) {
            bounds.union(place.geometry.viewport);
          } else {
            bounds.extend(place.geometry.location);
          }
        });
        map.fitBounds(bounds);
      });
}