
var OverlayLoader = {
  show: function(target){

    if(target != ""){
      if(target == "s4a_resevation" || target == "s4a_enquiry"){
        if(!jQuery("#loading-overlay-full").length){
            jQuery("#"+target).find(".row")
              .prepend('<div id="loading-overlay-full"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>');
            jQuery("#loading-overlay-full").show(0).delay(10000).hide(0);
        }else{
            jQuery("#loading-overlay-full").show(0).delay(10000).hide(0);
        }
      }
     if(target == "get_available_rooms"){
        if(!jQuery("#loading-overlay").length){
            jQuery(".s4a_availability_control")
              .find(".col-md-12")
              .prepend('<div id="loading-overlay"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>');
            jQuery("#loading-overlay").show(0).delay(5000).hide(0);
        }else{
             jQuery("#loading-overlay").show(0).delay(5000).hide(0);
        }
      }
    }
  },
  hide:function(target){
    if(target != ""){
      if(target == "get_available_rooms"){
         jQuery("#loading-overlay").hide();
     }
    }
  }
}
