function S4APayment(wrapper) {

    S4AValidation("null", jQuery(wrapper).find(".s4a_val-card_first_name"));
    S4AValidation("null", jQuery(wrapper).find(".s4a_val-card_last_name"));
    S4AValidation("email", jQuery(wrapper).find(".s4a_val-card_email"));
    S4AValidation("null", jQuery(wrapper).find(".s4a_val-card_number"));
    S4AValidation("null", jQuery(wrapper).find(".s4a_val-card_cvv"));
    S4AValidation("checked", jQuery(wrapper).find(".s4a_val-terms"));
    jQuery(wrapper).find("input.s4a_val-error:first").focus();
    if (!S4AValidationSucess(wrapper)) {
        return false;
    }
}