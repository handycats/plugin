<?php

namespace S4A {

    class OperatorsControls
    {

        public static function search_results($ListingSummery, $uid, $imageType = "results", $noLasy = true, $display = false)
        {
            return include S4A_DIR_PATH_OPERATORS . '/templates/controls/search_results.php';
        }

        public static function latest_listings_widget($ListingSummeries)
        {
            include S4A_DIR_PATH_OPERATORS . '/templates/controls/latest_listings_widget.php';
        }

        public static function getLocationFromURL()
        {
            return include S4A_DIR_PATH_OPERATORS . '/templates/controls/getLocationFromURL.php';
        }

        public static function horizontal_search_widget($instance)
        {
            include S4A_DIR_PATH_OPERATORS . '/templates/controls/horizontal_search_widget.php';
        }

        public static function the_search_widget($instance)
        {
            include S4A_DIR_PATH_OPERATORS . '/templates/controls/the_search_widget.php';
        }

    }

}
?>
