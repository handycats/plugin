<?php

namespace S4A {

    require_once(S4A_DIR_PATH . '/includes/phpFastCache/phpFastCache.php');

    class OperatorsCache {

        const CACHEDURATION = 7200;
        const INITIALISATIONDATACACHEKEY = "OPERATORSINITIALISATIONDATACACHE";
        const LOCATIONREDIRECTCACHEKEY = "OPERATORSLOCATIONREDIRECTCACHE";
        const TYPEREDIRECTCACHEKEY = "OPERATORSTYPEREDIRECTCACHEKEY";
        const COUNTRYCACHEKEY = "OPERATORSCOUNTRYCACHE";

        private static $InitialisationDataCache;
        private static $CountryCache;
        private static $LocationRedirectCache;
        private static $TypeRedirectCache;

        static function InitialisationDataCache() {


            if (!OperatorsCache::$InitialisationDataCache) {

                OperatorsCache::$InitialisationDataCache = OperatorsCache::Get(OperatorsCache::INITIALISATIONDATACACHEKEY);

                if (!OperatorsCache::$InitialisationDataCache || isset($_GET['s4a_plugin']) || S4A_DISABLECACHE) {

                    $InitialisationDataCache = OperatorsService::get_InitialisationData();

                    OperatorsCache::$InitialisationDataCache = $InitialisationDataCache;

                    OperatorsCache::Put(OperatorsCache::INITIALISATIONDATACACHEKEY, $InitialisationDataCache, OperatorsCache::CACHEDURATION);
                }
            }

            return OperatorsCache::$InitialisationDataCache;
        }

        static function LocationRedirectCache() {

            if (!OperatorsCache::$LocationRedirectCache && 1 != 2) {

                OperatorsCache::$LocationRedirectCache = OperatorsCache::Get(OperatorsCache::LOCATIONREDIRECTCACHEKEY);

                if (!OperatorsCache::$LocationRedirectCache || isset($_GET['s4a_plugin']) || S4A_DISABLECACHE) {

                    $redirectsArry = array();

                    $InitialisationDataCache = OperatorsCache::InitialisationDataCache()->Locations;

                    foreach ($InitialisationDataCache as $countries) {

                        $data['location'] = "c" . $countries->id;
                        $postID = get_option(S4A_AREA_OPTION_OPERATOR . $data['location'], '');

                        if ($postID > 0) {
                            $redirectsArry[$postID] = \S4A\OperatorsHelpers::BuildResultsUrl(new \S4A\OperatorsStructs\SearchCriteria($data));
                        }
                    }

                    OperatorsCache::$LocationRedirectCache = $redirectsArry;

                    OperatorsCache::Put(OperatorsCache::LOCATIONREDIRECTCACHEKEY, $redirectsArry, OperatorsCache::CACHEDURATION);
                }
            }

            return OperatorsCache::$LocationRedirectCache;
        }

        static function CountryCache() {

            if (!OperatorsCache::$CountryCache) {

                OperatorsCache::$CountryCache = OperatorsCache::Get(OperatorsCache::COUNTRYCACHEKEY);

                if (!OperatorsCache::$CountryCache || isset($_GET['s4a_plugin']) || S4A_DISABLECACHE) {

                    $InitialisationDataCache = OperatorsCache::InitialisationDataCache();

                    $Countries = array();

                    foreach ($InitialisationDataCache->Locations as $Location) {
                        $Countries[$Location->id] = $Location;
                    }

                    OperatorsCache::$CountryCache = $Countries;

                    OperatorsCache::Put(OperatorsCache::COUNTRYCACHEKEY, $Countries, OperatorsCache::CACHEDURATION);
                }
            }

            return OperatorsCache::$CountryCache;
        }

        static function get_ListingByIdCache($id) {

            if (S4A_DISABLECACHE || S4A_DISABLELISTINGCACHE)
                return OperatorsService::get_ListingById($id);

            $ListingCacheKey = OperatorsCache::ListingCacheKey($id);

            $listing = \S4A\OperatorsCache::Get($ListingCacheKey);

            if (!$listing || isset($_GET['s4a_plugin'])) {

                $listing = OperatorsService::get_ListingById($id);

                \S4A\OperatorsCache::Put($ListingCacheKey, $listing, OperatorsCache::CACHEDURATION);
            }

            return $listing;
        }

        static function get_ListingBySearchCache($searchCriteria) {

            if (S4A_DISABLECACHE || S4A_DISABLESEARCHCACHE)
                return OperatorsService::get_ListingBySearch($searchCriteria);

            $SearchCriteriaCacheKey = \S4A\OperatorsCache::SearchCriteriaCacheKey($searchCriteria);

            $results = \S4A\OperatorsCache::Get($SearchCriteriaCacheKey);

            if (!$results || isset($_GET['s4a_plugin'])) {

                $results = OperatorsService::get_ListingBySearch($searchCriteria);

                \S4A\OperatorsCache::Put($SearchCriteriaCacheKey, $results, OperatorsCache::CACHEDURATION);
            }

            return $results;
        }

        static function Put($key, $data, $duration) {
            $cache = \phpFastCache();
            $cache->set($key, $data, $duration);
        }

        static function Get($key) {
            $cache = \phpFastCache();
            return $cache->get($key);
        }

        static function ListingCacheKey($id) {
            $key = "OperatorCacheKey_";
            $key .= $id;

            return $key;
        }

        static function SearchCriteriaCacheKey($SearchCriteria) {

            $key = "OperatorSearchCriteriaCacheKey_";
            $key .= "title" . $SearchCriteria->title;
            $key .= "areaId" . $SearchCriteria->areaId;
            $key .= "countryId" . $SearchCriteria->countryId;
            $key .= "provinceId" . $SearchCriteria->provinceId;
            $key .= "listingTypes" . $SearchCriteria->listingTypes;
            $key .= "priceFrom" . $SearchCriteria->priceFrom;
            $key .= "priceTo" . $SearchCriteria->priceTo;
            $key .= "specialId" . $SearchCriteria->specialId;
            $key .= "checkInDate" . $SearchCriteria->checkInDate;
            $key .= "checkOutDate" . $SearchCriteria->checkOutDate;
            $key .= "Page" . $SearchCriteria->Page;
            $key .= "perPage" . $SearchCriteria->perPage;

            if ($SearchCriteria->listingIds) {
                foreach ($SearchCriteria->listingIds as $listingIds) {
                    $key .= "listingIds" . $listingIds;
                }
            }

            return $key;
        }

    }

}
