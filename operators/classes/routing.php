<?php

$vp = new S4A_VirtualPages();

$search_url = "/" . S4A_URL_BASE_OPERATOR . "/search/";
$listing_url = "/" . S4A_URL_BASE_OPERATOR . "/listing/";
$reservation_url = "/" . S4A_URL_BASE_OPERATOR . "/reservation/";
$payment_url = "/" . S4A_URL_BASE_OPERATOR . "/payment/";
$enquiry_url = "/" . S4A_URL_BASE_OPERATOR . "/enquiry/";
$map_url = "/" . S4A_URL_BASE_OPERATOR . "/map/";
$csv_url = "/" . S4A_URL_BASE_OPERATOR . "/csv/";
$results_url = "/" . S4A_URL_BASE_OPERATOR . "/";

define('S4A_OPERATORS_RESULTS_URL', site_url() . $results_url);
define('S4A_OPERATORS_SEARCH_URL', site_url() . $search_url);
define('S4A_OPERATORS_LISTING_URL', site_url() . $listing_url);
define('S4A_OPERATORS_RESERVATION_URL', site_url() . $reservation_url);
define('S4A_OPERATORS_PAYMENT_URL', site_url() . $payment_url);
define('S4A_OPERATORS_ENQUIRY_URL', site_url() . $enquiry_url);

$vp->add('#' . $search_url . '#i', 'S4A_OperatorsSearch');
$vp->add('#' . $listing_url . '([a-z0-9\-]*)/([0-9]*)#i', 'S4A_OperatorsListing');
$vp->add('#' . $reservation_url . '([a-z0-9\-]*)/([0-9]*)#i', 'S4A_OperatorsReservation');
$vp->add('#' . $enquiry_url . '([a-z0-9\-]*)/([0-9]*)#i', 'S4A_OperatorsEnquiry');
$vp->add('#' . $payment_url . '([a-z0-9\-]*)/([0-9]*)#i', 'S4A_OperatorsPayment');
$vp->add('#' . $map_url . '#i', 'S4A_OperatorsMap');
$vp->add('#' . $csv_url . '#i', 'S4A_OperatorsCSV');

$vp->add('#' . $results_url . '([a-z\-]*)/([a-z\-]*)/([a-z\-]*)/([a-z\-]*)/([0-9]*)#i', 'S4A_OperatorsType');
$vp->add('#' . $results_url . '([a-z\-]*)/([a-z\-]*)/([a-z\-]*)/([0-9]*)#i', 'S4A_OperatorsArea');
$vp->add('#' . $results_url . '([a-z\-]*)/([a-z\-]*)/([0-9]*)#i', 'S4A_OperatorsProvince');
$vp->add('#' . $results_url . '([a-z\-]*)/([0-9]*)#i', 'S4A_OperatorsCountry');
$vp->add('#' . $results_url . '#i', 'S4A_OperatorsResults');