<?php

function S4A_OperatorsType($v, $url)
{
    $id = S4A\OperatorsHelpers::GetURLId($url);
    $type = S4A\OperatorsHelpers::GetURLType($url);
    S4A_OperatorsResults($v, $url, "a" . $id, $type);
}

function S4A_OperatorsTypeProvince($v, $url, $type)
{
    $id = S4A\Helpers::GetURLId($url);
    S4A_Results($v, $url, "p" . $id, $type);
}

function S4A_OperatorsArea($v, $url)
{
    if ($type = S4A\OperatorsHelpers::GetURLType($url)) {
        S4A_OperatorsTypeProvince($v, $url, $type);
    } else {
        $id = S4A\OperatorsHelpers::GetURLId($url);
        S4A_OperatorsResults($v, $url, "a" . $id);
    }
}

function S4A_OperatorsProvince($v, $url)
{
    $id = S4A\OperatorsHelpers::GetURLId($url);
    S4A_OperatorsResults($v, $url, "p" . $id);
}

function S4A_OperatorsCountry($v, $url)
{
    $id = S4A\OperatorsHelpers::GetURLId($url);
    S4A_OperatorsResults($v, $url, "c" . $id);
}

function S4A_OperatorsResults($v, $url, $location = null, $type = null)
{

    $model = new stdClass();

    $_GET['location'] = $location;

    $model->SearchCriteria = new S4A\OperatorsStructs\SearchCriteria($_GET);

    if ($type)
        $model->SearchCriteria->operatorTypes = $type;

    $model->UserSearchDates = S4A\Session::get_user_search_data();

    $model->ListingSearchResults = S4A\OperatorsCache::get_ListingBySearchCache($model->SearchCriteria);

    $model->Breadcrumbs = S4A\OperatorsHelpers::BuildBreadcrumbs($model->ListingSearchResults->Locations->Countries->id, $model->ListingSearchResults->Locations->Province->id, $model->ListingSearchResults->Locations->Area->id);

    switch ($model->SearchCriteria->locationType) {
        case "c":
            if (!$type && $countyPage = S4A\OperatorsHelpers::GetWpAreaPage($model->SearchCriteria->countryId, $model->SearchCriteria->locationType)) {
                if ($metaDescription = get_post_meta($countyPage->ID, '_yoast_wpseo_metadesc', true))
                    \S4A\OperatorsHelpers::AddMetaDescription($metaDescription);
                \S4A\OperatorsHelpers::RenderTemplate($v, $countyPage->post_title, "results", $model, ($model->SearchCriteria->Page < 2) ? $countyPage->post_content . $body : $body, get_option('wps4a_location_template'));
            } else {
                \S4A\OperatorsHelpers::RenderTemplate($v, "Results in " . $model->ListingSearchResults->Location->v_name, "results", $model, null, get_option('wps4a_location_template'));
            }
            break;
        case "p":
            if (!$type && $provincePage = S4A\OperatorsHelpers::GetWpAreaPage($model->SearchCriteria->provinceId, $model->SearchCriteria->locationType)) {
                if ($metaDescription = get_post_meta($provincePage->ID, '_yoast_wpseo_metadesc', true))
                    \S4A\OperatorsHelpers::AddMetaDescription($metaDescription);
                \S4A\OperatorsHelpers::RenderTemplate($v, $provincePage->post_title, "results", $model, ($model->SearchCriteria->Page < 2) ? $provincePage->post_content . $body : $body, get_option('wps4a_location_template'));
            } else {
                \S4A\OperatorsHelpers::RenderTemplate($v, "Results in " . $model->ListingSearchResults->Location->v_name, "results", $model, null, get_option('wps4a_location_template'));
            }
            break;
        case "a":
            if (!$type && $areaPage = S4A\OperatorsHelpers::GetWpAreaPage($model->SearchCriteria->areaId, $model->SearchCriteria->locationType)) {
                if ($metaDescription = get_post_meta($areaPage->ID, '_yoast_wpseo_metadesc', true))
                    \S4A\OperatorsHelpers::AddMetaDescription($metaDescription);
                \S4A\OperatorsHelpers::RenderTemplate($v, $areaPage->post_title, "results", $model, ($model->SearchCriteria->Page < 2) ? $areaPage->post_content . $body : $body, get_option('wps4a_location_template'));
            } else {
                \S4A\OperatorsHelpers::RenderTemplate($v, "Results in " . $model->ListingSearchResults->Location->v_name, "results", $model, null, get_option('wps4a_location_template'));
            }
            break;
        default:
            \S4A\OperatorsHelpers::RenderTemplate($v, "Search Results", "results", $model, null, get_option('wps4a_location_template'));
            break;
    }
}
