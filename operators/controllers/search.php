<?php
function S4A_OperatorsSearch($v, $url) {
    $SearchCriteria = new S4A\OperatorsStructs\SearchCriteria($_POST);
    S4A\Session::UserBookingData($SearchCriteria);
    header("Location: " . S4A\OperatorsHelpers::BuildResultsUrl($SearchCriteria));
    die();
}