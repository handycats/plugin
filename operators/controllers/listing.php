<?php

function S4A_OperatorsListing($v, $url)
{

    $model = new stdClass();

    $model->listing_id = S4A\OperatorsHelpers::GetURLId($url);

    $listing = S4A\OperatorsCache::get_ListingByIdCache($model->listing_id);

    if (!property_exists($listing, 'Operator'))
		\S4A\Helpers::Throw404();

    $model->Listing = $listing;
    $model->Provider = $listing->provider;

    $model->v_title = $model->Listing->Operator->v_title;

    \S4A\OperatorsHelpers::AddMetaDescription($model->Listing->Operator->t_full_description);

    $model->UserSearchDates = S4A\Session::get_user_search_data();

    if ($model->Listing->Operator->v_geo_coordinates)
        wp_enqueue_script('wps4a-gmap', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBxFxGBHK0LuiBCNhNCVhJU3d1pU58gir0', '', null, true);

    if ($model->Listing->Operator->i_tripadvisor)
        wp_enqueue_script('wps4a-tripadvisor', 'https://www.tripadvisor.co.za/WidgetEmbed-selfserveprop?border=true&popIdx=true&iswide=false&locationId=' . $model->Listing->Operator->i_tripadvisor . '&display_version=2&uniq=2010&rating=true&lang=en_ZA&nreviews=5&writereviewlink=true', '', null, true);

    wp_enqueue_script('wps4a-addthis', '//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-595ab63edcbb8fd0', '', null, true);

    $ListingGlobalInformation = $model->Listing->Operator->operator_global_information_global_information;

    if ($ListingGlobalInformation)
        uasort($ListingGlobalInformation, function ($a, $b) {
        return strcmp($a->v_name, $b->v_name);
    });

    if ($ListingGlobalInformation) {

        foreach ($ListingGlobalInformation as $listingGlobalInformation) {
            if ($listingGlobalInformation->e_type == 'Activities')
                $model->Activities[] = $listingGlobalInformation->operator_global_information[0];
            if ($listingGlobalInformation->e_type == 'Services')
                $model->Services[] = $listingGlobalInformation->operator_global_information[0];
            if ($listingGlobalInformation->e_type == 'Facts')
                $model->Facts[] = $listingGlobalInformation->operator_global_information[0];
            if ($listingGlobalInformation->e_type == 'Includes')
                $model->Includes[] = $listingGlobalInformation->operator_global_information[0];
            if ($listingGlobalInformation->e_type == 'Excludes')
                $model->Excludes[] = $listingGlobalInformation->operator_global_information[0];
            if ($listingGlobalInformation->e_type == 'Policies')
                $model->Policies[] = $listingGlobalInformation->operator_global_information[0];
                if ($listingGlobalInformation->e_type == 'Certifications')
                    $model->Certifications[] = $listingGlobalInformation->operator_global_information[0];
        }

        if ($model->Facts && $model->Services) {

            $FactsFacilities = array_merge($model->Facts, $model->Services);

            $usortFactsFacilities = uasort($FactsFacilities, function ($a, $b) {
                return strcmp($a->v_name, $b->v_name);
            });

            if ($usortFactsFacilities)
                $model->FactsFacilities[] = $FactsFacilities;
        }
    }

    $model->FactsIdArry = array();
    if ($model->Facts)
        foreach ($model->Facts as $fact)
        $model->FactsIdArry[$fact->id] = $fact->id;

    $model->Breadcrumbs = S4A\OperatorsHelpers::BuildBreadcrumbs($model->Listing->Operator->i_country_id, $model->Listing->Operator->i_province_id, $model->Listing->Operator->i_area_id, $model->Listing->Operator->v_title);

    if (S4A_LOGIN) {

        $current_user = wp_get_current_user();

        $fav = new stdClass();

        $fav->ShowFav = true;
        $fav->IsFav = (($current_user->ID && S4A\Meta::CheckIfFav($current_user->ID, $model->Listing->Operator->id)) ? true : false);
        $fav->ListingId = $model->Listing->Operator->id;

        $model->Fav = $fav;
    }


    \S4A\OperatorsHelpers::RenderTemplate($v, $model->Listing->Operator->v_title, "listing/" . S4A_LISTING_TEMPLATE, $model, ($this_post = get_option("wps4a_property_single_page")) ? $this_post->post_content : null, get_option('wps4a_property_single_page_template'), true);
}
