<?php
function S4A_OperatorsMap($v, $url) {

    $model = new stdClass();

    $translation_array = array(
        'plugin_path' => S4A_DIR_URL,
        'singlePagepath' => $fsingleprop_page
    );

    wp_localize_script('finnermerker-jquery-min-js', 'map_object', $translation_array);

    wp_enqueue_script('finnermerker-jquery-min-js');

    \S4A\OperatorsHelpers::RenderTemplate($v, "Map Search", "map", $model, ($this_post = get_option("wps4a_map_search_page")) ? $this_post->post_content : null, get_option('wps4a_map_search_template'));
}