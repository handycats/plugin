<?php if ($wrap) : ?>
    <div class="container">
        <div class="col-md-12">
            <?php if ($model->Fav) { ?>
                <i title="<?=($model->Fav->IsFav) ? 'Remove from Favorites' : 'Add to Favorites' ?>" class="fa <?=($model->Fav->IsFav) ? 'fa-star' : 'fa-star-o' ?> s4a_fav" data-id="<?= $model->Fav->ListingId ?>"></i>
            <?php } ?>
        <?php endif; ?>
        <?php if ($model->Breadcrumbs) { ?>
            <ul class="css_breadcrumb">
                <li><a href="<?php echo home_url(); ?>/">Home</a></li>
                <?php
                if (is_array($model->Breadcrumbs)) {
                    foreach ($model->Breadcrumbs as $breadcrumb) {
                        ?>
                        <?php if (is_array($breadcrumb)) { ?>
                            <li><a href="<?= $breadcrumb['url'] ?>"><?= $breadcrumb['name'] ?></a></li>
                            <?php
                        } else {
                            echo $breadcrumb;
                        }
                        ?>
                    <?php } ?>

                    <?php
                } else {
                    echo $model->Breadcrumbs;
                }
                echo "</ul>";
            }
            ?>
            <?php if ($model->Sucess) { ?>
                <p class="sucess_text"><?= $model->Sucess ?></p>
            <?php } ?>
            <?php if ($model->Error) { ?>
                <p class="error_text"><?= $model->Error ?></p>
            <?php } ?>
            <?php if ($wrap) : ?>
        </div>
    </div>
<?php endif; ?>