<div id="s4a_login_model" class="modal fade" role="dialog" style="display:block">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <h3 class="s4a-title primary_color_font">Login</h3>
                    <div class="col-md-6">
                        <form class="s4a_login-wrap" method="post" action="javascript:S4ALogin('.s4a_login-wrap');">

                              <input type="text" class="s4a_val-special" name="special" placeholder="other" />
                            <?php if(S4A_LOYALTY_ENABLE){ ?>

                              <label>Member Number:</label>
                              <input type="text" class="email s4a_val-email" name="email" placeholder="Email" />
                              <label>Member PIN:</label>
                              <input type="password" class="password s4a_val-password" name="password" placeholder="Password" />
                              <p class="primary_color_font">
                                (Please use Email and Password if you are not a Loyalty Member)
                              </p>

                            <?php } else { ?>

                            <label>Email</label>
                            <input type="text" class="email s4a_val-email" name="email" placeholder="Email" />
                            <label>Password</label>
                            <input type="password" class="password s4a_val-password" name="password" placeholder="Password" />

                            <?php  }?>

                            <input class="grn_button primary_color" value="Login" type="submit" />
                            <input type="checkbox" class="remeber" /><span>Remember Me</span><br>
                            <p class="s4a_error-message"><a href="/wp-login.php?action=lostpassword">Lost your password?</a></p>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form class="s4a_register-wrap" method="post" action="javascript:S4ARegister('.s4a_register-wrap');">
                            <h3 class="s4a-title primary_color_font">Register</h3>
                            <input type="text" class="s4a_val-special" name="special" placeholder="other" />
                            <label>Email</label>
                            <input type="text" class="email s4a_val-email" name="email" placeholder="Email" />
                            <label>Password</label>
                            <input type="password" class="password s4a_val-password" name="password" placeholder="Password" />
                            <?php if(S4A_LOYALTY_ENABLE){ ?>

                              <p class="primary_color_font">
                                (Interested in becoming a Loyalty Member? <a href="/real-rewards/">Click Here!</a>)
                              </p>

                            <?php } ?>
                            <input class="grn_button primary_color" value="Register" type="submit" />
                            <p class="s4a_error-message"></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div id="advanceSearchModal" class="modal fade" role="dialog" style="display:none">
    <div class="modal-dialog" style="max-width: 950px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3>Advance Filters </h3>
            </div>
            <div class="modal-body">
                <form class="property_advance_search_form" name="property_advance_search_form" method="post" >

                <div class="row">
                    <div class="col-md-12">
                        <h3>Facts</h3>
                        <div class="facts_container">
                            <?php
                            $userSearchData = \S4A\Session::get_user_search_data();
                            $facts_serach = explode(',',$_GET['facts']);
                            foreach (\S4A\Cache::InitialisationDataCache()->Facts as $facts) {
                                    $checked = ($_GET['facts'] && in_array($facts->id, $facts_serach)) ? "checked" : null;
                                    ?>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">
                                            <div class="checkbox-list">
                                                <label class="checkbox-inline">
                                                    <input class="facts" type="checkbox" name="facts[]" value="<?= $facts->id ?>" <?= $checked ?>/>
                                                    <span class="break-word"><?= $facts->v_name?></span>
                                                </label>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            <?php
                                }
                            ?>

                        </div>
                    </div>
                    <hr/>
                    <div class="col-md-12">
                        <h3>Facilities</h3>
                        <div class="facilities_container">
                            <?php
                            $facilities_serach = explode(',',$_GET['facilities']);
                            foreach (\S4A\Cache::InitialisationDataCache()->Facilities as $facilities) {
                                    $checked = ($_GET['facilities'] && in_array($facilities->id, $facilities_serach)) ? "checked" : null;
                                    ?>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">
                                            <div class="checkbox-list">
                                                <label class="checkbox-inline">
                                                    <input class="facilities" type="checkbox" name="facilities[]" value="<?= $facilities->id ?>" <?= $checked ?>/>
                                                    <span class="break-word"><?= $facilities->v_name?></span>
                                                </label>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            <?php
                                }
                            ?>
                        </div>
                    </div>
                    <hr/>
                    <div class="col-md-12">
                        <h3>Activities</h3>
                        <div class="activities_container">

                            <?php
                            $activities_serach = explode(',',$_GET['activities']);
                            foreach (\S4A\Cache::InitialisationDataCache()->Activities as $activities) {
                                    $checked = ($_GET['activities'] && in_array($activities->id, $activities_serach)) ? "checked" : null;
                                    ?>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">
                                            <div class="checkbox-list">
                                                <label class="checkbox-inline">
                                                    <input class="activities" type="checkbox" name="activities[]" value="<?= $activities->id ?>" <?= $checked ?>/>
                                                    <span class="break-word"><?= $activities->v_name?></span>
                                                </label>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            <?php
                                }
                            ?>

                        </div>
                    </div>
                    <div class="col-md-12 advance_search_button">
                        <input type="button" name="search" value="Apply" class="submit_search grn_button" onClick="javascript:AdvanceSearchApply();">
                        <button type="button" value="Reset" class="btn default" onClick="javascript:AdvanceSearchReset();"> Clear </button>
                        <!-- <input type="button" name="reset" value="Clear" class="reset_search" onClick="property_advance_search_form[[0].reset();"> -->
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
