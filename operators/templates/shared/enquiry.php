<?php
$current_user = wp_get_current_user();
?>
<form id="s4a_enquiry" action="<?=\S4A\OperatorsHelpers::EnquiryUrl($model->v_title, $model->listing_id)?>" method="post" class="css_form" onSubmit="return S4AEnquiry('#s4a_enquiry')">
    <input type="text" class="s4a_val-special" name="special" placeholder="other" />
    <div>
        <?php if ($model->error) { ?>
            <p class="error_text"><?php print_r($model->error); ?></p>
        <?php
    } ?>
        <?php if ($model->AvailabilityCriteria->ListingRoom) { ?>
        <div class="get-available-room cf">
            <table class="s4a_table">
                <tbody>

                    <tr class="first-cell">
                        <th>Establishment</th>
                        <th>Room Type</th>
                        <th>Check In</th>
                        <th>Check Out</th>
                        <th>Price</th>
                    </tr>
                    <tr>
                        <td data-label="Establishment">
                            <?= $model->Reservation->ReservationCriteria->establishment ?>
                        </td>
                        <td data-label="Room Type">
                            <?= $model->Reservation->ReservationCriteria->ListingRoom->description ?>
                        </td>
                        <td data-label="Check In">
                            <?= $model->Reservation->ReservationCriteria->the_arrival_date ?>
                        </td>
                        <td data-label="Check Out">
                            <?= $model->Reservation->ReservationCriteria->the_departure_date ?>
                        </td>
                        <td data-label="Price">
                            R <?= number_format($model->Reservation->ReservationCriteria->ListingRoom->price) ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <?php
    } ?>
    </div>
    <div class="row">
        <div class="col-md-3">
            <input placeholder="First Name" type="text" name="user_name" class="s4a_val-user_name" value="<?= $model->FormData->user_name ?>" />
        </div>
        <div class="col-md-3">
            <input placeholder="Last Name" type="text" name="user_last_name" class="s4a_val-user_last_name" value="<?= $model->FormData->user_last_name ?>" />
        </div>
        <div class="col-md-6">
            <input <?= ($current_user->exists()) ? "readonly" : null ?> placeholder="Email..." type="text" name="user_email" class="s4a_val-user_email" value="<?= $model->FormData->user_email ?>" />
        </div>
</div>
<div class="row">
        <div class="col-md-6">
            <input placeholder="Phone..." type="text" name="user_phone" class="s4a_val-user_phone" value="<?= $model->FormData->user_phone ?>" />
        </div>




		<?php if(S4A_DISABLE_AVAILABILITY) {?>
        <div class="col-md-6">
            <?php
            $country = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Antigua and Barbuda", "Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Costa Rica", "Côte d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "North Korea", "South Korea", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestine, State of", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Romania", "Russia", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia and Montenegro", "Seychelles", "Sierra Leone", "Singapore", "Sint Maarten", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "Spain", "Sri Lanka", "Sudan", "Sudan, South", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Virgin Islands, British", "Virgin Islands, U.S.", "Yemen", "Zambia", "Zimbabwe");
            ?>
            <select name="user_nationality">
                <?php
                if ($country) {
                    $user_nationality = ($model->FormData->user_nationality) ? $model->FormData->user_nationality : "Nationality...";
                    foreach ($country as $key => $value) {
                        $selected = "";
                        if ($user_nationality == $value)
                            $selected = "selected='selected'";
                        echo '<option value="' . $value . '"  ' . $selected . ' >' . $value . '</option>';
                    }
                }
                ?>

            </select>
        </div>
		<?php } else { ?>
        <div class="col-md-4">
            <?php
            $country = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Antigua and Barbuda", "Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Costa Rica", "Côte d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "North Korea", "South Korea", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestine, State of", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Romania", "Russia", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia and Montenegro", "Seychelles", "Sierra Leone", "Singapore", "Sint Maarten", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "Spain", "Sri Lanka", "Sudan", "Sudan, South", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Virgin Islands, British", "Virgin Islands, U.S.", "Yemen", "Zambia", "Zimbabwe");
            ?>
            <select name="user_nationality">
                <?php
                if ($country) {
                    $user_nationality = ($model->FormData->user_nationality) ? $model->FormData->user_nationality : "Nationality...";
                    foreach ($country as $key => $value) {
                        $selected = "";
                        if ($user_nationality == $value)
                            $selected = "selected='selected'";
                        echo '<option value="' . $value . '"  ' . $selected . ' >' . $value . '</option>';
                    }
                }
                ?>

            </select>
        </div>
        <?php } ?>
          </div>
    <div class="row">
        <div class="col-md-12">
            <textarea placeholder="Your Message..." name="user_special" rows="5" cols="50"><?= $model->FormData->user_special ?></textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          <br />
          <?php if ($model->Terms) { ?>
              <p><input class="s4a_val-terms" type="checkbox" /> I agree to terms (<a href="<?= esc_url(get_permalink($model->Terms)); ?>" target="_blank">Terms and Conditions</a>)</p>
          <?php
        } ?>
          <br />
        </div>
    </div>
    <input id="sub-but" type="submit" value="Submit" class="grn_button" />
    <br /><br />
    <input type="hidden" name="isBooking" value="true" />
    <?php $utmData = \S4A\Session::get_utm_data(); ?>
    <input type="hidden" name="source" value="<?= $utmData->utm_source ?>" />
    <input type="hidden" name="AvailabilityCriteria" value='<?= json_encode($model->AvailabilityCriteria, JSON_HEX_APOS) ?>' />
</form>
