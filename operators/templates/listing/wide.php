<div class="listing-page-wide-wrapper">
    <div class="single-property-list row">
        <div class="row" >
            <div class="col-md-9">
                <!-- Gallery START -->
                <?php if (!empty($model->Listing->Operator->operator_image)) {?>
                    <div id="gallery" style="display:none">
                      <?php if (!empty(($model->Listing->Listing->listing_video))) {

    foreach ($model->Listing->Listing->listing_video as $listing_video) {?>

                          <?php
$curr_url = $listing_video->v_url;

        $yt_rx = '/^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/';
        $has_match_youtube = preg_match($yt_rx, $curr_url, $yt_matches);

        $vm_rx = '/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([‌​0-9]{6,11})[?]?.*/';
        $has_match_vimeo = preg_match($vm_rx, $curr_url, $vm_matches);

        if ($has_match_youtube) {
            $video_id = $yt_matches[5];
            $type = 'youtube';
        } elseif ($has_match_vimeo) {
            $video_id = $vm_matches[5];
            $type = 'vimeo';
        } else {
            $video_id = 0;
            $type = 'none';
        }
        $id = $video_id;
        ?>

                            <?php if ($type == 'youtube') {?>
                                <img data-type="youtube" data-videoid="<?php echo $id ?>" data-image="//img.youtube.com/vi/<?php echo $id ?>/0.jpg">
                            <?php }?>

                            <?php if ($type == 'vimeo') {

            $link = "http://vimeo.com/" . $id;
            $link = str_replace('http://vimeo.com/', 'http://vimeo.com/api/v2/video/', $link) . '.php';
            $html_returned = unserialize(file_get_contents($link));
            $thumb_url = $html_returned[0]['thumbnail_large'];
            ?>
                              <img 	data-type="vimeo"	src="<?php echo $thumb_url ?>"	data-image="<?php echo $thumb_url ?>"		 data-videoid="<?php echo $id ?>">

                            <?php }?>
                      <?php }}?>
                        <?php foreach ($model->Listing->Operator->operator_image as $image) {?>
                            <img class="realImage" src="<?=S4A_IMG_URL . '?id=' . $image->id . '&type=listing_thumb&file=operator'?>" data-image="<?=S4A_IMG_URL . '?id=' . $image->id . '&type=origional&file=operator'?>">
                        <?php
}?>
                    </div>
                <?php
}?>
            </div>
            <div class="col-md-3" id="listing_side">

                <div class="row css_listing_det">
                  <dl class="prop_val">
                      <?php if ($Country = \S4A\OperatorsCache::CountryCache()[$model->Listing->Operator->i_country_id]) {?><dt class="prop_cou">Country</dt><dd class="prop_cou"><?=$Country->v_name?></dd><?php }?>

                      <?php if (($model->Listing->Operator->v_op_from == $model->Listing->Operator->v_op_to) || ($model->Listing->Operator->v_op_from == "January" && $model->Listing->Operator->v_op_to == "December")) {?>
                          <dt class="prop_cou">Operates</dt><dd class="prop_cou">All Year Round</dd>
                        <?php } else {?>

                          <dt class="prop_cou">Operates</dt><dd class="prop_cou"><?=$model->Listing->Operator->v_op_from?> - <?=$model->Listing->Operator->v_op_to?></dd>
                        <?php }?>
                    <?php if (isset($model->Listing->Operator->operator_type) && !empty($model->Listing->Operator->operator_type)) {
    $i = 0;?>  <dt class="prop_cou">Services</dt>             <?php foreach ($model->Listing->Operator->operator_type as $operator_type) {
        echo '<dd class="prop_cou">' . $model->Listing->Operator->operator_type[$i][0]->v_name . '</dd>';
        $i += 1;
    }

    ?><?php
}?>



                </div>
                <!-- <div id="accordion" role="tablist" aria-multiselectable="true"> -->


                             <!-- <div class="card">
                                <div class="card-header" role="tab" id="headingOne">
                                    <h4 class="mb-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#features_type" aria-expanded="true" aria-controls="features_type" class="collapsed">
                                            <i class="fa fa-minus" aria-hidden="true"></i> Tour Type
                                        </a> -->

                                    <!-- </h4>
                                </div>
                                <div id="features_type" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="card-block css_features_type">
                                        <ul>

                                        </ul>
                                    </div>
                                </div>
                            </div> -->

                            <!-- </div>                            -->

                <div class="css_listing_fac">
                    <h3>Quick Facts</h3>
                    <ul>
                        <?php
foreach (\S4A\OperatorsCache::InitialisationDataCache()->Facts as $fact) {
    $value = ($model->FactsIdArry != null && in_array($fact->id, $model->FactsIdArry)) ? "Yes" : "No";
    echo "<li>" . $fact->v_name . ": <b>" . $value . "</b></li>";
}
?>
                    </ul>

                </div>
                <div class='row enqButton'>
                    <a href="javascript:ScrollToTarget('#s4a_enquiry');" type="button" class="btn btn-primary btn-lg btn-block">CONTACT OPERATOR</a>
                    </div>
            </div>
        </div>
        <div class="row s4a_z_description">

        <div class="col-md-12">
            <?php if ($model->Listing->Operator->t_full_description) {?>
                <h3 class="s4a-title">Description</h3>
                <div class="addthis_inline_share_toolbox"></div>
                <div class="full-description">
                    <?=nl2br($model->Listing->Operator->t_full_description, false)?>
                </div>
            <?php
}?>
        </div>
        <div class="col-md-12">
            <?php
if ($model->Listing->Operator->listing_video) {
    foreach ($model->Listing->Operator->listing_video as $listing_video) {?>

            <?php
$curr_url = $listing_video->v_url;

        $yt_rx = '/^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/';
        $has_match_youtube = preg_match($yt_rx, $curr_url, $yt_matches);

        $vm_rx = '/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([‌​0-9]{6,11})[?]?.*/';
        $has_match_vimeo = preg_match($vm_rx, $curr_url, $vm_matches);

        if ($has_match_youtube) {
            $video_id = $yt_matches[5];
            $type = 'youtube';
        } elseif ($has_match_vimeo) {
            $video_id = $vm_matches[5];
            $type = 'vimeo';
        } else {
            $video_id = 0;
            $type = 'none';
        }
        $id = $video_id;
        /*  $width = '600px';
        $height = '380px'; */
        ?>

                <?php if ($type == 'youtube') {?>
                    <div class="col-md-6">
                    <iframe id="ytplayer" type="text/html" width="100%" height="380"
                    src="https://www.youtube.com/embed/<?php echo $id ?>?rel=0&showinfo=0&color=white&iv_load_policy=3"
                    frameborder="0" allowfullscreen>
                    </iframe></div>

            <?php }?>

            <?php if ($type == 'vimeo') {?>


            <iframe src="https://player.vimeo.com/video/<?php echo $id ?>" width="600" height="380"
                frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen>
            </iframe>


            <?php }?>


        <?php }}?>
        </div>


        </div>
        <div >
            <div class="css_features cf">
            <div class='row'>
                <div class="col-md-5">
                    <h3 class="s4a-title">MORE <strong>INFORMATION</strong></h3>
                    <div id="accordion" role="tablist" aria-multiselectable="true">
                        <?php if ($model->Includes) {?>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingOne">
                                    <h4 class="mb-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#features_inc" aria-expanded="true" aria-controls="features_inc" class="collapsed">
                                            <i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i>Includes
                                        </a>
                                    </h4>
                                </div>
                                <div id="features_inc" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="card-block css_features_inc">
                                        <ul>
                                            <?php
foreach ($model->Includes as $listingGlobalInformation) {
    echo "<li>" . $listingGlobalInformation->v_name . "</li>";
}
    ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php

}?>
                        <?php if ($model->Excludes) {?>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingOne">
                                    <h4 class="mb-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#features_exc" aria-expanded="true" aria-controls="features_exc" class="collapsed">
                                            <i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i>Excludes
                                        </a>
                                    </h4>
                                </div>
                                <div id="features_exc" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="card-block css_features_exc">
                                        <ul>
                                            <?php
foreach ($model->Excludes as $listingGlobalInformation) {
    echo "<li>" . $listingGlobalInformation->v_name . "</li>";
}
    ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php

}?>
                        <?php if ($model->FactsFacilities) {?>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingOne">
                                    <h4 class="mb-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#listing_faci" aria-expanded="true" aria-controls="listing_faci" class="collapsed">
                                            <i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i>Services
                                        </a>
                                    </h4>
                                </div>
                                <div id="listing_faci" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="card-block css_listing_faci">
                                        <ul>
                                            <?php
foreach ($model->FactsFacilities[0] as $listingGlobalInformation) {
    echo "<li>" . $listingGlobalInformation->v_name . "</li>";
}
    ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php

}?>
                        <?php if ($model->Activities) {?>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingOne">
                                    <h4 class="mb-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#listing_act" aria-expanded="true" aria-controls="listing_act" class="collapsed">
                                            <i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i>Activities
                                        </a>
                                    </h4>
                                </div>
                                <div id="listing_act" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="card-block css_listing_act">
                                        <ul>
                                            <?php
foreach ($model->Activities as $listingGlobalInformation) {
    echo "<li>" . $listingGlobalInformation->v_name . "</li>";
}
    ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php

}?>
                    <?php if ($model->Services) {?>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingOne">
                                <h4 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#features_serv" aria-expanded="true" aria-controls="features_serv" class="collapsed">
                                        <i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i>Services
                                    </a>
                                </h4>
                            </div>
                            <div id="features_serv" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="card-block css_features_serv">
                                    <ul>
                                        <?php
foreach ($model->Services as $listingGlobalInformation) {
    echo "<li>" . $listingGlobalInformation->v_name . "</li>";
}
    ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php

}?>
                        <?php if ($model->Certifications) { ?>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingOne">
                                    <h4 class="mb-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#features_ass" aria-expanded="true" aria-controls="features_ass" class="collapsed">
                                             <i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i> Associations
                                        </a>
                                    </h4>
                                </div>
                                <div id="features_ass" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="card-block css_features_ass">
                                        <ul>
                                            <?php
                                            foreach ($model->Certifications as $certification) {
                                                echo "<li>" . $certification->v_name . "</li>";
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <?php
                        } ?>


                    </div>

<div class='row webRow'>
      </br>
    <div class='row'>
      <?php echo "<a type=' button ' class='btn btn-primary btn-sm ourWeb' onclick='TrackGoogleAnalyticsEvent(\"listing-website\", \"accommodation\", \"" . $model->Listing->Operator->id . "\", null);' id='s4a_listingURL' data-id='" . $model->Listing->Operator->id . "'>Vist our website</a>" ?>
        <a class="web_hidden"  target='_blank' rel='nofollow' href="<?php echo ((strpos($model->Listing->Operator->v_url, "http") !== false) ? null : "http://") . $model->Listing->Operator->v_url ?>"><?php echo ((strpos($model->Listing->Operator->v_url, "http") !== false) ? null : "http://") . $model->Listing->Operator->v_url ?></a>
  </div>
</div>
                </div>


 <?php
if (S4A_DISABLE_AVAILABILITY) {
    echo '<div class="col-md-7" style="padding-bottom:50px">';
    echo '<h3 class="s4a-title">CONTACT <strong>OPERATOR</strong></h3>';
    include S4A_DIR_PATH_OPERATORS . '/templates/shared/enquiry.php';
    echo '</div>';
}
?>



            </div>



                </br>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        window.addEventListener('load', function () {

			<?php if ($latLong) {?>
			jQuery('#map_canvas').appear(function() {
			  initialize_GMap("<?=$longitude?>", "<?=$latitude?>");
			});
			<?php }?>

            jQuery("#gallery").unitegallery({
                gallery_width: null,
                gallery_height: null,
                slider_zoom_mousewheel: false,
                gallery_autoplay: true,
                gallery_play_interval: 10000
            });

			var main_pic = jQuery('.ug-slide2 img').attr('src');
            $('.z_slide.z_parallax').css({'background-image': 'url('+main_pic+')'});

            if (typeof fbq === 'function') {

                fbq('track', 'ViewContent', {
                    content_name: '<?=\S4A\Helpers::AlphaNumeric($model->Listing->Operator->v_title)?>',
                    content_category: 'Accomodation',
                    content_ids: '<?=$model->Listing->Operator->id?>',
                    content_type: 'product',
                    value: '<?=$model->Listing->Operator->i_price_from?>',
                    currency: 'ZAR'
                });

            }


        });
    </script>
</div>
