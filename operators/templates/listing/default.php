<div class="listing-page-wrapper">
    <div class="single-property-list row">
        <div class="container">
            <div class="col-md-9">
              <!-- Gallery START -->
                <?php if (!empty($model->Listing->Listing->listing_image)) { ?>
                    <div id="gallery" style="display:none">
                                <?php if (!empty(($model->Listing->Listing->listing_video))) {

                                    foreach ($model->Listing->Listing->listing_video as $listing_video) { ?>

                                    <?php
                                    $curr_url = $listing_video->v_url;

                                    $yt_rx = '/^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/';
                                    $has_match_youtube = preg_match($yt_rx, $curr_url, $yt_matches);

                                    $vm_rx = '/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([‌​0-9]{6,11})[?]?.*/';
                                    $has_match_vimeo = preg_match($vm_rx, $curr_url, $vm_matches);

                                    if($has_match_youtube) {
                                    $video_id = $yt_matches[5];
                                    $type = 'youtube';
                                    }
                                    elseif($has_match_vimeo) {
                                    $video_id = $vm_matches[5];
                                    $type = 'vimeo';
                                    }
                                    else {
                                    $video_id = 0;
                                    $type = 'none';
                                    }
                                $id = $video_id;
                            ?>

                              <?php if ($type == 'youtube') { ?>
                                  <img data-type="youtube" data-videoid="<?php echo $id ?>" data-image="//img.youtube.com/vi/<?php echo $id ?>/0.jpg">
                              <?php } ?>

                              <?php if ($type == 'vimeo') {

                                  $link = "http://vimeo.com/". $id;
                                  $link = str_replace('http://vimeo.com/', 'http://vimeo.com/api/v2/video/', $link) . '.php';
                                  $html_returned = unserialize(file_get_contents($link));
                                  $thumb_url = $html_returned[0]['thumbnail_large'];
                                  ?>
                                <img 	data-type="vimeo"	src="<?php echo $thumb_url ?>"	data-image="<?php echo $thumb_url ?>"		 data-videoid="<?php echo $id ?>">

                              <?php } ?>
                        <?php } ?>

                        <?php foreach ($model->Listing->Listing->listing_image as $image) { ?>
                            <img class="realImage" src="<?= S4A_IMG_URL . '?id=' . $image->id . '&type=listing_thumb' ?>" data-image="<?= S4A_IMG_URL . '?id=' . $image->id . '&type=origional' ?>">
                        <?php
                    } ?>
                    </div>
                <?php
              } ?>
              </div>
              <!-- Gallery END -->
            </div>
            <div class="col-md-3" id="listing_side">
            <a href="<?= \S4A\OperatorsHelpers::EnquiryUrl($model->Listing->Operator->v_title, $model->Listing->Operator->id) ?>" class="row el_button grn_button">Make Enquiry</a>
                <div class="row css_listing_det">
                    <dl class="prop_val">
                        <?php if ($Country = \S4A\OperatorsCache::CountryCache()[$model->Listing->Operator->i_country_id]) { ?><dt class="prop_cou">Country</dt><dd class="prop_cou"><?= $Country->v_name ?></dd><?php } ?>
                        <?php if ($Country = \S4A\OperatorsCache::CountryCache()[$model->Listing->Operator->i_country_id]) { ?><dt class="prop_cou">Operates</dt><dd class="prop_cou"><?= $model->Listing->Operator->v_op_from?> - <?= $model->Listing->Operator->v_op_to?></dd><?php } ?>
                    </dl>
                </div>
                <div class="css_listing_fac">
                    <h3>Quick Facts</h3>
                    <ul>
                        <?php
                        foreach (\S4A\OperatorsCache::InitialisationDataCache()->Facts as $fact) {
                        // foreach ($model->Facts as $fact) {
                            $value = ($model->FactsIdArry != null && in_array($fact->id, $model->FactsIdArry)) ? "Yes" : "No";
                            echo "<li>" . $fact->v_name . ": <b>" . $value . "</b></li>";
                        }
                        // foreach ($model->Facts as $listingGlobalInformation) {
                        //   echo "<li>" . $listingGlobalInformation->v_name . ": <b>" . $value . "</b></li>";
                        // }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="col-md-12">
                <?php if ($model->Listing->Operator->t_full_description) { ?>
                    <h3 class="s4a-title">Description</h3>
                    <div class="addthis_inline_share_toolbox"></div>
                    <div class="full-description">
                        <?= nl2br($model->Listing->Operator->t_full_description, false) ?>
                    </div>
                <?php

            } ?>
            </div>
        </div>
        <div class="container">
            <div class="css_features cf">
                <div class="col-md-6">
                    <h3 class="s4a-title">Features</h3>
                    <div id="accordion" role="tablist" aria-multiselectable="true">
                        <?php if ($model->Includes) { ?>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingOne">
                                    <h4 class="mb-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#features_inc" aria-expanded="true" aria-controls="features_inc" class="collapsed">
                                            <i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i>Includes
                                        </a>
                                    </h4>
                                </div>
                                <div id="features_inc" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="card-block css_features_inc">
                                        <ul>
                                            <?php
                                            foreach ($model->Includes as $listingGlobalInformation) {
                                                echo "<li>" . $listingGlobalInformation->v_name . "</li>";
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php

                    } ?>
                        <?php if ($model->FactsFacilities) { ?>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingOne">
                                    <h4 class="mb-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#listing_faci" aria-expanded="true" aria-controls="listing_faci" class="collapsed">
                                            <i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i>Services
                                        </a>
                                    </h4>
                                </div>
                                <div id="listing_faci" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="card-block css_listing_faci">
                                        <ul>
                                            <?php
                                            foreach ($model->FactsFacilities[0] as $listingGlobalInformation) {
                                                echo "<li>" . $listingGlobalInformation->v_name . "</li>";
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php

                    } ?>
                        <?php if ($model->Activities) { ?>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingOne">
                                    <h4 class="mb-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#listing_act" aria-expanded="true" aria-controls="listing_act" class="collapsed">
                                            <i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i>Activities
                                        </a>
                                    </h4>
                                </div>
                                <div id="listing_act" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="card-block css_listing_act">
                                        <ul>
                                            <?php
                                            foreach ($model->Activities as $listingGlobalInformation) {
                                                echo "<li>" . $listingGlobalInformation->v_name . "</li>";
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php

                    } ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <h3 class="s4a-title">Terms & Conditions</h3>
                    <div id="accordion" role="tablist" aria-multiselectable="true">
                        <?php if ($model->Excludes) { ?>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingOne">
                                    <h4 class="mb-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#features_exc" aria-expanded="true" aria-controls="features_exc" class="collapsed">
                                            <i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i>Excludes
                                        </a>
                                    </h4>
                                </div>
                                <div id="features_exc" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="card-block css_features_exc">
                                        <ul>
                                            <?php
                                            foreach ($model->Excludes as $listingGlobalInformation) {
                                                echo "<li>" . $listingGlobalInformation->v_name . "</li>";
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php

                    } ?>
                        <?php if ($model->Policies) { ?>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingOne">
                                    <h4 class="mb-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#policies_exc" aria-expanded="true" aria-controls="policies_exc" class="collapsed">
                                            <i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i>Child Policy
                                        </a>
                                    </h4>
                                </div>
                                <div id="policies_exc" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="card-block css_policies_exc">
                                        <ul>
                                            <?php
                                            foreach ($model->Policies as $listingGlobalInformation) {
                                                echo "<li>" . $listingGlobalInformation->v_name . "</li>";
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php

                    } ?>
                        <?php if ($model->Listing->Operator->terms_conditions || get_option('wps4a_global_settings_tc')) { ?>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingOne">
                                    <h4 class="mb-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#features_ter" aria-expanded="true" aria-controls="features_ter" class="collapsed">
                                            <i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i>Terms and Conditions
                                        </a>
                                    </h4>
                                </div>
                                <div id="features_ter" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="card-block css_features_ter">
                                        <?= $model->Listing->Operator->t_terms_conditions ?>
                                    </div>
                                </div>
                            </div>
                        <?php

                    } ?>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <?php if ($model->Listing->Operator->i_tripadvisor) { ?>
                        <div class="col-md-3">
                            <div id="TA_selfserveprop2010" class="TA_selfserveprop">
                                <ul id="laWm0i78Xvx" class="TA_links 8342infZV">
                                    <li id="J6uIHkDY7" class="zit2VOza">
                                    </li>
                                </ul>
                            </div>
                        </div>
                    <?php

                } ?>
                    <?php
                    $latLong = $model->Listing->Operator->v_geo_coordinates;

                    if ($latLong) {
                        $coordinates = explode(',', $latLong);
                        $longitude = floatval($coordinates[0]);
                        $latitude = floatval($coordinates[1]);
                        ?>
                        <div class="col-md-9">
                            <div id="map_canvas"></div>
                        </div>
                    <?php

                } ?>
                </div>
                </br>
            </div>
            <div class="row">
              <div class="col-md-6">
                <h3 class="s4a-title">Address</h3>
              </div>
              <div class="col-md-6">
                <h3 class="s4a-title">Website</h3>
              </div>
          </div>

        </div>
    </div>
    <script type="text/javascript">

        window.addEventListener('load', function () {

			<?php if ($latLong) { ?>
			jQuery('#map_canvas').appear(function() {
			  initialize_GMap("<?= $longitude ?>", "<?= $latitude ?>");
			});
			<?php } ?>

            jQuery("#gallery").unitegallery({
                gallery_width: null,
                gallery_height: null,
                slider_zoom_mousewheel: false,
                gallery_autoplay: true,
                gallery_play_interval: 10000
            });

            if (typeof fbq === 'function') {

                fbq('track', 'ViewContent', {
                    content_name: '<?= \S4A\Helpers::AlphaNumeric($model->Listing->Operator->v_title) ?>',
                    content_category: 'Accomodation',
                    content_ids: '<?= $model->Listing->Operator->id ?>',
                    content_type: 'product',
                    value: '<?= $model->Listing->Operator->i_price_from ?>',
                    currency: 'ZAR'
                });

            }


        });
    </script>
</div>