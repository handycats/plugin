<section class="row s4a_css_results">
    <?php
    $current_user = wp_get_current_user();
    if ($model->ListingSearchResults && $model->ListingSearchResults->OperatorSummeries):
        foreach ($model->ListingSearchResults->OperatorSummeries as $ListingSummery) :
            echo '<article class="col-md-4"><div class="css_inner">' . S4A\OperatorsControls::search_results($ListingSummery, $current_user->ID) . '</div></article>';
        endforeach;
    else:
        if (!S4A_SIGNUP_ID) {
            echo "<p class='s4a_no-results'>Nooo results found, please contact us for assistance.</p>";
        } else {
            echo "<p class='s4a_no-results'><a href='" . get_permalink(S4A_SIGNUP_ID) . "'>List Your Business</a></p>";
        }
    endif;
    ?>
</section>

<?php
$total = $model->ListingSearchResults->total;

$pages = ceil($total / $model->SearchCriteria->perPage);

$pages = ($pages == 0 ) ? 1 : $pages;

$Page = ($model->SearchCriteria->Page) ? $model->SearchCriteria->Page : 1;
?>
<div id='results_navigation'>
    <div class='previous_page'>
        <?php if ($Page > 1) { ?>
            <a href="<?= S4A\OperatorsHelpers::BuildResultsUrl($model->SearchCriteria, ($Page - 1)) ?>" class="<?= $class ?>">< Previous Page</a>
        <?php } else { ?>
            < Previous Page
        <?php } ?>
    </div>
    <div class='total_pages'>
        Page <?= $Page ?> of <?= $pages ?>
    </div>
    <div class='next_page'>
        <?php if ($Page < $pages) { ?>
            <a href="<?= S4A\OperatorsHelpers::BuildResultsUrl($model->SearchCriteria, ($Page + 1)) ?>" class="<?= $class ?>">Next Page ></a>
        <?php } else { ?>
            Next Page >
        <?php } ?>
    </div>
</div>
