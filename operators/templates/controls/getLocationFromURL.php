<?php

            $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

            $pages = array();

            $pages['#' . S4A_OPERATORS_RESULTS_URL . '([a-z\-]*)/([a-z\-]*)/([a-z\-]*)/([0-9]*)#i'] = 'a';
            $pages['#' . S4A_OPERATORS_RESULTS_URL . '([a-z\-]*)/([a-z\-]*)/([0-9]*)#i'] = 'p';
            $pages['#' . S4A_OPERATORS_RESULTS_URL . '([a-z\-]*)/([0-9]*)#i'] = 'c';

            foreach ($pages as $regexp => $func) {

                if (preg_match($regexp, $url)) {

                    $id = \S4A\OperatorsHelpers::GetURLId($url);

                    switch ($func) {
                        case "c":
                            return "c" . $id;
                            break;
                        case "p":
                            return "p" . $id;
                            break;
                        case "a":
                            return "a" . $id;
                            break;
                    }

                    break;
                }
            }
            return null;
