=== S4A PLUGIN ===
Contributors: Handy Cats
Tags: wordpress custom code library plugin
Requires at least: 4.1
Tested up to: 4.5.2
Stable tag: 1.0

== Description ==

A wordpress plugin to contain custom code.

== Installation ==

1. Upload 's4a' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
