<?php

add_action('widgets_init', 'wps4a_register_listing_quick_links_widget');

function wps4a_register_listing_quick_links_widget() {
    register_widget('S4A_listing_quick_links_Widget');
}

class S4A_listing_quick_links_Widget extends WP_Widget {

    function __construct() {
        $widget_ops = array(
            'classname' => 'listingquicklinks',
            'description' => 'Allows you to display listing quick links widget on front-end'
        );

        $control_ops = array(
            'width' => 250,
            'height' => 250,
            'id_base' => 'listingquicklinks-widget'
        );

        $this->WP_Widget('listingquicklinks-widget', 'Listing Quick Links Widget', $widget_ops, $control_ops);
    }

    function form($instance) {
        $defaults = array();
        $instance = wp_parse_args((array) $instance, $defaults);
        ?>

        <p>
            <label for="<?php echo $this->get_field_id('wps4a_title'); ?>">Title: </label>
            <input type="text" name="<?php echo $this->get_field_name('wps4a_title'); ?>" id="<?php echo $this->get_field_id('wps4a_title'); ?> " value="<?php echo $instance['wps4a_title']; ?>" size="20">
        </p>

        <?php
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['wps4a_title'] = $new_instance['wps4a_title'];
        return $instance;
    }

    function widget($args, $instance) {
        
        S4A\Controls::the_listing_quick_links_widget($instance);
        
        }

    }
    ?>