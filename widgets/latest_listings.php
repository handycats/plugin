<?php
add_action('widgets_init', 'wps4a_register_latest_listings_widget');

function wps4a_register_latest_listings_widget() {
    register_widget('S4A_latest_listings_Widget');
}

class S4A_latest_listings_Widget extends WP_Widget {

    function __construct() {
        $widget_ops = array(
            'classname' => 'latestlistings',
            'description' => 'Allows you to display property search widget on front-end'
        );

        $control_ops = array(
            'width' => 250,
            'height' => 250,
            'id_base' => 'latestlistings-widget'
        );

        $this->WP_Widget('latestlistings-widget', 'Latest Listings Widget', $widget_ops, $control_ops);
    }

    function form($instance) {
        $defaults = array();
        $instance = wp_parse_args((array) $instance, $defaults);
        ?>

        <p>
        <div>
            <label for="<?php echo $this->get_field_id('wps4a_title'); ?>">Title: </label>
            <input type="text" name="<?php echo $this->get_field_name('wps4a_title'); ?>" id="<?php echo $this->get_field_id('wps4a_title'); ?> " value="<?php echo $instance['wps4a_title']; ?>" size="20">
        </div>
        <div>
            <label for="<?php echo $this->get_field_id('wps4a_amount'); ?>">Amount: </label>
            <input type="number" name="<?php echo $this->get_field_name('wps4a_amount'); ?>" id="<?php echo $this->get_field_id('wps4a_amount'); ?> " value="<?php echo $instance['wps4a_amount']; ?>" size="20">
        </div>
        </p>

        <?php
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['wps4a_title'] = $new_instance['wps4a_title'];
        $instance['wps4a_amount'] = $new_instance['wps4a_amount'];
        return $instance;
    }

    function widget($args, $instance) {
        $SearchCriteria = new S4A\Structs\SearchCriteria(null);
        $SearchCriteria->perPage = $instance['wps4a_amount'];
        $SearchCriteria->order = "discount";
        $searchWidgetData = S4A\Cache::get_ListingBySearchCache($SearchCriteria);
        S4A\Controls::latest_listings_widget($searchWidgetData->ListingSummeries);
    }

}
